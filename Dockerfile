FROM yiisoftware/yii2-php:7.3-apache

WORKDIR /app/web/pcd-erw

COPY  ./web/assets ./web/assets
COPY  ./runtime ./runtime
COPY ./yii .
COPY ./config/web.php ./config/web.php
COPY ./composer.json ./composer.json
#COPY . .
#RUN composer global require "fxp/composer-asset-plugin:~1.3"
RUN composer install

COPY . .
#RUN chmod 755 ./yii
#RUN ./yii migrate