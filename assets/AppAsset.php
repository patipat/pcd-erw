<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //       'css/site.css',
        'css/main.css',
        'css/print.css',
        'js/ajaxcrud/ajaxcrud.css',
    ];
    public $js = [
        'js/main.js',
        'js/jquery.blockUI.js',
        'js/ajaxcrud/ModalRemote.js',
        'js/ajaxcrud/ajaxcrud.js',
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
        'yii\web\YiiAsset',
        'yii\widgets\ActiveFormAsset',
        'yii\validators\ValidationAsset',
        'yii\grid\GridViewAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\widgets\MaskedInputAsset',
        'app\assets\RemarkAsset',
        'app\assets\TableAsset',
        'app\assets\RemarkIconsAsset',
        'app\assets\LaddaAsset',
        'app\assets\ToastrAsset',
        'app\assets\NumeralAsset',
        'app\assets\MomentAsset',
        'app\assets\ThaiDateAsset',
        'kartik\widgets\WidgetAsset',
        'kartik\select2\Select2Asset',
        'kartik\datecontrol\DateControlAsset',
        'kartik\grid\GridViewAsset',
        'kartik\depdrop\DepDropAsset',
        'kartik\depdrop\DepDropExtAsset',
    ];

}
