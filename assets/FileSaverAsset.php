<?php

namespace app\assets;

use yii\web\AssetBundle;

class FileSaverAsset extends AssetBundle {

    public $sourcePath = '@bower/file-saverjs';
    public $css = [
    ];
    public $js = [
        'FileSaver.min.js',
    ];
    public $depends = [
    ];

}
