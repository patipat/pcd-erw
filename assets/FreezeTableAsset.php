<?php

namespace app\assets;

use yii\web\AssetBundle;

class FreezeTableAsset extends AssetBundle {

    public $sourcePath = '@bower/jquery-freeze-table';
    public $js = [
        'dist/js/freeze-table.min.js',
    ];
    public $publishOptions = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JQueryAsset',
    ];

}
