<?php

namespace app\assets;

use yii\web\AssetBundle;

class FreezeTableSiteAsset extends AssetBundle {

    public $sourcePath = '@bower/jquery-freeze-table';
    public $js = [
        'dist/js/freeze-table.min.js',
    ];
    public $publishOptions = [
    ];
    public $depends = [
        'app\assets\AppAssetSite',
        'yii\web\JqueryAsset',
    ];

}
