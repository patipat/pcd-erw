<?php

namespace app\assets;

use yii\web\AssetBundle;

class JsXlsxAsset extends AssetBundle {

    public $sourcePath = '@bower/js-xlsx';
    public $css = [
    ];
    public $js = [
        'dist/xlsx.core.min.js',
    ];
    public $depends = [
    ];

}
