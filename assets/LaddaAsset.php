<?php

namespace app\assets;

use yii\web\AssetBundle;

class LaddaAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'remark/global/vendor/ladda-bootstrap/ladda.min.css',
    ];
    public $js = [
        'remark/global/vendor/ladda-bootstrap/spin.min.js',
        'remark/global/vendor/ladda-bootstrap/ladda.min.js',
        'remark/global/js/components/ladda-bootstrap.min.js',
    ];
    public $depends = [
//        'app\assets\RemarkAsset'
    ];

}
