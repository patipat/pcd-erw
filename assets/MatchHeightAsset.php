<?php

namespace app\assets;

use yii\web\AssetBundle;

class MatchHeightAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'remark/global/vendor/matchheight/jquery.matchHeight-min.js',
        'remark/global/js/components/matchheight.js',
    ];
    public $depends = [
        'app\assets\RemarkAsset'
    ];

}
