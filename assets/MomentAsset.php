<?php

namespace app\assets;

use yii\web\AssetBundle;

class MomentAsset extends AssetBundle {

    public $sourcePath = '@npm';
    public $js = [
        'moment/min/moment-with-locales.min.js',
        'moment-duration-format/lib/moment-duration-format.js',
    ];
    public $publishOptions = [
    ];
    public $depends = [
//        'app\assets\AppAsset',
    ];

}
