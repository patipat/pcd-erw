<?php

namespace app\assets;

use yii\web\AssetBundle;

class PeityAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'remark/global/vendor/peity/jquery.peity.min.js',
        'remark/global/js/components/peity.min.js',
    ];
    public $depends = [
        'app\assets\RemarkAsset'
    ];

}
