<?php

namespace app\assets;

use yii\web\AssetBundle;

class PieProgressAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'remark/global/vendor/aspieprogress/asPieProgress.min.css'
    ];
    public $js = [
        'remark/global/vendor/aspieprogress/jquery-asPieProgress.min.js',
        'remark/global/js/components/aspieprogress.min.js',
    ];
    public $depends = [
        'app\assets\RemarkAsset'
    ];

}
