<?php

namespace app\assets;

use yii\web\AssetBundle;

class TableAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'remark/global/vendor/bootstrap-table/bootstrap-table.min.css',
    ];
    public $js = [
        'remark/global/vendor/bootstrap-table/bootstrap-table.min.js',
        'remark/global/vendor/bootstrap-table/extensions/mobile/bootstrap-table-mobile.min.js',
    ];
    public $depends = [
//        'app\assets\RemarkAsset'
    ];

}
