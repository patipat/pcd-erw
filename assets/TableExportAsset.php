<?php

namespace app\assets;

use yii\web\AssetBundle;

class TableExportAsset extends AssetBundle {

    public $sourcePath = '@bower/tableexport.js';
    public $css = [
        'dist/css/tableexport.min.css',
    ];
    public $js = [
        'dist/js/tableexport.min.js',
    ];
    public $depends = [
        'app\assets\JsXlsxAsset',
        'app\assets\FileSaverAsset',
    ];

}
