<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Office;
use app\models\CalculationQueue;
use app\models\Setting;
use app\models\Alert;
use yii\helpers\ArrayHelper;
use app\models\Person;
use app\models\Role;
use app\models\LineQueue;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AlertController extends Controller {

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex() {
        $queues = CalculationQueue::find()->isDeleted(false)->isCompleted(false)->all();
        foreach ($queues as $q) {
            $q->calculate();
        }
    }

    public function actionSixMonthFormCheck() {
        $budgetYear = Setting::getValue(Setting::CURRENT_YEAR);
        $firstMonth = Setting::getValue(Setting::FIRST_MONTH);
        $alertDay = Setting::getValue(Setting::SIX_MONTH_COMPLETE_ALERT_DAY_MONTH);
        $alertDayParts = explode('/', $alertDay);
        $year = $budgetYear - 543;
        if ($alertDayParts[1] >= $firstMonth) {
            $year--;
        }
        $alertDate = new \DateTime("{$year}-{$alertDayParts[1]}-{$alertDayParts[0]}");
        $now = new \DateTime();
        if ($now->format('Y-m-d') == $alertDate->format('Y-m-d')) {
            $superAdmins = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role(Role::SUPER_ADMINISTRATOR)->all();
            $offices = Office::find()->isDeleted(false)->noConfirmedSixMonthForm($budgetYear)->all();
//                \yii\helpers\VarDumper::dump(count($offices));
//                exit;
            foreach ($offices as $office) {
                Alert::addSixMonthLateConfirm($office, $budgetYear);
            }

            $officesByLevel = ArrayHelper::index($offices, null, 'level');

            foreach ($officesByLevel as $offLevel => $offs) {
                if ($offLevel <= $level) {
                    $officesByType = ArrayHelper::index($offs, null, 'officeType.name');

                    foreach ($officesByType as $type => $offs2) {
                        Alert::addChildrenSixMonthLateSubmit($superAdmins, $offs2, $budgetYear);
                        $message = Alert::buildChildrenSixMonthLateMessage($offs2, $budgetYear);
                        foreach ($superAdmins as $sa) {
                            if (isset($sa->line_notify_token)) {
                                LineQueue::addQueue($sa->line_notify_token, $message['message']);
                            }
                        }
                    }
                    if ($offLevel > 1) {
                        $officesByParent = ArrayHelper::index($offs, null, 'parent.id');
                        foreach ($officesByParent as $parentId => $offs2) {
                            $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR])->office($parentId)->all();
                            Alert::addChildrenSixMonthLateSubmit($people, $offs2, $budgetYear);
                            $office = Office::findOne($parentId);
                            if (isset($office) && isset($office->line_token)) {
                                $message = Alert::buildChildrenSixMonthLateMessage($offs2, $budgetYear);
                                LineQueue::addQueue($office->line_token, $message['message']);
                            }
                        }
                    }
                } else {
                    $officesByParent = ArrayHelper::index($offs, null, 'parent.id');
                    foreach ($officesByParent as $parentId => $offs2) {
                        $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR])->office($parentId)->all();
                        Alert::addChildrenSixMonthLateSubmit($people, $offs2, $budgetYear);
                        $office = Office::findOne($parentId);
                        if (isset($office) && isset($office->line_token)) {
                            $message = Alert::buildChildrenSixMonthLateMessage($offs2, $budgetYear);
                            LineQueue::addQueue($office->line_token, $message['message']);
                        }
                    }
                }
            }
            \Yii::$app->util->execInBackground(\Yii::$app->params['sendMailCmd']);
            \Yii::$app->util->execInBackground(\Yii::$app->params['sendLineCmd']);
        }
    }

    public function actionMonthlyFormCheck() {
        $budgetYear = Setting::getValue(Setting::CURRENT_YEAR);
        $adYear = date('Y');
        $month = date('m');
        $yearMonths = \Yii::$app->util->getYearMonths($budgetYear);
        $noSubmitDay = Setting::getValue(Setting::MONTHLY_NO_SUBMIT_ALERT_DAY);
        $noCompleteDay = Setting::getValue(Setting::MONTHLY_COMPLETE_ALERT_DAY);
        $noSubmitDate = new \DateTime("{$adYear}-{$month}-01");
        if ($noSubmitDate->format('t') < $noSubmitDay) {
            $noSubmitDate = new \DateTime("{$adYear}-{$month}-" . $noSubmitDate->format('t'));
        } else {
            $noSubmitDate = new \DateTime("{$adYear}-{$month}-{$noSubmitDay}");
        }
//        \yii\helpers\VarDumper::dump("{$adYear}-{$month}-{$noSubmitDay}");
//        \yii\helpers\VarDumper::dump($noSubmitDate);
//        exit;
        $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
        $lastMonth = $month - 1;
        $completeYear = $adYear;
        if ($lastMonth == 0) {
            $lastMonth = 12;
            $completeYear--;
        }
        $noCompleteDate = new \DateTime("{$completeYear}-{$month}-{$noCompleteDay}");
        if ($noCompleteDate->format('t') < $noCompleteDay) {
            $noCompleteDate = new \DateTime("{$adYear}-{$month}-" . $noCompleteDate->format('t'));
        } else {
            $noCompleteDate = new \DateTime("{$adYear}-{$month}-{$noCompleteDay}");
        }
        $n = count($yearMonths);
        $superAdmins = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role(Role::SUPER_ADMINISTRATOR)->all();
        if ($noSubmitDate->format('Y-m-d') == date('Y-m-d')) {
            if ($noSubmitDate >= $yearMonths[0]['firstDate'] && $noSubmitDate <= $yearMonths[$n - 1]['lastDate']) {
                $offices = Office::find()->isDeleted(false)->noConfirmedMonthlyForm($noSubmitDate->format('Y') + 543, $noSubmitDate->format('n'))->all();
//                \yii\helpers\VarDumper::dump(count($offices));
//                exit;
                foreach ($offices as $office) {
                    Alert::addMonthlyLateConfirm($office, $noSubmitDate->format('Y') + 543, $noSubmitDate->format('n'));
                }

                $officesByLevel = ArrayHelper::index($offices, null, 'level');

                foreach ($officesByLevel as $offLevel => $offs) {
                    if ($offLevel <= $level) {
                        $officesByType = ArrayHelper::index($offs, null, 'officeType.name');

                        foreach ($officesByType as $type => $offs2) {
                            Alert::addChildrenMonthlyLateSubmit($superAdmins, $offs2, $noSubmitDate->format('Y') + 543, $noSubmitDate->format('n'));
                            $message = Alert::buildChildrenMonthlyLateMessage($offs2, $noSubmitDate->format('Y') + 543, $noSubmitDate->format('n'));
                            foreach ($superAdmins as $sa) {
                                if (isset($sa->line_notify_token)) {
                                    LineQueue::addQueue($sa->line_notify_token, $message['message']);
                                }
                            }
                        }
                        if ($offLevel > 1) {
                            $officesByParent = ArrayHelper::index($offs, null, 'parent.id');
                            foreach ($officesByParent as $parentId => $offs2) {
                                $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR])->office($parentId)->all();
                                Alert::addChildrenMonthlyLateSubmit($people, $offs2, $noSubmitDate->format('Y') + 543, $noSubmitDate->format('n'));
                                $office = Office::findOne($parentId);
                                if (isset($office) && isset($office->line_token)) {
                                    $message = Alert::buildChildrenMonthlyLateMessage($offs2, $noSubmitDate->format('Y') + 543, $noSubmitDate->format('n'));
                                    LineQueue::addQueue($office->line_token, $message['message']);
                                }
                            }
                        }
                    } else {
                        $officesByParent = ArrayHelper::index($offs, null, 'parent.id');
                        foreach ($officesByParent as $parentId => $offs2) {
                            $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR])->office($parentId)->all();
                            Alert::addChildrenMonthlyLateSubmit($people, $offs2, $noSubmitDate->format('Y') + 543, $noSubmitDate->format('n'));
                            $office = Office::findOne($parentId);
                            if (isset($office) && isset($office->line_token)) {
                                $message = Alert::buildChildrenMonthlyLateMessage($offs2, $noSubmitDate->format('Y') + 543, $noSubmitDate->format('n'));
                                LineQueue::addQueue($office->line_token, $message['message']);
                            }
                        }
                    }
                }
                \Yii::$app->util->execInBackground(\Yii::$app->params['sendMailCmd']);
                \Yii::$app->util->execInBackground(\Yii::$app->params['sendLineCmd']);

//                $officesByParent = ArrayHelper::index($offices, null, 'parent.id');
////                \yii\helpers\VarDumper::dump(array_keys($officesByParent));
////                exit;
//                foreach ($officesByParent as $parentId => $offs) {
//                    $parent = Office::findOne($parentId);
//                    if (!isset($parent)) {
//                        $officesByType = ArrayHelper::index($offs, null, 'officeType.name');
//                        
//                        foreach ($officesByType as $type => $offs2) {
//                            Alert::addChildrenMonthlyLateSubmit($superAdmins, $offs2, $noSubmitDate->format('Y')+543, $noSubmitDate->format('n'));
//                        }
//                    } else {
//                        $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR])->office($parentId)->all();
//                        Alert::addChildrenMonthlyLateSubmit($people, $offs, $noSubmitDate->format('Y')+543, $noSubmitDate->format('n'));
//                        
//                        if ($parent->level < $level) {
//                            $officesByType = ArrayHelper::index($offs, null, 'officeType.name');
//                            foreach ($officesByType as $type => $offs2) {
//                                Alert::addChildrenMonthlyLateSubmit($superAdmins, $offs2, $noSubmitDate->format('Y')+543, $noSubmitDate->format('n'));
//                            }
//                        }
//                    }
//                }
            }
        }

        if ($noCompleteDate->format('Y-m-d') == date('Y-m-d')) {
            $actualNoCompleteDate = (clone $noCompleteDate)->sub(new \DateInterval('P1M'));
            if ($actualNoCompleteDate >= $yearMonths[0]['firstDate'] && $actualNoCompleteDate <= $yearMonths[$n - 1]['lastDate']) {
                $offices = Office::find()->isDeleted(false)->noConfirmedMonthlyForm($actualNoCompleteDate->format('Y'), $actualNoCompleteDate->format('n'))->all();
                foreach ($offices as $office) {
                    Alert::addMonthlyLateConfirm($office, $actualNoCompleteDate->format('Y') + 543, $actualNoCompleteDate->format('n'));
                }

                $officesByLevel = ArrayHelper::index($offices, null, 'level');

                foreach ($officesByLevel as $offLevel => $offs) {
                    if ($offLevel <= $level) {
                        $officesByType = ArrayHelper::index($offs, null, 'officeType.name');

                        foreach ($officesByType as $type => $offs2) {
                            Alert::addChildrenMonthlyLateSubmit($superAdmins, $offs2, $actualNoCompleteDate->format('Y') + 543, $actualNoCompleteDate->format('n'));
                            $message = Alert::buildChildrenMonthlyLateMessage($offs2, $actualNoCompleteDate->format('Y') + 543, $actualNoCompleteDate->format('n'));
                            foreach ($superAdmins as $sa) {
                                if (isset($sa->line_notify_token)) {
                                    LineQueue::addQueue($sa->line_notify_token, $message['message']);
                                }
                            }
                        }
                        if ($offLevel > 1) {
                            $officesByParent = ArrayHelper::index($offs, null, 'parent.id');
                            foreach ($officesByParent as $parentId => $offs2) {
                                $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR])->office($parentId)->all();
                                Alert::addChildrenMonthlyLateSubmit($people, $offs2, $actualNoCompleteDate->format('Y') + 543, $actualNoCompleteDate->format('n'));
                                $office = Office::findOne($parentId);
                                if (isset($office) && isset($office->line_token)) {
                                    $message = Alert::buildChildrenMonthlyLateMessage($offs2, $actualNoCompleteDate->format('Y') + 543, $actualNoCompleteDate->format('n'));
                                    LineQueue::addQueue($office->line_token, $message['message']);
                                }
                            }
                        }
                    } else {
                        $officesByParent = ArrayHelper::index($offs, null, 'parent.id');
                        foreach ($officesByParent as $parentId => $offs2) {
                            $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR])->office($parentId)->all();
                            Alert::addChildrenMonthlyLateSubmit($people, $offs2, $actualNoCompleteDate->format('Y') + 543, $actualNoCompleteDate->format('n'));
                            $office = Office::findOne($parentId);
                            if (isset($office) && isset($office->line_token)) {
                                $message = Alert::buildChildrenMonthlyLateMessage($offs2, $actualNoCompleteDate->format('Y') + 543, $actualNoCompleteDate->format('n'));
                                LineQueue::addQueue($office->line_token, $message['message']);
                            }
                        }
                    }
                }
                \Yii::$app->util->execInBackground(\Yii::$app->params['sendMailCmd']);

//                $officesByParent = ArrayHelper::index($offices, null, 'parent.id');
////                \yii\helpers\VarDumper::dump(array_keys($officesByParent));
////                exit;
//                foreach ($officesByParent as $parentId => $offs) {
//                    $parent = Office::findOne($parentId);
//                    if (!isset($parent)) {
//                        $officesByType = ArrayHelper::index($offs, null, 'officeType.name');
//                        foreach ($officesByType as $type => $offs2) {
//                            Alert::addChildrenMonthlyLateSubmit($superAdmins, $offs2, $actualNoCompleteDate->format('Y')+543, $actualNoCompleteDate->format('n'));
//                        }
//                    } else {
//                        $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR])->office($parentId)->all();
//                        Alert::addChildrenMonthlyLateSubmit($people, $offs, $actualNoCompleteDate->format('Y')+543, $actualNoCompleteDate->format('n'));
//                        if ($parent->level < $level) {
//                            $officesByType = ArrayHelper::index($offs, null, 'officeType.name');
//                            foreach ($officesByType as $type => $offs2) {
//                                Alert::addChildrenMonthlyLateSubmit($superAdmins, $offs2, $actualNoCompleteDate->format('Y')+543, $actualNoCompleteDate->format('n'));
//                            }
//                        }
//                    }
//                    
//                }
            }
        }
    }

}
