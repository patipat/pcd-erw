<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Office;
use app\models\CalculationQueue;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CalculationController extends Controller {

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex() {
        $queues = CalculationQueue::find()->isDeleted(false)->isCompleted(false)->all();
        foreach ($queues as $q) {
            echo "id={$q->id}, office_id={$q->office_id}, year={$q->year}\n";
            $q->calculate();
        }
    }

}
