<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Office;
use app\models\OfficeType;
use app\models\Person;
use app\models\PersonRole;
use app\models\Role;
use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionImportUser($filename) {
        $lines = file(\Yii::getAlias("@app/web/user/{$filename}.csv"));
        $result = array();
        if ($lines === FALSE) {
            $result['error'] = \Yii::t('import', 'Cannot read the file.');
        } else {
            try {
                $tran = \Yii::$app->db->beginTransaction();
                foreach ($lines as $lineNum => $line) {
                    if ($lineNum == 0) {
                        continue;
                    }
                    $vals = explode(',', $line);
                    if ($filename == 'prov') {
                        $office = Office::find()->isDeleted(false)->andWhere(['name' => $vals[2], 'office_type_id' => 2])->one();
                    } else if ($filename == 'mon' || $filename == 'pcd') {
                        $office = Office::find()->isDeleted(false)->andWhere(['name' => $vals[2], 'office_type_id' => 3])->one();
                    } else {
                        $office = Office::find()->isDeleted(false)->andWhere(['name' => $vals[2], 'office_type_id' => 1])->one();
                    }
                    
                    // $oId = isset($office) ? $office->id : '';
                    // echo "{$vals[1]} = {$oId}, {$vals[2]}\n";
                    if (!isset($office)) {
                        echo "{$vals[2]}\n"; 
                    }
                    if ($office->level != 2) {
                        echo "{$office->id}:{$office->name}:{$office->level}\n"; 
                    }
                    // $u = User::find()->andWhere(['username' => $vals[3]])->one();
                    // if (isset($u)) {
                    //     echo "{$u->id}\n";
                    // }
    
                    $this->createUser($vals, $office);
                }
                $tran->commit();
            } catch (\Exception $ex) {
                $tran->rollBack();
                throw $ex;
                // echo $ex->getTraceAsString();
            }
        }
        exit;
    }

    private function createUser($vals, $o) {
        // $o = Office::find()->isDeleted(false)->andWhere(['name' => $vals[2]])->one();
        $u = new User();
        $u = new User;
        $u->username = $vals[3];
        $u->setPassword($vals[4]);
        $u->generateAuthKey();
        $u->status = User::STATUS_ACTIVE;
        $u->save(false);
        $p = new Person();
        $p->office_id = $o->id;
        $p->first_name = $vals[5];
        $p->user_id = $u->id;
        $p->save(false);

        $auth = \Yii::$app->getAuthManager();
        $roles = [Role::ADMINISTRATOR, Role::STAFF];
        foreach ($roles as $r) {
            $pr = new PersonRole();
            $pr->person_id = $p->id;
            $pr->role_id = $r;
            $pr->save(false);

            $role = $auth->getRole($pr->role->name);
            if (null === $auth->getAssignment($role->name, $u->id)) {
                $auth->assign($role, $u->id);
            }
        }

    }
}
