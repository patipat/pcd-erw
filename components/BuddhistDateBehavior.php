<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components;

use yii\base\InvalidCallException;
use yii\db\BaseActiveRecord;
use yii\behaviors\AttributeBehavior;

/**
 * TimestampBehavior automatically fills the specified attributes with the current timestamp.
 *
 * To use TimestampBehavior, insert the following code to your ActiveRecord class:
 *
 * ```php
 * use yii\behaviors\TimestampBehavior;
 *
 * public function behaviors()
 * {
 *     return [
 *         TimestampBehavior::className(),
 *     ];
 * }
 * ```
 *
 * By default, TimestampBehavior will fill the `created_at` and `updated_at` attributes with the current timestamp
 * when the associated AR object is being inserted; it will fill the `updated_at` attribute
 * with the timestamp when the AR object is being updated. The timestamp value is obtained by `time()`.
 *
 * Because attribute values will be set automatically by this behavior, they are usually not user input and should therefore
 * not be validated, i.e. `created_at` and `updated_at` should not appear in the [[\yii\base\Model::rules()|rules()]] method of the model.
 *
 * For the above implementation to work with MySQL database, please declare the columns(`created_at`, `updated_at`) as int(11) for being UNIX timestamp.
 *
 * If your attribute names are different or you want to use a different way of calculating the timestamp,
 * you may configure the [[createdAtAttribute]], [[updatedAtAttribute]] and [[value]] properties like the following:
 *
 * ```php
 * use yii\db\Expression;
 *
 * public function behaviors()
 * {
 *     return [
 *         [
 *             'class' => TimestampBehavior::className(),
 *             'createdAtAttribute' => 'create_time',
 *             'updatedAtAttribute' => 'update_time',
 *             'value' => new Expression('NOW()'),
 *         ],
 *     ];
 * }
 * ```
 *
 * In case you use an [[\yii\db\Expression]] object as in the example above, the attribute will not hold the timestamp value, but
 * the Expression object itself after the record has been saved. If you need the value from DB afterwards you should call
 * the [[\yii\db\ActiveRecord::refresh()|refresh()]] method of the record.
 *
 * TimestampBehavior also provides a method named [[touch()]] that allows you to assign the current
 * timestamp to the specified attribute(s) and save them to the database. For example,
 *
 * ```php
 * $model->touch('creation_time');
 * ```
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since 2.0
 */
class BuddhistDateBehavior extends AttributeBehavior {

    /**
     * @var string the attribute that will receive timestamp value
     * Set this property to false if you do not want to record the creation time.
     */
    public $dateAttributes = 'created_at';
    public $value;

    /**
     * {@inheritdoc}
     *
     * In case, when the value is `null`, the result of the PHP function [time()](http://php.net/manual/en/function.time.php)
     * will be used as value.
     */

    /**
     * {@inheritdoc}
     */
    public function init() {
        parent::init();

        $this->attributes = [
            BaseActiveRecord::EVENT_BEFORE_INSERT => $this->dateAttributes,
            BaseActiveRecord::EVENT_BEFORE_UPDATE => $this->dateAttributes,
            BaseActiveRecord::EVENT_AFTER_FIND => $this->dateAttributes,
        ];
    }

    public function events() {
        return array_fill_keys(
                array_keys($this->attributes),
                'evaluateAttributes'
        );
    }

    public function evaluateAttributes($event) {
        if (\Yii::$app->language == 'th' && !empty($this->attributes[$event->name])) {
            $attributes = (array) $this->attributes[$event->name];
            foreach ($attributes as $attribute) {
//                \yii\helpers\VarDumper::dump();
                if (!isset($this->owner->$attribute)) {
                    continue;
                }
                if (in_array($event->name, ['afterFind'])) {
//                    \yii\helpers\VarDumper::dump($attribute);
//                    \yii\helpers\VarDumper::dump($this->owner->$attribute);
                    $val = new \DateTime($this->owner->$attribute);
                    if ($val->format('Y') < \Yii::$app->params['minBEYear']) {
                        $this->owner->$attribute = ($val->format('Y') + 543) . $val->format('-m-d H:i:s');
                    }
                }
                if (in_array($event->name, ['beforeInsert', 'beforeUpdate'])) {
//                    if ($this->owner->className() == \app\models\PersonRegistration::className()) {
//                        \yii\helpers\VarDumper::dump($attribute);
//                        \yii\helpers\VarDumper::dump($this->owner->$attribute);
////                        exit;
//                    }
                    $val = new \DateTime($this->owner->$attribute);
                    if ($val->format('Y') >= \Yii::$app->params['minBEYear']) {
                        $this->owner->$attribute = ($val->format('Y') - 543) . $val->format('-m-d H:i:s');
                    }
//                    if ($this->owner->className() == \app\models\PersonRegistration::className()) {
//                        \yii\helpers\VarDumper::dump($this->owner->attributes, 10, TRUE);
////                        exit;
//                    }
                }
            }
        }
    }

}
