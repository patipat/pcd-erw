<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use app\models\Setting;
use app\models\Role;
use DateTime;

class Util extends \yii\base\Component {

    public function getFormControlClass($val) {
        return "form-control " . (empty($val) ? "empty" : "");
    }

    public function errorSummary($model) {
        $message = '';
        if (count($model->errors) > 0)
            $message .= '<br /><ul>';
        foreach ($model->errors as $error) {
            $message .= '<li>' . $error[0] . '</li>';
        }
        if (count($model->errors) > 0)
            $message .= '</ul>';
        return $message;
    }

    public function toDateFormat($fromFormat, $toFormat, $dateString) {
        $dt = empty($dateString) ? NULL : \DateTime::createFromFormat($fromFormat, $dateString);
        return $dt !== FALSE && isset($dt) ? $dt->format($toFormat) : "";
    }

    public function getCheckBox($value) {
        if ($value) {
            return '<i class="icon md-check primary-500"></i>';
        }
        return '<i class="icon md-close red-500"></i>';
        ;
    }

    public function checkPermission($permission) {
        return \Yii::$app->user->can(\Yii::$app->id . ".{$permission}");
    }

    public function getBudgetYear($date) {
        $month = \Yii::$app->formatter->asDate($date, 'php:n');
        $year = \Yii::$app->formatter->asDate($date, 'php:Y') + 543;
        if ($month >= 10) {
            $year++;
        }
        return $year;
    }

    public function getPassFailLabels() {
        return [
            1 => \Yii::t('app', 'ผ่าน'),
            0 => \Yii::t('app', 'ไม่ผ่าน'),
        ];
    }

    public function getYears() {
        $minYear = 2562;
        $currentYear = Setting::getValue(Setting::CURRENT_YEAR);
        $years = [];
        for ($i = $currentYear; $i >= $minYear; $i--) {
            $years[$i] = $i;
        }
        return $years;
    }

    public function getYearMonths($year) {
        $month = Setting::getValue(Setting::FIRST_MONTH);
        $monthlyFirstMonth = Setting::getValue(Setting::MONTHLY_FIRST_MONTH);
        $totalMonths = Setting::getValue(Setting::MONTHLY_MONTHS);
        if ($month > 1) {
            if ($monthlyFirstMonth >= $month) {
                $year--;
            }
        }
        $date = new \DateTime("{$year}-{$monthlyFirstMonth}-01");
        $results = [];
        for ($i = 0; $i < $totalMonths; $i++) {
            $d = (clone $date)->add(new \DateInterval("P{$i}M"));
            $adYear = $d->format('Y') - 543;
            $adDate = new \DateTime("{$year}-{$d->format('m')}-01");
            $results[] = [
                'year' => $d->format('Y'),
                'month' => $d->format('n'),
                'adYear' => $adYear,
                'firstDate' => $adDate,
                'code' => $d->format('Y-n'),
                'label' => \Yii::$app->formatter->asDate($d->format('Y-m-d'), 'php:F Y'),
                'lastDate' => new \DateTime($adDate->format('Y-m-t')),
                'monthName' => \Yii::$app->formatter->asDate($d->format('Y-m-d'), 'php:F'),
            ];
        }
        return $results;
    }

    public function getMonthNames() {
        $months = [];
        for ($i = 1; $i <= 12; $i++) {
            $d = new \DateTime(date('Y') . "-{$i}-01");
            $months[$i] = \Yii::$app->formatter->asDate($d->format('Y-m-d'), 'php:F');
        }
        return $months;
    }

    function execInBackground($cmd) {
        if (substr(php_uname(), 0, 7) == "Windows") {
            pclose(popen("start /B " . $cmd, "r"));
        } else {
            exec($cmd . " > /dev/null &");
        }
    }

    public function getMonthlyFormStatusBadge($monthlyForm) {
        if (isset($monthlyForm)) {
            return $monthlyForm->getStatusBadge();
        }
        $color = 'dark';
        $label = \Yii::t('app', 'ยังไม่ส่งรายงาน');
        $res = "<span class='label label-{$color}'>{$label}</span>";
        return $res;
    }

    public function getLastSubmitDate($year) {
        $str = Setting::getValue(Setting::LAST_SUBMIT_DAY_MONTH) . "/" . ($year - 543);
        if ($year == 2563) {
            $str = (new DateTime('2021-01-31'))->format('d/m/Y');
        }
        return \DateTime::createFromFormat('d/m/Y', $str);
    }
    public function getLast6SubmitDate($year) {
        $str = Setting::getValue(Setting::LAST_SUBMIT_DAY_6MONTH) . "/" . ($year - 543);
        return \DateTime::createFromFormat('d/m/Y', $str);
    }
    public function isValidSubmitDate($year) {
        $lastSubmitDate = $this->getLastSubmitDate($year);
        $now = new \DateTime();
        $yesterday = (clone $now)->sub(new \DateInterval('P1D'));
        return $yesterday < $lastSubmitDate;
    }
    public function isValidSixMonthSubmitDate($year) {
        $lastSubmitDate = $this->getLast6SubmitDate($year);
        $now = new \DateTime();
        $yesterday = (clone $now)->sub(new \DateInterval('P1D'));
        return $yesterday < $lastSubmitDate;
    }

    public function getMonthlyFormBudgetYearCond($year, $prefix = 'mf') {
        $yearMonths = $this->getYearMonths($year);
        $n = count($yearMonths);
        if ($yearMonths[0]['year'] == $yearMonths[$n - 1]['year']) {
            $cond = <<<c
                    AND (
                        {$prefix}.year = {$yearMonths[0]['year']} AND {$prefix}.month >= {$yearMonths[0]['month']}
                        AND {$prefix}.month <= {$yearMonths[count($yearMonths) - 1]['month']}
                    )
c;
        } else {
            $cond = <<<c
                AND (
                    ({$prefix}.year = {$yearMonths[0]['year']} AND {$prefix}.month >= {$yearMonths[0]['month']} AND {$prefix}.month <= 12)
                    OR
                    ({$prefix}.year = {$yearMonths[count($yearMonths) - 1]['year']} AND {$prefix}.month >= 1 AND {$prefix}.month <= {$yearMonths[count($yearMonths) - 1]['month']})
                )
c;
        }
        return $cond;
    }

    public function getProgressColor($percent) {
        return $percent >= 80 ? 'green' : 'red';
    }

    public function getLevelLabels() {
        $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
        $res = [];
        for ($i = 1; $i <= $level; $i++) {
            $res[$i] = "ระดับ {$i}";
        }
        return $res;
    }

    public function getDashboardChartCond($office, $officeType = null, $level = null) {
//        $currentRole = \Yii::$app->session->get('currentRole');
        $cond = "";
        if (!empty($officeType) || !empty($level)) {
            if (!empty($officeType)) {
                $cond .= " AND o.office_type_id = {$officeType}";
            }
            if (!empty($level)) {
                $cond .= " AND o.level = {$level}";
            }
        } else {
            $cond .= " AND o.id = {$office->id}";
        }
        return $cond;
    }

    public function getDashboardCond($office, $officeType = null, $level = null) {
//        $currentRole = \Yii::$app->session->get('currentRole');
        $cond = "";
        if (!empty($officeType) || !empty($level)) {
            if (!empty($officeType)) {
                $cond .= " AND o.office_type_id = {$officeType}";
            }
            if (!empty($level)) {
                $cond .= " AND o.level = {$level}";
            }
        } else {
//            if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR) {
//                $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
//                $cond .= " AND o.level <= {$level}";
//            } else {
//                $cond .= " AND o.parent_id = {$office->id}";
//            }
            $cond .= " AND o.parent_id = {$office->id}";
        }
        return $cond;
    }

    public function highlighterText($text, $words) {
        $split_words = explode(" ", $words);
//        \yii\helpers\VarDumper::dump($split_words);
//        \yii\helpers\VarDumper::dump($text);
//        exit;
        foreach ($split_words as $word) {
            if (!empty($word)) {
                $color = "#ffff00";
                $text = preg_replace("|($word)|Ui",
                        "<span style=\"background:" . $color . ";\"><b>$1</b></span>", $text);
//            \yii\helpers\VarDumper::dump($text);
//        exit;
            }
        }
        return $text;
    }

}
