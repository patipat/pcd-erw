<?php

return [
    'adminEmail' => 'patipat.tip@gmail.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'กรมควบคุมมลพิษ',
    'calculateCmd' => 'php /app/web/pcd-erw/yii calculation',
    'sendMailCmd' => 'php /app/web/pcd-erw/yii email-queue/send-mail',
    'sendLineCmd' => 'php /app/web/pcd-erw/yii line-queue/send',
    'alertCheckInterval' => 60 * 1000,
];
