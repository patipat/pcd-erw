<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'pcd-erw',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'Asia/Bangkok',
    'language' => 'th-TH',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'defaultRoute' => 'site/main',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '1G5qyAgwOYPlYAi1XO9m-odaC80-3TXp',
            'baseUrl' => '/pcd-erw',
            'csrfParam' => '_pcderwCSRF',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => [
            'name' => 'PCDERWSESSID',
//            'savePath' => __DIR__ . '/../tmp',
        ],
        'thaiFormatter' => [
            'class' => 'dixonsatit\thaiYearFormatter\ThaiYearFormatter',
            'dateFormat' => 'php:d/m/Y',
            'datetimeFormat' => 'php:d/m/Y H:i',
            'timeZone' => 'UTC',
        ],
        'util' => [
            'class' => 'app\components\Util',
        ],
        'thai' => [
            'class' => 'app\components\Thai',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d/m/Y',
            'datetimeFormat' => 'php:d/m/Y H:i',
            'timeFormat' => 'php:H:i',
//            'timeZone' => 'Asia/Bangkok'
            'timeZone' => 'UTC',
            'locale' => 'th',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'class' => 'app\rbac\RbacUser',
            'superUserRole' => 'Super Administrator',
//            'loginUrl' => ['site/main'],
            'identityCookie' => [
                'name' => '_pcderwIdentity',
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'patipat.tip@gmail.com',
                'password' => 'lgiwtnirrfcyuqpd',
                'authMode' => 'login',
                'port' => '587',
                'encryption' => 'tls',
                'StreamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ]
                ]
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/pcd-erw',
            'rules' => [
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],
        'assetManager' => [
//            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null, // do not publish the bundle
                    'js' => [
                        '/pcd-erw/remark/global/vendor/jquery/jquery.js',
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null, // do not publish the bundle
                    'depends' => [
                        'yii\web\YiiAsset'
                    ],
                    'js' => [
                        '/pcd-erw/remark/global/vendor/bootstrap/bootstrap.js',
                    ],
                    'css' => [
                        '/pcd-erw/remark/global/css/bootstrap.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => FALSE,
            ],
        ],
    ],
    'modules' => [
        'rbac' => [
            'class' => 'johnitvn\rbacplus\Module',
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
            // format settings for displaying each date attribute (ICU format example)
            'displaySettings' => [
                kartik\datecontrol\Module::FORMAT_DATE => 'php:d/m/Y',
                kartik\datecontrol\Module::FORMAT_TIME => 'php:H:i',
                kartik\datecontrol\Module::FORMAT_DATETIME => 'php:d/m/Y H:i',
            ],
            // format settings for saving each date attribute (PHP format example)
            'saveSettings' => [
                kartik\datecontrol\Module::FORMAT_DATE => 'php:Y-m-d', // saves as unix timestamp
                kartik\datecontrol\Module::FORMAT_TIME => 'php:H:i:s',
                kartik\datecontrol\Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
            ],
            // set your display timezone
            'displayTimezone' => 'Asia/Bangkok',
            // set your timezone for date saved to db
            'saveTimezone' => 'Asia/bangkok',
            // automatically use kartik\widgets for each of the above formats
            'autoWidget' => true,
            // default settings for each widget from kartik\widgets used when autoWidget is true
            'autoWidgetSettings' => [
                kartik\datecontrol\Module::FORMAT_DATE => ['type' => kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose' => true]], // example
                kartik\datecontrol\Module::FORMAT_DATETIME => ['type' => \kartik\datetime\DateTimePicker::TYPE_COMPONENT_APPEND, 'pluginOptions' => ['autoclose' => true]], // setup if needed
                kartik\datecontrol\Module::FORMAT_TIME => [
                    'pluginOptions' => [
                        'defaultTime' => FALSE,
                    ]
                ], // setup if needed
            ],
            // custom widget settings that will be used to render the date input instead of kartik\widgets,
            // this will be used when autoWidget is set to false at module or widget level.
            'widgetSettings' => [
                kartik\datecontrol\Module::FORMAT_DATE => [
                    'class' => 'yii\jui\DatePicker', // example
                    'options' => [
                        'dateFormat' => 'php:d/M/Y',
                        'options' => ['class' => 'form-control'],
                    ]
                ]
            ]
        // other settings
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
//        'allowedIPs' => ['127.0.0.1', '::1'],
        'allowedIPs' => ['*'],
        'generators' => [//here
            'ajaxcrud' => [// generator name
                'class' => 'johnitvn\ajaxcrud\generators\Generator', //'yii\gii\generators\crud\Generator', // generator class
                'templates' => [//setting for out templates
                    'finedayCrud' => '@app/generators/ajaxcrud/default', // template name => path to template
                ]
            ]
        ],
    ];
}

return $config;
