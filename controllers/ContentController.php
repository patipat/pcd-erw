<?php

namespace app\controllers;

use Yii;
use app\models\Content;
use app\models\ContentSearch;
use app\rbac\RbacController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use kartik\widgets\Alert;
use yii\web\UploadedFile;
use yii\helpers\Json;

/**
 * ContentController implements the CRUD actions for Content model.
 */
class ContentController extends RbacController {

    /**
     * @inheritdoc
     */
    protected $allowedActions = ['show', 'knowlage', 'index-all', 'detail', 'law', 'view', 'content-list', 'content-list-admin'];

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {

        return [
            'browse-images' => [
                'class' => 'bajadev\ckeditor\actions\BrowseAction',
                'quality' => 80,
                'maxWidth' => 800,
                'maxHeight' => 800,
                'useHash' => true,
                'url' => '@web/uploads/editor/',
                'path' => '@app/web/uploads/editor/',
            ],
            'upload-images' => [
                'class' => 'bajadev\ckeditor\actions\UploadAction',
                'quality' => 80,
                'maxWidth' => 800,
                'maxHeight' => 800,
                'useHash' => true,
                'url' => '@web/uploads/editor/',
                'path' => '@app/web/uploads/editor/',
            ],
        ];
    }

    /**
     * Lists all Content models.
     * @return mixed
     */
    public function actionIndex($groupId = NULL, $categoryId = NULL) {
        $searchModel = new ContentSearch();
        $searchModel->deleted = 0;
        $searchModel->content_group_id = $groupId;
        if (isset($categoryId)) {
            $searchModel->content_category_id = $categoryId;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'groupId' => $groupId,
                    'categoryId' => $categoryId
        ]);
    }

    public function actionIndexAll($groupId = NULL) {
        $this->layout = 'index';

        $searchModel = new ContentSearch();
        $searchModel->deleted = 0;
        $searchModel->content_group_id = $groupId;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 10];

        return $this->render('index-all', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'groupId' => $groupId,
                    'model' => $dataProvider
        ]);
    }

    public function actionContentList($group, $categoryId = NULL) {
        $this->layout = 'site';

        $searchModel = new ContentSearch();
        $searchModel->deleted = 0;
        if (isset($categoryId)) {
            $searchModel->content_category_id = $categoryId;
        }
        $searchModel->content_group_id = $group;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 10];

        $contents = $dataProvider->getModels();

//        $contents = Content::find()->isDeleted(FALSE)->contentType($group)->all();
        $group = \app\models\ContentGroup::findOne($group);
//        \yii\helpers\VarDumper::dump($searchModel->attributes);
//        exit;
        return $this->render('content-list', [
                    'contents' => $contents,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'group' => $group,
                    'categoryId' => $categoryId
        ]);
    }

    public function actionContentListAdmin($group, $categoryId = NULL) {
        $searchModel = new ContentSearch();
        $searchModel->deleted = 0;
        if (isset($categoryId)) {
            $searchModel->content_category_id = $categoryId;
        }
        $searchModel->content_group_id = $group;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 10];

        $contents = $dataProvider->getModels();

//        $contents = Content::find()->isDeleted(FALSE)->contentType($group)->all();
        $group = \app\models\ContentGroup::findOne($group);
        return $this->render('content-list-admin', [
                    'contents' => $contents,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'group' => $group,
                    'categoryId' => $categoryId
        ]);
    }

    /**
     * Displays a single Content model.
     * @param integer $id
     * @return mixed
     */
    public function actionDetail($id) {
        $this->layout = 'site';

        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            return [
                'size' => 'large',
                'title' => Yii::t('app', "บทความ {name}", [
                    'name' => $model->contentGroup->name
                ]),
                'content' => $this->renderAjax('detail', [
                    'model' => $model,
                ]),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('app', "แก้ไข"), ['update', 'id' => $id], ['class' => 'btn btn-primary btn-lg', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('detail', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionView($id) {
        $this->layout = 'site';
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            return [
                'size' => 'large',
                'title' => Yii::t('app', "บทความ {name}", [
                    'name' => $model->contentGroup->name
                ]),
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('app', "แก้ไข"), ['update', 'id' => $id], ['class' => 'btn btn-primary btn-lg', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionShow($id) {
        $this->layout = 'index';
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            return [
                'size' => 'large',
                'title' => Yii::t('app', "บทความ {name}", [
                    'name' => $model->contentGroup->name
                ]),
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('app', "แก้ไข"), ['update', 'id' => $id], ['class' => 'btn btn-primary btn-lg', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('show', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionLaw($id) {
        $this->layout = 'index';
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            return [
                'size' => 'large',
                'title' => Yii::t('app', "บทความ {name}", [
                    'name' => $model->contentGroup->name
                ]),
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('app', "แก้ไข"), ['update', 'id' => $id], ['class' => 'btn btn-primary btn-lg', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('law', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Content model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($groupId, $categoryId = NULL) {
        $request = Yii::$app->request;
        $model = new Content();
        $model->content_group_id = $groupId;
        if (isset($categoryId)) {
            $model->content_category_id = $categoryId;
        }
        $model->view = 2;

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "บทความ {name}", [
                        'name' => $model->contentGroup->name
                    ]),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'groupId' => $groupId,
                        'categoryId' => $categoryId
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate(FALSE)) {

                $image = UploadedFile::getInstance($model, 'image');
                if (isset($image)) {
                    $model->image = $image;
                    $model->image->name = 'download-'.uniqid() . '.' . $model->image->extension;
                    $path = 'uploads/content/images/';
                    $model->image->saveAs($path . $model->image->name);
                    $model->image = $model->image->name;
                }
                $file = UploadedFile::getInstance($model, 'file_name');
                if (isset($file)) {
                    $model->file_name = $file;
                    $model->file_name->name = uniqid() . '.' . $model->file_name->extension;
                    $path = 'uploads/content/files/';
                    $model->file_name->saveAs($path . $model->file_name->name);
                    $model->file_name = $model->file_name->name;
                }
                $model->save(FALSE);
                $model = new Content();
                $model->content_group_id = $groupId;
                Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, Yii::t('app', "เพิ่มบทความเรียนร้อยแล้ว"));
                return [
                    'size' => 'large',
                    'forceReload' => '#crud-datatable-content-pjax',
                    'title' => Yii::t('app', "บทความ {name}", [
                        'name' => $model->contentGroup->name
                    ]),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'groupId' => $groupId,
                        'categoryId' => $categoryId
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
                ];
            } else {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "บทความ {name}", [
                        'name' => $model->contentGroup->name
                    ]),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'groupId' => $groupId,
                        'categoryId' => $categoryId
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                            'groupId' => $groupId,
                            'categoryId' => $categoryId
                ]);
            }
        }
    }

    /**
     * Updates an existing Content model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $image = $model->image;
        $file = $model->file_name;

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "บทความ {name}", [
                        'name' => $model->contentGroup->name
                    ]),
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'groupId' => $model->content_group_id,
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate(FALSE)) {
                $imageEdit = UploadedFile::getInstance($model, 'image');
                if (isset($imageEdit)) {
                    $model->image = $imageEdit;
                    $model->image->name = 'download-'.uniqid() . '.' . $model->image->extension;
                    $path = 'uploads/content/images/';
                    $model->image->saveAs($path . $model->image->name);
                    $model->image = $model->image->name;
                } else {
                    $model->image = $image;
                }
                $fileEdit = UploadedFile::getInstance($model, 'file_name');
                if (isset($fileEdit)) {
                    $model->file_name = $fileEdit;
                    $model->file_name->name = 'download-'.uniqid() . '.' . $model->file_name->extension;
                    $path = 'uploads/content/files/';
                    $model->file_name->saveAs($path . $model->file_name->name);
                    $model->file_name = $model->file_name->name;
                } else {
                    $model->file_name = $file;
                }
                $model->save(FALSE);
                return [
                    'size' => 'large',
                    'forceReload' => '#crud-datatable-content-pjax',
                    'title' => Yii::t('app', "บทความ {name}", [
                        'name' => $model->contentGroup->name
                    ]),
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            } else {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "บทความ {name}", [
                        'name' => $model->contentGroup->name
                    ]),
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'groupId' => $model->content_group_id,
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->validate(FALSE)) {
                $imageEdit = UploadedFile::getInstance($model, 'image');
                if (isset($imageEdit)) {
                    $model->image = $imageEdit;
                    $model->image->name = uniqid() . '.' . $model->image->extension;
                    $path = 'uploads/content/images/';
                    $model->image->saveAs($path . $model->image->name);
                    $model->image = $model->image->name;
                } else {
                    $model->image = $image;
                }
                $fileEdit = UploadedFile::getInstance($model, 'file_name');
                if (isset($fileEdit)) {
                    $model->file_name = $fileEdit;
                    $model->file_name->name = uniqid() . '.' . $model->file_name->extension;
                    $path = 'uploads/content/files/';
                    $model->file_name->saveAs($path . $model->file_name->name);
                    $model->file_name = $model->file_name->name;
                } else {
                    $model->file_name = $file;
                }
                $model->save(FALSE);
                Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, Yii::t('app', "แก้ไขบทความเรียนร้อยแล้ว"));
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'groupId' => $model->content_group_id,
                ]);
            }
        }
    }

    /**
     * Delete an existing Content model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->deleted = 1;
        if (!$model->save()) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถลบข้อมูลได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#crud-datatable-content-pjax',
                    'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                    'content' => $this->renderAjax('@app/views/widgets/_alert'),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            } else {
                return $this->redirect(['index']);
            }
        }
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-content-pjax'];
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Content model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkdelete() {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Content model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Content the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
