<?php

namespace app\controllers;

use Yii;
use app\models\FormIssue;
use app\models\FormIssueSearch;
//use app\rbac\RbacController;
use app\rbac\RbacController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\Json;
use kartik\widgets\Alert;
use app\models\FormIssueFile;

/**
 * FormIssueController implements the CRUD actions for FormIssue model.
 */
class FormIssueController extends RbacController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FormIssue models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new FormIssueSearch();
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FormIssue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $type) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'size' => 'large',
                'title' => $type == FormIssueFile::TYPE_FILE ? "แสดงไฟล์" : "แสดงรูป",
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                    'type' => $type,
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
//                . (Yii::$app->util->isValidSixMonthSubmitDate($model->sixMonthForm->year) 
//                    ? Html::a(Yii::t('app', $type == FormIssueFile::TYPE_FILE ? "เพิ่มไฟล์" : "เพิ่มรูป"), ['create', 'id' => $id, 'type' => $type], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
//                    : "")
                . Html::a(Yii::t('app', $type == FormIssueFile::TYPE_FILE ? "เพิ่มไฟล์" : "เพิ่มรูป"), ['create', 'id' => $id, 'type' => $type], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                        'model' => $model,
                        'type' => $type
            ]);
        }
    }

    /**
     * Creates a new FormIssue model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id, $type, $view = NULL) {

        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $sixMonth = $model->sixMonthForm;
        $year = $sixMonth->year;

        $issueFile = new \app\models\FormIssueFile();
        if ($type == \app\models\FormIssueFile::TYPE_FILE) {
            $issueFile->scenario = \app\models\FormIssueFile::SCENARIO_FILE;
        } else {
            $issueFile->scenario = \app\models\FormIssueFile::SCENARIO_IMAGE;
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "เพิ่มข้อมูลรายงาน 6 เดือนประจำปี") . $year,
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'issueFile' => $issueFile,
                        'type' => $type
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($issueFile->load($request->post()) && $issueFile->validate()) {
//                \yii\helpers\VarDumper::dump($issueFile->attributes);
//                exit;
                $files = UploadedFile::getInstances($issueFile, 'uploadFiles');
                if (count($files) == 0) {
                    $issueFile->addError('uploadFiles', 'กรุณาอับโหลดไฟล์');
                    return [
                        'size' => 'large',
                        'title' => Yii::t('app', "เพิ่มข้อมูลรายงาน 6 เดือนประจำปี") . $year,
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'issueFile' => $issueFile,
                            'type' => $type
                        ]),
                        'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
//                $name = $issueFile->name;
                foreach ($files as $file) {
                    $newFile = new \app\models\FormIssueFile();
                    $newFile->form_issue_id = $model->id;
//                    $issueFile->name = $issueFile->name;
                    $newFile->type = $type;
                    $fileName = uniqid() . '.' . $file->extension;
                    $path = $newFile->getPath();
                    \yii\helpers\FileHelper::createDirectory($path);
                    $fullFileName = $path . '/' . $fileName;
                    $file->saveAs($fullFileName);
                    $newFile->file_name = $fullFileName;
                    $newFile->name = $issueFile->name;
                    $newFile->save(false);
                }
                if ($model->hasFile && $model->hasImage) {
                    $model->submitted_by = \Yii::$app->user->id;
                    $model->submitted_at = date('Y-m-d H:i:s');
                    $model->save(false);
                }

//                Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, Yii::t('app', "เพิ่มข้อมูลรายงาน 6 เดือนประจำปี") . $year);
                return [
                    'size' => 'large',
                    'forceReload' => '#form-issue-status-pjax',
                    'title' => $type == FormIssueFile::TYPE_FILE ? "แสดงไฟล์" : "แสดงรูป",
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                        'type' => $type,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    . Html::a(Yii::t('app', $type == FormIssueFile::TYPE_FILE ? "เพิ่มไฟล์" : "เพิ่มรูป"), ['create', 'id' => $id, 'type' => $type], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "เพิ่มข้อมูลรายงาน 6 เดือนประจำปี") . $year,
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'issueFile' => $issueFile,
                        'type' => $type
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                            'issueFile' => $issueFile,
                            'type' => $type
                ]);
            }
        }
    }

    /**
     * Updates an existing FormIssue model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " FormIssue #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-form-issue-pjax',
                    'title' => Yii::t('app', "แก้ไข") . " FormIssue #" . $id,
                    'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'แก้ไขข้อมูล form-issue เรียบร้อยแล้ว') . '</div>',
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('app', 'แก้ไข'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " FormIssue #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing FormIssue model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->deleted = 1;
        if (!$model->save()) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถลบข้อมูลได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#crud-datatable-form-issue-pjax',
                    'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                    'content' => $this->renderAjax('@app/views/widgets/_alert'),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            } else {
                return $this->redirect(['index']);
            }
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-form-issue-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing FormIssue model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete() {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-form-issue-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    public function actionCheck($id, $reload = null) {
        $request = Yii::$app->request;
        $currentRole = Yii::$app->session->get('currentRole');

        $model = $this->findModel($id);
        $model->is_pass = 1;
        if (!isset($reload)) {
            $reload = '#crud-datatable-form-issue-' . $model->six_month_form_id . '-pjax';
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงาน 6 เดือน"),
                    'content' => $this->renderAjax('check', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->checked_by = \Yii::$app->user->id;
                $model->checked_at = date('Y-m-d H:i:s');
                if ($model->is_pass) {
                    $model->score = $model->assessIssue->score;
                } else {
                    $model->score = 0;
                }
                $model->save();

                return [
                    'forceReload' => $reload,
                    'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงาน 6 เดือน"),
                    'content' => '<div class="alert alert-success dark">' . Yii::t('app', "ตรวจสอบข้อมูลรายงาน 6 เดือน หัวข้อ ") . $model->assessIssue->name . Yii::t('app', "เรียบร้อยแล้ว") . '</div>',
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงาน 6 เดือน"),
                    'content' => $this->renderAjax('check', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
//            if ($model->load($request->post()) && $model->save()) {
//                return $this->redirect(['view', 'id' => $model->id]);
//            } else {
//                return $this->render('check', [
//                            'model' => $model,
//                ]);
//            }
        }
    }

    /**
     * Finds the FormIssue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormIssue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = FormIssue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
