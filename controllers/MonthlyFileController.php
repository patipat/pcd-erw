<?php

namespace app\controllers;

use Yii;
use app\models\MonthlyFile;
use app\models\MonthlyFileSearch;
use app\models\MonthlyFormSubmit;
use app\models\Office;
//use app\rbac\RbacController;
use app\rbac\RbacController;
use GuzzleHttp\Client;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\Json;
use kartik\widgets\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * MonthlyFileController implements the CRUD actions for MonthlyFile model.
 */
class MonthlyFileController extends RbacController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays a single MonthlyFile model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($officeId, $year, $month)
    {
        $request = Yii::$app->request;
        $o = Office::findOne($officeId);
        $children = $o->getAllChildren();
        $officeIds = ArrayHelper::getColumn($children, 'id');
        $officeIds[] = $officeId;
        $files = MonthlyFile::find()->isDeleted(false)->office($officeIds)->year($year)->month($month)->all();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'size' => 'large',
                'title' => "แสดงไฟล์",
                'content' => $this->renderAjax('view', [
                    'files' => $files,
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    //                . (Yii::$app->util->isValidSixMonthSubmitDate($model->sixMonthForm->year) 
                    //                    ? Html::a(Yii::t('app', $type == MonthlyFile::TYPE_FILE ? "เพิ่มไฟล์" : "เพิ่มรูป"), ['create', 'id' => $id, 'type' => $type], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    //                    : "")
                    . Html::a(Yii::t('app', "เพิ่มไฟล์"), ['create', 'officeId' => $officeId, 'year' => $year, 'month' => $month], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'files' => $files,
            ]);
        }
    }

    /**
     * Creates a new MonthlyFile model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($officeId, $year, $month, $reload = 'monthly-form-status-pjax', $monthlySubmitId=null, $from=null)
    {

        $request = Yii::$app->request;
        $model = new MonthlyFile();
        $model->office_id = $officeId;
        $model->year = $year;
        $model->month = $month;

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "เพิ่มไฟล์รายเดือน") . " {$month}/{$year}",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                //                \yii\helpers\VarDumper::dump($issueFile->attributes);
                //                exit;
                $files = UploadedFile::getInstances($model, 'uploadFiles');
                if (count($files) == 0) {
                    $model->addError('uploadFiles', 'กรุณาอับโหลดไฟล์');
                    return [
                        'size' => 'large',
                        'title' => Yii::t('app', "เพิ่มไฟล์รายเดือน")  . " {$month}/{$year}",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
                //                $name = $issueFile->name;
                foreach ($files as $file) {
                    $newFile = new \app\models\MonthlyFile();
                    $newFile->office_id = $model->office_id;
                    $newFile->year = $model->year;
                    $newFile->month = $model->month;
                    $fileName = uniqid() . '.' . $file->extension;
                    $path = $newFile->getPath();
                    \yii\helpers\FileHelper::createDirectory($path);
                    $fullFileName = $path . '/' . $fileName;
                    $file->saveAs($fullFileName);
                    $newFile->file_name = $fullFileName;
                    $newFile->name = $model->name;
                    $newFile->save(false);
                }

                //                Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, Yii::t('app', "เพิ่มข้อมูลรายงาน 6 เดือนประจำปี") . $year);

                $o = Office::findOne($officeId);
                $children = $o->getAllChildren();
                $officeIds = ArrayHelper::getColumn($children, 'id');
                $officeIds[] = $officeId;
                $files = MonthlyFile::find()->isDeleted(false)->office($officeIds)->year($year)->month($month)->all();
                if (!empty($monthlySubmitId)) {
                    $submit = MonthlyFormSubmit::findOne($monthlySubmitId);
                    if ($from == 'check') {
                        $submit->scenario = MonthlyFormSubmit::SCENARIO_CHECK;
                        return [
                            'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงานรายเดือน"),
                            'size' => 'large',
                            'content' => $this->renderAjax('@app/views/monthly-form-submit/check', [
                                'model' => $submit,
                                'file' => 1,
                            ]),
                            'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    } else if ($from == 'confirm') {
                        return [
                            'title' => Yii::t('app', "ยืนยันข้อมูลรายงานรายเดือน"),
                            'size' => 'large',
                            'content' => $this->renderAjax('@app/views/monthly-form-submit/confirm', [
                                'model' => $submit,
                                'file' => 1,
                            ]),
                            'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button(Yii::t('app', 'ยืนยัน'), ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    } else {
                        return [
                            'title' => Yii::t('app', "ยืนยันข้อมูลรายงานรายเดือน"),
                            'size' => 'large',
                            'content' => $this->renderAjax('@app/views/monthly-form-submit/view', [
                                'model' => $submit,
                                'file' => 1,
                                'from' => 'view',
                            ]),
                            'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) 
                        ];
                    }
                }
                $res = [
                    'size' => 'large',
                    'title' => "แสดงไฟล์",
                    'forceReload' => "#{$reload}",
                    'content' => $this->renderAjax('view', [
                        'files' => $files,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                        . Html::a(Yii::t('app', "เพิ่มไฟล์"), ['create', 'officeId' => $officeId, 'year' => $year, 'month' => $month], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
                if (!empty($reload)) {
                    $res['forceReload'] = "#{$reload}";
                }

                return $res;
            } else {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "เพิ่มไฟล์รายเดือน")  . " {$month}/{$year}",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing MonthlyFile model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $model = $this->findModel($request->post('key'));
        $model->deleted = 1;
        $model->save();

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload' => '#form-issue-status-pjax',
            ];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing MonthlyFile model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-form-issue-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    public function actionDownload($key)
    {
        $model = $this->findModel($key);
        if (file_exists($model->filePath)) {
            $info = pathinfo($model->filePath);
            //            \yii\helpers\VarDumper::dump($info);
            //            exit;
            return Yii::$app->response->sendFile($model->filePath, isset($model->name) ? "{$model->name}.{$info['extension']}" : $info['basename']);
        }
    }

    /**
     * Finds the MonthlyFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MonthlyFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MonthlyFile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
