<?php

namespace app\controllers;

use Yii;
use app\models\MonthlyFormSubmit;
use app\models\MonthlyFormSubmitSearch;
//use app\rbac\RbacController;
use app\rbac\RbacController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use kartik\widgets\Alert;
use yii\web\HttpException;
use app\models\MonthlyForm;
use app\models\OfficePopulationH;
use app\models\CalculationQueue;
use app\models\Setting;
use yii\helpers\Url;

/**
 * MonthlyFormSubmitController implements the CRUD actions for MonthlyFormSubmit model.
 */
class MonthlyFormSubmitController extends RbacController {
    protected $allowedActions = ['checked-list', 'check'];

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MonthlyFormSubmit models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MonthlyFormSubmitSearch();
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MonthlyFormSubmit model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => \Yii::t('app', "รานงานรายเดือน"),
                'size' => 'large',
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                    'redirect' => Url::to(['monthly-form-submit/view', 'id' => $id, 'file' => 1], true),
                    'file' => isset($file) ? $file : null,
                    'from' => 'view',
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new MonthlyFormSubmit model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($year, $month, $reload = null) {
        if (!isset(\Yii::$app->user->identity->person->office->officePopulationH)) {
            throw new HttpException(500, \Yii::t('app', 'กรุณาระบุข้อมูลบุคลากรในหน่วยงานของท่านก่อน'));
        }
        $request = Yii::$app->request;
        $model = MonthlyFormSubmit::find()->isDeleted(false)->year($year)->month($month)->office(\Yii::$app->user->identity->person->office_id)->last()->one();

        if (!isset($model)) {
            $model = new MonthlyFormSubmit();
            $model->year = $year;
            $model->month = $month;
            $model->office_id = \Yii::$app->user->identity->person->office_id;
            if (isset(\Yii::$app->user->identity->person->office->officePopulationH)) {
                $model->pop_int = \Yii::$app->user->identity->person->office->officePopulationH->pop_int;
                $model->pop_ext = \Yii::$app->user->identity->person->office->officePopulationH->pop_ext;
            }
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "บันทึกรายงานรายเดือน"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->office_population = $model->pop_int + $model->pop_ext;
                if (isset($model->confirmed_by)) {
                    $attrs = $model->attributes;
                    $model = new MonthlyFormSubmit();
                    $model->attributes = $attrs;
                    $model->confirmed_by = null;
                    $model->confirmed_at = null;
                    $model->checked_by = null;
                    $model->checked_at = null;
                    $model->is_pass = 0;
                    $model->is_late = 0;
                    $model->comment = null;
                }

                $model->submitted_by = \Yii::$app->user->id;
                $model->submitted_at = date('Y-m-d H:i:s');
                $model->save();
                if (!isset($reload)) {
                    $reload = '#crud-datatable-monthly-form-submit-pjax';
                }
                \app\models\Alert::addMonthlyConfirmAlert($model);
                return [
                    'forceReload' => $reload,
                    'title' => Yii::t('app', "บันทึกรายงานรายเดือน"),
                    'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'บันทึกรายงานรายเดือนเรียบร้อยแล้ว') . '</div>',
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "บันทึกรายงานรายเดือน"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing MonthlyFormSubmit model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " MonthlyFormSubmit #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-monthly-form-submit-pjax',
                    'title' => Yii::t('app', "แก้ไข") . " MonthlyFormSubmit #" . $id,
                    'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'แก้ไขข้อมูล monthly-form-submit เรียบร้อยแล้ว') . '</div>',
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('app', 'แก้ไข'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " MonthlyFormSubmit #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing MonthlyFormSubmit model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->deleted = 1;
        if (!$model->save()) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถลบข้อมูลได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#crud-datatable-monthly-form-submit-pjax',
                    'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                    'content' => $this->renderAjax('@app/views/widgets/_alert'),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            } else {
                return $this->redirect(['index']);
            }
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-monthly-form-submit-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    public function actionConfirm($id, $reload = null, $file = null) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if (!isset($reload)) {
            $reload = '#crud-datatable-monthly-form-submit-pjax';
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "ยืนยันข้อมูลรายงานรายเดือน"),
                    'size' => 'large',
                    'content' => $this->renderAjax('confirm', [
                        'model' => $model,
                        'file' => $file,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'ยืนยัน'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->confirmed_by = \Yii::$app->user->id;
                $model->confirmed_at = date('Y-m-d H:i:s');
                $alertDay = Setting::getValue(Setting::MONTHLY_COMPLETE_ALERT_DAY);
                $year = $model->year - 543;
                $alertDate = new \DateTime("{$year}-{$model->month}-{$alertDay}");
                $alertDate->add(new \DateInterval('P1M'));
                $now = new \DateTime();
                if ($now > $alertDate) {
                    $model->is_late = 1;
                }

                if (!$model->save()) {
                    Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถบันทึกข้อมูลได้ {error}", [
                                'error' => \Yii::$app->util->errorSummary($model),
                    ]));
                    if ($request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'forceReload' => $reload,
                            'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                            'content' => $this->renderAjax('@app/views/widgets/_alert'),
                            'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                        ];
                    } else {
                        return $this->redirect(['index']);
                    }
                }

                $op = $model->office->officePopulationH;
                if ($op->pop_int != $model->pop_int || $op->pop_ext != $model->pop_ext) {
                    $newOp = new OfficePopulationH();
                    $newOp->office_id = $model->office_id;
                    $newOp->pop_ext = $model->pop_ext;
                    $newOp->pop_int = $model->pop_int;
                    $newOp->total = $model->office_population;
                    $newOp->save();
                }
                CalculationQueue::addQueue(CalculationQueue::TYPE_MONTHLY_PROGRESS, $model->office_id, $model->year, $model->month);
                CalculationQueue::runCalculateCmd();
                \app\models\Alert::addMonthlyCheckAlert($model);
                return [
                    'forceReload' => $reload,
                    'size' => 'large',
                    'title' => Yii::t('app', "ยืนยันข้อมูลรายงานรายเดือน"),
                    'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'ยืนยันข้อมูลรายงานรายเดือนเรียบร้อยแล้ว') . '</div>',
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "ยืนยันข้อมูลรายงานรายเดือน"),
                    'size' => 'large',
                    'content' => $this->renderAjax('confirm', [
                        'model' => $model,
                        'file' => $file,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'ยืนยัน'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        }
    }

    public function actionCheck($id, $reload = null, $file = null) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = MonthlyFormSubmit::SCENARIO_CHECK;
        if (!isset($reload)) {
            $reload = '#crud-datatable-monthly-form-submit-pjax';
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงานรายเดือน"),
                    'size' => 'large',
                    'content' => $this->renderAjax('check', [
                        'model' => $model,
                        'file' => $file,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->checked_by = \Yii::$app->user->id;
                $model->checked_at = date('Y-m-d H:i:s');
                $tran = $model->getDb()->beginTransaction();
                try {
                    $model->save();
                    if ($model->is_pass) {
                        $mf = MonthlyForm::find()->isDeleted(false)->office($model->office_id)->year($model->year)->month($model->month)->one();
                        if (!isset($mf)) {
                            $mf = new MonthlyForm();
                            $mf->office_id = $model->office_id;
                            $mf->year = $model->year;
                            $mf->month = $model->month;
                        }
                        $mf->source_type_id = $model->source_type_id;
                        $mf->pop_int = $model->pop_int;
                        $mf->pop_ext = $model->pop_ext;
                        $mf->office_population = $model->office_population;
                        $mf->work_day = $model->work_day;
                        $mf->garbage = $model->garbage;
                        $mf->bag = $model->bag;
                        $mf->cup = $model->cup;
                        $mf->foam = $model->foam;
                        $mf->mask = $model->mask;
                        $mf->is_pass = $model->is_pass;
                        $mf->submitted_by = $model->submitted_by;
                        $mf->submitted_at = $model->submitted_at;
                        $mf->confirmed_by = $model->confirmed_by;
                        $mf->confirmed_at = $model->confirmed_at;
                        $mf->checked_by = $model->checked_by;
                        $mf->checked_at = $model->checked_at;
                        $mf->save();
                    }
                    if ($model->is_pass) {
                        $year = Setting::getValue(Setting::CURRENT_YEAR);
                        CalculationQueue::addQueue(CalculationQueue::TYPE_PARENT_MONTHLY_FORM, $model->office_id, $model->year, $model->month);
                        CalculationQueue::addQueue(CalculationQueue::TYPE_YEARLY_ASSESSMENT, $model->office_id, $year);
                    }
                    $tran->commit();
                } catch (Exception $ex) {
                    $tran->rollBack();
                    throw $ex;
                }
                CalculationQueue::runCalculateCmd();
                if (!$model->is_pass) {
                    \app\models\Alert::addMonthlyFailAlert($model);
                }
                return [
                    'forceReload' => $reload,
                    'size' => 'large',
                    'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงานรายเดือน"),
                    'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'ตรวจสอบข้อมูลรายงานรายเดือนเรียบร้อยแล้ว') . '</div>',
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงานรายเดือน"),
                    'size' => 'large',
                    'content' => $this->renderAjax('check', [
                        'model' => $model,
                        'file' => $file,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
//            if ($model->load($request->post()) && $model->save()) {
//                return $this->redirect(['view', 'id' => $model->id]);
//            } else {
//                return $this->render('check', [
//                            'model' => $model,
//                ]);
//            }
        }
    }

    /**
     * Delete multiple existing MonthlyFormSubmit model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete() {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-monthly-form-submit-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    public function actionCheckList() {
        $searchModel = new MonthlyFormSubmitSearch();
        $searchModel->deleted = 0;
        $searchModel->isLastUncheck = true;
        $searchModel->isCheckChildrenByRole = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('check-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheckedList() {
        $searchModel = new MonthlyFormSubmitSearch();
        $searchModel->deleted = 0;
//        $searchModel->isLastUncheck = true;
        $searchModel->isLastSubmit = true;
//        $searchModel->isConfirmed = true;
//        $searchModel->isAssessed = true;
        $searchModel->isCheckChildrenByRole = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('checked-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSubList() {
        $model = $this->findModel($_POST['expandRowKey']);
        $searchModel = new MonthlyFormSubmitSearch();
        $searchModel->deleted = 0;
        $searchModel->parentId = $model->office_id;
        $searchModel->year = $model->year;
        $searchModel->month = $model->month;
//        $searchModel->isLastUncheck = true;
        $searchModel->isLastSubmit = true;
//        $searchModel->isConfirmed = true;
//        $searchModel->isAssessed = true;
        // $searchModel->isCheckChildrenByRole = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderPartial('sub-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRecordList() {
        $request = Yii::$app->request;
        $searchModel = new MonthlyFormSubmitSearch();
        $searchModel->deleted = 0;
        $searchModel->isConfirmed = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = [
            'year' => SORT_DESC,
            'month' => SORT_DESC,
        ];

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'size' => 'large',
                'title' => "ประวัติข้อมูลรายงานรายเดือน {$searchModel->office->name} ปี {$searchModel->year} เดือน {$searchModel->monthName}",
                'content' => $this->renderAjax('_record-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
            ];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->render('record-list', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionOfficeConfirmPending($year, $month, $officeType = null, $level = null) {
        $request = Yii::$app->request;
        $dataProvider = Yii::$app->user->identity->person->office->getCurrentConfirmPendingMonthlyChildren($year, $month, $officeType, $level);
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $d = new \DateTime("{$year}-{$month}-01");
            return [
                'title' => "รายชื่อหน่วยงานทที่รอยืนยันข้อมูลประจำเดือน" . \Yii::$app->formatter->asDate($d->format('Y-m-d'), 'php:F'),
                'content' => $this->renderAjax('office-confirm-pending', [
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('office-confirm-pending', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }
    
    /**
     * Finds the MonthlyFormSubmit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MonthlyFormSubmit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MonthlyFormSubmit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
