<?php

namespace app\controllers;

use Yii;
use app\models\Office;
use app\models\OfficeSearch;
//use app\rbac\RbacController;
use app\rbac\RbacController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use kartik\widgets\Alert;
use yii\helpers\VarDumper;

/**
 * OfficeController implements the CRUD actions for Office model.
 */
class OfficeController extends RbacController {

    /**
     * @inheritdoc
     */
//    protected $allowedActions = ['index-admin'];
    protected $allowedActions = ['yearly-assessment-report-web'];

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Office models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new OfficeSearch();
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexAdmin() {
        $searchModel = new OfficeSearch();
        $searchModel->parent_id = Yii::$app->user->identity->person->office_id;
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-admin', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Office model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Office #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('app', 'แก้ไข'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Office model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $request = Yii::$app->request;
        $currentRole = Yii::$app->session->get('currentRole');
        $model = new Office(['scenario' => Office::SCENARIO_CREATE_UPDATE]);
        if ($currentRole['role_id'] == \app\models\Role::ADMINISTRATOR) {
            $model->parent_id = Yii::$app->user->identity->person->office_id;
            $model->office_type_id = Yii::$app->user->identity->person->office->office_type_id;
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "เพิ่ม Office"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->level = $model->calculatedLevel;
                $model->save();
                $model = new Office(['scenario' => Office::SCENARIO_CREATE_UPDATE]);
                Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, Yii::t('app', 'เพิ่มข้อมูล office เรียบร้อยแล้ว'));
                return [
                    'forceReload' => '#crud-datatable-office-pjax',
                    'title' => Yii::t('app', "เพิ่ม Office"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "เพิ่ม Office"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Office model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = Office::SCENARIO_CREATE_UPDATE;

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " Office #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->level = $model->calculatedLevel;
                $model->save();
                return [
                    'forceReload' => '#crud-datatable-office-pjax',
                    'title' => Yii::t('app', "แก้ไข") . " Office #" . $id,
                    'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'แก้ไขข้อมูล office เรียบร้อยแล้ว') . '</div>',
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('app', 'แก้ไข'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " Office #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Office model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->deleted = 1;
        if (!$model->save()) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถลบข้อมูลได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#crud-datatable-office-pjax',
                    'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                    'content' => $this->renderAjax('@app/views/widgets/_alert'),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            } else {
                return $this->redirect(['index']);
            }
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-office-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Office model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete() {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-office-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    public function actionOfficeUnsend($month, $year, $nameMonth,$officeType = NULL) {
        $currentRole = Yii::$app->session->get('currentRole');
        if ($currentRole['role_id'] == \app\models\Role::ADMINISTRATOR) {
            $unSend = Office::find()->joinWith('monthlyFormSubmits')->isDeleted(false)->noConfirmedMonthlyForm($year, $month)->parent(Yii::$app->user->identity->person->office_id)->all();
        } else {
            $unSend = Office::find()->joinWith('monthlyFormSubmits')->isDeleted(false)->noConfirmedMonthlyForm($year, $month)->level(2)->officeType($officeType)->all();
        }
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "รายชื่อหน่วยงานที่ยังไม่ส่งข้อมูลประจำเดือน" . $nameMonth,
                'content' => $this->renderAjax('office-unsend', [
                    'unSend' => $unSend,
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('office-unsend', [
                        'unSend' => $unSend,
            ]);
        }
    }
    public function actionOfficeUnsendSix( $year,$officeType = NULL) {
        $currentRole = Yii::$app->session->get('currentRole');
        if ($currentRole['role_id'] == \app\models\Role::ADMINISTRATOR) {
            $unSend = Office::find()->joinWith('sixMonthForms')->isDeleted(false)->noConfirmedSixMonthForm($year)->parent(Yii::$app->user->identity->person->office_id)->all();
        } else {
            $unSend = Office::find()->joinWith('sixMonthForms')->isDeleted(false)->noConfirmedSixMonthForm($year)->level(2)->officeType($officeType)->all();
        }
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "รายชื่อหน่วยงานที่ยังไม่ส่งรายงาน",
                'content' => $this->renderAjax('office-unsend-six', [
                    'unSend' => $unSend,
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            return $this->render('office-unsend-six', [
                        'unSend' => $unSend,
            ]);
        }
    }
    public function actionUnsetList() {
        $request = Yii::$app->request;
        $searchModel = new OfficeSearch();
        $searchModel->deleted = 0;
        $searchModel->unsetPopulation = true;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'size' => 'large',
                'title' => Yii::t('app', "หน่วยงานที่ยังไม่บันทึกจำนวนบุคลากร"),
                'content' => $this->renderAjax('unset-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
            ];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->render('unset-list', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionMonthlyAssessmentReport($year, $officeType, $level)
    {
        $searchModel = new OfficeSearch();
        $searchModel->deleted = 0;
        $searchModel->office_type_id = $officeType;
        $searchModel->level = $level;
        $searchModel->year = $year;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('monthly-assessment-report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionYearlyAssessmentReport($year, $officeType, $level) {
        $searchModel = new OfficeSearch();
        $searchModel->deleted = 0;
        $searchModel->office_type_id = $officeType;
        $searchModel->level = $level;
        $searchModel->year = $year;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('yearly-assessment-report', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionYearlyAssessmentReportOffice($year, $officeId) {
        $searchModel = new OfficeSearch();
        $searchModel->deleted = 0;
        $searchModel->parent_id = $officeId;
        $searchModel->year = $year;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('yearly-assessment-report-office', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'officeId'=>$officeId
        ]);
    }

    public function actionYearlyAssessmentReportWeb($year, $officeType, $level) {
        $this->layout = 'site';
        $typeName = \app\models\OfficeType::findOne($officeType);
        $searchModel = new OfficeSearch();
        $searchModel->deleted = 0;
        $searchModel->office_type_id = $officeType;
        $searchModel->level = $level;
        $searchModel->year = $year;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('yearly-assessment-report-web', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'typeName' => $typeName
        ]);
    }

    public function actionList() {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $officeTypeId = end($_POST['depdrop_parents']);
            $list = Office::find()->isDeleted(FALSE)->officeType($officeTypeId)->orderBy('CONVERT(office.name USING TIS620)')->asArray()->all();
            $selected = null;
            if ($officeTypeId != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $office) {
                    $out[] = ['id' => $office['id'], 'name' => $office['name']];
                    if ($i == 0) {
                        $selected = $office['id'];
                    }
                }
                // Shows how you can preselect a value
//                echo Json::encode(['output' => $out, 'selected' => '']);
                return ['output' => $out, 'selected' => ''];
            }
        }
//        echo Json::encode(['output' => '', 'selected' => '']);
        return ['output' => '', 'selected' => ''];
    }

    /**
     * Finds the Office model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Office the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Office::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
