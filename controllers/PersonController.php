<?php

namespace app\controllers;

use Yii;
use app\models\Person;
use app\models\PersonSearch;
//use app\rbac\RbacController;
use app\rbac\RbacController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use kartik\widgets\Alert;
use app\models\User;
use yii\helpers\ArrayHelper;
use app\models\RegistrationForm;
use yii\data\ArrayDataProvider;

ini_set('max_execution_time', 300);

/**
 * PersonController implements the CRUD actions for Person model.
 */
class PersonController extends RbacController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Person models.
     * @return mixed
     */
    public function actionIndex() {
        $currentRole = \Yii::$app->session->get('currentRole');
        if ($currentRole['role_id'] != \app\models\Role::SUPER_ADMINISTRATOR) {
            throw new \yii\web\UnauthorizedHttpException(Yii::t('app', 'ไม่มีสิทธิ์เข้าถึงข้อมูลโครงการ'));
        }
        $searchModel = new PersonSearch();
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexAdmin() {
        $searchModel = new PersonSearch();
        $searchModel->parentId = Yii::$app->user->identity->person->office_id;
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-admin', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImport() {
        try {
            $file = Yii::getAlias('@webroot') . '/images/user.xlsx';
            $inputFile = \PHPExcel_IOFactory::identify($file);
            $objReader = \PHPExcel_IOFactory::createReader($inputFile);
            $objPHPExcel = $objReader->load($file);
        } catch (Exception $e) {
            Yii::$app->session->addFlash('error', 'เกิดข้อผิดพลาด' . $e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $objWorksheet = $objPHPExcel->getActiveSheet();

        foreach ($objWorksheet->getRowIterator() as $rowIndex => $row) {
            $arr[] = $objWorksheet->rangeToArray('A' . $rowIndex . ':' . $highestColumn . $rowIndex);
        }

        if (Yii::$app->request->post('selection')) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $selection = Yii::$app->request->post('selection');
//                var_dump($selection);
//                die();
                foreach ($objWorksheet->getRowIterator() as $rowIndex => $row) {

                    $tmpdata = $objWorksheet->rangeToArray('A' . $rowIndex . ':' . $highestColumn . $rowIndex);
                    $data[] = $tmpdata[0];
                }

                foreach ($selection as $key => $val) {
                    //Check if specific
                    if (!empty($val)) {//ถ้าเลือกส่ง
                        //var_dump($data[$key]);
                        //มีการ Import หรือยัง
                        $importUser = new User;
                        $importUser->username = $data[$key][3];
                        $importUser->setPassword($data[$key][4]);
                        $importUser->generateAuthKey();
                        $importUser->status = User::STATUS_ACTIVE;
                        $importUser->save(false);
                        $importPerson = new Person();
                        $importPerson->office_id = $data[$key][1];
                        $importPerson->first_name = $data[$key][5];
                        $importPerson->user_id = $importUser->id;
                        $importPerson->save(false);
                        Yii::$app->session->addFlash('info', $data[$key][1] . ' รายการนี้ส่งเข้าระบบแล้ว');
                    } else {//ถ้าไม่ได้เลือกส่ง
                        Yii::$app->session->addFlash('warning', $data[$key][1] . ' ไม่ได้เลือกส่ง');
                    }
                }//end foreach

                $transaction->commit();
                Yii::$app->session->addFlash('success', 'ดำเนินการนำเข้าข้อมูลเรียบร้อยแล้ว กรุณาตรวจสอบความถูกต้องอีกครั้ง');
                return $this->redirect(['import']);
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash('error', 'เกิดข้อผิดพลาด');
            }
        }///end if post


        $dataProvider = new ArrayDataProvider([
            'allModels' => $arr,
            'pagination' => false,
        ]);

        return $this->render('import', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Person model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Person #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('app', 'แก้ไข'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Person model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $currentRole = \Yii::$app->session->get('currentRole');
        $request = Yii::$app->request;
        $model = new Person();
//        if ($currentRole['role_id'] == \app\models\Role::ADMINISTRATOR) {
//            $model->office_id = Yii::$app->user->identity->person->office_id;
//        }
        $regForm = new RegistrationForm();
        $regForm->scenario = 'register';
        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "เพิ่ม เจ้าหน้าที่"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'regForm' => $regForm,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load(Yii::$app->request->post()) && $regForm->load(Yii::$app->request->post()) && $model->validate() && $regForm->validate()) {

                if (!empty($regForm->username) || !empty($regForm->password) || !empty($regForm->password_repeat)) {

                    if (!empty($regForm->username)) {
                        $user = new User();
                        $user->username = $regForm->username;
                        $user->setPassword($regForm->password);
                        $user->generateAuthKey();
                        $user->status = User::STATUS_ACTIVE;
                        $user->save(false);
                        $model->user_id = $user->id;
                        $model->save(false);
                        // $r = \app\models\Role::findOne($model->role_id);
//                        $auth = \Yii::$app->getAuthManager();
//                        $roles = $auth->getRole($model->role->name);
//                        $auth->assign($roles, $user->id);
                    }

                    $model = new Person();
                    $regForm = new RegistrationForm();
                    $regForm->scenario = 'register';
                    Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, 'เพิ่มเจ้าหน้าที่เรียบร้อยแล้ว');
                    return [
                        'size' => 'large',
                        'forceReload' => '#crud-datatable-person-pjax',
                        'title' => Yii::t('app', "เพิ่มบุคลากร"),
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'regForm' => $regForm,
                        ]),
                        'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                } else {
//                    $model = new Person();
//                    $regForm = new RegistrationForm();
//                    $regForm->scenario = 'register';
                    Yii::$app->session->setFlash(Alert::TYPE_DANGER, 'ข้อมูลไม่ถูกบันทึกกรุณาตรวจสอบข้อมูลให้ถูกต้อง');
                    return [
                        'forceReload' => '#crud-datatable-person-pjax',
                        'size' => 'large',
                        'title' => "เพิ่มบุคลากร",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'regForm' => $regForm,
                        ]),
                        'footer' => Html::button('ปิด', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('บันทึก', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            } else {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "เพิ่มเจ้าหน้าที่"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'regForm' => $regForm,
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save() && $model->validate()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Person model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $regForm = new RegistrationForm();
        if (isset($model->user)) {
            $regForm->username = $model->user->username;
            $regForm->user_id = $model->user_id;
        }
        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " Person #" . $id,
                     'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'regForm' => $regForm,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load(Yii::$app->request->post()) && $regForm->load(Yii::$app->request->post()) && $model->validate()) {
                if (!empty($regForm->username) || !empty($regForm->password) || !empty($regForm->password_repeat)) {

                    if (isset($model->user)) {
                        $user = $model->user;
                    } else {
                        $user = new User();
                    }
                    if (!empty($regForm->username)) {

                        $user->username = $regForm->username;
                        if (!empty($regForm->password)) {
                            $user->setPassword($regForm->password);
                            $user->generateAuthKey();
                        }
                        $user->status = User::STATUS_ACTIVE;
                        $user->save(false);
                        $model->user_id = $user->id;
                        $model->save(false);
                    }
                    return [
                        'forceReload' => '#crud-datatable-person-pjax',
                        'size' => 'large',
                        'title' => "เจ้าหน้าที่ " . $model->fullName,
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
//                            'searchModel' => $searchModel,
//                            'dataProvider' => $dataProvider,
                        ]),
                        'footer' => Html::button('ปิด', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('แก้ไข', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    $model->save(false);
                    return [
                        'forceReload' => '#crud-datatable-person-pjax',
                        'size' => 'large',
                        'title' => "เจ้าหน้าที่ " . $model->fullName,
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
//                            'searchModel' => $searchModel,
//                            'dataProvider' => $dataProvider,
                        ]),
                        'footer' => Html::button('ปิด', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('แก้ไข', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                }
            } else {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " Office #" . $id,
                     'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'regForm' => $regForm,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load(Yii::$app->request->post()) && $regForm->load(Yii::$app->request->post()) && $model->validate()) {
                if (!empty($regForm->username) || !empty($regForm->password) || !empty($regForm->password_repeat)) {
                    if (isset($model->user)) {
                        $user = $model->user;
                    } else {
                        $user = new User();
                    }
                    if (!empty($regForm->username)) {

                        $user->username = $regForm->username;
                        if (!empty($regForm->password)) {
                            $user->setPassword($regForm->password);
                            $user->generateAuthKey();
                        }
                        $user->status = User::STATUS_ACTIVE;
                        $user->save(false);
                        $model->user_id = $user->id;
                        $model->save(false);
                        Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, Yii::t('app', "บันทึกเรียบร้อยแล้ว"));
                    }
                }
                // $model->user_id = $user->id;

                $model->save(false);
                Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, Yii::t('app', "บันทึกเรียบร้อยแล้ว"));

                return $this->render('update', [
                            'model' => $model,
                            'regForm' => $regForm,
//                            'searchModel' => $searchModel,
//                            'dataProvider' => $dataProvider,
                ]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'regForm' => $regForm,
//                            'searchModel' => $searchModel,
//                            'dataProvider' => $dataProvider,
                ]);
            }
        }
    }

    /**
     * Delete an existing Person model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $user = User::find()->status(User::STATUS_ACTIVE)->user($model->user_id)->one();
        $model->deleted = 1;
        $model->save(FALSE);
        if ($user) {
            $user->status = User::STATUS_DELETED;
            $user->save(FALSE);
        }
        if (!$model->save()) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถลบข้อมูลได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#crud-datatable-person-pjax',
                    'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                    'content' => $this->renderAjax('@app/views/widgets/_alert'),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            } else {
                return $this->redirect(['index']);
            }
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-person-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Person model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete() {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-person-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Person model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Person the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Person::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
