<?php

namespace app\controllers;

use Yii;
use app\models\ProjectFiles;
use app\models\ProjectFilesSearch;
use app\rbac\RbacController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use kartik\widgets\Alert;
use yii\web\UploadedFile;
use yii\helpers\Json;

/**
 * ProjectFilesController implements the CRUD actions for ProjectFiles model.
 */
class ProjectFilesController extends RbacController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProjectFiles models.
     * @return mixed
     */
//    public function actionIndex($fileCategoryId) {
//        $searchModel = new ProjectFilesSearch();
//        $searchModel->deleted = 0;
//        $searchModel->file_category_id = $fileCategoryId;
//
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//        ]);
//    }

    public function actionIndex($fileCategoryId) {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $searchModel = new ProjectFilesSearch();
            $searchModel->deleted = 0;
            $searchModel->file_category_id = $fileCategoryId;

            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return [
                'size' => 'large',
                'title' => Yii::t('app', "ข้อมูลเอกสาร"),
                'content' => $this->renderAjax('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
            ];
        } else {
            $searchModel = new ProjectFilesSearch();
            $searchModel->deleted = 0;
            $searchModel->file_category_id = $fileCategoryId;

            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'fileCategoryId' => $fileCategoryId
            ]);
        }
    }

    public function actionShow($fileCategoryId) {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $searchModel = new ProjectFilesSearch();
            $searchModel->deleted = 0;
            $searchModel->file_category_id = $fileCategoryId;

            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return [
                'size' => 'large',
                'title' => Yii::t('app', "ข้อมูลเอกสาร"),
                'content' => $this->renderAjax('show', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
            ];
        } else {
            $searchModel = new ProjectFilesSearch();
            $searchModel->deleted = 0;
            $searchModel->file_category_id = $fileCategoryId;

            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('show', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'fileCategoryId' => $fileCategoryId
            ]);
        }
    }

    /**
     * Displays a single ProjectFiles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            return [
                'title' => Yii::t('app', "ข้อมูลเอกสาร {name}", [
                    'name' => $model->name
                ]),
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('app', "แก้ไข"), ['update', 'id' => $id], ['class' => 'btn btn-primary btn-lg', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new ProjectFiles model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($fileCategoryId, $isDialog = FALSE) {
        $request = Yii::$app->request;
        $model = new ProjectFiles();
        $model->file_category_id = $fileCategoryId;
        $name = \app\models\FileCategory::findOne(['id' => $fileCategoryId]);

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "เพิ่ม ข้อมูลเอกสาร"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
                ];
            } else if ($model->load($request->post())) {
                $files = UploadedFile::getInstances($model, 'file');
                foreach ($files as $file) {
                    $model->file_category_id = $fileCategoryId;
//                    $model->name = uniqid($name->name . '-');
                    $model->file = $file;
                    $model->file->name = uniqid() . '.' . $model->file->extension;
                    $path = 'uploads/files/' . $model->file_category_id;
                    \yii\helpers\FileHelper::createDirectory($path);
                    $model->file->saveAs($path . '/' . $model->file->name);
                    $model->file = $model->file->name;
                    $model->save();
                }
                $model = new ProjectFiles();
                $model->file_category_id = $fileCategoryId;
//                $model->file = UploadedFile::getInstance($model, 'file');
                Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, Yii::t('app', "เพิ่ม ข้อมูลเอกสารเรียบร้อยแล้ว"));

                if ($isDialog) {
                    return [
                        'size' => 'large',
//                        'forceClose' => TRUE,
                        'forceReload' => '#crud-datatable-project-files-pjax',
                        'title' => Yii::t('app', "เพิ่ม ข้อมูลเอกสาร"),
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'fileCategoryId' => $fileCategoryId
                        ]),
                        'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                    ];
                } else {
                    return [
                        'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'บันทึกเรียบร้อยแล้ว') . '</div>',
                        'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                    ];
                }

//                return [
//                    'size' => 'large',
//                    'forceReload' => '#crud-datatable-project-files-pjax',
//                    'title' => Yii::t('app', "เพิ่ม ข้อมูลเอกสาร"),
//                    'content' => $this->renderAjax('create', [
//                        'model' => $model,
//                    ]),
//                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
//                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
//                ];
            } else {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "เพิ่มข้อมูลเอกสาร"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing ProjectFiles model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $files = $model->file;

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "แก้ไข ข้อมูลเอกสาร{name}", [
                        'name' => $model->name
                    ]),
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate(FALSE)) {
                $file = UploadedFile::getInstance($model, 'file');
                if (isset($file)) {
                    $model->file = $file;
                    $model->file->name = uniqid() . '.' . $model->file->extension;
                    $path = 'uploads/content/images/';
                    $model->file->saveAs($path . $model->file->name);
                    $model->file = $model->file->name;
                } else {
                    $model->file = $files;
                }
                $model->save(FALSE);
                return [
                    'size' => 'large',
                    'forceReload' => '#crud-datatable-project-files',
                    'title' => Yii::t('app', "แก้ไข ข้อมูลเอกสาร{name}", [
                        'name' => $model->name
                    ]),
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('app', "บันทึก"), ['update', 'id' => $id], ['class' => 'btn btn-primary btn-lg', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'size' => 'large',
                    'title' => Yii::t('app', "แก้ไขข้อมูลเอกสาร{name}", [
                        'name' => $model->name
                    ]),
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "บันทึก"), ['class' => 'btn btn-primary btn-lg', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing ProjectFiles model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->deleted = TRUE;
        if (!$model->save(FALSE)) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถลบข้อมูลได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#crud-datatable-project-files-pjax',
                    'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                    'content' => $this->renderAjax('@app/views/widgets/_alert'),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            } else {
                return $this->redirect(['index']);
            }
        }
        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-project-files-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing ProjectFiles model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete() {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the ProjectFiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProjectFiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ProjectFiles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
