<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\rbac\RbacController;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use app\models\Setting;
use kartik\alert\Alert;
use yii\helpers\Html;
use app\models\DashboardForm;
use app\models\Role;
use app\models\OfficeType;
use app\models\YearlyAssessment;
use kartik\mpdf\Pdf;

class SiteController extends RbacController {

//    public $enableCsrfValidation = false;
    protected $allowedActions = ['login', 'logout', 'error', 'captcha', 'test-template', 'select-role', 'change-role', 'about', 'main', 'progress'];

    /**
     * {@inheritdoc}
     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout'],
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

    public function beforeAction($action) {
        if ($action->id == 'upload') {
            Yii::$app->request->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndexAdmin() {
        return $this->render('index-admin');
    }

    public function actionIndexStaff() {
        return $this->render('index-staff');
    }

    public function actionIndex() {
        $currentRole = Yii::$app->session->get('currentRole');
        $dashboardSearch = new DashboardForm();
        $dashboardSearch->year = Setting::getValue(Setting::CURRENT_YEAR);
        if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR || $currentRole['role_id'] == Role::REVIEWER) {
            $ot = OfficeType::find()->isDeleted(false)->one();
            $dashboardSearch->officeType = $ot->id;
            $dashboardSearch->level = 2;
        }
        // elseif ($currentRole['role_id'] == Role::STAFF || $currentRole['role_id'] == Role::ADMINISTRATOR) {
        //     $dashboardSearch->year = $year;
        //     $dashboardSearch->load(Yii::$app->request->queryParams);
        // }
        $dashboardSearch->load(Yii::$app->request->queryParams);
        $year = $dashboardSearch->year;
        $months = Yii::$app->util->getYearMonths($year);
        $smf = \app\models\SixMonthForm::find()->isDeleted(false)->office(Yii::$app->user->identity->person->office_id)->year($year)->one();

        return $this->render('@app/views/site/index.php', [
                    'year' => $year,
                    'months' => $months,
                    'smf' => $smf,
                    'currentRole' => $currentRole,
                    'dashboardSearch' => $dashboardSearch,
        ]);

//        return $this->render('index');
    }

    public function actionMonthlyFormStatus($year) {
        $months = Yii::$app->util->getYearMonths($year);
        return $this->render('@app/views/site/month-form-status.php', [
                    'year' => $year,
                    'months' => $months,
        ]);
    }

    public function actionReportSixForm($officeType = NULL, $level = NULL, $year = NULL, $pdf = NULL) {
        // $year = Setting::getValue(Setting::CURRENT_YEAR);
        $dashboardSearch = new DashboardForm();
        $dashboardSearch->year = $year;
        $dashboardSearch->load(Yii::$app->request->queryParams);

        if (isset($pdf)) {
            $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            $dd = date('Y-m-d H:i:s');
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'orientation' => Pdf::ORIENT_LANDSCAPE,
                'content' => $this->renderPartial('report-six-form-pdf', [
                    'year' => $dashboardSearch->year,
                    'officeType' => $officeType,
                    'level' => $level,
                    'dashboardSearch' => $dashboardSearch
                ]),
                'options' => [
                    'fontDir' => array_merge($fontDirs, [
                        Yii::getAlias('@app/web/fonts'),
                    ]),
                    'fontdata' => $fontData + [
                'thsarabun' => [
                    'R' => "THSarabunNew.ttf",
                    'B' => "THSarabunNew-Bold.ttf",
                    'I' => "THSarabunNew-Italic.ttf",
                    'BI' => "THSarabunNew-BoldItalic.ttf"
                ]
                    ],
                ],
                'cssInline' => 'body { font-family: thsarabun !important }',
                'methods' => [
                    'SetTitle' => 'พิมพ์รายงาน',
                    'SetSubject' => 'พิมพ์รายงาน',
                    'SetHeader' => ['พิมพ์รายงาน||พิมพ์เมื่อวันที่: ' . Yii::$app->thaiFormatter->asDateTime($dd, 'php:d-m-Y H:i:s')],
                    'SetFooter' => ['|หน้า {PAGENO}|'],
                ]
            ]);
            $mPdf = $pdf->getApi();
            $mPdf->SetDefaultFont('thsarabun');
            return $pdf->render();
        }
        return $this->render('@app/views/site/report-six-form.php', [
                    'officeType' => $officeType,
                    'level' => $level,
                    'year' => $dashboardSearch->year,
                    'dashboardSearch' => $dashboardSearch
        ]);
    }

    public function actionFormIssueStatus($year) {
        $smf = \app\models\SixMonthForm::find()->isDeleted(false)->office(Yii::$app->user->identity->person->office_id)->last()->year($year)->one();
        if ($year == 2562) {
            $assessIssues = \app\models\AssessIssue::find()->isDeleted(false)->year(2563)->all();
        } else {
            $assessIssues = \app\models\AssessIssue::find()->isDeleted(false)->year($year)->all();
        }
        if (!isset($smf)) {
            $smf = new \app\models\SixMonthForm();
            $smf->year = $year;
            $smf->office_id = Yii::$app->user->identity->person->office_id;
            $smf->save();
        }
        $formIssues = [];
        foreach ($assessIssues as $ai) {
            $fi = \app\models\FormIssue::find()->isDeleted(false)->sixMonth($smf->id)->assessIssue($ai->id)->one();
            if (!isset($fi)) {
                $fi = new \app\models\FormIssue();
                $fi->six_month_form_id = $smf->id;
                $fi->assess_issue_id = $ai->id;
                $fi->save();
            }
            $formIssues[] = $fi;
        }
        return $this->render('@app/views/site/form-issue-status.php', [
                    'assessIssues' => $assessIssues,
                    'formIssues' => $formIssues,
                    'year' => $year,
                    'smf' => $smf,
        ]);
    }

    public function actionMain() {
        $this->layout = 'site';
        $newss = \app\models\Content::find()->isDeleted(FALSE)->contentType(\app\models\Content::NEWS)->limit(8)->orderBy('created_at DESC')->all();
        $measures = \app\models\Content::find()->isDeleted(FALSE)->contentType(\app\models\Content::MEASURE)->limit(8)->orderBy('created_at DESC')->all();
        $acadamics = \app\models\Content::find()->isDeleted(FALSE)->contentType(\app\models\Content::ACADAMIC)->limit(8)->orderBy('created_at DESC')->all();
        $downloads = \app\models\Content::find()->isDeleted(FALSE)->contentType(\app\models\Content::DOWNLOAD)->limit(8)->orderBy('created_at DESC')->all();

        return $this->render('main', [
                    'measures' => $measures,
                    'acadamics' => $acadamics,
                    'newss' => $newss,
                    'downloads' => $downloads
        ]);
    }

    public function actionProgress() {
        $this->layout = 'site';
        $year = Setting::getValue(Setting::CURRENT_YEAR);
        $months = Yii::$app->util->getYearMonths($year);
        $currentRole = Yii::$app->session->get('currentRole');
        $dashboardSearch = new DashboardForm();
        $ot = OfficeType::find()->isDeleted(false)->one();
        $dashboardSearch->officeType = $ot->id;
        $dashboardSearch->level = 1;
        $dashboardSearch->year = Setting::getValue(Setting::CURRENT_YEAR);
        $dashboardSearch->load(Yii::$app->request->queryParams);

        return $this->render('progress', ['dashboardSearch' => $dashboardSearch, 'year' => $year, 'months' => $months
        ]);
    }

    public function actionSelectRole($personRoleId) {
        $personRole = \app\models\PersonRole::findOne($personRoleId);
//        Yii::$app->user->identity->currentRole = $personRole;
        Yii::$app->session->set('currentRole', $personRole->toArrayData());

//        exit;
        return $this->redirect(['site/index']);
    }

    public function actionChangeRole() {
        $this->layout = 'full';
        $roles = Yii::$app->user->identity->person->getAvailableRoles();
        return $this->render('select-role', [
                    'roles' => $roles
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        $this->layout = 'full';

        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->person->deleted == 1) {
                Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', 'ผู้ใช้งานถูกลบกรุณาติดต่อผู้ดูแลระบบ'));
                Yii::$app->user->logout();
                $model = new LoginForm();
                return $this->render('login', [
                            'model' => $model,
                ]);
            }
            if (Yii::$app->user->identity->person->office->deleted == 1) {
                Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', 'หน่วยงานของผู้ใช้งานถูกลบกรุณาติดต่อผู้ดูแลระบบ'));
                Yii::$app->user->logout();
                $model = new LoginForm();
                return $this->render('login', [
                            'model' => $model,
                ]);
            }
            if (!Yii::$app->session->has('currentRole')) {
                $roles = Yii::$app->user->identity->person->getPersonRoles()->isDeleted(FALSE)->all();
                if (count($roles) == 0) {
                    Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', 'ยังไม่กำหนดหน้าที่สำหรับผู้ใช้งาน {user} กรุณาติดต่อผู้ดูแลระบบ', [
                                'user' => Yii::$app->user->identity->username
                    ]));
                    Yii::$app->user->logout();
                    $model = new LoginForm();
                    return $this->render('login', [
                                'model' => $model,
                    ]);
                }
                return $this->render('select-role', [
                            'roles' => $roles
                ]);
            }
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->identity->person->deleted == 1) {
                Yii::$app->user->logout();
                Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', 'ผู้ใช้งานถูกลบกรุณาติดต่อผู้ดูแลระบบ'));
                $model = new LoginForm();
                return $this->render('login', [
                            'model' => $model,
                ]);
            }
            if (Yii::$app->user->identity->person->office->deleted == 1) {
                Yii::$app->user->logout();
                Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', 'หน่วยงานของผู้ใช้งานถูกลบกรุณาติดต่อผู้ดูแลระบบ'));
                $model = new LoginForm();
                return $this->render('login', [
                            'model' => $model,
                ]);
            }
            $roles = Yii::$app->user->identity->getPerson()->one()->getPersonRoles()->isDeleted(FALSE)->all();
            if (count($roles) > 1) {
//                \yii\helpers\VarDumper::dump($roles);
//                exit;
                return $this->render('select-role', [
                            'roles' => $roles
                ]);
            } else if (count($roles) == 0) {
                Yii::$app->user->logout();
                Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', 'ยังไม่กำหนดหน้าที่สำหรับผู้ใช้งาน {user} กรุณาติดต่อผู้ดูแลระบบ', [
                            'user' => $model->username
                ]));
                $model = new LoginForm();
                return $this->render('login', [
                            'model' => $model,
                ]);
            } else {
                Yii::$app->session->set('currentRole', $roles[0]->toArrayData());
                return $this->redirect(['site/index']);
//                \yii\helpers\VarDumper::dump($roles);
//                \yii\helpers\VarDumper::dump(Yii::$app->user->identity, 10, TRUE);
//                exit;
            }

            return $this->goBack();
        }

//        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['location' => Yii::$app->getHomeUrl()];
        } else {
            return $this->goHome();
        }
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    public function actionMasterList() {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['site/main']);
        }

        $menus = [
            'general' => [
                'label' => 'ข้อมูลทั่วไป',
                'items' => [
                    ['label' => 'ตั้งค่าระบบ', 'url' => ['setting/index'], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'จังหวัด', 'url' => ['province/index'], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'อำเภอ', 'url' => ['amphur/index'], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'ตำบล ', 'url' => ['tambon/index'], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'หน่วยงาน', 'url' => ['office/index'], 'visible' => Yii::$app->user->can('core.index')],
//                    ['label' => 'ประเภทขยะ', 'url' => ['population/index'], 'visible' => Yii::$app->user->can('core.index')],
                ],
            ],
            'user' => [
                'label' => 'การจัดการผู้ใช้งาน',
                'items' => [
                    ['label' => 'ข้อมูลผู้ใช้งาน', 'url' => ['person/index'], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'กำหนดข้อมูล Super Administrator', 'url' => ['person-role/select-person', 'id' => \app\models\Role::SUPER_ADMINISTRATOR], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'กำหนดข้อมูล Administrator', 'url' => ['person-role/select-person', 'id' => \app\models\Role::ADMINISTRATOR], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'กำหนดข้อมูล Staff ', 'url' => ['person-role/select-person', 'id' => \app\models\Role::STAFF], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'กำหนดข้อมูล Reviewer ', 'url' => ['person-role/select-person', 'id' => \app\models\Role::REVIEWER], 'visible' => Yii::$app->user->can('core.index')],
                ],
            ],
        ];
        return $this->render('master-list', ['menus' => $menus]);
    }

    public function actionPerson() {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['site/main']);
        }

        $menus = [
            'user' => [
                'label' => 'การจัดการผู้ใช้งาน',
                'items' => [
                    ['label' => 'ข้อมูลหน่วยงาน', 'url' => ['office/index-admin'], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'ข้อมูลผู้ใช้งาน', 'url' => ['person/index-admin'], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'กำหนดข้อมูล Administrator', 'url' => ['person-role/select-person', 'id' => \app\models\Role::ADMINISTRATOR], 'visible' => Yii::$app->user->can('core.index')],
                    ['label' => 'กำหนดข้อมูล Staff ', 'url' => ['person-role/select-person', 'id' => \app\models\Role::STAFF], 'visible' => Yii::$app->user->can('core.index')],
                ],
            ],
        ];
        return $this->render('person', ['menus' => $menus]);
    }

    public function actionUpload() {
        $uploadedFile = UploadedFile::getInstanceByName('upload');
        $mime = \yii\helpers\FileHelper::getMimeType($uploadedFile->tempName);
        $file = time() . "_" . $uploadedFile->name;

//        $user_id = Yii::$app->user->getId();

        $url = Yii::$app->urlManager->createAbsoluteUrl('/web/uploads/editor/' . $file);
        $uploadPath = Yii::getAlias('@app/web/uploads/editor/') . $file;
        FileHelper::createDirectory(Yii::getAlias('@app/web/uploads/editor/'));
//        if (!is_dir(Yii::getAlias('@app/web/uploads/editor/'))) { //ถ้ายังไม่มี folder ให้สร้าง folder ตาม user id
//            mkdir(Yii::getAlias('@app/web/uploads/editor/'));
//        }
        //ตรวจสอบ
        if ($uploadedFile == null) {
            $message = "ไม่มีไฟล์ที่ Upload";
        } else if ($uploadedFile->size == 0) {
            $message = "ไฟล์มีขนาด 0";
        } else if ($uploadedFile->tempName == null) {
            $message = "มีข้อผิดพลาด";
        } else {
            $message = "";
            $move = $uploadedFile->saveAs($uploadPath);
            if (!$move) {
                $message = "ไม่สามารถนำไฟล์ไปไว้ใน Folder ได้กรุณาตรวจสอบ Permission Read/Write/Modify";
            }
        }
        $funcNum = $_GET['CKEditorFuncNum'];
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionYearlyAssessment($year, $pdf = null) {
        $request = Yii::$app->request;
        $year = $year;
//        $year = Setting::getValue(Setting::CURRENT_YEAR);
        $dashboardSearch = new DashboardForm();
        $dashboardSearch->load(Yii::$app->request->queryParams);
        $ya = Yii::$app->user->identity->person->getYearlyAssessment($year, $dashboardSearch->officeType, $dashboardSearch->level);
        $office = null;
        if (empty($dashboardSearch->officeType)) {
            $office = Yii::$app->user->identity->person->office;
        }
        if (isset($pdf)) {
            $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
            $fontDirs = $defaultConfig['fontDir'];

            $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
            $fontData = $defaultFontConfig['fontdata'];

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            $dd = date('Y-m-d H:i:s');
            $pdf = new Pdf([
                'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'orientation' => Pdf::ORIENT_LANDSCAPE,
                'content' => $this->renderPartial('yearly-assessment', [
                    'year' => $year,
                    'ya' => $ya,
                    'office' => $office,
                ]),
                'options' => [
                    'fontDir' => array_merge($fontDirs, [
                        Yii::getAlias('@app/web/fonts'),
                    ]),
                    'fontdata' => $fontData + [
                'thsarabun' => [
                    'R' => "THSarabunNew.ttf",
                    'B' => "THSarabunNew-Bold.ttf",
                    'I' => "THSarabunNew-Italic.ttf",
                    'BI' => "THSarabunNew-BoldItalic.ttf"
                ]
                    ],
                ],
                'cssInline' => 'body { font-family: thsarabun !important }',
                'methods' => [
                    'SetTitle' => 'พิมพ์รายงาน',
                    'SetSubject' => 'พิมพ์รายงาน',
                    'SetHeader' => ['พิมพ์รายงาน||พิมพ์เมื่อวันที่: ' . Yii::$app->thaiFormatter->asDateTime($dd, 'php:d-m-Y H:i:s')],
                    'SetFooter' => ['|หน้า {PAGENO}|'],
                ]
            ]);
            $mPdf = $pdf->getApi();
            $mPdf->SetDefaultFont('thsarabun');
            return $pdf->render();
        }


        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "ผลการดำเนินการ ปีงบประมาณ {$year}",
                'content' => $this->renderAjax('_yearly-assessment', [
                    'year' => $year,
                    'ya' => $ya,
                    'office' => $office,
                ]),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('app', "EXPORT PDF"), ['site/yearly-assessment', 'DashboardForm[officeType]' => $dashboardSearch->officeType, 'DashboardForm[level]' => $dashboardSearch->level, 'year' => $year, 'pdf' => true], ['class' => 'btn btn-default pull-right btn-lg', 'type' => "submit", 'target' => '_blank'])
            ];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->render('yearly-assessment', [
                        'year' => $year,
                        'ya' => $ya,
                        'office' => $office,
            ]);
        }
    }

}
