<?php

namespace app\controllers;

use Yii;
use app\models\SixMonthForm;
use app\models\SixMonthFormSearch;
//use app\rbac\RbacController;
use app\rbac\RbacController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use kartik\widgets\Alert;
use app\models\Setting;
use app\models\CalculationQueue;
use app\models\FormIssue;
use app\models\FormIssueFile;

/**
 * SixMonthFormController implements the CRUD actions for SixMonthForm model.
 */
class SixMonthFormController extends RbacController {

    /**
     * @inheritdoc
     */
    protected $allowedActions = ['report', 'confirm', 'check', 'check-list', 'check-list-confirm', 'check-list-checked'];

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SixMonthForm models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SixMonthFormSearch();
        $searchModel->deleted = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheckList($list = NULL) {
        $currentRole = Yii::$app->session->get('currentRole');
        $searchModel = new SixMonthFormSearch();
        $searchModel->deleted = 0;
        if (isset($list)) {
            $searchModel->isLastUncheck = true;
            $searchModel->isCheckChildrenByRole = true;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('check-list', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheckListChecked($isPass = NULL) {
        $currentRole = Yii::$app->session->get('currentRole');
        $searchModel = new SixMonthFormSearch();
        $searchModel->deleted = 0;
        $searchModel->isConfirmed = true;
        $searchModel->isCheckChildrenByRole = true;

        $searchModel->is_pass = $isPass;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('check-list-checked', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheckListConfirm($list = NULL) {
        $currentRole = Yii::$app->session->get('currentRole');
        $searchModel = new SixMonthFormSearch();
        $searchModel->deleted = 0;
        $searchModel->isConfirmed = true;
        $searchModel->isCheckChildrenByRole = true;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('check-list-confirm', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SixMonthForm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "SixMonthForm #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                Html::a(Yii::t('app', 'แก้ไข'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new SixMonthForm model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $request = Yii::$app->request;
        $model = new SixMonthForm();

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "เพิ่ม SixMonthForm"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                $model = new SixMonthForm();
                Yii::$app->session->setFlash(Alert::TYPE_SUCCESS, Yii::t('app', 'เพิ่มข้อมูล six-month-form เรียบร้อยแล้ว'));
                return [
                    'forceReload' => '#crud-datatable-six-month-form-pjax',
                    'title' => Yii::t('app', "เพิ่ม SixMonthForm"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "เพิ่ม SixMonthForm"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing SixMonthForm model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " SixMonthForm #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-six-month-form-pjax',
                    'title' => Yii::t('app', "แก้ไข") . " SixMonthForm #" . $id,
                    'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'แก้ไขข้อมูล six-month-form เรียบร้อยแล้ว') . '</div>',
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a(Yii::t('app', 'แก้ไข'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "แก้ไข") . " SixMonthForm #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
             *   Process for non-ajax request
             */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing SixMonthForm model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->deleted = 1;
        if (!$model->save()) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถลบข้อมูลได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#crud-datatable-six-month-form-pjax',
                    'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                    'content' => $this->renderAjax('@app/views/widgets/_alert'),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            } else {
                return $this->redirect(['index']);
            }
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-six-month-form-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    public function actionReport($id) {
        $request = Yii::$app->request;
        if (!$request->isAjax) {
            return;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

//        if ($model->getUnsubmittedFormIssues()->count() > 0) {
//
//            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', 'กรุณาตรวจสอบการบันทึกข้อมูลให้ครบทุกข้อ'));
//            return [
//                'forceReload' => '#form-issue-status-pjax',
//                'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
//                'content' => $this->renderAjax('@app/views/widgets/_alert'),
//                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
//            ];
//        }

        $model->submitted_by = \Yii::$app->user->id;
        $model->submitted_at = date('Y-m-d H:i:s');
        FormIssue::updateAll(['checked_by' => null, 'checked_at' => null], ['six_month_form_id' => $model->id]);
        FormIssue::updateAll(['submitted_by' => \Yii::$app->user->id, 'submitted_at' => date('Y-m-d H:i:s')], ['six_month_form_id' => $model->id, 'submitted_at' => null]);

        if (!$model->save()) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถส่งรายงานข้อมูลได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload' => '#form-issue-status-pjax',
                'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                'content' => $this->renderAjax('@app/views/widgets/_alert'),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
            ];
        }
        \app\models\Alert::addSixMonthConfirmAlert($model);

        return [
            'forceReload' => '#form-issue-status-pjax',
            'title' => Yii::t('app', 'ส่งรายงาน 6 เดือน'),
            'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'ส่งรายงาน 6 เดือนเรียบร้อยแล้ว') . '</div>',
            'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
        ];
    }

    public function actionResubmit($id) {
        $request = Yii::$app->request;
        if (!$request->isAjax) {
            return;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $newSubmit = new SixMonthForm();
        $newSubmit->attributes = $model->attributes;
        $newSubmit->submitted_by = null;
        $newSubmit->submitted_at = null;
        $newSubmit->confirmed_by = null;
        $newSubmit->confirmed_at = null;
        $newSubmit->checked_by = null;
        $newSubmit->checked_at = null;
        $newSubmit->is_pass = 0;
        $newSubmit->created_at = null;
        $newSubmit->updated_at = null;
        $newSubmit->is_late = 0;
        $newSubmit->save();
        $formIssues = $model->getFormIssues()->isDeleted(false)->all();
        foreach ($formIssues as $formIssue) {
            $newFi = new FormIssue();
            $newFi->attributes = $formIssue->attributes;
            $newFi->six_month_form_id = $newSubmit->id;
            $newFi->created_at = null;
            $newFi->updated_at = null;
            $newFi->submitted_by = Yii::$app->user->id;
            $newFi->submitted_at = date('Y-m-d H:i:s');
            $newFi->confirmed_by = null;
            $newFi->confirmed_at = null;
//            $newFi->checked_by = null;
//            $newFi->checked_at = null;
            $newFi->save();
            $files = $formIssue->getFormIssueFiles()->isDeleted(false)->all();
            foreach ($files as $file) {
                $newFif = new FormIssueFile();
                $newFif->attributes = $file->attributes;
                $newFif->form_issue_id = $newFi->id;
                $newFif->created_at = null;
                $newFif->updated_at = null;
                $newFif->save();
            }
        }

        return [
            'forceClose' => true,
            'forceReload' => '#form-issue-status-pjax',
        ];
    }

    public function actionConfirm($id) {
        $request = Yii::$app->request;
        if (!$request->isAjax) {
            return;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $formIssues = $model->getFormIssues()->isDeleted(false)->all();
        foreach ($formIssues as $formIssue) {
            $formIssue->confirmed_by = \Yii::$app->user->id;
            $formIssue->confirmed_at = date('Y-m-d H:i:s');
            $formIssue->save(false);
        }
        $model->confirmed_by = \Yii::$app->user->id;
        $model->confirmed_at = date('Y-m-d H:i:s');
        $firstMonth = Setting::getValue(Setting::FIRST_MONTH);
        $alertDay = Setting::getValue(Setting::SIX_MONTH_COMPLETE_ALERT_DAY_MONTH);
        $alertDayParts = explode('/', $alertDay);
        $year = $model->year - 543;
        if ($firstMonth > 1 && $alertDayParts[1] >= $firstMonth) {
            $year--;
        }
        $alertDate = new \DateTime("{$year}-{$alertDayParts[1]}-{$alertDayParts[0]}");
        $now = new \DateTime();
        if ($now > $alertDate) {
            $model->is_late = 1;
        }

        if (!$model->save()) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถยืนยันข้อมูลรายงาน 6 เดือนได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload' => '#form-issue-status-pjax',
                'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                'content' => $this->renderAjax('@app/views/widgets/_alert'),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
            ];
        }
        CalculationQueue::addQueue(CalculationQueue::TYPE_PARENT_SIX_MONTH_FORM, $model->office_id, $model->year);
        CalculationQueue::runCalculateCmd();
        \app\models\Alert::addSixMonthCheckAlert($model);

        return [
            'forceReload' => '#form-issue-status-pjax',
            'title' => Yii::t('app', 'ยืนยันข้อมูลรายงาน 6 เดือน'),
            'content' => '<div class="alert alert-success dark">' . Yii::t('app', 'ยืนยันข้อมูลรายงาน 6 เดือนเรียบร้อยแล้ว') . '</div>',
            'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
        ];
    }

    public function actionCheck($id) {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->scenario = SixMonthForm::SCENARIO_CHECK;
        if ($model->getUncheckedFormIssues()->count() > 0) {

            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', 'กรุณาตรวจสอบข้อมูลให้ครบทุกข้อ'));
            return [
//                'forceReload' => '#form-issue-status-pjax',
                'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                'content' => $this->renderAjax('@app/views/widgets/_alert'),
                'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
            ];
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงาน 6 เดือน"),
                    'content' => $this->renderAjax('check', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->validate()) {
                $model->checked_by = \Yii::$app->user->id;
                $model->checked_at = date('Y-m-d H:i:s');
                $model->updateTotalScore();
                $model->save();
                if (!$model->is_pass) {
                    \app\models\Alert::addSixMonthFailAlert($model);
                }

                return [
                    'forceReload' => '#crud-datatable-six-month-form-pjax',
                    'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงาน 6 เดือน"),
                    'content' => '<div class="alert alert-success dark">' . Yii::t('app', "ตรวจสอบข้อมูลรายงาน 6 เดือนเรียบร้อยแล้ว") . '</div>',
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            } else {
                return [
                    'title' => Yii::t('app', "ตรวจสอบข้อมูลรายงาน 6 เดือน"),
                    'content' => $this->renderAjax('check', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', 'ปิด'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        }
    }

    public function actionCheckSuperAdmin($id) {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->checked_by = \Yii::$app->user->id;
        $model->checked_at = date('Y-m-d H:i:s');
        if (!$model->save()) {
            Yii::$app->session->setFlash(Alert::TYPE_DANGER, Yii::t('app', "ไม่สามารถส่งรายงานข้อมูลได้ {error}", [
                        'error' => \Yii::$app->util->errorSummary($model),
            ]));
            if ($request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'forceReload' => '#form-issue-status-pjax',
                    'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                    'content' => $this->renderAjax('@app/views/widgets/_alert'),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            } else {
                return [
                    'forceReload' => '#form-issue-status-pjax',
                    'title' => Yii::t('app', 'เกิดข้อผิดพลาด'),
                    'content' => $this->renderAjax('@app/views/widgets/_alert'),
                    'footer' => Html::button(Yii::t('app', "ปิด"), ['class' => 'btn btn-default pull-left btn-lg', 'data-dismiss' => "modal"])
                ];
            }
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#form-issue-status-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing SixMonthForm model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete() {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             *   Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-six-month-form-pjax'];
        } else {
            /*
             *   Process for non-ajax request
             */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the SixMonthForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SixMonthForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = SixMonthForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
