<?php

use yii\db\Migration;

/**
 * Class m190807_055931_init_tables_user
 */
class m190807_055931_init_tables_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string(),
            'email' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
        $this->createIndex('idx_user_username', 'user', ['username']);
        $this->createIndex('idx_user_password_hash', 'user', ['password_hash']);
        $this->createIndex('idx_user_password_reset_token', 'user', ['password_reset_token']);
        $this->createIndex('idx_user_password_email', 'user', ['email']);
        $this->createIndex('idx_user_password_status', 'user', ['status']);

        $this->createTable('role', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('role', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('role', 'name', 'ชื่อหน้าที่');
        $this->addCommentOnColumn('role', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('role', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('role', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('role', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('role', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_role_name', 'role', ['name']);
        $this->addForeignKey('fk_role_user_created_by', 'role', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_role_user_updated_by', 'role', 'updated_by', 'user', 'id');


    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        $this->dropTable('role');
        $this->dropTable('user');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190807_055931_init_tables_user cannot be reverted.\n";

        return false;
    }
    */
}
