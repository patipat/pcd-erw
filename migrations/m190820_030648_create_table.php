<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%}}`.
 */
class m190820_030648_create_table extends Migration {

    public function safeUp() {

        $this->createTable('region', [
            'id' => $this->integer()->notNull()->unique() . " PRIMARY KEY",
            'name' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('region', 'id', 'รหัสภาค');
        $this->addCommentOnColumn('region', 'name', 'ชื่อภาค');
        $this->addCommentOnColumn('region', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('region', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('region', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('region', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('region', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_region_name', 'region', ['name']);
        $this->addForeignKey('fk_region_created_by', 'region', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_region_updated_by', 'region', 'updated_by', 'user', 'id');

        $this->createTable('province', [
            'id' => $this->integer()->notNull()->unique() . " PRIMARY KEY",
            'region_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'name_eng' => $this->string(),
            'short_name' => $this->string(),
            'short_name_eng' => $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('province', 'id', 'รหัสจังหวัด');
        $this->addCommentOnColumn('province', 'name', 'ชื่อจังหวัด');
        $this->addCommentOnColumn('province', 'name_eng', 'ชื่อจังหวัดภาษาอังกฤษ');
        $this->addCommentOnColumn('province', 'short_name', 'ชื่อย่อจังหวัด');
        $this->addCommentOnColumn('province', 'short_name_eng', 'ชื่อย่อจังหวัดภาษาอังกฤษ');
        $this->addCommentOnColumn('province', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('province', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('province', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('province', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('province', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_province_name', 'province', ['name']);
        $this->createIndex('idx_province_name_eng', 'province', ['name_eng']);
        $this->createIndex('idx_province_short_name', 'province', ['short_name']);
        $this->createIndex('idx_province_short_name_eng', 'province', ['short_name_eng']);
        $this->addForeignKey('fk_province_region_id', 'province', 'region_id', 'region', 'id');
        $this->addForeignKey('fk_province_created_by', 'province', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_province_updated_by', 'province', 'updated_by', 'user', 'id');

        $this->createTable('amphur', [
            'id' => $this->integer()->notNull()->unique() . " PRIMARY KEY",
            'province_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'name_eng' => $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('amphur', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('amphur', 'province_id', 'จังหวัด');
        $this->addCommentOnColumn('amphur', 'name', 'ชื่ออำเภอ');
        $this->addCommentOnColumn('amphur', 'name_eng', 'ชื่ออำเภอภาษาอังกฤษ');
        $this->addCommentOnColumn('amphur', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('amphur', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('amphur', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('amphur', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('amphur', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_amphur_name', 'amphur', ['name']);
        $this->createIndex('idx_amphur_name_eng', 'amphur', ['name_eng']);
        $this->addForeignKey('fk_amphur_province_id', 'amphur', 'province_id', 'province', 'id');
        $this->addForeignKey('fk_amphur_created_by', 'amphur', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_amphur_updated_by', 'amphur', 'updated_by', 'user', 'id');

        $this->createTable('tambon', [
            'id' => $this->integer()->notNull()->unique() . " PRIMARY KEY",
            'province_id' => $this->integer(),
            'amphur_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'name_eng' => $this->string(),
            'postcode' => $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('tambon', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('tambon', 'province_id', 'จังหวัด');
        $this->addCommentOnColumn('tambon', 'amphur_id', 'อำเภอ');
        $this->addCommentOnColumn('tambon', 'name', 'ชื่อตำบล');
        $this->addCommentOnColumn('tambon', 'name_eng', 'ชื่อตำบลภาษาอังกฤษ');
        $this->addCommentOnColumn('tambon', 'postcode', 'รหัสไปรษณีย์');
        $this->addCommentOnColumn('tambon', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('tambon', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('tambon', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('tambon', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('tambon', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_tambon_name', 'tambon', ['name']);
        $this->createIndex('idx_tambon_name_eng', 'tambon', ['name_eng']);
        $this->createIndex('idx_tambon_postcode', 'tambon', ['postcode']);
        $this->addForeignKey('fk_tambon_province_id', 'tambon', 'province_id', 'province', 'id');
        $this->addForeignKey('fk_tambon_amphur_id', 'tambon', 'amphur_id', 'amphur', 'id');
        $this->addForeignKey('fk_tambon_created_by', 'tambon', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_tambon_updated_by', 'tambon', 'updated_by', 'user', 'id');

        $this->createTable('file_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('file_category', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('file_category', 'name', 'กลุ่มไฟล์');
        $this->addCommentOnColumn('file_category', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('file_category', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('file_category', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('file_category', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('file_category', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_file_category_name', 'file_category', ['name']);
        $this->addForeignKey('fk_file_category_created_by', 'file_category', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_file_category_updated_by', 'file_category', 'updated_by', 'user', 'id');

        $this->createTable('content_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('content_category', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('content_category', 'name', 'ประเภทบทความ');
        $this->addCommentOnColumn('content_category', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('content_category', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('content_category', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('content_category', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('content_category', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_content_category_name', 'content_category', ['name']);
        $this->addForeignKey('fk_content_category_created_by', 'content_category', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_content_category_updated_by', 'content_category', 'updated_by', 'user', 'id');

        $this->createTable('content_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('content_group', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('content_group', 'name', 'กลุ่มบทความ');
        $this->addCommentOnColumn('content_group', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('content_group', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('content_group', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('content_group', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('content_group', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_content_group_name', 'content_group', ['name']);
        $this->addForeignKey('fk_content_group_created_by', 'content_group', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_content_group_updated_by', 'content_group', 'updated_by', 'user', 'id');

        $this->createTable('content', [
            'id' => $this->primaryKey(),
            'content_group_id' => $this->integer(),
            'content_category_id' => $this->integer(),
            'title' => $this->string()->notNull(),
            'intro_text' => $this->text(),
            'main_text' => $this->text(),
            'image' => $this->string(),
            'status' => $this->integer(),
            'file_name' => $this->string(),
            'view' => $this->integer(),
            'public' => $this->boolean()->notNull()->defaultValue(FALSE),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('content', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('content', 'content_group_id', 'กลุ่มบทความ');
        $this->addCommentOnColumn('content', 'content_category_id', 'ประเภทบทความ');
        $this->addCommentOnColumn('content', 'title', 'ชื่อเรื่อง');
        $this->addCommentOnColumn('content', 'intro_text', 'ข้อความเริ่มต้น');
        $this->addCommentOnColumn('content', 'main_text', 'ข้อความหลัก');
        $this->addCommentOnColumn('content', 'image', 'รูปภาพโชว์หน้าแรก');
        $this->addCommentOnColumn('content', 'status', 'สถานะข่าว');
        $this->addCommentOnColumn('content', 'file_name', 'ไฟล์แนบ');
        $this->addCommentOnColumn('content', 'view', 'ตำแหน่งที่ให้แสดงในกรณีที่เป็นข่าว');
        $this->addCommentOnColumn('content', 'public', 'สถานะข่าว');
        $this->addCommentOnColumn('content', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('content', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('content', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('content', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('content', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_content_title', 'content', ['title']);
        $this->createIndex('idx_content_status', 'content', ['status']);
        $this->createIndex('idx_content_public', 'content', ['public']);
        $this->addForeignKey('fk_content_content_group', 'content', 'content_group_id', 'content_group', 'id');
        $this->addForeignKey('fk_content_content_category', 'content', 'content_category_id', 'content_category', 'id');
        $this->addForeignKey('fk_content_created_by', 'content', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_content_updated_by', 'content', 'updated_by', 'user', 'id');



        $this->createTable('web_link', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'name' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'status' => $this->integer(),
            'banner' => $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('web_link', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('web_link', 'name', 'ชื่อเว็บลิงค์');
        $this->addCommentOnColumn('web_link', 'type', 'ประเภทเว็บลิงค์');
        $this->addCommentOnColumn('web_link', 'url', 'url');
        $this->addCommentOnColumn('web_link', 'banner', 'banner');
        $this->addCommentOnColumn('web_link', 'status', 'status');
        $this->addCommentOnColumn('web_link', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('web_link', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('web_link', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('web_link', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('web_link', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_web_link_type', 'web_link', ['type']);
        $this->createIndex('idx_web_link_name', 'web_link', ['name']);
        $this->createIndex('idx_web_link_status', 'web_link', ['status']);
        $this->addForeignKey('fk_web_link_created_by', 'web_link', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_web_link_updated_by', 'web_link', 'updated_by', 'user', 'id');

        $this->createTable('project_files', [
            'id' => $this->primaryKey(),
            'file_category_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'status' => $this->integer(),
            'file' => $this->string(),
            'public' => $this->boolean()->notNull()->defaultValue(FALSE),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('project_files', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('project_files', 'file_category_id', 'กลุ่มเอกสาร');
        $this->addCommentOnColumn('project_files', 'name', 'ชื่อเอกสาร');
        $this->addCommentOnColumn('project_files', 'description', 'รายละเอียด');
        $this->addCommentOnColumn('project_files', 'status', 'สถานะ');
        $this->addCommentOnColumn('project_files', 'file', 'ไฟล์แนบ');
        $this->addCommentOnColumn('project_files', 'public', 'เปิดเผยแพร่');
        $this->addCommentOnColumn('project_files', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('project_files', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('project_files', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('project_files', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('project_files', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_project_files_name', 'project_files', ['name']);
        $this->createIndex('idx_project_files_status', 'project_files', ['status']);
        $this->createIndex('idx_project_files_public', 'project_files', ['public']);
        $this->addForeignKey('fk_project_files_file_category_id', 'project_files', 'file_category_id', 'file_category', 'id');
        $this->addForeignKey('fk_project_files_created_by', 'project_files', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_project_files_updated_by', 'project_files', 'updated_by', 'user', 'id');

        $this->createTable('title', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('title', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('title', 'name', 'ชื่อคำนำหน้าชื่อ');
        $this->addCommentOnColumn('title', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('title', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('title', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('title', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('title', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_title_name', 'title', ['name']);
        $this->addForeignKey('fk_title_user_created_by', 'title', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_title_user_updated_by', 'title', 'updated_by', 'user', 'id');

        $this->createTable('office_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('office_type', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('office_type', 'name', 'ประเภทหน่วยงาน');
        $this->addCommentOnColumn('office_type', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('office_type', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('office_type', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('office_type', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('office_type', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_office_type_name', 'office_type', ['name']);
        $this->addForeignKey('fk_office_type_user_created_by', 'office_type', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_office_type_user_updated_by', 'office_type', 'updated_by', 'user', 'id');

        $this->createTable('office', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'address' => $this->text(),
            'email' => $this->string(),
            'tel' => $this->string(),
            'line_token' => $this->string(),
            'parent_id' => $this->integer(),
            'office_type_id' => $this->integer(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('office', 'id', 'รหัส');
        $this->addCommentOnColumn('office', 'name', 'สำนักงาน');
        $this->addCommentOnColumn('office', 'address', 'ที่อยู่');
        $this->addCommentOnColumn('office', 'email', 'อีเมลล์');
        $this->addCommentOnColumn('office', 'tel', 'เบอร์โทรศัพท์');
        $this->addCommentOnColumn('office', 'line_token', 'Line token');
        $this->addCommentOnColumn('office', 'parent_id', 'parent');
        $this->addCommentOnColumn('office', 'office_type_id', 'ประเภทหน่วยงาน');
        $this->addCommentOnColumn('office', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('office', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('office', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('office', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('office', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_office_name', 'office', ['name']);
        $this->createIndex('idx_office_email', 'office', ['email']);
        $this->createIndex('idx_office_line_token', 'office', ['line_token']);
        $this->addForeignKey('fk_office_office_type_id', 'office', 'office_type_id', 'office_type', 'id');
        $this->addForeignKey('fk_office_parent_id', 'office', 'parent_id', 'office', 'id');
        $this->addForeignKey('fk_office_created_by', 'office', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_office_updated_by', 'office', 'updated_by', 'user', 'id');

        $this->createTable('person', [
            'id' => $this->primaryKey(),
            'idcard_no' => $this->string(),
            'office_id' => $this->integer(),
            'title_id' => $this->integer(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'user_id' => $this->integer(),
            'email' => $this->string(),
            'tel' => $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('person', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('person', 'idcard_no', 'เลขบัตรประชาขน');
        $this->addCommentOnColumn('person', 'title_id', 'รหัสคำนำหน้าชื่อ');
        $this->addCommentOnColumn('person', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('person', 'first_name', 'ชื่อ');
        $this->addCommentOnColumn('person', 'last_name', 'นามสกุล');
        $this->addCommentOnColumn('person', 'email', 'อีเมลล์');
        $this->addCommentOnColumn('person', 'tel', 'เบอร์โทรศัพท์');
        $this->addCommentOnColumn('person', 'user_id', 'รหัสผู้ใช้');
        $this->addCommentOnColumn('person', 'deleted', '0=ไม่ลบ,1=ลบ');
        $this->addCommentOnColumn('person', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('person', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('person', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('person', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_person_idcard_no', 'person', ['idcard_no']);
        $this->createIndex('idx_person_first_name', 'person', ['first_name']);
        $this->createIndex('idx_person_last_name', 'person', ['last_name']);
        $this->createIndex('idx_person_email', 'person', ['email']);
        $this->addForeignKey('fk_person_title_title_id', 'person', 'title_id', 'title', 'id');
        $this->addForeignKey('fk_person_office_id', 'person', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_person_created_by', 'person', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_person_updated_by', 'person', 'updated_by', 'user', 'id');
        $this->addForeignKey('fk_person_user_id', 'person', 'user_id', 'user', 'id');

        $this->createTable('person_role', [
            'id' => $this->primaryKey(),
            'role_id' => $this->integer(),
            'person_id' => $this->integer(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('person_role', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('person_role', 'role_id', 'หน้าที่');
        $this->addCommentOnColumn('person_role', 'person_id', 'เจ้าหน้าที่');
        $this->addCommentOnColumn('person_role', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('person_role', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('person_role', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('person_role', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('person_role', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->addForeignKey('fk_person_role_role_id', 'person_role', 'role_id', 'role', 'id');
        $this->addForeignKey('fk_person_role_person_id', 'person_role', 'person_id', 'person', 'id');
        $this->addForeignKey('fk_person_role_created_by', 'person_role', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_person_role_updated_by', 'person_role', 'updated_by', 'user', 'id');

        $this->createTable('assessment_score', [
            'id' => $this->primaryKey(),
            'field' => $this->string()->notNull(),
            'min_percent' => $this->decimal(18, 2),
            'max_percent' => $this->decimal(18, 2),
            'score' => $this->decimal(18, 2),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('assessment_score', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('assessment_score', 'field', 'field');
        $this->addCommentOnColumn('assessment_score', 'min_percent', 'min');
        $this->addCommentOnColumn('assessment_score', 'max_percent', 'max');
        $this->addCommentOnColumn('assessment_score', 'score', 'score');
        $this->addCommentOnColumn('assessment_score', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('assessment_score', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('assessment_score', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('assessment_score', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('assessment_score', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_assessment_score_field', 'assessment_score', ['field']);
        $this->addForeignKey('fk_assessment_score_created_by', 'assessment_score', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_assessment_score_updated_by', 'assessment_score', 'updated_by', 'user', 'id');

        $this->createTable('source_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('source_type', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('source_type', 'name', 'ประเภทการเก็บข้อมูล (ร้านค้า,ถังขยะ)');
        $this->addCommentOnColumn('source_type', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('source_type', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('source_type', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('source_type', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('source_type', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_source_type_name', 'source_type', ['name']);
        $this->addForeignKey('fk_source_type_user_created_by', 'source_type', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_source_type_user_updated_by', 'source_type', 'updated_by', 'user', 'id');

        $this->createTable('yearly_assessment', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'year' => $this->integer(),
            'garbage_prev' => $this->decimal(18, 2),
            'garbage_cur' => $this->decimal(18, 2),
            'bag_prev' => $this->decimal(18, 2),
            'bag_cur' => $this->decimal(18, 2),
            'cup_prev' => $this->decimal(18, 2),
            'cup_cur' => $this->decimal(18, 2),
            'foam_prev' => $this->decimal(18, 2),
            'foam_cur' => $this->decimal(18, 2),
            'garbage_percent' => $this->decimal(18, 2),
            'bag_percent' => $this->decimal(18, 2),
            'cup_percent' => $this->decimal(18, 2),
            'foam_percent' => $this->decimal(18, 2),
            'garbage_score' => $this->decimal(18, 2),
            'bag_score' => $this->decimal(18, 2),
            'cup_score' => $this->decimal(18, 2),
            'foam_score' => $this->decimal(18, 2),
            'plus_score' => $this->decimal(18, 2),
            'minus_score' => $this->decimal(18, 2),
            'total_score' => $this->decimal(18, 2),
            'confirmed_by' => $this->integer(),
            'confirmed_at' => $this->dateTime(),
            'status' => $this->integer(),
            'population_total' => $this->decimal(18, 2),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('yearly_assessment', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('yearly_assessment', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('yearly_assessment', 'year', 'ปี');
        $this->addCommentOnColumn('yearly_assessment', 'garbage_prev', 'garbage_prev');
        $this->addCommentOnColumn('yearly_assessment', 'garbage_cur', 'garbage_cur');
        $this->addCommentOnColumn('yearly_assessment', 'bag_prev', 'bag_prev');
        $this->addCommentOnColumn('yearly_assessment', 'bag_cur', 'bag_cur');
        $this->addCommentOnColumn('yearly_assessment', 'cup_prev', 'cup_prev');
        $this->addCommentOnColumn('yearly_assessment', 'cup_cur', 'cup_cur');
        $this->addCommentOnColumn('yearly_assessment', 'foam_prev', 'foam_prev');
        $this->addCommentOnColumn('yearly_assessment', 'foam_cur', 'foam_cur');
        $this->addCommentOnColumn('yearly_assessment', 'garbage_percent', 'garbage_percent');
        $this->addCommentOnColumn('yearly_assessment', 'bag_percent', 'bag_percent');
        $this->addCommentOnColumn('yearly_assessment', 'cup_percent', 'cup_percent');
        $this->addCommentOnColumn('yearly_assessment', 'foam_percent', 'foam_percent');
        $this->addCommentOnColumn('yearly_assessment', 'garbage_score', 'garbage_score');
        $this->addCommentOnColumn('yearly_assessment', 'bag_score', 'bag_score');
        $this->addCommentOnColumn('yearly_assessment', 'cup_score', 'cup_score');
        $this->addCommentOnColumn('yearly_assessment', 'foam_score', 'foam_score');
        $this->addCommentOnColumn('yearly_assessment', 'plus_score', 'plus_score');
        $this->addCommentOnColumn('yearly_assessment', 'minus_score', 'minus_score');
        $this->addCommentOnColumn('yearly_assessment', 'total_score', 'total_score');
        $this->addCommentOnColumn('yearly_assessment', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('yearly_assessment', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('yearly_assessment', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('yearly_assessment', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('yearly_assessment', 'updated_at', 'ปรับปรุงเมื่อ');
        $this->addCommentOnColumn('yearly_assessment', 'confirmed_by', 'ยืนยันข้อมูลโดย');
        $this->addCommentOnColumn('yearly_assessment', 'confirmed_at', 'ยืนยันข้อมูลเมื่อ');
        $this->addCommentOnColumn('yearly_assessment', 'status', 'สถานะ');
        $this->addCommentOnColumn('yearly_assessment', 'population_total', 'จำนวนบุคคลากร');

        $this->createIndex('idx_yearly_assessment_year', 'yearly_assessment', ['year']);
        $this->createIndex('idx_yearly_assessment_status', 'yearly_assessment', ['status']);
        $this->addForeignKey('fk_yearly_assessment_office_id', 'yearly_assessment', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_yearly_assessment_user_confirmed_by', 'yearly_assessment', 'confirmed_by', 'user', 'id');
        $this->addForeignKey('fk_yearly_assessment_user_created_by', 'yearly_assessment', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_yearly_assessment_user_updated_by', 'yearly_assessment', 'updated_by', 'user', 'id');

        $this->createTable('yearly_assessment_total', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'year' => $this->integer(),
            'garbage_prev' => $this->decimal(18, 2),
            'garbage_cur' => $this->decimal(18, 2),
            'bag_prev' => $this->decimal(18, 2),
            'bag_cur' => $this->decimal(18, 2),
            'cup_prev' => $this->decimal(18, 2),
            'cup_cur' => $this->decimal(18, 2),
            'foam_prev' => $this->decimal(18, 2),
            'foam_cur' => $this->decimal(18, 2),
            'garbage_percent' => $this->decimal(18, 2),
            'bag_percent' => $this->decimal(18, 2),
            'cup_percent' => $this->decimal(18, 2),
            'foam_percent' => $this->decimal(18, 2),
            'garbage_score' => $this->decimal(18, 2),
            'bag_score' => $this->decimal(18, 2),
            'cup_score' => $this->decimal(18, 2),
            'foam_score' => $this->decimal(18, 2),
            'plus_score' => $this->decimal(18, 2),
            'minus_score' => $this->decimal(18, 2),
            'total_score' => $this->decimal(18, 2),
            'confirmed_by' => $this->integer(),
            'confirmed_at' => $this->dateTime(),
            'status' => $this->integer(),
            'population_total' => $this->decimal(18, 2),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('yearly_assessment_total', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('yearly_assessment_total', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('yearly_assessment_total', 'year', 'ปี');
        $this->addCommentOnColumn('yearly_assessment_total', 'garbage_prev', 'garbage_prev');
        $this->addCommentOnColumn('yearly_assessment_total', 'garbage_cur', 'garbage_cur');
        $this->addCommentOnColumn('yearly_assessment_total', 'bag_prev', 'bag_prev');
        $this->addCommentOnColumn('yearly_assessment_total', 'bag_cur', 'bag_cur');
        $this->addCommentOnColumn('yearly_assessment_total', 'cup_prev', 'cup_prev');
        $this->addCommentOnColumn('yearly_assessment_total', 'cup_cur', 'cup_cur');
        $this->addCommentOnColumn('yearly_assessment_total', 'foam_prev', 'foam_prev');
        $this->addCommentOnColumn('yearly_assessment_total', 'foam_cur', 'foam_cur');
        $this->addCommentOnColumn('yearly_assessment_total', 'garbage_percent', 'garbage_percent');
        $this->addCommentOnColumn('yearly_assessment_total', 'bag_percent', 'bag_percent');
        $this->addCommentOnColumn('yearly_assessment_total', 'cup_percent', 'cup_percent');
        $this->addCommentOnColumn('yearly_assessment_total', 'foam_percent', 'foam_percent');
        $this->addCommentOnColumn('yearly_assessment_total', 'garbage_score', 'garbage_score');
        $this->addCommentOnColumn('yearly_assessment_total', 'bag_score', 'bag_score');
        $this->addCommentOnColumn('yearly_assessment_total', 'cup_score', 'cup_score');
        $this->addCommentOnColumn('yearly_assessment_total', 'foam_score', 'foam_score');
        $this->addCommentOnColumn('yearly_assessment_total', 'plus_score', 'plus_score');
        $this->addCommentOnColumn('yearly_assessment_total', 'minus_score', 'minus_score');
        $this->addCommentOnColumn('yearly_assessment_total', 'total_score', 'total_score');
        $this->addCommentOnColumn('yearly_assessment_total', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('yearly_assessment_total', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('yearly_assessment_total', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('yearly_assessment_total', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('yearly_assessment_total', 'updated_at', 'ปรับปรุงเมื่อ');
        $this->addCommentOnColumn('yearly_assessment_total', 'confirmed_by', 'ยืนยันข้อมูลโดย');
        $this->addCommentOnColumn('yearly_assessment_total', 'confirmed_at', 'ยืนยันข้อมูลเมื่อ');
        $this->addCommentOnColumn('yearly_assessment_total', 'status', 'สถานะ');
        $this->addCommentOnColumn('yearly_assessment_total', 'population_total', 'จำนวนบุคคลากร');

        $this->createIndex('idx_yearly_assessment_total_year', 'yearly_assessment_total', ['year']);
        $this->createIndex('idx_yearly_assessment_total_status', 'yearly_assessment_total', ['status']);
        $this->addForeignKey('fk_yearly_assessment_total_office_id', 'yearly_assessment_total', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_yearly_assessment_total_user_confirmed_by', 'yearly_assessment_total', 'confirmed_by', 'user', 'id');
        $this->addForeignKey('fk_yearly_assessment_total_user_created_by', 'yearly_assessment_total', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_yearly_assessment_total_user_updated_by', 'yearly_assessment_total', 'updated_by', 'user', 'id');

        $this->createTable('alert', [
            'id' => $this->primaryKey(),
            'message' => $this->text(),
            'is_acknowledged' => $this->boolean()->notNull()->defaultValue(FALSE),
            'acknowledged_by' => $this->integer(),
            'acknowledged_at' => $this->dateTime(),
            'user_id' => $this->integer(),
            'created_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('alert', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('alert', 'message', 'message');
        $this->addCommentOnColumn('alert', 'is_acknowledged', 'acknowledged');
        $this->addCommentOnColumn('alert', 'acknowledged_by', 'รับทราบโดย');
        $this->addCommentOnColumn('alert', 'acknowledged_at', 'รับทราบเมื่อ');
        $this->addCommentOnColumn('alert', 'user_id', 'user');
        $this->addCommentOnColumn('alert', 'created_at', 'สร้างเมื่อ');

        $this->addForeignKey('fk_alert_user_acknowledged_by', 'alert', 'acknowledged_by', 'user', 'id');
        $this->addForeignKey('fk_alert_user_id', 'alert', 'user_id', 'user', 'id');

        $this->createTable('monthly_form', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'source_type_id' => $this->integer(),
            'year' => $this->integer(),
            'month' => $this->integer(),
            'office_population' => $this->integer(),
            'work_day' => $this->integer(),
            'garbage' => $this->decimal(18, 2),
            'bag' => $this->decimal(18, 2),
            'cup' => $this->decimal(18, 2),
            'foam' => $this->decimal(18, 2),
            'submitted_by' => $this->integer(),
            'submitted_at' => $this->dateTime(),
            'confirmed_by' => $this->integer(),
            'confirmed_at' => $this->dateTime(),
            'checked_by' => $this->integer(),
            'checked_at' => $this->dateTime(),
            'is_pass' => $this->boolean()->notNull()->defaultValue(FALSE),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('monthly_form', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('monthly_form', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('monthly_form', 'source_type_id', 'source type');
        $this->addCommentOnColumn('monthly_form', 'year', 'ปี');
        $this->addCommentOnColumn('monthly_form', 'month', 'เดือน');
        $this->addCommentOnColumn('monthly_form', 'office_population', 'จำนวนบุคคลากรในหน่วยงาน(คน)');
        $this->addCommentOnColumn('monthly_form', 'work_day', 'จำนวนวันทำงาน(วัน)');
        $this->addCommentOnColumn('monthly_form', 'garbage', 'ปริมาณขยะมูลฝอย (กก.)');
        $this->addCommentOnColumn('monthly_form', 'bag', 'จำนวนถุงพลาสติก (ใบ)');
        $this->addCommentOnColumn('monthly_form', 'cup', 'จำนวนแก้วพลาสติก (ใบ)');
        $this->addCommentOnColumn('monthly_form', 'foam', 'จำนวนโฟมบรรจุอาหาร (ใบ)');
        $this->addCommentOnColumn('monthly_form', 'submitted_by', 'ส่งล่าสุดโดย');
        $this->addCommentOnColumn('monthly_form', 'submitted_at', 'ส่งล่าสุดวันที่');
        $this->addCommentOnColumn('monthly_form', 'confirmed_by', 'ยืนยันล่าสุดโดย');
        $this->addCommentOnColumn('monthly_form', 'confirmed_at', 'ยืนยันล่าสุดวันที่');
        $this->addCommentOnColumn('monthly_form', 'confirmed_at', 'ยืนยันล่าสุดวันที่');
        $this->addCommentOnColumn('monthly_form', 'checked_by', 'อนุมัติล่าสุดโดย');
        $this->addCommentOnColumn('monthly_form', 'checked_at', 'อนุมัติล่าสุดวันที่');
        $this->addCommentOnColumn('monthly_form', 'is_pass', 'ผ่าน/ไม่ผ่าน');
        $this->addCommentOnColumn('monthly_form', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('monthly_form', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('monthly_form', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('monthly_form', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('monthly_form', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_monthly_form_year', 'monthly_form', ['year']);
        $this->createIndex('idx_monthly_form_month', 'monthly_form', ['month']);
        $this->createIndex('idx_monthly_form_is_pass', 'monthly_form', ['is_pass']);
        
        $this->addForeignKey('fk_monthly_form_office_id', 'monthly_form', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_monthly_form_source_type_id', 'monthly_form', 'source_type_id', 'source_type', 'id');
        $this->addForeignKey('fk_monthly_form_user_created_by', 'monthly_form', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_form_user_updated_by', 'monthly_form', 'updated_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_form_user_submitted_by', 'monthly_form', 'submitted_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_form_user_confirmed_by', 'monthly_form', 'confirmed_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_form_user_checked_by', 'monthly_form', 'checked_by', 'user', 'id');

        $this->createTable('monthly_form_submit', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'source_type_id' => $this->integer(),
            'year' => $this->integer(),
            'month' => $this->integer(),
            'office_population' => $this->integer(),
            'work_day' => $this->integer(),
            'garbage' => $this->decimal(18, 2),
            'bag' => $this->decimal(18, 2),
            'cup' => $this->decimal(18, 2),
            'foam' => $this->decimal(18, 2),
            'ref_id' => $this->integer(),
            'remark' => $this->text(),
            'submitted_by' => $this->integer(),
            'submitted_at' => $this->dateTime(),
            'confirmed_by' => $this->integer(),
            'confirmed_at' => $this->dateTime(),
            'checked_by' => $this->integer(),
            'checked_at' => $this->dateTime(),
            'is_pass' => $this->boolean()->notNull()->defaultValue(FALSE),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('monthly_form_submit', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('monthly_form_submit', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('monthly_form_submit', 'source_type_id', 'source type');
        $this->addCommentOnColumn('monthly_form_submit', 'year', 'ปี');
        $this->addCommentOnColumn('monthly_form_submit', 'month', 'เดือน');
        $this->addCommentOnColumn('monthly_form_submit', 'office_population', 'จำนวนบุคคลากรในหน่วยงาน(คน)');
        $this->addCommentOnColumn('monthly_form_submit', 'work_day', 'จำนวนวันทำงาน(วัน)');
        $this->addCommentOnColumn('monthly_form_submit', 'garbage', 'ปริมาณขยะมูลฝอย (กก.)');
        $this->addCommentOnColumn('monthly_form_submit', 'bag', 'จำนวนถุงพลาสติก (ใบ)');
        $this->addCommentOnColumn('monthly_form_submit', 'cup', 'จำนวนแก้วพลาสติก (ใบ)');
        $this->addCommentOnColumn('monthly_form_submit', 'foam', 'จำนวนโฟมบรรจุอาหาร (ใบ)');
        $this->addCommentOnColumn('monthly_form_submit', 'ref_id', 'ref');
        $this->addCommentOnColumn('monthly_form_submit', 'remark', 'remark');
        $this->addCommentOnColumn('monthly_form_submit', 'submitted_by', 'ส่งล่าสุดโดย');
        $this->addCommentOnColumn('monthly_form_submit', 'submitted_at', 'ส่งล่าสุดวันที่');
        $this->addCommentOnColumn('monthly_form_submit', 'confirmed_by', 'ยืนยันล่าสุดโดย');
        $this->addCommentOnColumn('monthly_form_submit', 'confirmed_at', 'ยืนยันล่าสุดวันที่');
        $this->addCommentOnColumn('monthly_form_submit', 'confirmed_at', 'ยืนยันล่าสุดวันที่');
        $this->addCommentOnColumn('monthly_form_submit', 'checked_by', 'อนุมัติล่าสุดโดย');
        $this->addCommentOnColumn('monthly_form_submit', 'checked_at', 'อนุมัติล่าสุดวันที่');
        $this->addCommentOnColumn('monthly_form_submit', 'is_pass', 'ผ่าน/ไม่ผ่าน');
        $this->addCommentOnColumn('monthly_form_submit', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('monthly_form_submit', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('monthly_form_submit', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('monthly_form_submit', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('monthly_form_submit', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_monthly_form_submit_year', 'monthly_form_submit', ['year']);
        $this->createIndex('idx_monthly_form_submit_month', 'monthly_form_submit', ['month']);
        $this->createIndex('idx_monthly_form_submit_is_pass', 'monthly_form_submit', ['is_pass']);
        
        $this->addForeignKey('fk_monthly_form_submit_office_id', 'monthly_form_submit', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_monthly_form_submit_ref_id', 'monthly_form_submit', 'ref_id', 'monthly_form_submit', 'id');
        $this->addForeignKey('fk_monthly_form_submit_source_type_id', 'monthly_form_submit', 'source_type_id', 'source_type', 'id');
        $this->addForeignKey('fk_monthly_form_submit_user_created_by', 'monthly_form_submit', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_form_submit_user_updated_by', 'monthly_form_submit', 'updated_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_form_submit_user_submitted_by', 'monthly_form_submit', 'submitted_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_form_submit_user_confirmed_by', 'monthly_form_submit', 'confirmed_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_form_submit_user_checked_by', 'monthly_form_submit', 'checked_by', 'user', 'id');

        $this->createTable('assess_issue', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'score' => $this->decimal(18, 2),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('assess_issue', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('assess_issue', 'name', 'Assess issue');
        $this->addCommentOnColumn('assess_issue', 'score', 'คะแนน');
        $this->addCommentOnColumn('assess_issue', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('assess_issue', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('assess_issue', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('assess_issue', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('assess_issue', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_assess_issue_name', 'assess_issue', ['name']);
        $this->addForeignKey('fk_assess_issue_user_created_by', 'assess_issue', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_assess_issue_user_updated_by', 'assess_issue', 'updated_by', 'user', 'id');

        $this->createTable('setting', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('setting', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('setting', 'key', 'key');
        $this->addCommentOnColumn('setting', 'name', 'name');
        $this->addCommentOnColumn('setting', 'value', 'value');
        $this->addCommentOnColumn('setting', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('setting', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('setting', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('setting', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('setting', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_setting_key', 'setting', ['key']);
        $this->createIndex('idx_setting_name', 'setting', ['name']);
        $this->addForeignKey('fk_setting_user_created_by', 'setting', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_setting_user_updated_by', 'setting', 'updated_by', 'user', 'id');

        $this->createTable('six_month_form', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'year' => $this->integer(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('six_month_form', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('six_month_form', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('six_month_form', 'year', 'ปี');
        $this->addCommentOnColumn('six_month_form', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('six_month_form', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('six_month_form', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('six_month_form', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('six_month_form', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_six_month_form_year', 'six_month_form', ['year']);
        $this->addForeignKey('fk_six_month_form_office_id', 'six_month_form', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_six_month_form_user_created_by', 'six_month_form', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_six_month_form_user_updated_by', 'six_month_form', 'updated_by', 'user', 'id');


        $this->createTable('form_issue_submit', [
            'id' => $this->primaryKey(),
            'six_month_form_id' => $this->integer(),
            'ref_id' => $this->integer(),
            'remark' => $this->text(),
            'submitted_by' => $this->integer(),
            'submitted_at' => $this->dateTime(),
            'confirmed_by' => $this->integer(),
            'confirmed_at' => $this->dateTime(),
            'checked_by' => $this->integer(),
            'checked_at' => $this->dateTime(),
            'is_pass' => $this->boolean()->notNull()->defaultValue(FALSE),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('form_issue_submit', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('form_issue_submit', 'submitted_by', 'ส่งล่าสุดโดย');
        $this->addCommentOnColumn('form_issue_submit', 'submitted_at', 'ส่งล่าสุดวันที่');
        $this->addCommentOnColumn('form_issue_submit', 'confirmed_by', 'ยืนยันล่าสุดโดย');
        $this->addCommentOnColumn('form_issue_submit', 'confirmed_at', 'ยืนยันล่าสุดวันที่');
        $this->addCommentOnColumn('form_issue_submit', 'confirmed_at', 'ยืนยันล่าสุดวันที่');
        $this->addCommentOnColumn('form_issue_submit', 'checked_by', 'อนุมัติล่าสุดโดย');
        $this->addCommentOnColumn('form_issue_submit', 'checked_at', 'อนุมัติล่าสุดวันที่');
        $this->addCommentOnColumn('form_issue_submit', 'is_pass', 'ผ่าน/ไม่ผ่าน');
        $this->addCommentOnColumn('form_issue_submit', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('form_issue_submit', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('form_issue_submit', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('form_issue_submit', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('form_issue_submit', 'updated_at', 'ปรับปรุงเมื่อ');
        
        $this->createIndex('idx_form_issue_submit_is_pass', 'form_issue_submit', ['is_pass']);

        $this->addForeignKey('fk_form_issue_submit_six_month_form_id', 'form_issue_submit', 'six_month_form_id', 'six_month_form', 'id');
        $this->addForeignKey('fk_form_issue_submit_ref_id', 'form_issue_submit', 'ref_id', 'form_issue_submit', 'id');
        $this->addForeignKey('fk_form_issue_submit_user_submitted_by', 'form_issue_submit', 'submitted_by', 'user', 'id');
        $this->addForeignKey('fk_form_issue_submit_user_confirmed_by', 'form_issue_submit', 'confirmed_by', 'user', 'id');
        $this->addForeignKey('fk_form_issue_submit_user_checked_by', 'form_issue_submit', 'checked_by', 'user', 'id');
        $this->addForeignKey('fk_form_issue_submit_user_created_by', 'form_issue_submit', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_form_issue_submit_user_updated_by', 'form_issue_submit', 'updated_by', 'user', 'id');

        $this->createTable('form_issue', [
            'id' => $this->primaryKey(),
            'six_month_form_id' => $this->integer(),
            'assess_issue_id' => $this->integer(),
            'form_issue_submit_id' => $this->integer(),
            'submitted_by' => $this->integer(),
            'submitted_at' => $this->dateTime(),
            'confirmed_by' => $this->integer(),
            'confirmed_at' => $this->dateTime(),
            'checked_by' => $this->integer(),
            'checked_at' => $this->dateTime(),
            'is_pass' => $this->boolean()->notNull()->defaultValue(FALSE),
            'score' => $this->decimal(18, 2),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('form_issue', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('form_issue', 'six_month_form_id', 'รายงานราย 6 เดือน');
        $this->addCommentOnColumn('form_issue', 'assess_issue_id', 'assess issue');
        $this->addCommentOnColumn('form_issue', 'submitted_by', 'ส่งล่าสุดโดย');
        $this->addCommentOnColumn('form_issue', 'submitted_at', 'ส่งล่าสุดวันที่');
        $this->addCommentOnColumn('form_issue', 'confirmed_by', 'ยืนยันล่าสุดโดย');
        $this->addCommentOnColumn('form_issue', 'confirmed_at', 'ยืนยันล่าสุดวันที่');
        $this->addCommentOnColumn('form_issue', 'confirmed_at', 'ยืนยันล่าสุดวันที่');
        $this->addCommentOnColumn('form_issue', 'checked_by', 'อนุมัติล่าสุดโดย');
        $this->addCommentOnColumn('form_issue', 'checked_at', 'อนุมัติล่าสุดวันที่');
        $this->addCommentOnColumn('form_issue', 'is_pass', 'ผ่าน/ไม่ผ่าน');
        $this->addCommentOnColumn('form_issue', 'score', 'คะแนน');
        $this->addCommentOnColumn('form_issue', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('form_issue', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('form_issue', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('form_issue', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('form_issue', 'updated_at', 'ปรับปรุงเมื่อ');
        
        $this->createIndex('idx_form_issue_is_pass', 'form_issue', ['is_pass']);

        $this->addForeignKey('fk_form_issue_six_month_form_id', 'form_issue', 'six_month_form_id', 'six_month_form', 'id');
        $this->addForeignKey('fk_form_issue_form_issue_submit_id', 'form_issue', 'form_issue_submit_id', 'form_issue_submit', 'id');
        $this->addForeignKey('fk_form_issue_assess_issue_id', 'form_issue', 'assess_issue_id', 'assess_issue', 'id');
        $this->addForeignKey('fk_form_issue_user_created_by', 'form_issue', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_form_issue_user_updated_by', 'form_issue', 'updated_by', 'user', 'id');
        $this->addForeignKey('fk_form_issue_user_submitted_by', 'form_issue', 'submitted_by', 'user', 'id');
        $this->addForeignKey('fk_form_issue_user_confirmed_by', 'form_issue', 'confirmed_by', 'user', 'id');
        $this->addForeignKey('fk_form_issue_user_checked_by', 'form_issue', 'checked_by', 'user', 'id');

        $this->createTable('form_issue_file', [
            'id' => $this->primaryKey(),
            'form_issue_id' => $this->integer(),
            'form_issue_submit_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'type' => $this->integer(),
            'name' => $this->string(),
            'file_name' => $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('form_issue_file', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('form_issue_file', 'form_issue_id', 'form issue');
        $this->addCommentOnColumn('form_issue_file', 'form_issue_submit_id', 'form issue review');
        $this->addCommentOnColumn('form_issue_file', 'parent_id', 'parent');
        $this->addCommentOnColumn('form_issue_file', 'type', '1=รูปภาพ, 2=ไฟล์เอกสาร');
        $this->addCommentOnColumn('form_issue_file', 'name', 'ชื่อเอกสาร/รูปภาพ');
        $this->addCommentOnColumn('form_issue_file', 'file_name', 'ชื่อไฟล์ที่บันทึกในระบบ');
        $this->addCommentOnColumn('form_issue_file', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('form_issue_file', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('form_issue_file', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('form_issue_file', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('form_issue_file', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_form_issue_file_type', 'form_issue_file', ['type']);
        $this->createIndex('idx_form_issue_file_name', 'form_issue_file', ['name']);
        
        $this->addForeignKey('fk_form_issue_file_form_issue_id', 'form_issue_file', 'form_issue_id', 'form_issue', 'id');
        $this->addForeignKey('fk_form_issue_file_form_issue_submit_id', 'form_issue_file', 'form_issue_submit_id', 'form_issue_submit', 'id');
        $this->addForeignKey('fk_form_issue_file_parent_id', 'form_issue_file', 'parent_id', 'form_issue_file', 'id');
        $this->addForeignKey('fk_form_issue_file_user_created_by', 'form_issue_file', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_form_issue_file_user_updated_by', 'form_issue_file', 'updated_by', 'user', 'id');

        $this->createTable('office_population_h', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'pop_int' => $this->integer(),
            'pop_ext' => $this->integer(),
            'total' => $this->integer(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('office_population_h', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('office_population_h', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('office_population_h', 'pop_int', 'บุคลากรภายใน');
        $this->addCommentOnColumn('office_population_h', 'pop_ext', 'บุคลากรภายนอก');
        $this->addCommentOnColumn('office_population_h', 'total', 'รวม');
        $this->addCommentOnColumn('office_population_h', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('office_population_h', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('office_population_h', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('office_population_h', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('office_population_h', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->addForeignKey('fk_office_population_h_office_id', 'office_population_h', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_office_population_h_user_created_by', 'office_population_h', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_office_population_h_user_updated_by', 'office_population_h', 'updated_by', 'user', 'id');

        $this->createTable('office_population_d', [
            'id' => $this->primaryKey(),
            'office_population_h_id' => $this->integer(),
            'office_name' => $this->string(),
            'pop_int' => $this->integer(),
            'pop_ext' => $this->integer(),
            'total' => $this->integer(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('office_population_d', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('office_population_d', 'office_population_h_id', 'หน่วยงาน');
        $this->addCommentOnColumn('office_population_d', 'office_name', 'หน่วยงาน');
        $this->addCommentOnColumn('office_population_d', 'pop_int', 'บุคลากรภายใน');
        $this->addCommentOnColumn('office_population_d', 'pop_ext', 'บุคลากรภายนอก');
        $this->addCommentOnColumn('office_population_d', 'total', 'รวม');
        $this->addCommentOnColumn('office_population_d', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('office_population_d', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('office_population_d', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('office_population_d', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('office_population_d', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_office_population_d_office_name', 'office_population_d', ['office_name']);
        
        $this->addForeignKey('fk_office_population_d_office_population_h_id', 'office_population_d', 'office_population_h_id', 'office_population_h', 'id');
        $this->addForeignKey('fk_office_population_d_user_created_by', 'office_population_d', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_office_population_d_user_updated_by', 'office_population_d', 'updated_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('office_population_d');
        $this->dropTable('office_population_h');
        $this->dropTable('form_issue_file');
        $this->dropTable('form_issue');
        $this->dropTable('form_issue_submit');
        $this->dropTable('six_month_form');
        $this->dropTable('setting');
        $this->dropTable('assess_issue');
        $this->dropTable('monthly_form_submit');
        $this->dropTable('monthly_form');
        $this->dropTable('alert');
        $this->dropTable('yearly_assessment_total');
        $this->dropTable('yearly_assessment');
        $this->dropTable('source_type');
        $this->dropTable('assessment_score');
        $this->dropTable('person_role');
        $this->dropTable('person');
        $this->dropTable('office');
        $this->dropTable('office_type');
        $this->dropTable('title');
        $this->dropTable('project_files');
        $this->dropTable('web_link');
        $this->dropTable('content');
        $this->dropTable('content_group');
        $this->dropTable('content_category');
        $this->dropTable('file_category');
        $this->dropTable('tambon');
        $this->dropTable('amphur');
        $this->dropTable('province');
        $this->dropTable('region');
    }

}
