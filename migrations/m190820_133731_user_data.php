<?php

use yii\db\Migration;

/**
 * Class m190820_133731_user_data
 */
class m190820_133731_user_data extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {

        $user = new app\models\User();
        $user->generateAuthKey();
        $user->setPassword('1q2w3e4r');
        $this->insert('user', ['id' => 1, 'username' => 'admin', 'auth_key' => $user->auth_key, 'password_hash' => $user->password_hash]);

        $this->insert('role', ['id' => 1, 'name' => 'Super Administrator']);
        $this->insert('role', ['id' => 2, 'name' => 'Administrator']);
        $this->insert('role', ['id' => 3, 'name' => 'Staff']);

        $this->insert('title', ['id' => 1, 'name' => 'นาย']);
        $this->insert('title', ['id' => 2, 'name' => 'น.ส.']);
        $this->insert('title', ['id' => 3, 'name' => 'นาง']);

        $this->insert('person', ['id' => 1, 'title_id' => 1, 'user_id' => 1, 'first_name' => 'ปฏิพัทธิ์', 'last_name' => 'ทิพยศิรินทร์']);
        $this->insert('person_role', ['id' => 1, 'role_id' => 1, 'person_id' => 1]);

        $auth = Yii::$app->authManager;
        $admin = $auth->createRole('Super Administrator');
        $auth->add($admin);
        $auth->assign($admin, 1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        
        $this->delete('person_role');
        $this->delete('person');
        $this->delete('title');
        $this->delete('role');
        $this->delete('user');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190820_133731_user_data cannot be reverted.\n";

      return false;
      }
     */
}
