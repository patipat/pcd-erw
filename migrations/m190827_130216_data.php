<?php

use yii\db\Migration;

/**
 * Class m190827_130216_data
 */
class m190827_130216_data extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        
        $this->insert('office_type', ['id' => 1, 'name' => 'ส่วนงานราชการ']);
        $this->insert('office_type', ['id' => 2, 'name' => 'ส่วนจังหวัด']);
        $this->insert('office_type', ['id' => 3, 'name' => 'ทสจ. สสภ.']);

        $this->insert('content_group', ['id' => 1, 'name' => 'ข่าวประชาสัมพันธ์']);
        $this->insert('content_group', ['id' => 2, 'name' => 'บทความทั่วไป']);
        $this->insert('content_group', ['id' => 3, 'name' => 'บทความมาตรการ']);
        $this->insert('content_group', ['id' => 4, 'name' => 'บทความทางวิชาการ']);

        $this->insert('office', ['id' => 1, 'name' => 'สำนักนายกรัฐมนตรี', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 2, 'name' => 'กระทรวงการคลัง', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 3, 'name' => 'กระทรวงการต่างประเทศ', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 4, 'name' => 'กระทรวงการพัฒนาสังคมและความมั่นคงของมนุษย์', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 5, 'name' => 'กระทรวงการท่องเที่ยวและกีฬา', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 6, 'name' => 'กระทรวงเกษตรและสหกรณ์', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 7, 'name' => 'กระทรวงคมนาคม', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 8, 'name' => 'กระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 9, 'name' => 'กระทรวงดิจิทัลเพื่อเศรษฐกิจและสังคม', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 10, 'name' => 'กระทรวงพลังงาน', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 11, 'name' => 'กระทรวงพาณิชย์', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 12, 'name' => 'กระทรวงมหาดไทย', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 13, 'name' => 'กระทรวงยุติธรรม', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 14, 'name' => 'กระทรวงแรงงาน', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 15, 'name' => 'กระทรวงวัฒนธรรม', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 16, 'name' => 'กระทรวงวิทยาศาสตร์และเทคโนโลยี', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 17, 'name' => 'กระทรวงสาธารณสุข', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 18, 'name' => 'กระทรวงอุตสาหกรรม', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 19, 'name' => 'กระทรวงศึกษาธิการ', 'office_type_id' => 1]);
        $this->insert('office', ['id' => 20, 'name' => 'อื่นๆ', 'office_type_id' => 1]);

        $this->insert('office', ['name' => 'กระบี่', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'กาญจนบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'กาฬสินธุ์', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'กำแพงเพชร', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ขอนแก่น', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'จันทบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ฉะเชิงเทรา', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ชลบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ชัยนาท', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ชัยภูมิ', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ชุมพร', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'เชียงราย', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'เชียงใหม่', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ตรัง', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ตราด', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ตาก', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'นครนายก', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'นครปฐม', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'นครพนม', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'นครราชสีมา', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'นครศรีธรรมราช', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'นครสวรรค์', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'นนทบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'นราธิวาส', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'น่าน', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'บึงกาฬ', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'บุรีรัมย์', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ปทุมธานี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ประจวบคีรีขันธ์', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ปราจีนบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ปัตตานี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'พระนครศรีอยุธยา', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'พะเยา', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'พังงา', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'พัทลุง', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'พิจิตร', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'พิษณุโลก', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'เพชรบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'เพชรบูรณ์', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'แพร่', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ภูเก็ต', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'มหาสารคาม', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'มุกดาหาร', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'แม่ฮ่องสอน', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ยโสธร', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ยะลา', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ร้อยเอ็ด', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ระนอง', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ระยอง', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ราชบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ลพบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ลำปาง', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ลำพูน', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'เลย', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'ศรีสะเกษ', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สกลนคร', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สงขลา', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สตูล', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สมุทรปราการ', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สมุทรสงคราม', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สมุทรสาคร', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สระแก้ว', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สระบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สิงห์บุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สุโขทัย', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สุพรรณบุรี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สุราษฎร์ธานี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'สุรินทร์', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'หนองคาย', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'หนองบัวลำพู', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'อ่างทอง', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'อำนาจเจริญ', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'อุดรธานี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'อุตรดิตถ์', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'อุทัยธานี', 'office_type_id' => 2]);
        $this->insert('office', ['name' => 'อุบลราชธานี', 'office_type_id' => 2]);

        $this->insert('office', ['name' => 'กระบี่', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'กาญจนบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'กาฬสินธุ์', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'กำแพงเพชร', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ขอนแก่น', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'จันทบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ฉะเชิงเทรา', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ชลบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ชัยนาท', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ชัยภูมิ', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ชุมพร', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'เชียงราย', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'เชียงใหม่', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ตรัง', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ตราด', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ตาก', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'นครนายก', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'นครปฐม', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'นครพนม', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'นครราชสีมา', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'นครศรีธรรมราช', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'นครสวรรค์', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'นนทบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'นราธิวาส', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'น่าน', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'บึงกาฬ', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'บุรีรัมย์', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ปทุมธานี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ประจวบคีรีขันธ์', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ปราจีนบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ปัตตานี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'พระนครศรีอยุธยา', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'พะเยา', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'พังงา', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'พัทลุง', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'พิจิตร', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'พิษณุโลก', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'เพชรบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'เพชรบูรณ์', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'แพร่', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ภูเก็ต', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'มหาสารคาม', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'มุกดาหาร', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'แม่ฮ่องสอน', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ยโสธร', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ยะลา', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ร้อยเอ็ด', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ระนอง', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ระยอง', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ราชบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ลพบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ลำปาง', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ลำพูน', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'เลย', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'ศรีสะเกษ', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สกลนคร', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สงขลา', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สตูล', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สมุทรปราการ', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สมุทรสงคราม', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สมุทรสาคร', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สระแก้ว', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สระบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สิงห์บุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สุโขทัย', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สุพรรณบุรี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สุราษฎร์ธานี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สุรินทร์', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'หนองคาย', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'หนองบัวลำพู', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'อ่างทอง', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'อำนาจเจริญ', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'อุดรธานี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'อุตรดิตถ์', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'อุทัยธานี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'อุบลราชธานี', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 1', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 2', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 3', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 4', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 5', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 6', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 7', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 8', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 9', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 10', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 11', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 12', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 13', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 14', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 15', 'office_type_id' => 3]);
        $this->insert('office', ['name' => 'สำนักงานสิ่งแวดล้อมภาคที่ 16', 'office_type_id' => 3]);


        $this->insert('office', ['name' => 'สำนักงานปลัดสำนักนายกรัฐมนตรี', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'กรมประชาสัมพันธ์', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการคุ้มครองผู้บริโภค', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักเลขาธิการนายกรัฐมนตรี', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักเลขาธิการคณะรัฐมนตรี', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักข่าวกรองแห่งชาติ', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักงบประมาณ', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักงานสภาความมั่นคงแห่งชาติ', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการกฤษฎีกา', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'คณะกรรมการข้าราชการพลเรือน', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักงานสภาพัฒนาการเศรษฐกิจและสังคมแห่งชาติ', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'คณะกรรมการพัฒนาระบบราชการ', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการส่งเสริมการลงทุน', 'office_type_id' => 1,'parent_id'=>1]);
        $this->insert('office', ['name' => 'สำนักงานทรัพยากรน้ำแห่งชาติ', 'office_type_id' => 1,'parent_id'=>1]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงการคลัง', 'office_type_id' => 1,'parent_id'=>2]);
        $this->insert('office', ['name' => 'กรมธนารักษ์', 'office_type_id' => 1,'parent_id'=>2]);
        $this->insert('office', ['name' => 'กรมบัญชีกลาง', 'office_type_id' => 1,'parent_id'=>2]);
        $this->insert('office', ['name' => 'กรมศุลกากร', 'office_type_id' => 1,'parent_id'=>2]);
        $this->insert('office', ['name' => 'กรมสรรพสามิต', 'office_type_id' => 1,'parent_id'=>2]);
        $this->insert('office', ['name' => 'กรมสรรพากร', 'office_type_id' => 1,'parent_id'=>2]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการนโยบายรัฐวิสาหกิจ', 'office_type_id' => 1,'parent_id'=>2]);
        $this->insert('office', ['name' => 'สำนักงานบริหารหนี้สาธารณะ', 'office_type_id' => 1,'parent_id'=>2]);
        $this->insert('office', ['name' => 'สำนักงานเศรษฐกิจการคลัง', 'office_type_id' => 1,'parent_id'=>2]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงการต่างประเทศ', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมการกงสุล', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมพิธีการทูต', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมยุโรป', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมเศรษฐกิจระหว่างประเทศ', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมสนธิสัญญาและกฎหมาย', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมสารนิเทศ', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมองค์การระหว่างประเทศ', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมอเมริกาและแปซิฟิกใต้', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมอาเซียน', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมเอเซียตะวันออก', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมเอเซียใต้ ตะวันออกกลางและแอฟริกา', 'office_type_id' => 1,'parent_id'=>3]);
        $this->insert('office', ['name' => 'กรมความร่วมมือระหว่างประเทศ', 'office_type_id' => 1,'parent_id'=>3]);

        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงการพัฒนาสังคมและความมั่นคงของมนุษย์', 'office_type_id' => 1,'parent_id'=>4]);
        $this->insert('office', ['name' => 'กรมพัฒนาสังคมและสวัสดิการ', 'office_type_id' => 1,'parent_id'=>4]);
        $this->insert('office', ['name' => 'กรมกิจการสตรีและสถาบันครอบครัว', 'office_type_id' => 1,'parent_id'=>4]);
        $this->insert('office', ['name' => 'กรมกิจการเด็กและเยาวชน', 'office_type_id' => 1,'parent_id'=>4]);
        $this->insert('office', ['name' => 'กรมกิจการผู้สูงอายุ', 'office_type_id' => 1,'parent_id'=>4]);
        $this->insert('office', ['name' => 'กรมส่งเสริมและพัฒนาคุณภาพชีวิตคนพิการ', 'office_type_id' => 1,'parent_id'=>4]);

        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงการท่องเที่ยวและกีฬา', 'office_type_id' => 1,'parent_id'=>5]);
        $this->insert('office', ['name' => 'กรมพลศึกษา', 'office_type_id' => 1,'parent_id'=>5]);
        $this->insert('office', ['name' => 'กรมการท่องเที่ยว', 'office_type_id' => 1,'parent_id'=>5]);

        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงเกษตรและสหกรณ์', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมชลประทาน', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมตรวจบัญชีสหกรณ์', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมประมง', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมปศุสัตว์', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมพัฒนาที่ดิน', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมวิชาการเกษตร', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมส่งเสริมการเกษตร', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมส่งเสริมสหกรณ์', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'สำนักงานการปฏิรูปที่ดินเพื่อเกษตรกรรม', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'สำนักงานมาตรฐานสินค้าเกษตรและอาหารแห่งชาติ', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'สำนักงานเศรษฐกิจการเกษตร', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมการข้าว', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมหม่อนไหม', 'office_type_id' => 1,'parent_id'=>6]);
        $this->insert('office', ['name' => 'กรมฝนหลวงและการบินเกษตร', 'office_type_id' => 1,'parent_id'=>6]);

        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงคมนาคม', 'office_type_id' => 1,'parent_id'=>7]);
        $this->insert('office', ['name' => 'กรมเจ้าท่า', 'office_type_id' => 1,'parent_id'=>7]);
        $this->insert('office', ['name' => 'กรมการขนส่งทางบก', 'office_type_id' => 1,'parent_id'=>7]);
        $this->insert('office', ['name' => 'กรมท่าอากาศยาน', 'office_type_id' => 1,'parent_id'=>7]);
        $this->insert('office', ['name' => 'กรมทางหลวง', 'office_type_id' => 1,'parent_id'=>7]);
        $this->insert('office', ['name' => 'กรมทางหลวงชนบท', 'office_type_id' => 1,'parent_id'=>7]);
        $this->insert('office', ['name' => 'สำนักงานนโยบายและแผนการขนส่งและจราจร', 'office_type_id' => 1,'parent_id'=>7]);

        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม', 'office_type_id' => 1,'parent_id'=>8]);
        $this->insert('office', ['name' => 'กรมควบคุมมลพิษ', 'office_type_id' => 1,'parent_id'=>8]);
        $this->insert('office', ['name' => 'กรมป่าไม้', 'office_type_id' => 1,'parent_id'=>8]);
        $this->insert('office', ['name' => 'กรมทรัพยากรทางทะเลและชายฝั่ง', 'office_type_id' => 1,'parent_id'=>8]);
        $this->insert('office', ['name' => 'กรมทรัพยากรธรณี', 'office_type_id' => 1,'parent_id'=>8]);
        $this->insert('office', ['name' => 'กรมทรัพยากรน้ำ', 'office_type_id' => 1,'parent_id'=>8]);
        $this->insert('office', ['name' => 'กรมทรัพยากรน้ำบาดาล', 'office_type_id' => 1,'parent_id'=>8]);
        $this->insert('office', ['name' => 'กรมส่งเสริมคุณภาพสิ่งแวดล้อม', 'office_type_id' => 1,'parent_id'=>8]);
        $this->insert('office', ['name' => 'กรมอุทยานแห่งชาติ สัตว์ป่า และพันธุ์พืช', 'office_type_id' => 1,'parent_id'=>8]);
        $this->insert('office', ['name' => 'สำนักงานนโยบายและแผนทรัพยากรธรรมชาติและสิ่งแวดล้อม', 'office_type_id' => 1,'parent_id'=>8]);

        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงดิจิทัลเพื่อเศรษฐกิจและสังคม', 'office_type_id' => 1,'parent_id'=>9]);
        $this->insert('office', ['name' => 'กรมอุตุนิยมวิทยา', 'office_type_id' => 1,'parent_id'=>9]);
        $this->insert('office', ['name' => 'สำนักงานสถิติแห่งชาติ', 'office_type_id' => 1,'parent_id'=>9]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการดิจิทัลเพื่อเศรษฐกิจและสังคมแห่งชาติ', 'office_type_id' => 1,'parent_id'=>9]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงพลังงาน', 'office_type_id' => 1,'parent_id'=>10]);
        $this->insert('office', ['name' => 'กรมเชื้อเพลิงธรรมชาติ', 'office_type_id' => 1,'parent_id'=>10]);
        $this->insert('office', ['name' => 'กรมธุรกิจพลังงาน', 'office_type_id' => 1,'parent_id'=>10]);
        $this->insert('office', ['name' => 'กรมพัฒนาพลังงานทดแทนและอนุรักษ์พลังงาน', 'office_type_id' => 1,'parent_id'=>10]);
        $this->insert('office', ['name' => 'สำนักงานนโยบายและแผนพลังงาน', 'office_type_id' => 1,'parent_id'=>10]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงพาณิชย์', 'office_type_id' => 1,'parent_id'=>11]);
        $this->insert('office', ['name' => 'กรมการค้าต่างประเทศ', 'office_type_id' => 1,'parent_id'=>11]);
        $this->insert('office', ['name' => 'กรมการค้าภายใน', 'office_type_id' => 1,'parent_id'=>11]);
        $this->insert('office', ['name' => 'กรมเจรจาการค้าระหว่างประเทศ', 'office_type_id' => 1,'parent_id'=>11]);
        $this->insert('office', ['name' => 'กรมทรัพย์สินทางปัญญา', 'office_type_id' => 1,'parent_id'=>11]);
        $this->insert('office', ['name' => 'กรมพัฒนาธุรกิจการค้า', 'office_type_id' => 1,'parent_id'=>11]);
        $this->insert('office', ['name' => 'กรมส่งเสริมการค้าระหว่างประเทศ', 'office_type_id' => 1,'parent_id'=>11]);
        $this->insert('office', ['name' => 'สำนักงานนโยบายและยุทธศาสตร์การค้า', 'office_type_id' => 1,'parent_id'=>11]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงมหาดไทย', 'office_type_id' => 1,'parent_id'=>12]);
        $this->insert('office', ['name' => 'กรมการปกครอง', 'office_type_id' => 1,'parent_id'=>12]);
        $this->insert('office', ['name' => 'กรมการพัฒนาชุมชน', 'office_type_id' => 1,'parent_id'=>12]);
        $this->insert('office', ['name' => 'กรมที่ดิน', 'office_type_id' => 1,'parent_id'=>12]);
        $this->insert('office', ['name' => 'กรมป้องกันและบรรเทาสาธารณภัย', 'office_type_id' => 1,'parent_id'=>12]);
        $this->insert('office', ['name' => 'กรมโยธาธิการและผังเมือง', 'office_type_id' => 1,'parent_id'=>12]);
        $this->insert('office', ['name' => 'กรมส่งเสริมการปกครองท้องถิ่น', 'office_type_id' => 1,'parent_id'=>12]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงยุติธรรม', 'office_type_id' => 1,'parent_id'=>13]);
        $this->insert('office', ['name' => 'กรมคุมประพฤติ', 'office_type_id' => 1,'parent_id'=>13]);
        $this->insert('office', ['name' => 'กรมคุ้มครองสิทธิและเสรีภาพ', 'office_type_id' => 1,'parent_id'=>13]);
        $this->insert('office', ['name' => 'กรมบังคับคดี', 'office_type_id' => 1,'parent_id'=>13]);
        $this->insert('office', ['name' => 'กรมพินิจและคุ้มครองเด็กและเยาวชน', 'office_type_id' => 1,'parent_id'=>13]);
        $this->insert('office', ['name' => 'กรมราชทัณฑ์', 'office_type_id' => 1,'parent_id'=>13]);
        $this->insert('office', ['name' => 'กรมสอบสวนคดีพิเศษ', 'office_type_id' => 1,'parent_id'=>13]);
        $this->insert('office', ['name' => 'สำนักงานกิจการยุติธรรม', 'office_type_id' => 1,'parent_id'=>13]);
        $this->insert('office', ['name' => 'สถาบันนิติวิทยาศาสตร์', 'office_type_id' => 1,'parent_id'=>13]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการป้องกันและปราบปรามยาเสพติด', 'office_type_id' => 1,'parent_id'=>13]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงแรงงาน', 'office_type_id' => 1,'parent_id'=>14]);
        $this->insert('office', ['name' => 'กรมการจัดหางาน', 'office_type_id' => 1,'parent_id'=>14]);
        $this->insert('office', ['name' => 'กรมพัฒนาฝีมือแรงงาน', 'office_type_id' => 1,'parent_id'=>14]);
        $this->insert('office', ['name' => 'กรมสวัสดิการและคุ้มครองแรงงาน', 'office_type_id' => 1,'parent_id'=>14]);
        $this->insert('office', ['name' => 'สำนักงานประกันสังคม', 'office_type_id' => 1,'parent_id'=>14]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงวัฒนธรรม', 'office_type_id' => 1,'parent_id'=>15]);
        $this->insert('office', ['name' => 'กรมการศาสนา', 'office_type_id' => 1,'parent_id'=>15]);
        $this->insert('office', ['name' => 'สำนักงานศิลปวัฒนธรรมร่วมสมัย', 'office_type_id' => 1,'parent_id'=>15]);
        $this->insert('office', ['name' => 'กรมศิลปากร', 'office_type_id' => 1,'parent_id'=>15]);
        $this->insert('office', ['name' => 'กรมส่งเสริมวัฒนธรรม', 'office_type_id' => 1,'parent_id'=>15]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงการอุดมศึกษา วิทยาศาสตร์ วิจัยและนวัตกรรม', 'office_type_id' => 1,'parent_id'=>16]);
        $this->insert('office', ['name' => 'กรมวิทยาศาสตร์บริการ', 'office_type_id' => 1,'parent_id'=>16]);
        $this->insert('office', ['name' => 'สำนักงานปรมาณูเพื่อสันติ', 'office_type_id' => 1,'parent_id'=>16]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงสาธารณสุข', 'office_type_id' => 1,'parent_id'=>17]);
        $this->insert('office', ['name' => 'กรมการแพทย์', 'office_type_id' => 1,'parent_id'=>17]);
        $this->insert('office', ['name' => 'กรมควบคุมโรค', 'office_type_id' => 1,'parent_id'=>17]);
        $this->insert('office', ['name' => 'กรมการแพทย์แผนไทยและการแพทย์ทางเลือก', 'office_type_id' => 1,'parent_id'=>17]);
        $this->insert('office', ['name' => 'กรมวิทยาศาสตร์การแพทย์', 'office_type_id' => 1,'parent_id'=>17]);
        $this->insert('office', ['name' => 'กรมสนับสนุนบริการสุขภาพ', 'office_type_id' => 1,'parent_id'=>17]);
        $this->insert('office', ['name' => 'กรมสุขภาพจิต', 'office_type_id' => 1,'parent_id'=>17]);
        $this->insert('office', ['name' => 'กรมอนามัย', 'office_type_id' => 1,'parent_id'=>17]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการอาหารและยา', 'office_type_id' => 1,'parent_id'=>17]);
        
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงอุตสาหกรรม', 'office_type_id' => 1,'parent_id'=>18]);
        $this->insert('office', ['name' => 'กรมโรงงานอุตสาหกรรม', 'office_type_id' => 1,'parent_id'=>18]);
        $this->insert('office', ['name' => 'กรมส่งเสริมอุตสาหกรรม', 'office_type_id' => 1,'parent_id'=>18]);
        $this->insert('office', ['name' => 'กรมอุตสาหกรรมพื้นฐานและการเหมืองแร่', 'office_type_id' => 1,'parent_id'=>18]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการอ้อยและน้ำตาลทราย', 'office_type_id' => 1,'parent_id'=>18]);
        $this->insert('office', ['name' => 'สำนักงานมาตรฐานผลิตภัณฑ์อุตสาหกรรม', 'office_type_id' => 1,'parent_id'=>18]);
        $this->insert('office', ['name' => 'สำนักงานเศรษฐกิจอุตสาหกรรม', 'office_type_id' => 1,'parent_id'=>18]);

        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงศึกษาธิการ', 'office_type_id' => 1,'parent_id'=>19]);
        $this->insert('office', ['name' => 'สำนักงานเลขาธิการสภาการศึกษา', 'office_type_id' => 1,'parent_id'=>19]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการการศึกษาขั้นพื้นฐาน', 'office_type_id' => 1,'parent_id'=>19]);
        $this->insert('office', ['name' => 'สำนักงานปลัดกระทรวงการอุดมศึกษา วิทยาศาสตร์ วิจัย และนวัตกรรม', 'office_type_id' => 1,'parent_id'=>19]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการการอาชีวศึกษา', 'office_type_id' => 1,'parent_id'=>19]);
        
        $this->insert('office', ['name' => 'สำนักงานพระพุทธศาสนาแห่งชาติ', 'office_type_id' => 1,'parent_id'=>20]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการพิเศษเพื่อประสานงานโครงการอันเนื่องมาจากพระราชดำริ', 'office_type_id' => 1,'parent_id'=>20]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการวิจัยแห่งชาติ', 'office_type_id' => 1,'parent_id'=>20]);
        $this->insert('office', ['name' => 'สำนักงานราชบัณฑิตยสภา', 'office_type_id' => 1,'parent_id'=>20]);
        $this->insert('office', ['name' => 'สำนักงานป้องกันและปราบปรามการฟอกเงิน', 'office_type_id' => 1,'parent_id'=>20]);
        $this->insert('office', ['name' => 'ศูนย์อำนวยการบริหารจังหวัดชายแดนภาคใต้', 'office_type_id' => 1,'parent_id'=>20]);
        $this->insert('office', ['name' => 'สำนักงานคณะกรรมการป้องกันและปราบปรามการทุจริตในภาครัฐ', 'office_type_id' => 1,'parent_id'=>20]);
        $this->insert('office', ['name' => 'สำนักงานเลขาธิการคุรุสภา', 'office_type_id' => 1,'parent_id'=>20]);


        

        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190827_130216_data cannot be reverted.\n";

      return false;
      }
     */
}
