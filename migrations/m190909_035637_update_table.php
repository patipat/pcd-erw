<?php

use yii\db\Migration;

/**
 * Class m190909_035637_update_table
 */
class m190909_035637_update_table extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('office', 'province_id', $this->integer());
        $this->addColumn('office', 'amphur_id', $this->integer());
        $this->addColumn('office', 'tambon_id', $this->integer());
        $this->addColumn('office', 'zipcode', $this->string());
        $this->addColumn('office', 'lat', $this->decimal(11, 8));
        $this->addColumn('office', 'lng', $this->decimal(11, 8));

        $this->addCommentOnColumn('office', 'province_id', 'จังหวัด');
        $this->addCommentOnColumn('office', 'amphur_id', 'อำเภอ');
        $this->addCommentOnColumn('office', 'tambon_id', 'ตำบล');
        $this->addCommentOnColumn('office', 'zipcode', 'รหัสไปรษณีย์');
        $this->addCommentOnColumn('office', 'lat', 'lat');
        $this->addCommentOnColumn('office', 'lng', 'lng');

        $this->addForeignKey('fk_office_province_id', 'office', 'province_id', 'province', 'id');
        $this->addForeignKey('fk_office_amphur_id', 'office', 'amphur_id', 'amphur', 'id');
        $this->addForeignKey('fk_office_tambon_id', 'office', 'tambon_id', 'tambon', 'id');

        $this->addColumn('monthly_form', 'pop_int', $this->integer());
        $this->addColumn('monthly_form', 'pop_ext', $this->integer());
        $this->addCommentOnColumn('monthly_form', 'pop_int', 'จำนวนบุคลากรภายใน');
        $this->addCommentOnColumn('monthly_form', 'pop_ext', 'จำนวนบุคลากรภายนอก');
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('monthly_form', 'pop_ext');
        $this->dropColumn('monthly_form', 'pop_int');
        
        $this->dropForeignKey('fk_office_tambon_id', 'office');
        $this->dropForeignKey('fk_office_amphur_id', 'office');
        $this->dropForeignKey('fk_office_province_id', 'office');
        
        $this->dropColumn('office', 'lng');
        $this->dropColumn('office', 'lat');
        $this->dropColumn('office', 'zipcode');
        $this->dropColumn('office', 'tambon_id');
        $this->dropColumn('office', 'amphur_id');
        $this->dropColumn('office', 'province_id');
        
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190909_035637_update_table cannot be reverted.\n";

      return false;
      }
     */
}
