<?php

use yii\db\Migration;

/**
 * Class m190909_083936_alter_monthly_form_submit
 */
class m190909_083936_alter_monthly_form_submit extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('monthly_form_submit', 'pop_ext', $this->integer());
        $this->addColumn('monthly_form_submit', 'pop_int', $this->integer());
        $this->addCommentOnColumn('monthly_form_submit', 'pop_int', 'จำนวนบุคลากรภายใน');
        $this->addCommentOnColumn('monthly_form_submit', 'pop_ext', 'จำนวนบุคลากรภายนอก');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('monthly_form_submit', 'pop_ext');
        $this->dropColumn('monthly_form_submit', 'pop_int');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190909_083936_alter_monthly_form_submit cannot be reverted.\n";

      return false;
      }
     */
}
