<?php

use yii\db\Migration;

/**
 * Class m190910_044439_data_permission
 */
class m190910_044439_data_permission extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {

        $auth = Yii::$app->authManager;
        
        $roleA = $auth->getRole('Administrator');
        if (!isset($roleA)) {
            $roleA = $auth->createRole('Administrator');
            $auth->add($roleA);
        }
        $roleS = $auth->getRole('Staff');
        if (!isset($roleS)) {
            $roleS = $auth->createRole('Staff');
            $auth->add($roleS);
        }
        $permissionsA = [
            'site.index',
            'administrator.can',
            'person.*',
            'person-role.*',
            'site.person',
        ];
        $permissionsS = [
            'site.index',
            'staff.can',
        ];

        foreach ($permissionsA as $permA) {
            $pA = $auth->getPermission(\Yii::$app->id . ".{$permA}");
            if (!isset($pA)) {
                $pA = $auth->createPermission(\Yii::$app->id . ".{$permA}");
                $auth->add($pA);
            }
            if ($auth->canAddChild($roleA, $pA)) {
                $auth->addChild($roleA, $pA);
            }
        }
        foreach ($permissionsS as $permS) {
            $pS = $auth->getPermission(\Yii::$app->id . ".{$permS}");
            if (!isset($pS)) {
                $pS = $auth->createPermission(\Yii::$app->id . ".{$permS}");
                $auth->add($pS);
            }
            if ($auth->canAddChild($roleS, $pS)) {
                $auth->addChild($roleS, $pS);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $auth = Yii::$app->authManager;
        $roleA = $auth->getRole('Administrator');
        $roleS = $auth->getRole('Staff');
        $permissionsA = [
            'site.index',
            'administrator.can',
            'person.*',
            'person-role.*',
            'site.person',
        ];
        $permissionsS = [
            'site.index',
            'staff.can',
        ];
        
        foreach ($permissionsA as $permR) {
            $pR = $auth->getPermission(\Yii::$app->id . ".{$permR}");
            if (isset($pR)) {
                $auth->removeChild($roleA, $pR);
            }
        }
        foreach ($permissionsS as $permR) {
            $pR = $auth->getPermission(\Yii::$app->id . ".{$permR}");
            if (isset($pR)) {
                $auth->removeChild($roleS, $pR);
            }
        }
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190910_044439_data_permission cannot be reverted.\n";

      return false;
      }
     */
}
