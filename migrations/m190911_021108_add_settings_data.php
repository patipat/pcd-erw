<?php

use yii\db\Migration;
use app\models\Setting;
/**
 * Class m190911_021108_add_settings_data
 */
class m190911_021108_add_settings_data extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->batchInsert('setting', ['key', 'name', 'value'], [
            [Setting::FIRST_MONTH, Setting::settingNames()[Setting::FIRST_MONTH], 1],
            [Setting::LAST_SUBMIT_DAY_MONTH, Setting::settingNames()[Setting::LAST_SUBMIT_DAY_MONTH], '15/01'],
            [Setting::MONTHLY_NO_SUBMIT_ALERT_DAY, Setting::settingNames()[Setting::MONTHLY_NO_SUBMIT_ALERT_DAY], '31'],
            [Setting::MONTHLY_COMPLETE_ALERT_DAY, Setting::settingNames()[Setting::MONTHLY_COMPLETE_ALERT_DAY], '07'],
            [Setting::SIX_MONTH_NO_SUBMIT_ALERT_DAY_MONTH, Setting::settingNames()[Setting::SIX_MONTH_NO_SUBMIT_ALERT_DAY_MONTH], '30/06'],
            [Setting::SIX_MONTH_COMPLETE_ALERT_DAY_MONTH, Setting::settingNames()[Setting::SIX_MONTH_COMPLETE_ALERT_DAY_MONTH], '07/07'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->delete('setting');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190911_021108_add_settings_data cannot be reverted.\n";

      return false;
      }
     */
}
