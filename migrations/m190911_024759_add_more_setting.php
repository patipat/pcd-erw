<?php

use yii\db\Migration;
use app\models\Setting;

/**
 * Class m190911_024759_add_more_setting
 */
class m190911_024759_add_more_setting extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->batchInsert('setting', ['key', 'name', 'value'], [
            [Setting::CURRENT_YEAR, Setting::settingNames()[Setting::CURRENT_YEAR], 2562],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->delete('setting', ['key' => Setting::CURRENT_YEAR]);
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190911_024759_add_more_setting cannot be reverted.\n";

      return false;
      }
     */
}
