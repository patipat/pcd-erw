<?php

use yii\db\Migration;

/**
 * Class m190911_040641_insert_source_type
 */
class m190911_040641_insert_source_type extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->insert('source_type', ['id' => 1, 'name' => 'ถังขยะ']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->delete('source_type');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190911_040641_insert_source_type cannot be reverted.\n";

      return false;
      }
     */
}
