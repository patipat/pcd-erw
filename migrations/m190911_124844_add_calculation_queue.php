<?php

use yii\db\Migration;

/**
 * Class m190911_124844_add_calculation_queue
 */
class m190911_124844_add_calculation_queue extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('calculation_queue', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'office_id' => $this->integer(),
            'year' => $this->integer(),
            'completed_at' => $this->dateTime(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('calculation_queue', 'type', 'ประเภทการคำนวณ');
        $this->addCommentOnColumn('calculation_queue', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('calculation_queue', 'year', 'ปี');
        $this->addCommentOnColumn('calculation_queue', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('calculation_queue', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('calculation_queue', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('calculation_queue', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('calculation_queue', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_calculation_queue_type', 'calculation_queue', ['type']);
        $this->createIndex('idx_calculation_queue_year', 'calculation_queue', ['year']);
        $this->addForeignKey('fk_calculation_queue_office_id', 'calculation_queue', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_calculation_queue_created_by', 'calculation_queue', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_calculation_queue_updated_by', 'calculation_queue', 'updated_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('calculation_queue');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190911_124844_add_calculation_queue cannot be reverted.\n";

      return false;
      }
     */
}
