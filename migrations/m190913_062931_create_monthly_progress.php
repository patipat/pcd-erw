<?php

use yii\db\Migration;

/**
 * Class m190913_062931_create_monthly_progress
 */
class m190913_062931_create_monthly_progress extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('monthly_progress', [
            'id' => $this->primaryKey(),
            'year' => $this->integer(),
            'month' => $this->integer(),
            'office_id' => $this->integer(),
            'current_pop' => $this->integer(),
            'total_pop' => $this->integer(),
            'percent' => $this->decimal(18, 2),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('monthly_progress', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('monthly_progress', 'year', 'ปี');
        $this->addCommentOnColumn('monthly_progress', 'month', 'เดือน');
        $this->addCommentOnColumn('monthly_progress', 'current_pop', 'จำนวนบุคลกรที่ส่งรายงานแล้ว');
        $this->addCommentOnColumn('monthly_progress', 'total_pop', 'จำนวนบุคลกรทั้งหมด');
        $this->addCommentOnColumn('monthly_progress', 'percent', '% ที่ส่งรายงาน');
        $this->addCommentOnColumn('monthly_progress', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('monthly_progress', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('monthly_progress', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('monthly_progress', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('monthly_progress', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_monthly_progress_month', 'monthly_progress', ['month']);
        $this->createIndex('idx_monthly_progress_year', 'monthly_progress', ['year']);
        $this->addForeignKey('fk_monthly_progress_office_id', 'monthly_progress', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_monthly_progress_created_by', 'monthly_progress', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_progress_updated_by', 'monthly_progress', 'updated_by', 'user', 'id');
        
        $this->addColumn('calculation_queue', 'month', $this->integer());
        $this->addCommentOnColumn('calculation_queue', 'month', 'เดือน');
        $this->createIndex('idx_calculation_queue_month', 'calculation_queue', 'month');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('calculation_queue', 'month');
        $this->dropTable('monthly_progress');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190913_062931_create_monthly_progress cannot be reverted.\n";

      return false;
      }
     */
}
