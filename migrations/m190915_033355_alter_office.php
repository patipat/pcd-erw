<?php

use yii\db\Migration;
use app\models\Office;

/**
 * Class m190915_033355_alter_office
 */
class m190915_033355_alter_office extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('office', 'level', $this->integer());
        $this->addCommentOnColumn('office', 'level', 'ระดับ');
        $this->createIndex('idx_office_level', 'office', 'level');
        
        $offices = Office::find()->isDeleted(false)->all();
        
        foreach ($offices as $office) {
            $office->level = $office->calculatedLevel;
            $office->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('office', 'level');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190915_033355_alter_office cannot be reverted.\n";

      return false;
      }
     */
}
