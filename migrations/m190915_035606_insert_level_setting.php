<?php

use yii\db\Migration;
use app\models\Setting;

/**
 * Class m190915_035606_insert_level_setting
 */
class m190915_035606_insert_level_setting extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->insert('setting', [
            'key' => Setting::SUPER_ADMIN_CHECK_LEVEL,
            'name' => Setting::settingNames()[Setting::SUPER_ADMIN_CHECK_LEVEL],
            'value' => 2
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->delete('setting', [
            'key' => Setting::SUPER_ADMIN_CHECK_LEVEL,
        ]);
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190915_035606_insert_level_setting cannot be reverted.\n";

      return false;
      }
     */
}
