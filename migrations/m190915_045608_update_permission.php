<?php

use yii\db\Migration;

/**
 * Class m190915_045608_update_permission
 */
class m190915_045608_update_permission extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $auth = Yii::$app->authManager;

        $roleA = $auth->getRole('Administrator');
        $roleS = $auth->getRole('Staff');
        $permissionsA = [
            'office-population-h.create',
            'monthly-form-submit.create',
            'monthly-form-submit.view',
        ];
        $permissionsB = [
            'monthly-form-submit.confirm',
            'monthly-form-submit.check',
        ];

        foreach ($permissionsA as $permA) {
            $pA = $auth->getPermission(\Yii::$app->id . ".{$permA}");
            if (!isset($pA)) {
                $pA = $auth->createPermission(\Yii::$app->id . ".{$permA}");
                $auth->add($pA);
            }
            if ($auth->canAddChild($roleA, $pA)) {
                $auth->addChild($roleA, $pA);
            }
            if ($auth->canAddChild($roleS, $pA)) {
                $auth->addChild($roleS, $pA);
            }
        }
        foreach ($permissionsB as $permA) {
            $pA = $auth->getPermission(\Yii::$app->id . ".{$permA}");
            if (!isset($pA)) {
                $pA = $auth->createPermission(\Yii::$app->id . ".{$permA}");
                $auth->add($pA);
            }
            if ($auth->canAddChild($roleA, $pA)) {
                $auth->addChild($roleA, $pA);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $auth = Yii::$app->authManager;
        $roleA = $auth->getRole('Administrator');
        $roleS = $auth->getRole('Staff');
        $permissionsA = [
            'office-population-h.create',
            'monthly-form-submit.create',
            'monthly-form-submit.view',
        ];
        $permissionsB = [
            'monthly-form-submit.confirm',
            'monthly-form-submit.check',
        ];
        
        foreach ($permissionsA as $permR) {
            $pR = $auth->getPermission(\Yii::$app->id . ".{$permR}");
            if (isset($pR)) {
                $auth->removeChild($roleA, $pR);
                $auth->removeChild($roleS, $pR);
            }
        }
        foreach ($permissionsB as $permR) {
            $pR = $auth->getPermission(\Yii::$app->id . ".{$permR}");
            if (isset($pR)) {
                $auth->removeChild($roleA, $pR);
            }
        }
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190915_045608_update_permission cannot be reverted.\n";

      return false;
      }
     */
}
