<?php

use yii\db\Migration;

/**
 * Class m190915_092048_alter_yearly_assessment
 */
class m190915_092048_alter_yearly_assessment extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('yearly_assessment', 'pop_int', $this->integer());
        $this->addColumn('yearly_assessment', 'pop_ext', $this->integer());
        $this->addColumn('yearly_assessment', 'office_population', $this->integer());
        $this->addColumn('yearly_assessment', 'work_day', $this->integer());
        $this->addColumn('yearly_assessment', 'garbage', $this->decimal(18, 2));
        $this->addColumn('yearly_assessment', 'bag', $this->decimal(18, 2));
        $this->addColumn('yearly_assessment', 'cup', $this->decimal(18, 2));
        $this->addColumn('yearly_assessment', 'foam', $this->decimal(18, 2));
        
        $this->addCommentOnColumn('yearly_assessment', 'pop_int', 'จำนวนบุคคลากรภายใน(คน)');
        $this->addCommentOnColumn('yearly_assessment', 'pop_ext', 'จำนวนบุคคลากรภายนอก(คน)');
        $this->addCommentOnColumn('yearly_assessment', 'office_population', 'จำนวนบุคคลากรในหน่วยงาน(คน)');
        $this->addCommentOnColumn('yearly_assessment', 'work_day', 'จำนวนวันทำงาน(วัน)');
        $this->addCommentOnColumn('yearly_assessment', 'garbage', 'ปริมาณขยะมูลฝอย (กก.)');
        $this->addCommentOnColumn('yearly_assessment', 'bag', 'จำนวนถุงพลาสติก (ใบ)');
        $this->addCommentOnColumn('yearly_assessment', 'cup', 'จำนวนแก้วพลาสติก (ใบ)');
        $this->addCommentOnColumn('yearly_assessment', 'foam', 'จำนวนโฟมบรรจุอาหาร (ใบ)');
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('yearly_assessment', 'foam');
        $this->dropColumn('yearly_assessment', 'cup');
        $this->dropColumn('yearly_assessment', 'bag');
        $this->dropColumn('yearly_assessment', 'garbage');
        $this->dropColumn('yearly_assessment', 'work_day');
        $this->dropColumn('yearly_assessment', 'office_population');
        $this->dropColumn('yearly_assessment', 'pop_ext');
        $this->dropColumn('yearly_assessment', 'pop_int');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190915_092048_alter_yearly_assessment cannot be reverted.\n";

      return false;
      }
     */
}
