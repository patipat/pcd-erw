<?php

use yii\db\Migration;
use app\models\Setting;

/**
 * Class m190916_024206_insert_setting
 */
class m190916_024206_insert_setting extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->insert('setting', [
            'key' => Setting::MONTHLY_FIRST_MONTH,
            'name' => Setting::settingNames()[Setting::MONTHLY_FIRST_MONTH],
            'value' => 1
        ]);
        $this->insert('setting', [
            'key' => Setting::MONTHLY_MONTHS,
            'name' => Setting::settingNames()[Setting::MONTHLY_MONTHS],
            'value' => 8
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->delete('setting', [
            'key' => [Setting::MONTHLY_FIRST_MONTH, Setting::MONTHLY_MONTHS]
        ]);
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190916_024206_insert_setting cannot be reverted.\n";

      return false;
      }
     */
}
