<?php

use yii\db\Migration;

/**
 * Class m190916_030035_edit_six_month_form
 */
class m190916_030035_edit_six_month_form extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('six_month_form', 'submitted_by', $this->integer());
        $this->addColumn('six_month_form', 'submitted_at', $this->dateTime());
        $this->addColumn('six_month_form', 'confirmed_by', $this->integer());
        $this->addColumn('six_month_form', 'confirmed_at', $this->dateTime());
        $this->addColumn('six_month_form', 'checked_by', $this->integer());
        $this->addColumn('six_month_form', 'checked_at', $this->dateTime());
        $this->addColumn('six_month_form', 'is_pass', $this->integer());

        $this->addForeignKey('fk_six_month_form_submitted_by', 'six_month_form', 'submitted_by', 'user', 'id');
        $this->addForeignKey('fk_six_month_form_confirmed_by', 'six_month_form', 'confirmed_by', 'user', 'id');
        $this->addForeignKey('fk_six_month_form_checked_by', 'six_month_form', 'checked_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropForeignKey('fk_six_month_form_submitted_by', 'six_month_form');
        $this->dropForeignKey('fk_six_month_form_confirmed_by', 'six_month_form');
        $this->dropForeignKey('fk_six_month_form_checked_by', 'six_month_form');
        $this->dropColumn('six_month_form', 'submitted_by');
        $this->dropColumn('six_month_form', 'submitted_at');
        $this->dropColumn('six_month_form', 'confirmed_by');
        $this->dropColumn('six_month_form', 'confirmed_at');
        $this->dropColumn('six_month_form', 'checked_by');
        $this->dropColumn('six_month_form', 'checked_at');
        $this->dropColumn('six_month_form', 'is_pass');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190916_030035_edit_six_month_form cannot be reverted.\n";

      return false;
      }
     */
}
