<?php

use yii\db\Migration;

/**
 * Class m190916_032244_add_column_remark
 */
class m190916_032244_add_column_remark extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('six_month_form', 'comment', $this->text());
        $this->addColumn('form_issue', 'comment', $this->text());
        $this->addColumn('monthly_form_submit', 'comment', $this->text());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('six_month_form', 'comment');
        $this->dropColumn('form_issue', 'comment');
        $this->dropColumn('monthly_form_submit', 'comment');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190916_032244_add_column_remark cannot be reverted.\n";

        return false;
    }
    */
}
