<?php

use yii\db\Migration;

/**
 * Class m190921_050325_alter_monthly_form_submit
 */
class m190921_050325_alter_monthly_form_submit extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('monthly_form_submit', 'is_late', $this->boolean()->notNull()->defaultValue(false));
        $this->addCommentOnColumn('monthly_form_submit', 'is_late', 'ยืนยันช้าเกินกำหนด');
        $this->createIndex('idx_monthly_form_submit_is_late', 'monthly_form_submit', 'is_late');
        
        $this->addColumn('six_month_form', 'is_late', $this->boolean()->notNull()->defaultValue(false));
        $this->addCommentOnColumn('six_month_form', 'is_late', 'ยืนยันช้าเกินกำหนด');
        $this->createIndex('idx_six_month_form_is_late', 'six_month_form', 'is_late');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('six_month_form', 'is_late');
        $this->dropColumn('monthly_form_submit', 'is_late');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190921_050325_alter_monthly_form_submit cannot be reverted.\n";

      return false;
      }
     */
}
