<?php

use yii\db\Migration;

/**
 * Class m190922_061424_insert_assessment_score
 */
class m190922_061424_insert_assessment_score extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->batchInsert('assessment_score', ['field', 'min_percent', 'max_percent', 'score'], [
            ['garbage_percent', 0, 0.99, 1],
            ['garbage_percent', 1, 1.49, 2],
            ['garbage_percent', 1.5, 1.99, 3],
            ['garbage_percent', 2, 2.49, 4],
            ['garbage_percent', 2.5, 2.99, 5],
            ['garbage_percent', 3, 3.49, 6],
            ['garbage_percent', 3.5, 3.99, 7],
            ['garbage_percent', 4, 4.49, 8],
            ['garbage_percent', 4.5, 4.99, 9],
            ['garbage_percent', 5, null, 10],
            ['bag_percent', 0, 1.99, 1],
            ['bag_percent', 2, 2.99, 2],
            ['bag_percent', 3, 3.99, 3],
            ['bag_percent', 4, 4.99, 4],
            ['bag_percent', 5, 5.99, 5],
            ['bag_percent', 6, 6.99, 6],
            ['bag_percent', 7, 7.99, 7],
            ['bag_percent', 8, 8.99, 8],
            ['bag_percent', 9, 9.99, 9],
            ['bag_percent', 10, null, 10],
            ['cup_percent', 0, 1.99, 1],
            ['cup_percent', 2, 2.99, 2],
            ['cup_percent', 3, 3.99, 3],
            ['cup_percent', 4, 4.99, 4],
            ['cup_percent', 5, 5.99, 5],
            ['cup_percent', 6, 6.99, 6],
            ['cup_percent', 7, 7.99, 7],
            ['cup_percent', 8, 8.99, 8],
            ['cup_percent', 9, 9.99, 9],
            ['cup_percent', 10, null, 10],
            ['foam_percent', 0, 19.99, 1],
            ['foam_percent', 20, 29.99, 2],
            ['foam_percent', 30, 39.99, 3],
            ['foam_percent', 40, 49.99, 4],
            ['foam_percent', 50, 59.99, 5],
            ['foam_percent', 60, 69.99, 6],
            ['foam_percent', 70, 79.99, 7],
            ['foam_percent', 80, 89.99, 8],
            ['foam_percent', 90, 99.99, 9],
            ['foam_percent', 100, null, 10],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->delete('assessment_score');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190922_061424_insert_assessment_score cannot be reverted.\n";

      return false;
      }
     */
}
