<?php

use yii\db\Migration;

/**
 * Class m190922_095042_alter_six_month_form
 */
class m190922_095042_alter_six_month_form extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('six_month_form', 'score', $this->decimal(18, 2));
        $this->addColumn('six_month_form', 'plus_score', $this->decimal(18, 2));
        $this->addColumn('six_month_form', 'minus_score', $this->decimal(18, 2));
        $this->addColumn('six_month_form', 'total_score', $this->decimal(18, 2));
        $this->addColumn('six_month_form', 'assessed_by', $this->integer());
        $this->addColumn('six_month_form', 'assessed_at', $this->dateTime());
        
        $this->addCommentOnColumn('six_month_form', 'score', 'คะแนน');
        $this->addCommentOnColumn('six_month_form', 'plus_score', 'บวกคะแนน');
        $this->addCommentOnColumn('six_month_form', 'minus_score', 'ลบคะแนน');
        $this->addCommentOnColumn('six_month_form', 'total_score', 'คะแนนรวม');
        $this->addCommentOnColumn('six_month_form', 'assessed_by', 'ผู้ประเมิน');
        $this->addCommentOnColumn('six_month_form', 'assessed_at', 'วันที่ประเมิน');
        
        $this->addForeignKey('fk_six_month_form_assessed_by', 'six_month_form', 'assessed_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropForeignKey('fk_six_month_form_assessed_by', 'six_month_form');
        $this->dropColumn('six_month_form', 'assessed_at');
        $this->dropColumn('six_month_form', 'assessed_by');
        $this->dropColumn('six_month_form', 'total_score');
        $this->dropColumn('six_month_form', 'minus_score');
        $this->dropColumn('six_month_form', 'plus_score');
        $this->dropColumn('six_month_form', 'score');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190922_095042_alter_six_month_form cannot be reverted.\n";

      return false;
      }
     */
}
