<?php

use yii\db\Migration;

/**
 * Class m190929_042300_alter_assessment_score
 */
class m190929_042300_alter_assessment_score extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('assessment_score', 'year', $this->integer());
        $this->createIndex('idx_assessment_score_year', 'assessment_score', 'year');
        
        $this->update('assessment_score', ['year' => 2562]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('assessment_score', 'year');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190929_042300_alter_assessment_score cannot be reverted.\n";

      return false;
      }
     */
}
