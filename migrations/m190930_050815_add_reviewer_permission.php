<?php

use yii\db\Migration;

/**
 * Class m190930_050815_add_reviewer_permission
 */
class m190930_050815_add_reviewer_permission extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->insert('role', ['id' => 4, 'name' => 'Reviewer']);
        $auth = Yii::$app->authManager;

        $roleA = $auth->getRole('Reviewer');
        if (!isset($roleA)) {
            $roleA = $auth->createRole('Reviewer');
            $auth->add($roleA);
        }
        $permissionsA = [
            'site.index',
            'office.yearly-assessment-report',
            'office.six-month-assessment-report',
        ];
        foreach ($permissionsA as $permA) {
            $pA = $auth->getPermission(\Yii::$app->id . ".{$permA}");
            if (!isset($pA)) {
                $pA = $auth->createPermission(\Yii::$app->id . ".{$permA}");
                $auth->add($pA);
            }
            if ($auth->canAddChild($roleA, $pA)) {
                $auth->addChild($roleA, $pA);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $auth = Yii::$app->authManager;
        $roleA = $auth->getRole('Reviewer');

        $permissionsA = [
            'site.index',
            'office.yearly-assessment-report',
            'office.six-month-form-report',
        ];
        foreach ($permissionsA as $permR) {
            $pR = $auth->getPermission(\Yii::$app->id . ".{$permR}");
            if (isset($pR)) {
                $auth->removeChild($roleA, $pR);
            }
        }
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190930_050815_add_reviewer_permission cannot be reverted.\n";

      return false;
      }
     */
}
