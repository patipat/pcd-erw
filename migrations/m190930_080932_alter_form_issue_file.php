<?php

use yii\db\Migration;

/**
 * Class m190930_080932_alter_form_issue_file
 */
class m190930_080932_alter_form_issue_file extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createIndex('idx_form_issue_file_file_name', 'form_issue_file', 'file_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropIndex('idx_form_issue_file_file_name', 'form_issue_file');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190930_080932_alter_form_issue_file cannot be reverted.\n";

      return false;
      }
     */
}
