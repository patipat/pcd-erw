<?php

use yii\db\Migration;

/**
 * Class m190930_091013_update_permission
 */
class m190930_091013_update_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $auth = Yii::$app->authManager;

        $roleA = $auth->getRole('Administrator');
        $roleS = $auth->getRole('Staff');
        $permissionsA = [
            'six-month-form.resubmit',
        ];

        foreach ($permissionsA as $permA) {
            $pA = $auth->getPermission(\Yii::$app->id . ".{$permA}");
            if (!isset($pA)) {
                $pA = $auth->createPermission(\Yii::$app->id . ".{$permA}");
                $auth->add($pA);
            }
            if ($auth->canAddChild($roleA, $pA)) {
                $auth->addChild($roleA, $pA);
            }
            if ($auth->canAddChild($roleS, $pA)) {
                $auth->addChild($roleS, $pA);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $auth = Yii::$app->authManager;
        $roleA = $auth->getRole('Administrator');
        $roleS = $auth->getRole('Staff');
        $permissionsA = [
            'six-month-form.resubmit',
        ];

        foreach ($permissionsA as $permR) {
            $pR = $auth->getPermission(\Yii::$app->id . ".{$permR}");
            if (isset($pR)) {
                $auth->removeChild($roleA, $pR);
                $auth->removeChild($roleS, $pR);
            }
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190930_091013_update_permission cannot be reverted.\n";

        return false;
    }
    */
}
