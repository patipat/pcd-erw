<?php

use yii\db\Migration;

/**
 * Class m191001_030831_add_data_content
 */
class m191001_030831_add_data_content extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->batchInsert('content_category', ['name',], [
            ['คู่มือ'],
            ['โปสเตอร์'],
            ['ไฟล์วีดีโอ'],
        ]);
        $this->batchInsert('content_group', ['name',], [
            ['ดาวน์โหลด'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m191001_030831_add_data_content cannot be reverted.\n";

      return false;
      }
     */
}
