<?php

use yii\db\Migration;

/**
 * Class m191002_020954_create_line_queue
 */
class m191002_020954_create_line_queue extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('line_queue', [
            'id' => $this->primaryKey(),
            'token' => $this->string(),
            'sent_at' => $this->dateTime(),
            'message' => $this->text(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('line_queue', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('line_queue', 'sent_at', 'ส่งเมล์เมื่อ');
        $this->addCommentOnColumn('line_queue', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('line_queue', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('line_queue', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('line_queue', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('line_queue', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_line_queue_token', 'line_queue', 'token');
        $this->createIndex('idx_line_queue_sent_at', 'line_queue', 'sent_at');
        $this->addForeignKey('fk_line_queue_created_by', 'line_queue', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_line_queue_updated_by', 'line_queue', 'updated_by', 'user', 'id');

    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('line_queue');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m191002_020954_create_line_queue cannot be reverted.\n";

      return false;
      }
     */
}
