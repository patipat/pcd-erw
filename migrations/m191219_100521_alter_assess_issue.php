<?php

use yii\db\Migration;

/**
 * Class m191219_100521_alter_assess_issue
 */
class m191219_100521_alter_assess_issue extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('assess_issue', 'year', 'integer');
        $this->addCommentOnColumn('assess_issue', 'year', 'ปีงบประมาณ');
        $this->createIndex('idx_assess_issue_year', 'assess_issue', 'year');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('assess_issue', 'year');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m191219_100521_alter_assess_issue cannot be reverted.\n";

      return false;
      }
     */
}
