<?php

use yii\db\Migration;

/**
 * Class m210128_090240_add_monthly_form_file
 */
class m210128_090240_add_monthly_form_file extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('monthly_file', [
            'id' => $this->primaryKey(),
            'office_id' => $this->integer(),
            'year' => $this->integer(),
            'month' => $this->integer(),
            'name' => $this->string(),
            'file_name' => $this->string(),
            'deleted' => $this->boolean()->notNull()->defaultValue(FALSE),
            'created_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addCommentOnColumn('monthly_file', 'id', 'รหัสอัตโนมัติ');
        $this->addCommentOnColumn('monthly_file', 'office_id', 'หน่วยงาน');
        $this->addCommentOnColumn('monthly_file', 'year', 'ปี');
        $this->addCommentOnColumn('monthly_file', 'month', 'เดือน');
        $this->addCommentOnColumn('monthly_file', 'name', 'ชื่อเอกสาร/รูปภาพ');
        $this->addCommentOnColumn('monthly_file', 'file_name', 'ชื่อไฟล์ที่บันทึกในระบบ');
        $this->addCommentOnColumn('monthly_file', 'deleted', '0=ใช้งาน,1=ไม่ใช้งาน');
        $this->addCommentOnColumn('monthly_file', 'created_by', 'สร้างโดย');
        $this->addCommentOnColumn('monthly_file', 'created_at', 'สร้างเมื่อ');
        $this->addCommentOnColumn('monthly_file', 'updated_by', 'ปรับปรุงโดย');
        $this->addCommentOnColumn('monthly_file', 'updated_at', 'ปรับปรุงเมื่อ');

        $this->createIndex('idx_monthly_file_name', 'monthly_file', ['name']);
        $this->createIndex('idx_monthly_file_year', 'monthly_file', ['year']);
        $this->createIndex('idx_monthly_file_month', 'monthly_file', ['month']);
        
        $this->addForeignKey('fk_monthly_file_office_id', 'monthly_file', 'office_id', 'office', 'id');
        $this->addForeignKey('fk_monthly_file_user_created_by', 'monthly_file', 'created_by', 'user', 'id');
        $this->addForeignKey('fk_monthly_file_user_updated_by', 'monthly_file', 'updated_by', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('monthly_file');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210128_090240_add_monthly_form_file cannot be reverted.\n";

        return false;
    }
    */
}
