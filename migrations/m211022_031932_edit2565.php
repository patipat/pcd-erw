<?php

use yii\db\Migration;

/**
 * Class m211022_031932_edit2565
 */
class m211022_031932_edit2565 extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->addColumn('monthly_form', 'mask', $this->decimal(18, 2));
        $this->addColumn('monthly_form_submit', 'mask', $this->decimal(18, 2));
        $this->addColumn('yearly_assessment', 'mask', $this->decimal(18, 2));
        $this->addColumn('yearly_assessment', 'mask_score', $this->decimal(18, 2));
        $this->addColumn('yearly_assessment', 'mask_percent', $this->decimal(18, 2));
        $this->addColumn('yearly_assessment', 'mask_cur', $this->decimal(18, 2));
        $this->addColumn('yearly_assessment', 'mask_prev', $this->decimal(18, 2));

        $this->addCommentOnColumn('monthly_form', 'mask', 'จำนวนหน้ากากอนามัย (ชิ้น)');
        $this->addCommentOnColumn('monthly_form_submit', 'mask', 'จำนวนหน้ากากอนามัย (ชิ้น)');
        $this->addCommentOnColumn('yearly_assessment', 'mask', 'จำนวนหน้ากากอนามัย (ชิ้น)');
        $this->addCommentOnColumn('yearly_assessment', 'mask_score', 'mask_score');
        $this->addCommentOnColumn('yearly_assessment', 'mask_percent', 'mask_percent');
        $this->addCommentOnColumn('yearly_assessment', 'mask_cur', 'mask_cur');
        $this->addCommentOnColumn('yearly_assessment', 'mask_prev', 'mask_prev');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropColumn('monthly_form', 'mask');
        $this->dropColumn('monthly_form_submit', 'mask');
        $this->dropColumn('yearly_assessment', 'mask');
        $this->dropColumn('yearly_assessment', 'mask_score');
        $this->dropColumn('yearly_assessment', 'mask_percent');
        $this->dropColumn('yearly_assessment', 'mask_cur');
        $this->dropColumn('yearly_assessment', 'mask_prev');

    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m211022_031932_edit2565 cannot be reverted.\n";

      return false;
      }
     */
}
