<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "alert".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property string $message message
 * @property int $is_acknowledged acknowledged
 * @property int $acknowledged_by รับทราบโดย
 * @property string $acknowledged_at รับทราบเมื่อ
 * @property int $user_id user
 * @property string $created_at สร้างเมื่อ
 *
 * @property User $acknowledgedBy
 * @property User $user
 */
class Alert extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'alert';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['message'], 'string'],
            [['is_acknowledged', 'acknowledged_by', 'user_id'], 'integer'],
            [['acknowledged_at', 'created_at'], 'safe'],
            [['acknowledged_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['acknowledged_by' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'message' => Yii::t('app', 'ข้อความ'),
            'is_acknowledged' => Yii::t('app', 'รับทราบ?'),
            'acknowledged_by' => Yii::t('app', 'รับทราบโดย'),
            'acknowledged_at' => Yii::t('app', 'รับทราบเมื่อ'),
            'user_id' => Yii::t('app', 'user'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcknowledgedBy() {
        return $this->hasOne(User::className(), ['id' => 'acknowledged_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return AlertQuery the active query used by this AR class.
     */
    public static function find() {
        return new AlertQuery(get_called_class());
    }

    public static function buildAlertItems($alerts) {
        $items = [];
        if (count($alerts) > 0) {
            $items[] = [
                'label' => '<div class="text-center"><i class="icon md-check"></i> ' . \Yii::t('app', 'รับทราบทั้งหมด') . '</div>',
                'url' => 'javascript:acknowledgeAll()',
                'options' => ['class' => 'dropdown-menu-footer'],
            ];
        }
        $items[] = [
            'label' => \Yii::t('app', 'แสดงทั้งหมด'),
            'url' => Url::to(['alert/index']),
            'options' => ['class' => 'dropdown-menu-footer'],
        ];
        foreach ($alerts as $alert) {
            $iconColor = "bg-success";
            $body = '<h6 class="media-heading" style="white-space:normal">' . $alert->message . '</h6>
                          <time class="media-meta">' . \Yii::$app->formatter->asDate($alert->created_at) . '</time>';
            $itemClass = $alert->is_acknowledged ? "bg-grey-300" : "";
            if (!$alert->is_acknowledged) {
                $js = "event.preventDefault();acknowledge({$alert->id});";
                $iconColor = "bg-danger";
//                if (isset($alert->submission_id)) {
//                    $url = Url::to(['submission/project-submission', 'submissionId' => $alert->submission_id]);
//                    $js .= "window.open('{$url}', '_blank');";
//                }
                $body .= \yii\helpers\Html::tag('div', \yii\helpers\Html::button('<i class="icon md-check"></i> ' . \Yii::t('app', 'รับทราบ'), ['class' => 'btn btn-xs btn-primary', 'onclick' => $js]), [
                            'class' => 'pull-right'
                ]);
            }

            $items[] = [
                'label' => '
                    
                      <div class="media" style="border-top: 1px solid #e0e0e0">
                        <div class="media-left padding-right-10">
                          <i class="icon md-notifications ' . $iconColor . ' white icon-circle" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          ' . $body . '
                        </div>
                      </div>',
                'url' => "javascript:void(0)",
//                                    'encode' => FALSE,
                'linkOptions' => ['class' => "list-group-item {$itemClass}", 'role' => 'menuitem'],
            ];
        }

        return $items;
    }

    public static function addMonthlyConfirmAlert($monthlyFormSubmit) {
        $message = "หน่วยงาน {0} ส่งรายงานประจำเดือน {1} ปี {2} เมื่อวันที่ {3} กรุณายืนยันข้อมูล";
        $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role(Role::ADMINISTRATOR)->office($monthlyFormSubmit->office_id)->all();
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = \Yii::t('app', $message, [
                        $monthlyFormSubmit->office->name,
                        $monthlyFormSubmit->monthName,
                        $monthlyFormSubmit->year,
                        Yii::$app->thaiFormatter->asDateTime($monthlyFormSubmit->submitted_at, 'php:d/m/Y H:i'),
            ]);

            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
        }
    }

    public static function addMonthlyCheckAlert($monthlyFormSubmit) {
        $message = "หน่วยงาน {0} ยืนยันรายงานประจำเดือน {1} ปี {2} เมื่อวันที่ {3} กรุณาตรวจสอบข้อมูล";
        $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
        if ($monthlyFormSubmit->office->level > $level) {
            $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role(Role::ADMINISTRATOR)->office($monthlyFormSubmit->office->parent_id)->all();
        } else {
            $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role(Role::SUPER_ADMINISTRATOR)->all();
        }
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = \Yii::t('app', $message, [
                        $monthlyFormSubmit->office->name,
                        $monthlyFormSubmit->monthName,
                        $monthlyFormSubmit->year,
                        Yii::$app->thaiFormatter->asDateTime($monthlyFormSubmit->confirmed_at, 'php:d/m/Y H:i'),
            ]);

            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
        }
    }

    public static function addMonthlyFailAlert($monthlyFormSubmit) {
        $message = "รายงานประจำเดือน {0} ปี {1} ของหน่วยงานท่านถูกตีกลับ";
        $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR, Role::STAFF])->office($monthlyFormSubmit->office_id)->all();
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = \Yii::t('app', $message, [
                        $monthlyFormSubmit->monthName,
                        $monthlyFormSubmit->year,
            ]);

            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
        }
    }

    public static function addMonthlyLateConfirm($office, $year, $month) {
        $message = "หน่วยงานของท่านยังไม่ยืนยันรายงานประจำเดือน {0} ปี {1}";
        $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR, Role::STAFF])->office($office->id)->all();
        $date = new \DateTime("{$year}-{$month}-01");
//        \yii\helpers\VarDumper::dump("{$year}-{$month}");
//        \yii\helpers\VarDumper::dump($date);
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = \Yii::t('app', $message, [
                        \Yii::$app->formatter->asDate($date->format('Y-m-d'), 'php:F'),
                        $date->format('Y'),
            ]);

            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
        }
    }

    public static function buildChildrenMonthlyLateMessage($offices, $year, $month) {
        $subject = "หน่วยงานภายใต้สังกัดของท่านที่ยังไม่ยืนยันรายงานประจำเดือน {0} ปี {1}";
        $message = "{$subject}<br>";
        $date = new \DateTime("{$year}-{$month}-01");
        foreach ($offices as $office) {
            $message .= "- {$office->name}<br>";
        }
        $subject = \Yii::t('app', $subject, [
                    \Yii::$app->formatter->asDate($date->format('Y-m-d'), 'php:F'),
                    $date->format('Y'),
        ]);
        $message = Yii::t('app', $message, [
                    \Yii::$app->formatter->asDate($date->format('Y-m-d'), 'php:F'),
                    $date->format('Y'),
        ]);
        return [
            'subject' => $subject,
            'message' => $message,
        ];
    }

    public static function buildChildrenSixMonthLateMessage($offices, $year) {
        $subject = "หน่วยงานภายใต้สังกัดของท่านที่ยังไม่ยืนยันรายงาน 6 เดือน ปี {0}";
        $message = "{$subject}<br>";
        foreach ($offices as $office) {
            $message .= "- {$office->name}<br>";
        }
        $subject = \Yii::t('app', $subject, [
                    $year,
        ]);
        $message = Yii::t('app', $message, [
                    $year
        ]);
        return [
            'subject' => $subject,
            'message' => $message,
        ];
    }

    public static function addChildrenMonthlyLateSubmit($people, $offices, $year, $month) {
//        $subject = "หน่วยงานภายใต้สังกัดของท่านที่ยังไม่ยืนยันรายงานประจำเดือน {0} ปี {1}";
        $message = Alert::buildChildrenMonthlyLateMessage($offices, $year, $month);
//        $subject = \Yii::t('app', $subject, [
//                    \Yii::$app->formatter->asDate($date->format('Y-m-d'), 'php:F'),
//                    $date->format('Y'),
//        ]);
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = $message['message'];
            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
            if (isset($person->email)) {
                EmailQueue::addQueue($person->user_id, $message['subject'], $alert->message);
            }
        }
    }

    public static function addSixMonthConfirmAlert($sixMonthForm) {
        $message = "หน่วยงาน {0} ส่งรายงาน 6 เดือน {1} ปี {1} เมื่อวันที่ {2} กรุณายืนยันข้อมูล";
        $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role(Role::ADMINISTRATOR)->office($sixMonthForm->office_id)->all();
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = \Yii::t('app', $message, [
                        $sixMonthForm->office->name,
                        $sixMonthForm->year,
                        Yii::$app->thaiFormatter->asDateTime($sixMonthForm->submitted_at, 'php:d/m/Y H:i'),
            ]);

            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
        }
    }

    public static function addSixMonthCheckAlert($sixMonthForm) {
        $message = "หน่วยงาน {0} ยืนยันรายงาน 6 เดือน ปี {1} เมื่อวันที่ {2} กรุณาตรวจสอบข้อมูล";
        $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
        if ($sixMonthForm->office->level > $level) {
            $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role(Role::ADMINISTRATOR)->office($sixMonthForm->office->parent_id)->all();
        } else {
            $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role(Role::SUPER_ADMINISTRATOR)->all();
        }
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = \Yii::t('app', $message, [
                        $sixMonthForm->office->name,
                        $sixMonthForm->year,
                        Yii::$app->thaiFormatter->asDateTime($sixMonthForm->confirmed_at, 'php:d/m/Y H:i'),
            ]);

            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
        }
    }

    public static function addSixMonthFailAlert($sixMonthForm) {
        $message = "รายงาน 6 เดือน ปี {0} ของหน่วยงานท่านถูกตีกลับ";
        $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR, Role::STAFF])->office($sixMonthForm->office_id)->all();
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = \Yii::t('app', $message, [
                        $sixMonthForm->year,
            ]);

            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
        }
    }

    public static function addSixMonthLateConfirm($office, $year) {
        $message = "หน่วยงานของท่านยังไม่ยืนยันรายงาน 6 เดือน ปี {0}";
        $people = Person::find()->joinWith(['personRoles'])->isDeleted(false)->role([Role::ADMINISTRATOR, Role::STAFF])->office($office->id)->all();
//        \yii\helpers\VarDumper::dump("{$year}-{$month}");
//        \yii\helpers\VarDumper::dump($date);
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = \Yii::t('app', $message, [
                        $year,
            ]);

            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
        }
    }

    public static function addChildrenSixMonthLateSubmit($people, $offices, $year) {
//        $subject = "หน่วยงานภายใต้สังกัดของท่านที่ยังไม่ยืนยันรายงาน 6 เดือน ปี {0}";
        $message = Alert::buildChildrenSixMonthLateMessage($offices, $year);
//        foreach ($offices as $office) {
//            $message .= "- {$office->name}<br>";
//        }
//        $subject = \Yii::t('app', $subject, [
//                    $year,
//        ]);
        foreach ($people as $person) {
            $alert = new Alert();
            $alert->message = $message['message'];

            $alert->user_id = $person->user_id;
            $alert->save(FALSE);
            if (isset($person->email)) {
                EmailQueue::addQueue($person->user_id, $message['subject'], $alert->message);
            }
        }
    }

}
