<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Alert;

/**
 * AlertSearch represents the model behind the search form about `app\models\Alert`.
 */
class AlertSearch extends Alert {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'is_acknowledged', 'acknowledged_by', 'user_id'], 'integer'],
            [['message', 'acknowledged_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Alert::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
                        'sort' => ['defaultOrder' => ['id' => SORT_DESC]]

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_acknowledged' => $this->is_acknowledged,
            'acknowledged_by' => $this->acknowledged_by,
            'acknowledged_at' => $this->acknowledged_at,
            'created_at' => $this->created_at,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }

}
