<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "amphur".
 *
 * @property integer $id
 * @property integer $province_id
 * @property string $name
 * @property integer $deleted
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $name_eng ชื่ออำเภอภาษาอังกฤษ
 *
 * @property Province $province
 * @property User $createdBy
 * @property User $updatedBy
 * @property Company[] $companies
 * @property Person[] $person
 * @property Station[] $stations
 * @property Tambon[] $tambons
 * 
 */
class Amphur extends \yii\db\ActiveRecord {

    public $amphurName;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'amphur';
    }

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'createdByUserProfile.fullName',
            'updatedByUserProfile.fullName',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'province_id', 'deleted', 'created_by', 'updated_by'], 'integer'],
                [['id', 'name', 'province_id'], 'required'],
                [['id', 'name'], 'unique'],
                [['created_at', 'updated_at'], 'safe'],
                [['name', 'name_eng'], 'string', 'max' => 255],
                [['id'], 'unique', 'filter' => ['deleted' => 0]],
                [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']],
                [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
                [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอำเภอ'),
            'province_id' => Yii::t('app', 'รหัสจังหวัด'),
            'name' => Yii::t('app', 'ชื่ออำเภอ'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'updatedByUserProfile.fullName' => Yii::t('app', 'ผู้แก้ไขข้อมูล'),
            'name_eng' => Yii::t('app', 'ชื่ออำเภอภาษาอังกฤษ'),
        ];
    }

    public function behaviors() {
        return [
                [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
                [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince() {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies() {
        return $this->hasMany(Company::className(), ['province_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson() {
        return $this->hasMany(Person::className(), ['province_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by'])
                        ->from(['uu' => User::tableName()]);
    }

    public function getCreatedByUserProfile() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('createdBy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByUserProfile() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('updatedBy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStations() {
        return $this->hasMany(Station::className(), ['province_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTambons() {
        return $this->hasMany(Tambon::className(), ['amphur_id' => 'id']);
    }

    public function getAmphurName() {
        return $this->province_id == 10 ? "เขต{$this->name}" : "อำเภอ{$this->name}";
    }

    /**
     * @inheritdoc
     * @return AmphurQuery the active query used by this AR class.
     */
    public static function find() {
        return new AmphurQuery(get_called_class());
    }

}
