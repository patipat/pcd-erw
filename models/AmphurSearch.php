<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Amphur;

/**
 * AmphurSearch represents the model behind the search form about `app\models\Amphur`.
 */
class AmphurSearch extends Amphur {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'province_id', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['name', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Amphur::find();
//        $query->joinWith(['createdByEmployee createdByEmployee', 'updatedByEmployee updatedByEmployee']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider->sort->attributes['name'] = [
            'asc' => ['CONVERT(amphur.name USING TIS620)' => SORT_ASC],
            'desc' => ['CONVERT(amphur.name USING TIS620)' => SORT_DESC],
        ];


        $query->andFilterWhere([
            'amphur.id' => $this->id,
            'amphur.province_id' => $this->province_id,
            'amphur.deleted' => $this->deleted,
            'amphur.created_by' => $this->created_by,
            'amphur.created_at' => $this->created_at,
            'amphur.updated_by' => $this->updated_by,
            'amphur.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'amphur.name', $this->name]);

        return $dataProvider;
    }

}
