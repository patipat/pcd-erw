<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AssessIssue]].
 *
 * @see AssessIssue
 */
class AssessIssueQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return AssessIssue[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * {@inheritdoc}
     * @return AssessIssue|array|null
     */
    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['assess_issue.deleted' => $deleted]);
    }

    /**
     * {@inheritdoc}
     * @return AssessIssue|array|null
     */
    public function year($year) {
        return $this->andWhere(['assess_issue.year' => $year]);
    }

}
