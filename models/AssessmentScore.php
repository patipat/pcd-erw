<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "assessment_score".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property string $field field
 * @property string $min_percent min
 * @property string $max_percent max
 * @property string $score score
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 * @property int $year ปีงบประมาณ
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class AssessmentScore extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'assessment_score';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['field'], 'required'],
            [['min_percent', 'max_percent', 'score'], 'number'],
            [['deleted', 'created_by', 'updated_by', 'year'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['field'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'field' => Yii::t('app', 'field'),
            'min_percent' => Yii::t('app', 'min'),
            'max_percent' => Yii::t('app', 'max'),
            'score' => Yii::t('app', 'score'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'year' => Yii::t('app', 'ปีงบประมาณ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return AssessmentScoreQuery the active query used by this AR class.
     */
    public static function find() {
        return new AssessmentScoreQuery(get_called_class());
    }

    public static function getScore($field, $percent, $year) {
        $as = AssessmentScore::find()->isDeleted(false)->field($field)->percent($percent)->year($year)->one();
        return isset($as) ? $as->score : 0;
    }
}
