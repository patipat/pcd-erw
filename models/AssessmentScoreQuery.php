<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AssessmentScore]].
 *
 * @see AssessmentScore
 */
class AssessmentScoreQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return AssessmentScore[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AssessmentScore|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['assessment_score.deleted' => $deleted]);
    }
    
    public function field($field) {
        return $this->andWhere(['assessment_score.field' => $field]);
    }
    
    public function year($year) {
        return $this->andWhere(['assessment_score.year' => $year]);
    }
    
    public function percent($percent) {
        return $this->andWhere(['<=', 'assessment_score.min_percent', $percent])
                ->andWhere(['or'
                        , ['>=', 'assessment_score.max_percent', $percent]
                        , ['assessment_score.max_percent' => null]
                    ]);
    }
}
