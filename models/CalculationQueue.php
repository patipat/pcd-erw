<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "calculation_queue".
 *
 * @property int $id
 * @property int $type ประเภทการคำนวณ
 * @property int $office_id หน่วยงาน
 * @property int $year ปี
 * @property int $month เดือน
 * @property string $completed_at
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property User $createdBy
 * @property Office $office
 * @property User $updatedBy
 */
class CalculationQueue extends \yii\db\ActiveRecord {

    const TYPE_YEARLY_ASSESSMENT = 1;
    const TYPE_MONTHLY_PROGRESS = 2;
    const TYPE_PARENT_MONTHLY_FORM = 3;
    const TYPE_PARENT_SIX_MONTH_FORM = 4;
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'calculation_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['type', 'office_id', 'year', 'deleted', 'created_by', 'updated_by', 'month'], 'integer'],
            [['completed_at', 'created_at', 'updated_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'ประเภทการคำนวณ'),
            'office_id' => Yii::t('app', 'หน่วยงาน'),
            'year' => Yii::t('app', 'ปี'),
            'month' => Yii::t('app', 'เดือน'),
            'completed_at' => Yii::t('app', 'Completed At'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice() {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    
    public function calculate() {
        if ($this->type == self::TYPE_YEARLY_ASSESSMENT) {
            $this->calculateYearlyAssessment();
            $this->completed_at = date('Y-m-d H:i:s');
            $this->save();
        } else if ($this->type == self::TYPE_MONTHLY_PROGRESS) {
            $this->calculateMonthlyProgress();
            $this->completed_at = date('Y-m-d H:i:s');
            $this->save();
        } else if ($this->type == self::TYPE_PARENT_MONTHLY_FORM) {
            $this->calculateParentMonthlyForm();
            $this->completed_at = date('Y-m-d H:i:s');
            $this->save();
        } else if ($this->type == self::TYPE_PARENT_SIX_MONTH_FORM) {
            $this->completed_at = date('Y-m-d H:i:s');
            $this->save();
            $this->calculateParentSixMonthForm();
        }
    }
    
    public function calculateYearlyAssessment() {
        $this->office->calculateYearlyAssessment($this->year);
    }
    
    public function calculateMonthlyProgress() {
        $this->office->calculateMonthlyProgress($this->year, $this->month);
    }
    
    public function calculateParentMonthlyForm() {
        $this->office->calculateParentMonthlyForm($this->year, $this->month);
    }
    
    public function calculateParentSixMonthForm() {
        $this->office->calculateParentSixMonthForm($this->year);
    }

    /**
     * {@inheritdoc}
     * @return CalculationQueueQuery the active query used by this AR class.
     */
    public static function find() {
        return new CalculationQueueQuery(get_called_class());
    }
    
    public static function addQueue($type, $officeId, $year, $month=null) {
        $model = new CalculationQueue();
        $model->type = $type;
        $model->office_id = $officeId;
        $model->year = $year;
        $model->month = $month;
        $model->save();
    }
    
    public static function runCalculateCmd() {
        \Yii::$app->util->execInBackground(\Yii::$app->params['calculateCmd']);
    }

}
