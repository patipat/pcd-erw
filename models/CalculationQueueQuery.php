<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CalculationQueue]].
 *
 * @see CalculationQueue
 */
class CalculationQueueQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return CalculationQueue[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CalculationQueue|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['calculation_queue.deleted' => $deleted]);
    }

    public function office($officeId) {
        return $this->andWhere(['calculation_queue.office_id' => $officeId]);
    }

    public function year($year) {
        return $this->andWhere(['calculation_queue.year' => $year]);
    }
    
    public function type($type) {
        return $this->andWhere(['calculation_queue.type' => $type]);
    }
    
    public function isCompleted($completed=true) {
        if ($completed) {
            return $this->andWhere(['not', ['calculation_queue.completed_at' => null]]);
        } else {
            return $this->andWhere(['calculation_queue.completed_at' => null]);
        }
    }

}
