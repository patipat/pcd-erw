<?php

namespace app\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "content".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $content_group_id กลุ่มบทความ
 * @property int $content_category_id ประเภทบทความ
 * @property string $title ชื่อเรื่อง
 * @property string $intro_text ข้อความเริ่มต้น
 * @property string $main_text ข้อความหลัก
 * @property string $image รูปภาพโชว์หน้าแรก
 * @property int $status สถานะข่าว
 * @property string $file_name ไฟล์แนบ
 * @property int $view ตำแหน่งที่ให้แสดงในกรณีที่เป็นข่าว
 * @property int $public สถานะข่าว
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property ContentCategory $contentCategory
 * @property ContentGroup $contentGroup
 * @property User $createdBy
 * @property User $updatedBy
 */
class Content extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    const NEWS = 1;
    const GENERAL = 2;
    const MEASURE = 3;
    const ACADAMIC = 4;
    const DOWNLOAD = 5;
    const NEWS_SLIDER = 1;
    const NEWS_INDEX = 2;

    public static function getContentLabel() {
        return[
            self::NEWS => Yii::t('app', 'ข่าวประชาสัมพันธ์'),
            self::GENERAL => Yii::t('app', 'บทความทั่วไป'),
            self::MEASURE => Yii::t('app', 'บทความมาตรการ'),
            self::ACADAMIC => Yii::t('app', 'บทความทางวิชาการ'),
            self::DOWNLOAD => Yii::t('app', 'ดาวน์โหลด'),
        ];
    }

    public static function getNewsLabel() {
        return[
            self::NEWS_INDEX => Yii::t('app', 'ข่าว Index'),
            self::NEWS_SLIDER => Yii::t('app', 'ข่าว Slider'),
        ];
    }

    public static function tableName() {
        return 'content';
    }

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'createdByPerson.fullName',
            'updatedByPerson.fullName',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['title', 'content_group_id'], 'required'],
            [['content_group_id', 'content_category_id', 'status', 'view', 'public', 'deleted', 'created_by', 'updated_by'], 'integer'],
//            [['title'], 'required'],
            [['intro_text', 'main_text'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'image', 'file_name'], 'string', 'max' => 255],
            [['file_name'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,gif,pdf,doc,xls,docx,xlsx,ppt,pptx,rar,zip'],
            [['content_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContentCategory::className(), 'targetAttribute' => ['content_category_id' => 'id']],
            [['content_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ContentGroup::className(), 'targetAttribute' => ['content_group_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'content_group_id' => Yii::t('app', 'กลุ่มบทความ'),
            'content_category_id' => Yii::t('app', 'ประเภทบทความ'),
            'title' => Yii::t('app', 'ชื่อเรื่อง'),
            'intro_text' => Yii::t('app', 'ข้อความเริ่มต้น'),
            'main_text' => Yii::t('app', 'ข้อความหลัก'),
            'image' => Yii::t('app', 'รูปภาพโชว์หน้าแรก'),
            'status' => Yii::t('app', 'สถานะข่าว'),
            'file_name' => Yii::t('app', 'ไฟล์แนบ'),
            'view' => Yii::t('app', 'ตำแหน่งที่แสดงข่าว'),
            'public' => Yii::t('app', 'สถานะข่าว'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'updatedByPerson.fullName' => Yii::t('app', 'ผู้แก้ไขข้อมูล'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentCategory() {
        return $this->hasOne(ContentCategory::className(), ['id' => 'content_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentGroup() {
        return $this->hasOne(ContentGroup::className(), ['id' => 'content_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by'])
                        ->from(['uu' => User::tableName()]);
    }

    public function getCreatedByPerson() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('createdBy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByPerson() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('updatedBy');
    }

    public function getImageUrl() {
        return isset($this->image) ? \yii\helpers\Url::to('@web/uploads/content/images/' . $this->image) : null;
    }

    public function getFileUrl() {
        return isset($this->file_name) ? \yii\helpers\Url::to('web/uploads/content/files/' . $this->file_name) : null;
    }

    /**
     * {@inheritdoc}
     * @return ContentQuery the active query used by this AR class.
     */
    public static function find() {
        return new ContentQuery(get_called_class());
    }

}
