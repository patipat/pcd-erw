<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ContentCategory]].
 *
 * @see ContentCategory
 */
class ContentCategoryQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return ContentCategory[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ContentCategory|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['content_category.deleted' => $deleted]);
    }

}
