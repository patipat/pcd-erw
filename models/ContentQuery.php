<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Content]].
 *
 * @see Content
 */
class ContentQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Content[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Content|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['content.deleted' => $deleted]);
    }

    public function contentType($contentType) {
        return $this->andWhere(['content.content_group_id' => $contentType]);
    }

    public function viewNews($viewNews) {
        return $this->andWhere(['content.view' => $viewNews]);
    }

}
