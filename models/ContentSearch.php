<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Content;

/**
 * ContentSearch represents the model behind the search form about `app\models\Content`.
 */
class ContentSearch extends Content {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['id', 'content_group_id', 'content_category_id', 'status', 'view', 'created_by', 'updated_by'], 'integer'],
                [['title', 'intro_text', 'main_text', 'image', 'public', 'deleted', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Content::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'content_group_id' => $this->content_group_id,
            'content_category_id' => $this->content_category_id,
            'status' => $this->status,
            'view' => $this->view,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'intro_text', $this->intro_text])
                ->andFilterWhere(['like', 'main_text', $this->main_text])
                ->andFilterWhere(['like', 'image', $this->image])
//                ->andFilterWhere(['like', 'file', $this->file])
                ->andFilterWhere(['like', 'public', $this->public])
                ->andFilterWhere(['like', 'deleted', $this->deleted]);

        return $dataProvider;
    }

}
