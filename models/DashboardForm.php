<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class DashboardForm extends Model {

    public $officeType;
    public $level;
    public $year;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['officeType', 'level','year'], 'integer'],
            [['year'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'officeType' => Yii::t('app', 'ประเภทหน่วยงาน'),
            'level' => Yii::t('app', 'ระดับ'),
            'year' => Yii::t('app', 'ปี'),
        ];
    }
}
