<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\models\User;
use app\models\ProjectResearcher;
use app\models\Person;

/**
 * This is the model class for table "email_queue".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $model_id รหัสข้อมูลหลัก
 * @property int $type ประเภทอีเมล์
 * @property string $mail_at ส่งเมล์เมื่อ
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 * @property string $subject
 * @property string $message
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class EmailQueue extends \yii\db\ActiveRecord {

//    const TYPE_MONTHLY_LATE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'email_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['model_id', 'type', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['message'], 'string'],
            [['subject'], 'string', 'max' => 255],
            [['mail_at', 'created_at', 'updated_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'model_id' => Yii::t('app', 'รหัสข้อมูลหลัก'),
            'type' => Yii::t('app', 'ประเภทอีเมล์'),
            'mail_at' => Yii::t('app', 'ส่งเมล์เมื่อ'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     * @return EmailQueueQuery the active query used by this AR class.
     */
    public static function find() {
        return new EmailQueueQuery(get_called_class());
    }

    public static function addQueue($modelId, $subject=null, $message=null) {
        $model = new EmailQueue();
//        $model->type = $type;
        $model->model_id = $modelId;
        $model->subject = $subject;
        $model->message = $message;
        $res = $model->save();
//        if ($res) {
//            \Yii::$app->util->execInBackground(\Yii::$app->params['sendMailCmd']);
//        }
        return $res;
    }

    public function sendMail() {
        $res = false;
        $adminName = '=?utf-8?B?' . base64_encode(\Yii::$app->params['senderName']) . '?=';
        $user = User::findOne($this->model_id);
        $msg = Yii::$app->mailer->compose('message', [
            'message' => $this->message,
        ]);
        if (isset($user->person->email)) {
            $res = $msg->setSubject(\Yii::t('app', $this->subject))
                ->setFrom([\Yii::$app->params['adminEmail'] => $adminName])
                ->setTo($user->person->email)
                ->send();
        }
//        if (isset($user->person->line_notify_token)) {
//            $client = new \GuzzleHttp\Client();
//            $message = str_replace('<br>', "\n", $this->message);
//            $client->request('POST', 'https://notify-api.line.me/api/notify', [
//                'headers' => [
//                    'Content-Type' => "application/x-www-form-urlencoded",
//                    'Authorization' => "Bearer {$user->person->line_notify_token}"
//    //                'Authorization' => "Bearer 8zJIZgVsKL9O20QEVWf5dQ1P57gEZXwLOgpE4ZVasQg"
//    //                'Authorization' => "Bearer q6IyJZMnqLciOJGJiEFcW4TQAAJ7UWyCn7mYkKSiHGz" // ระบบทะเบียน
//                ],
//                'form_params' => [
//                    'message' => $message
//                ]
//            ]);
//        }
        if ($res) {
            $this->mail_at = date('Y-m-d H:i:s');
            $this->save(FALSE);
        }
        return $res;
    }

}
