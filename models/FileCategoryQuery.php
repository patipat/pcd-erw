<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FileCategory]].
 *
 * @see FileCategory
 */
class FileCategoryQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return FileCategory[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FileCategory|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['file_category.deleted' => $deleted]);
    }

}
