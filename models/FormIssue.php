<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "form_issue".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $six_month_form_id รายงานราย 6 เดือน
 * @property int $assess_issue_id assess issue
 * @property int $form_issue_submit_id
 * @property int $submitted_by ส่งล่าสุดโดย
 * @property string $submitted_at ส่งล่าสุดวันที่
 * @property int $confirmed_by ยืนยันล่าสุดโดย
 * @property string $confirmed_at ยืนยันล่าสุดวันที่
 * @property int $checked_by อนุมัติล่าสุดโดย
 * @property string $checked_at อนุมัติล่าสุดวันที่
 * @property int $is_pass ผ่าน/ไม่ผ่าน
 * @property string $score คะแนน
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 * @property string $comment Comment
 *
 * @property AssessIssue $assessIssue
 * @property FormIssueSubmit $formIssueSubmit
 * @property SixMonthForm $sixMonthForm
 * @property User $checkedBy
 * @property User $confirmedBy
 * @property User $createdBy
 * @property User $submittedBy
 * @property User $updatedBy
 * @property FormIssueFile[] $formIssueFiles
 */
class FormIssue extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    const IS_PASS = 1;
    const NO_PASS = 0;

    public static function getTypeLabel() {
        return[
            self::IS_PASS => '<i class="icon fa-check green-900"></i>',
            self::NO_PASS => '<i class="icon fa-close red-900"></i>',
        ];
    }

    public static function tableName() {
        return 'form_issue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['six_month_form_id', 'assess_issue_id', 'form_issue_submit_id', 'submitted_by', 'confirmed_by', 'checked_by', 'is_pass', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['submitted_at', 'confirmed_at', 'checked_at', 'created_at', 'updated_at'], 'safe'],
            [['score'], 'number'],
            [['comment'], 'trim'],
            [['comment'], 'default'],
            [['comment'], 'string'],
            [['assess_issue_id'], 'exist', 'skipOnError' => true, 'targetClass' => AssessIssue::className(), 'targetAttribute' => ['assess_issue_id' => 'id']],
            [['form_issue_submit_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormIssueSubmit::className(), 'targetAttribute' => ['form_issue_submit_id' => 'id']],
            [['six_month_form_id'], 'exist', 'skipOnError' => true, 'targetClass' => SixMonthForm::className(), 'targetAttribute' => ['six_month_form_id' => 'id']],
            [['checked_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['checked_by' => 'id']],
            [['confirmed_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['confirmed_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['submitted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['submitted_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'six_month_form_id' => Yii::t('app', 'รายงานราย 6 เดือน'),
            'assess_issue_id' => Yii::t('app', 'assess issue'),
            'form_issue_submit_id' => Yii::t('app', 'Form Issue Submit ID'),
            'submitted_by' => Yii::t('app', 'ส่งล่าสุดโดย'),
            'submitted_at' => Yii::t('app', 'ส่งล่าสุดวันที่'),
            'confirmed_by' => Yii::t('app', 'ยืนยันล่าสุดโดย'),
            'confirmed_at' => Yii::t('app', 'ยืนยันล่าสุดวันที่'),
            'checked_by' => Yii::t('app', 'อนุมัติล่าสุดโดย'),
            'checked_at' => Yii::t('app', 'อนุมัติล่าสุดวันที่'),
            'is_pass' => Yii::t('app', 'ผ่าน/ไม่ผ่าน'),
            'score' => Yii::t('app', 'คะแนน'),
            'year' => Yii::t('app', 'ปี'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'confirmedBy.person.fullName' => Yii::t('app', 'ยืนยันล่าสุดโดย'),
            'statusBadge' => Yii::t('app', 'สถานะ'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessIssue() {
        return $this->hasOne(AssessIssue::className(), ['id' => 'assess_issue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssueSubmit() {
        return $this->hasOne(FormIssueSubmit::className(), ['id' => 'form_issue_submit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSixMonthForm() {
        return $this->hasOne(SixMonthForm::className(), ['id' => 'six_month_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckedBy() {
        return $this->hasOne(User::className(), ['id' => 'checked_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedBy() {
        return $this->hasOne(User::className(), ['id' => 'confirmed_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubmittedBy() {
        return $this->hasOne(User::className(), ['id' => 'submitted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssueFiles() {
        return $this->hasMany(FormIssueFile::className(), ['form_issue_id' => 'id']);
    }
    
    public function getFiles() {
        return $this->hasMany(FormIssueFile::className(), ['form_issue_id' => 'id'])->isDeleted(false)->type(FormIssueFile::TYPE_FILE);
    }
    
    public function getImages() {
        return $this->hasMany(FormIssueFile::className(), ['form_issue_id' => 'id'])->isDeleted(false)->type(FormIssueFile::TYPE_IMAGE);
    }

    /**
     * {@inheritdoc}
     * @return FormIssueQuery the active query used by this AR class.
     */
    public function getStatusColor() {
        $color = '';
        if (isset($this->checked_at)) {
            if ($this->is_pass) {
                $color = 'success';
            } else {
                $color = 'danger';
            }
        } else if (isset($this->confirmed_at)) {
            $color = 'warning';
        } else if (isset($this->submitted_at)) {
            $color = 'info';
        }
        return $color;
    }

    public function getStatusLabel() {
        $label = '';
        if (isset($this->checked_at)) {
            if ($this->is_pass) {
                $label = Yii::t('app', 'ผ่าน');
            } else {
                $label = Yii::t('app', 'ไม่ผ่าน');
            }
        } else if (isset($this->confirmed_at)) {
            $label = Yii::t('app', 'รอการตรวจสอบ');
        } else if (isset($this->submitted_at)) {
            $label = Yii::t('app', 'รอการยืนยัน');
        }
        return $label;
    }

    public function getStatusBadge() {
        $color = $this->getStatusColor();
        $label = $this->getStatusLabel();
        $res = "<span class='label label-{$color}'>{$label}</span>";
        return $res;
    }

    public function getSubmittedByName() {
        if (isset($this->submitted_at)) {
            if (isset($this->submitted_by)) {
                return $this->submittedBy->person->fullName;
            } else {
                return Yii::t('app', 'ระบบอัตโนมัติ');
            }
        } else {
            return '';
        }
    }
    
    public function getHasFile() {
        return $this->getFormIssueFiles()->isDeleted(false)->type(FormIssueFile::TYPE_FILE)->limit(1)->count() > 0;
    }
    
    public function getHasImage() {
        return $this->getFormIssueFiles()->isDeleted(false)->type(FormIssueFile::TYPE_IMAGE)->limit(1)->count() > 0;
    }

    public static function find() {
        return new FormIssueQuery(get_called_class());
    }

}
