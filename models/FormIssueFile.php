<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "form_issue_file".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $form_issue_id form issue
 * @property int $form_issue_submit_id form issue review
 * @property int $parent_id parent
 * @property int $type 1=รูปภาพ, 2=ไฟล์เอกสาร
 * @property string $name ชื่อเอกสาร/รูปภาพ
 * @property string $file_name ชื่อไฟล์ที่บันทึกในระบบ
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property FormIssue $formIssue
 * @property FormIssueSubmit $formIssueSubmit
 * @property FormIssueFile $parent
 * @property FormIssueFile[] $formIssueFiles
 * @property User $createdBy
 * @property User $updatedBy
 */
class FormIssueFile extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    const TYPE_IMAGE = 1;
    const TYPE_FILE = 2;
    const SCENARIO_FILE = 'file';
    const SCENARIO_IMAGE = 'image';

    public $uploadFiles;

    public static function getTypeLabel() {
        return[
            self::TYPE_IMAGE => Yii::t('app', 'รูปภาพ'),
            self::TYPE_FILE => Yii::t('app', 'ไฟล์'),
        ];
    }

    public static function tableName() {
        return 'form_issue_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['form_issue_id', 'form_issue_submit_id', 'parent_id', 'type', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'required', 'on' => self::SCENARIO_FILE],
            [['uploadFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, gif', 'on' => self::SCENARIO_IMAGE],
            [['uploadFiles'], 'file', 'skipOnEmpty' => true, 'on' => self::SCENARIO_FILE],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'file_name'], 'string', 'max' => 255],
            [['form_issue_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormIssue::className(), 'targetAttribute' => ['form_issue_id' => 'id']],
            [['form_issue_submit_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormIssueSubmit::className(), 'targetAttribute' => ['form_issue_submit_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormIssueFile::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'form_issue_id' => Yii::t('app', 'form issue'),
            'form_issue_submit_id' => Yii::t('app', 'form issue review'),
            'parent_id' => Yii::t('app', 'parent'),
            'type' => Yii::t('app', '1=รูปภาพ, 2=ไฟล์เอกสาร'),
            'name' => Yii::t('app', 'ชื่อเอกสาร/รูปภาพ'),
            'file_name' => Yii::t('app', 'ชื่อไฟล์ที่บันทึกในระบบ'),
            'uploadFiles' => Yii::t('app', 'ไฟล์'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssue() {
        return $this->hasOne(FormIssue::className(), ['id' => 'form_issue_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssueSubmit() {
        return $this->hasOne(FormIssueSubmit::className(), ['id' => 'form_issue_submit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(FormIssueFile::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssueFiles() {
        return $this->hasMany(FormIssueFile::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getFileIconClass() {
        $info = pathinfo($this->file_name);
        if (in_array($info['extension'], ['doc', 'docx'])) {
            return 'icon fa-file-word-o';
        } else if (in_array($info['extension'], ['pdf'])) {
            return 'icon fa-file-pdf-o';
        } else if (in_array($info['extension'], ['xls', 'xlsx'])) {
            return 'icon fa-file-excel-o';
        } else if (in_array($info['extension'], ['jpg', 'jpeg', 'gif', 'png'])) {
            return 'fa-file-image-o';
        } else {
            return 'icon fa-file-text-o';
        }
    }

    public function getFileUrl() {
        return Url::to("@web/{$this->file_name}", true);
    }

    public function getFilePath() {
        return Yii::getAlias("@app/web/{$this->file_name}");
    }

    public function getPath() {
        if ($this->type == self::TYPE_FILE) {
            return "uploads/six-month/files/{$this->form_issue_id}";
        } else {
            return "uploads/six-month/images/{$this->form_issue_id}";
        }
    }
    
    public function getPreviewType() {
        $info = pathinfo($this->fileUrl);
        if (in_array($info['extension'], ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'csv'])) {
            return 'office';
        } else if (in_array($info['extension'], ['tif', 'ai', 'eps'])) {
            return 'gdocs';
        } else if (in_array($info['extension'], ['txt'])) {
            return 'text';
        } else if (in_array($info['extension'], ['pdf'])) {
            return 'pdf';
        }
        return 'image';
    }

    public function getPreviewConfig() {
        $info = pathinfo($this->fileUrl);
        return [
            'caption' => $this->type == self::TYPE_FILE ? $this->name : $info['basename'],
            'key' => $this->id,
            'type' => $this->previewType,
            'size' => file_exists($this->filePath) ? filesize($this->filePath) : 0,
        ];
    }

    /**
     * {@inheritdoc}
     * @return FormIssueFileQuery the active query used by this AR class.
     */
    public static function find() {
        return new FormIssueFileQuery(get_called_class());
    }

}
