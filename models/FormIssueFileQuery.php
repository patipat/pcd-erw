<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FormIssueFile]].
 *
 * @see FormIssueFile
 */
class FormIssueFileQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return FormIssueFile[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FormIssueFile|array|null
     */
    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['form_issue_file.deleted' => $deleted]);
    }

    public function formIssue($formIssueId) {
        return $this->andWhere(['form_issue_file.form_issue_id' => $formIssueId]);
    }

    public function assessIssue($assessIssueId) {
        return $this->andWhere(['form_issue.assess_issue_id' => $assessIssueId]);
    }

    public function type($type) {
        return $this->andWhere(['form_issue_file.type' => $type]);
    }
    
    public function officeParent($parentId) {
        return $this->andWhere(['office.parent_id' => $parentId]);
    }
    
    public function fileName($fileName) {
        return $this->andWhere(['form_issue_file.file_name' => $fileName]);
    }

    public function one($db = null) {
        return parent::one($db);
    }

}
