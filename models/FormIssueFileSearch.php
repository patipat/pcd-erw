<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormIssueFile;

/**
 * FormIssueFileSearch represents the model behind the search form about `app\models\FormIssueFile`.
 */
class FormIssueFileSearch extends FormIssueFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'form_issue_id', 'form_issue_submit_id', 'parent_id', 'type', 'created_by', 'updated_by'], 'integer'],
            [['name', 'file_name', 'deleted', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormIssueFile::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'form_issue_id' => $this->form_issue_id,
            'form_issue_submit_id' => $this->form_issue_submit_id,
            'parent_id' => $this->parent_id,
            'type' => $this->type,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'file_name', $this->file_name])
            ->andFilterWhere(['like', 'deleted', $this->deleted]);

        return $dataProvider;
    }
}
