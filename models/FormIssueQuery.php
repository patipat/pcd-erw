<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FormIssue]].
 *
 * @see FormIssue
 */
class FormIssueQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return FormIssue[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FormIssue|array|null
     */
    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['form_issue.deleted' => $deleted]);
    }

    public function office($officeId) {
        return $this->andWhere(['six_month_form.office_id' => $officeId]);
    }

    public function year($year) {
        return $this->andWhere(['six_month_form.year' => $year]);
    }

    public function assessIssue($assessIssueId) {
        return $this->andWhere(['form_issue.assess_issue_id' => $assessIssueId]);
    }

    public function sixMonth($sixId) {
        return $this->andWhere(['form_issue.six_month_form_id' => $sixId]);
    }
    
    public function isSubmitted($submitted = TRUE) {
        if ($submitted) {
            return $this->andWhere(['not', ['form_issue.submitted_at' => null]]);
        } else {
            return $this->andWhere(['form_issue.submitted_at' => null]);
        }
    }

    public function isConfirmed($confirmed = TRUE) {
        if ($confirmed) {
            return $this->andWhere(['not', ['form_issue.confirmed_at' => null]]);
        } else {
            return $this->andWhere(['form_issue.confirmed_at' => null]);
        }
    }

    public function isChecked($checked = TRUE) {
        if ($checked) {
            return $this->andWhere(['not', ['form_issue.checked_at' => null]]);
        } else {
            return $this->andWhere(['form_issue.checked_at' => null]);
        }
    }
    
    public function isPass($pass = TRUE) {
        return $this->andWhere(['form_issue.is_pass' => $pass]);
    }

    public function one($db = null) {
        return parent::one($db);
    }

}
