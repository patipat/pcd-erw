<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormIssue;

/**
 * FormIssueSearch represents the model behind the search form about `app\models\FormIssue`.
 */
class FormIssueSearch extends FormIssue {

    public $year;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'six_month_form_id', 'assess_issue_id', 'form_issue_submit_id', 'submitted_by', 'confirmed_by', 'checked_by', 'year', 'created_by', 'updated_by'], 'integer'],
            [['submitted_at', 'confirmed_at', 'checked_at', 'is_pass', 'deleted', 'created_at', 'updated_at'], 'safe'],
            [['score'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = FormIssue::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['assess_issue_id' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['sixMonthForm']);
        $query->andFilterWhere([
            'form_issue.id' => $this->id,
            'form_issue.six_month_form_id' => $this->six_month_form_id,
            'form_issue.assess_issue_id' => $this->assess_issue_id,
            'form_issue.form_issue_submit_id' => $this->form_issue_submit_id,
            'form_issue.submitted_by' => $this->submitted_by,
            'form_issue.submitted_at' => $this->submitted_at,
            'form_issue.confirmed_by' => $this->confirmed_by,
            'form_issue.confirmed_at' => $this->confirmed_at,
            'form_issue.checked_by' => $this->checked_by,
            'form_issue.checked_at' => $this->checked_at,
            'form_issue.score' => $this->score,
            'six_month_form.year' => $this->year,
            'form_issue.created_by' => $this->created_by,
            'form_issue.created_at' => $this->created_at,
            'form_issue.updated_by' => $this->updated_by,
            'form_issue.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'form_issue.is_pass', $this->is_pass])
                ->andFilterWhere(['like', 'form_issue.deleted', $this->deleted]);

        return $dataProvider;
    }

}
