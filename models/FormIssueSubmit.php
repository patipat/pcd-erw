<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_issue_submit".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $six_month_form_id
 * @property int $ref_id
 * @property string $remark
 * @property int $submitted_by ส่งล่าสุดโดย
 * @property string $submitted_at ส่งล่าสุดวันที่
 * @property int $confirmed_by ยืนยันล่าสุดโดย
 * @property string $confirmed_at ยืนยันล่าสุดวันที่
 * @property int $checked_by อนุมัติล่าสุดโดย
 * @property string $checked_at อนุมัติล่าสุดวันที่
 * @property int $is_pass ผ่าน/ไม่ผ่าน
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property FormIssue[] $formIssues
 * @property FormIssueFile[] $formIssueFiles
 * @property FormIssueSubmit $ref
 * @property FormIssueSubmit[] $formIssueSubmits
 * @property SixMonthForm $sixMonthForm
 * @property User $checkedBy
 * @property User $confirmedBy
 * @property User $createdBy
 * @property User $submittedBy
 * @property User $updatedBy
 */
class FormIssueSubmit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_issue_submit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['six_month_form_id', 'ref_id', 'submitted_by', 'confirmed_by', 'checked_by', 'is_pass', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['remark'], 'string'],
            [['submitted_at', 'confirmed_at', 'checked_at', 'created_at', 'updated_at'], 'safe'],
            [['ref_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormIssueSubmit::className(), 'targetAttribute' => ['ref_id' => 'id']],
            [['six_month_form_id'], 'exist', 'skipOnError' => true, 'targetClass' => SixMonthForm::className(), 'targetAttribute' => ['six_month_form_id' => 'id']],
            [['checked_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['checked_by' => 'id']],
            [['confirmed_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['confirmed_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['submitted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['submitted_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'six_month_form_id' => Yii::t('app', 'Six Month Form ID'),
            'ref_id' => Yii::t('app', 'Ref ID'),
            'remark' => Yii::t('app', 'Remark'),
            'submitted_by' => Yii::t('app', 'ส่งล่าสุดโดย'),
            'submitted_at' => Yii::t('app', 'ส่งล่าสุดวันที่'),
            'confirmed_by' => Yii::t('app', 'ยืนยันล่าสุดโดย'),
            'confirmed_at' => Yii::t('app', 'ยืนยันล่าสุดวันที่'),
            'checked_by' => Yii::t('app', 'อนุมัติล่าสุดโดย'),
            'checked_at' => Yii::t('app', 'อนุมัติล่าสุดวันที่'),
            'is_pass' => Yii::t('app', 'ผ่าน/ไม่ผ่าน'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssues()
    {
        return $this->hasMany(FormIssue::className(), ['form_issue_submit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssueFiles()
    {
        return $this->hasMany(FormIssueFile::className(), ['form_issue_submit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRef()
    {
        return $this->hasOne(FormIssueSubmit::className(), ['id' => 'ref_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssueSubmits()
    {
        return $this->hasMany(FormIssueSubmit::className(), ['ref_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSixMonthForm()
    {
        return $this->hasOne(SixMonthForm::className(), ['id' => 'six_month_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'checked_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'confirmed_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubmittedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'submitted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return FormIssueSubmitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FormIssueSubmitQuery(get_called_class());
    }
}
