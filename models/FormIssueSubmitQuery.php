<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FormIssueSubmit]].
 *
 * @see FormIssueSubmit
 */
class FormIssueSubmitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return FormIssueSubmit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return FormIssueSubmit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
