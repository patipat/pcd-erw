<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "line_queue".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property string $token
 * @property string $sent_at ส่งเมล์เมื่อ
 * @property string $message
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class LineQueue extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'line_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['sent_at', 'created_at', 'updated_at'], 'safe'],
            [['message'], 'string'],
            [['deleted', 'created_by', 'updated_by'], 'integer'],
            [['token'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'token' => Yii::t('app', 'Token'),
            'sent_at' => Yii::t('app', 'ส่งเมล์เมื่อ'),
            'message' => Yii::t('app', 'Message'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function send() {
        $client = new \GuzzleHttp\Client();
        $message = str_replace('<br>', "\n", $this->message);
        $client->request('POST', 'https://notify-api.line.me/api/notify', [
            'headers' => [
                'Content-Type' => "application/x-www-form-urlencoded",
                'Authorization' => "Bearer {$this->token}"
            //                'Authorization' => "Bearer 8zJIZgVsKL9O20QEVWf5dQ1P57gEZXwLOgpE4ZVasQg"
            //                'Authorization' => "Bearer q6IyJZMnqLciOJGJiEFcW4TQAAJ7UWyCn7mYkKSiHGz" // ระบบทะเบียน
            ],
            'form_params' => [
                'message' => $message
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     * @return LineQueueQuery the active query used by this AR class.
     */
    public static function find() {
        return new LineQueueQuery(get_called_class());
    }

    public static function addQueue($token, $message) {
        $model = new LineQueue();
//        $model->type = $type;
        $model->token = $token;
        $model->message = $message;
        $res = $model->save();
//        if ($res) {
//            \Yii::$app->util->execInBackground(\Yii::$app->params['sendMailCmd']);
//        }
        return $res;
    }

}
