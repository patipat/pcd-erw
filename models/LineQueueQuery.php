<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LineQueue]].
 *
 * @see LineQueue
 */
class LineQueueQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return LineQueue[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LineQueue|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['line_queue.deleted' => $deleted]);
    }
    
    public function unsent() {
        return $this->andWhere(['line_queue.sent_at' => NULL]);
    }
}
