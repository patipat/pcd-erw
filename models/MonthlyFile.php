<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "monthly_file".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $office_id หน่วยงาน
 * @property int $year ปี
 * @property int $month เดือน
 * @property string $name ชื่อเอกสาร/รูปภาพ
 * @property string $file_name ชื่อไฟล์ที่บันทึกในระบบ
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property Office $office
 * @property User $createdBy
 * @property User $updatedBy
 */
class MonthlyFile extends \yii\db\ActiveRecord
{
    public $uploadFiles;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'monthly_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['office_id', 'year', 'month', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['uploadFiles'], 'file', 'skipOnEmpty' => true],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'file_name'], 'string', 'max' => 255],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสอัตโนมัติ',
            'office_id' => 'หน่วยงาน',
            'year' => 'ปี',
            'month' => 'เดือน',
            'name' => 'ชื่อเอกสาร/รูปภาพ',
            'file_name' => 'ชื่อไฟล์ที่บันทึกในระบบ',
            'deleted' => '0=ใช้งาน,1=ไม่ใช้งาน',
            'created_by' => 'สร้างโดย',
            'created_at' => 'สร้างเมื่อ',
            'updated_by' => 'ปรับปรุงโดย',
            'updated_at' => 'ปรับปรุงเมื่อ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getFileIconClass() {
        $info = pathinfo($this->file_name);
        if (in_array($info['extension'], ['doc', 'docx'])) {
            return 'icon fa-file-word-o';
        } else if (in_array($info['extension'], ['pdf'])) {
            return 'icon fa-file-pdf-o';
        } else if (in_array($info['extension'], ['xls', 'xlsx'])) {
            return 'icon fa-file-excel-o';
        } else if (in_array($info['extension'], ['jpg', 'jpeg', 'gif', 'png'])) {
            return 'fa-file-image-o';
        } else {
            return 'icon fa-file-text-o';
        }
    }

    public function getFileUrl() {
        return Url::to("@web/{$this->file_name}", true);
    }

    public function getFilePath() {
        return Yii::getAlias("@app/web/{$this->file_name}");
    }

    public function getPath() {
        return "uploads/monthly/{$this->office_id}/{$this->year}{$this->month}";
    }
    
    public function getPreviewType() {
        $info = pathinfo($this->fileUrl);
        if (in_array($info['extension'], ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'csv'])) {
            return 'office';
        } else if (in_array($info['extension'], ['tif', 'ai', 'eps'])) {
            return 'gdocs';
        } else if (in_array($info['extension'], ['txt'])) {
            return 'text';
        } else if (in_array($info['extension'], ['pdf'])) {
            return 'pdf';
        }
        return 'image';
    }

    public function getPreviewConfig() {
        $info = pathinfo($this->fileUrl);
        return [
            'caption' => $this->name,
            'key' => $this->id,
            'type' => $this->previewType,
            'size' => file_exists($this->filePath) ? filesize($this->filePath) : 0,
        ];
    }

    /**
     * {@inheritdoc}
     * @return MonthlyFileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MonthlyFileQuery(get_called_class());
    }
}
