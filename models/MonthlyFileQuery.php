<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MonthlyFile]].
 *
 * @see MonthlyFile
 */
class MonthlyFileQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MonthlyFile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MonthlyFile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * {@inheritdoc}
     * @return MonthlyFile|array|null
     */
    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['monthly_file.deleted' => $deleted]);
    }

    /**
     * {@inheritdoc}
     * @return MonthlyFile|array|null
     */
    public function year($year) {
        return $this->andWhere(['monthly_file.year' => $year]);
    }

    /**
     * {@inheritdoc}
     * @return MonthlyFile|array|null
     */
    public function month($month) {
        return $this->andWhere(['monthly_file.month' => $month]);
    }

    /**
     * {@inheritdoc}
     * @return MonthlyFile|array|null
     */
    public function office($officeId) {
        return $this->andWhere(['monthly_file.office_id' => $officeId]);
    }
}
