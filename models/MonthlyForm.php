<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "monthly_form".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $office_id หน่วยงาน
 * @property int $source_type_id source type
 * @property int $year ปี
 * @property int $month เดือน
 * @property int $office_population จำนวนบุคคลากรในหน่วยงาน(คน)
 * @property int $work_day จำนวนวันทำงาน(วัน)
 * @property string $garbage ปริมาณขยะมูลฝอย (กก.)
 * @property string $bag จำนวนถุงพลาสติก (ใบ)
 * @property string $cup จำนวนแก้วพลาสติก (ใบ)
 * @property string $foam จำนวนโฟมบรรจุอาหาร (ใบ)
 * @property string $mask จำนวนหน้ากากอนามัย (ชิ้น)
 * @property int $submitted_by ส่งล่าสุดโดย
 * @property string $submitted_at ส่งล่าสุดวันที่
 * @property int $confirmed_by ยืนยันล่าสุดโดย
 * @property string $confirmed_at ยืนยันล่าสุดวันที่
 * @property int $checked_by อนุมัติล่าสุดโดย
 * @property string $checked_at อนุมัติล่าสุดวันที่
 * @property int $is_pass ผ่าน/ไม่ผ่าน
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 * @property int $pop_int จำนวนบุคลากรภายใน 
 * @property int $pop_ext จำนวนบุคลากรภายนอก 
 *
 * @property Office $office
 * @property SourceType $sourceType
 * @property User $checkedBy
 * @property User $confirmedBy
 * @property User $createdBy
 * @property User $submittedBy
 * @property User $updatedBy
 */
class MonthlyForm extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'monthly_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['office_id', 'source_type_id', 'year', 'month', 'office_population', 'work_day', 'submitted_by', 'confirmed_by', 'checked_by', 'is_pass', 'deleted', 'created_by', 'updated_by', 'pop_int', 'pop_ext'], 'integer'],
            [['garbage', 'bag', 'cup', 'foam','mask'], 'number'],
            [['submitted_at', 'confirmed_at', 'checked_at', 'created_at', 'updated_at'], 'safe'],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['source_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SourceType::className(), 'targetAttribute' => ['source_type_id' => 'id']],
            [['checked_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['checked_by' => 'id']],
            [['confirmed_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['confirmed_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['submitted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['submitted_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'office_id' => Yii::t('app', 'หน่วยงาน'),
            'source_type_id' => Yii::t('app', 'source type'),
            'year' => Yii::t('app', 'ปี'),
            'month' => Yii::t('app', 'เดือน'),
            'office_population' => Yii::t('app', 'จำนวนบุคคลากรในหน่วยงาน(คน)'),
            'work_day' => Yii::t('app', 'จำนวนวันทำงาน(วัน)'),
            'garbage' => Yii::t('app', 'ปริมาณขยะมูลฝอย (กก.)'),
            'bag' => Yii::t('app', 'จำนวนถุงพลาสติก (ใบ)'),
            'cup' => Yii::t('app', 'จำนวนแก้วพลาสติก (ใบ)'),
            'foam' => Yii::t('app', 'จำนวนโฟมบรรจุอาหาร (ใบ)'),
            'mask' => Yii::t('app', 'จำนวนหน้ากากอนามัย (ชิ้น)'),
            'submitted_by' => Yii::t('app', 'ส่งล่าสุดโดย'),
            'submitted_at' => Yii::t('app', 'ส่งล่าสุดวันที่'),
            'confirmed_by' => Yii::t('app', 'ยืนยันล่าสุดโดย'),
            'confirmed_at' => Yii::t('app', 'ยืนยันล่าสุดวันที่'),
            'checked_by' => Yii::t('app', 'อนุมัติล่าสุดโดย'),
            'checked_at' => Yii::t('app', 'อนุมัติล่าสุดวันที่'),
            'is_pass' => Yii::t('app', 'ผ่าน/ไม่ผ่าน'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'pop_int' => Yii::t('app', 'จำนวนบุคลากรภายใน'),
            'pop_ext' => Yii::t('app', 'จำนวนบุคลากรภายนอก'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice() {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceType() {
        return $this->hasOne(SourceType::className(), ['id' => 'source_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckedBy() {
        return $this->hasOne(User::className(), ['id' => 'checked_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedBy() {
        return $this->hasOne(User::className(), ['id' => 'confirmed_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubmittedBy() {
        return $this->hasOne(User::className(), ['id' => 'submitted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getMonthName() {
        return Yii::$app->formatter->asDate((new \DateTime("{$this->year}-{$this->month}-01"))->format('Y-m-d H:i:s'), 'php:F');
    }

    /**
     * {@inheritdoc}
     * @return MonthlyFormQuery the active query used by this AR class.
     */
    public static function find() {
        return new MonthlyFormQuery(get_called_class());
    }

}
