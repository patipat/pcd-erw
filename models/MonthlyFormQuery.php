<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MonthlyForm]].
 *
 * @see MonthlyForm
 */
class MonthlyFormQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return MonthlyForm[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MonthlyForm|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['monthly_form.deleted' => $deleted]);
    }

    public function office($officeId) {
        return $this->andWhere(['monthly_form.office_id' => $officeId]);
    }

    public function year($year) {
        return $this->andWhere(['monthly_form.year' => $year]);
    }

    public function month($month) {
        return $this->andWhere(['monthly_form.month' => $month]);
    }

    public function budgetYear($year) {
        $yearMonths = \Yii::$app->util->getYearMonths($year);
        return $this->andWhere([
            'or', 
            [
                'and',
                ['monthly_form.year' => $yearMonths[0]['year']],
                ['>=', 'monthly_form.month', $yearMonths[0]['month']]
            ],
            [
                'and',
                ['monthly_form.year' => $yearMonths[count($yearMonths) - 1]['year']],
                ['<=', 'monthly_form.month', $yearMonths[count($yearMonths) - 1]['month']]
            ]
        ]);
    }
}
