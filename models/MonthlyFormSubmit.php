<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "monthly_form_submit".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $office_id หน่วยงาน
 * @property int $source_type_id source type
 * @property int $year ปี
 * @property int $month เดือน
 * @property int $office_population จำนวนบุคคลากรในหน่วยงาน(คน)
 * @property int $work_day จำนวนวันทำงาน(วัน)
 * @property string $garbage ปริมาณขยะมูลฝอย (กก.)
 * @property string $bag จำนวนถุงพลาสติก (ใบ)
 * @property string $cup จำนวนแก้วพลาสติก (ใบ)
 * @property string $foam จำนวนโฟมบรรจุอาหาร (ใบ)
 * @property string $mask จำนวนหน้ากากอนามัย (ชิ้น)
 * @property int $ref_id ref
 * @property string $remark remark
 * @property int $submitted_by ส่งล่าสุดโดย
 * @property string $submitted_at ส่งล่าสุดวันที่
 * @property int $confirmed_by ยืนยันล่าสุดโดย
 * @property string $confirmed_at ยืนยันล่าสุดวันที่
 * @property int $checked_by อนุมัติล่าสุดโดย
 * @property string $checked_at อนุมัติล่าสุดวันที่
 * @property int $is_pass ผ่าน/ไม่ผ่าน
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 * @property int $pop_int จำนวนบุคลากรภายใน
 * @property int $pop_ext จำนวนบุคลากรภายนอก
 * @property string $comment หมายเหตุ
 * @property int $is_late ช้า/ไม่ช้า
 *
 * @property Office $office
 * @property MonthlyFormSubmit $ref
 * @property MonthlyFormSubmit[] $monthlyFormSubmits
 * @property SourceType $sourceType
 * @property User $checkedBy
 * @property User $confirmedBy
 * @property User $createdBy
 * @property User $submittedBy
 * @property User $updatedBy
 */
class MonthlyFormSubmit extends \yii\db\ActiveRecord {

    const SCENARIO_CHECK = 'check';

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'monthly_form_submit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['pop_int', 'pop_ext', 'garbage', 'bag', 'cup', 'foam', 'source_type_id', 'work_day'], 'required'],
            [['is_pass'], 'required', 'on' => self::SCENARIO_CHECK],
            [['pop_int', 'pop_ext', 'garbage', 'bag', 'cup', 'foam', 'work_day'], 'filter', 'filter' => function($value) {
                    return str_replace(',', '', $value);
                }],
            [['office_id', 'source_type_id', 'year', 'month', 'office_population', 'work_day', 'ref_id', 'submitted_by', 'confirmed_by', 'checked_by', 'is_pass', 'deleted', 'created_by', 'updated_by', 'pop_int', 'pop_ext', 'is_late'], 'integer'],
            [['work_day', 'pop_int'], 'integer', 'min' => 1],
            [['garbage', 'bag', 'cup', 'foam','mask'], 'number'],
            [['remark', 'comment'], 'string'],
            [['submitted_at', 'confirmed_at', 'checked_at', 'created_at', 'updated_at'], 'safe'],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['ref_id'], 'exist', 'skipOnError' => true, 'targetClass' => MonthlyFormSubmit::className(), 'targetAttribute' => ['ref_id' => 'id']],
            [['source_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SourceType::className(), 'targetAttribute' => ['source_type_id' => 'id']],
            [['checked_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['checked_by' => 'id']],
            [['confirmed_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['confirmed_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['submitted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['submitted_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'office_id' => Yii::t('app', 'หน่วยงาน'),
            'source_type_id' => Yii::t('app', 'การเก็บข้อมูล'),
            'year' => Yii::t('app', 'ปี'),
            'month' => Yii::t('app', 'เดือน'),
            'office_population' => Yii::t('app', 'จำนวนบุคคลากรในหน่วยงาน(คน)'),
            'work_day' => Yii::t('app', 'จำนวนวันทำงาน(วัน)'),
            'garbage' => Yii::t('app', 'ปริมาณขยะมูลฝอย (กก.)'),
            'bag' => Yii::t('app', 'จำนวนถุงพลาสติก (ใบ)'),
            'cup' => Yii::t('app', 'จำนวนแก้วพลาสติก (ใบ)'),
            'foam' => Yii::t('app', 'จำนวนโฟมบรรจุอาหาร (ใบ)'),
            'mask' => Yii::t('app', 'จำนวนหน้ากากอนามัย (ชิ้น)'),
            'ref_id' => Yii::t('app', 'ref'),
            'remark' => Yii::t('app', 'หมายเหตุ'),
            'submitted_by' => Yii::t('app', 'ส่งล่าสุดโดย'),
            'submitted_at' => Yii::t('app', 'ส่งล่าสุดวันที่'),
            'confirmed_by' => Yii::t('app', 'ยืนยันล่าสุดโดย'),
            'confirmed_at' => Yii::t('app', 'ยืนยันล่าสุดวันที่'),
            'checked_by' => Yii::t('app', 'อนุมัติล่าสุดโดย'),
            'checked_at' => Yii::t('app', 'อนุมัติล่าสุดวันที่'),
            'is_pass' => Yii::t('app', 'ผลการตรวจสอบ'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'pop_int' => Yii::t('app', 'จำนวนบุคลากรภายใน'),
            'pop_ext' => Yii::t('app', 'จำนวนบุคลากรภายนอก'),
            'statusBadge' => Yii::t('app', 'สถานะ'),
            'monthName' => Yii::t('app', 'เดือน'),
            'comment' => Yii::t('app', 'หมายเหตุการตรวจสอบ'),
            'is_late' => Yii::t('app', 'ยืนยันข้อมูลเกินกำหนด'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice() {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRef() {
        return $this->hasOne(MonthlyFormSubmit::className(), ['id' => 'ref_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonthlyFormSubmits() {
        return $this->hasMany(MonthlyFormSubmit::className(), ['ref_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceType() {
        return $this->hasOne(SourceType::className(), ['id' => 'source_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckedBy() {
        return $this->hasOne(User::className(), ['id' => 'checked_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedBy() {
        return $this->hasOne(User::className(), ['id' => 'confirmed_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubmittedBy() {
        return $this->hasOne(User::className(), ['id' => 'submitted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getLastYearForm() {
        return MonthlyFormSubmit::find()->isDeleted(false)->year($this->year - 1)->month($this->month)
            ->office($this->office_id)->one();
    }

    public function getMonthName() {
        return Yii::$app->formatter->asDate((new \DateTime("{$this->year}-{$this->month}-01"))->format('Y-m-d H:i:s'), 'php:F');
    }

    public function getStatusColor() {
        $color = '';
        if (isset($this->checked_at)) {
            if ($this->is_pass) {
                $color = 'success';
            } else {
                $color = 'danger';
            }
        } else if (isset($this->confirmed_at)) {
            $color = 'warning';
        } else if (isset($this->submitted_at)) {
            $color = 'info';
        }
        return $color;
    }

    public function getStatusLabel() {
        $label = '';
        if (isset($this->checked_at)) {
            if ($this->is_pass) {
                $label = Yii::t('app', 'ตรวจสอบแล้ว');
            } else {
                $label = Yii::t('app', 'ข้อมูลตีกลับ');
            }
        } else if (isset($this->confirmed_at)) {
            $label = Yii::t('app', 'รอการตรวจสอบ');
        } else if (isset($this->submitted_at)) {
            $label = Yii::t('app', 'รอการยืนยัน');
        }
        return $label;
    }

    public function getStatusBadge() {
        $color = $this->getStatusColor();
        $label = $this->getStatusLabel();
        $res = "<span class='label label-{$color}'>{$label}</span>";
        return $res;
    }

    public function getSubmittedByName() {
        if (isset($this->submitted_at)) {
            if (isset($this->submitted_by)) {
                return $this->submittedBy->person->fullName;
            } else {
                return Yii::t('app', 'ระบบอัตโนมัติ');
            }
        } else {
            return '';
        }
    }

    public function getHasRecord() {
        return MonthlyFormSubmit::find()->isDeleted(false)->isConfirmed()
                        ->office($this->office_id)->year($this->year)->month($this->month)
                        ->andWhere(['<', 'id', $this->id])->count() > 0;
    }

    public function getMonthlyFiles() {
        $o = Office::findOne($this->office_id);
        $children = $o->getAllChildren();
        $officeIds = ArrayHelper::getColumn($children, 'id');
        $officeIds[] = $this->office_id;
        $files = MonthlyFile::find()->isDeleted(false)->office($this->office_id)->year($this->year)->month($this->month)->all();
        return $files;
    }

    /**
     * {@inheritdoc}
     * @return MonthlyFormSubmitQuery the active query used by this AR class.
     */
    public static function find() {
        return new MonthlyFormSubmitQuery(get_called_class());
    }

    public static function getChartDataByYear($year, $office, $officeType = null, $level = null) {
        $yearMonths = Yii::$app->util->getYearMonths($year);
        $cond = Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $cond1 = Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs1');
        $oCond = Yii::$app->util->getDashboardChartCond($office, $officeType, $level);
        $sql = <<<sql
            SELECT CONCAT(mfs.year, mfs.month) AS yearmonth, mfs.year, mfs.month, SUM(mfs.garbage) AS garbage
                , SUM(mfs.bag) AS bag, SUM(mfs.cup) AS cup
                , SUM(mfs.foam) AS foam , SUM(mfs.mask) AS mask
            FROM monthly_form_submit mfs
                INNER JOIN office o ON mfs.office_id = o.id
            WHERE mfs.deleted = 0 {$cond}
                AND mfs.confirmed_at IS NOT NULL
                AND mfs.id IN (
                    SELECT MAX(mfs1.id)
                    FROM monthly_form_submit mfs1
                        INNER JOIN office o ON mfs1.office_id = o.id
                    WHERE mfs1.deleted = 0 {$cond1} {$oCond}
                    GROUP BY mfs1.office_id, mfs1.year, mfs1.month
                )
            GROUP BY mfs.year, mfs.month
sql;

        return Yii::$app->db->createCommand($sql)->queryAll();
    }

    public static function getAssessmentStatusLabels() {
        return [
            0 => 'ยังไม่ตรวจสอบ',
            1 => 'ตรวจสอบแล้ว',
        ];
    }

}
