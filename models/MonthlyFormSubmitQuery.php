<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MonthlyFormSubmit]].
 *
 * @see MonthlyFormSubmit
 */
class MonthlyFormSubmitQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return MonthlyFormSubmit[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MonthlyFormSubmit|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['monthly_form_submit.deleted' => $deleted]);
    }

    public function office($officeId) {
        return $this->andWhere(['monthly_form_submit.office_id' => $officeId]);
    }

    public function year($year) {
        return $this->andWhere(['monthly_form_submit.year' => $year]);
    }

    public function month($month) {
        return $this->andWhere(['monthly_form_submit.month' => $month]);
    }

    public function isSubmitted($submitted = TRUE) {
        if ($submitted) {
            return $this->andWhere(['not', ['monthly_form_submit.submitted_at' => null]]);
        } else {
            return $this->andWhere(['monthly_form_submit.submitted_at' => null]);
        }
    }

    public function isConfirmed($confirmed = TRUE) {
        if ($confirmed) {
            return $this->andWhere(['not', ['monthly_form_submit.confirmed_at' => null]]);
        } else {
            return $this->andWhere(['monthly_form_submit.confirmed_at' => null]);
        }
    }

    public function isChecked($checked = TRUE) {
        if ($checked) {
            return $this->andWhere(['not', ['monthly_form_submit.checked_at' => null]]);
        } else {
            return $this->andWhere(['monthly_form_submit.checked_at' => null]);
        }
    }

    public function budgetYear($year) {
        $yearMonths = Yii::$app->util->getYearMonths($year);
        return $this->andWhere([
                    'or',
                    [
                        'and',
                        ['monthly_form_submit.year' => $yearMonths[0]['year']],
                        ['>=', 'monthly_form_submit.month', $yearMonths[0]['month']]
                    ],
                    [
                        'and',
                        ['monthly_form_submit.year' => $yearMonths[count($yearMonths) - 1]['year']],
                        ['<=', 'monthly_form_submit.month', $yearMonths[count($yearMonths) - 1]['month']]
                    ]
        ]);
    }

    public function isAssessed($isAssessed = true) {
        if ($isAssessed) {
            return $this->andWhere(['not', ['monthly_form_submit.checked_at' => null]]);
        }
        return $this->andWhere(['monthly_form_submit.checked_at' => null]);
    }

    public function last() {
        $subQuery = (new \yii\db\Query())->select('MAX(id) AS id')
                ->from('monthly_form_submit mfs')
                ->andWhere('mfs.year = monthly_form_submit.year')
                ->andWhere('mfs.month = monthly_form_submit.month')
                ->andWhere('mfs.office_id = monthly_form_submit.office_id')
                ->andWhere('mfs.deleted=0');
        return $this->andWhere(['monthly_form_submit.id' => $subQuery]);
    }

}
