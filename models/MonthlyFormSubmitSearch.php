<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MonthlyFormSubmit;
use yii\helpers\ArrayHelper;

/**
 * MonthlyFormSubmitSearch represents the model behind the search form about `app\models\MonthlyFormSubmit`.
 */
class MonthlyFormSubmitSearch extends MonthlyFormSubmit {

    public $isLastSubmit;
    public $isLastUncheck;
    public $isCheckChildrenByRole;
    public $officeType;
    public $level;
    public $isConfirmed;
    public $isAssessed;
    public $parentId;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'office_id', 'source_type_id', 'year', 'month', 'office_population', 'work_day', 'ref_id', 'submitted_by', 'confirmed_by', 'checked_by', 'created_by', 'updated_by', 'pop_ext', 'pop_int', 'is_pass', 'is_late', 'deleted', 'officeType', 'level', 'isAssessed', 'parentId'], 'integer'],
            [['garbage', 'bag', 'cup', 'foam'], 'number'],
            [['remark', 'submitted_at', 'confirmed_at', 'checked_at', 'created_at', 'updated_at', 'isLastSubmit', 'isLastUncheck', 'isCheckChildrenByRole', 'isConfirmed'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = MonthlyFormSubmit::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith(['office']);
        $dataProvider->sort->attributes['office.name'] = [
            'asc' => ['CONVERT(office.name USING TIS620)' => SORT_ASC],
            'desc' => ['CONVERT(office.name USING TIS620)' => SORT_DESC],
        ];

        $query->andFilterWhere([
            'monthly_form_submit.id' => $this->id,
            'monthly_form_submit.office_id' => $this->office_id,
            'monthly_form_submit.source_type_id' => $this->source_type_id,
            'monthly_form_submit.year' => $this->year,
            'monthly_form_submit.month' => $this->month,
            'monthly_form_submit.office_population' => $this->office_population,
            'monthly_form_submit.work_day' => $this->work_day,
            'monthly_form_submit.garbage' => $this->garbage,
            'monthly_form_submit.bag' => $this->bag,
            'monthly_form_submit.cup' => $this->cup,
            'monthly_form_submit.foam' => $this->foam,
            'monthly_form_submit.ref_id' => $this->ref_id,
            'monthly_form_submit.submitted_by' => $this->submitted_by,
            'monthly_form_submit.submitted_at' => $this->submitted_at,
            'monthly_form_submit.confirmed_by' => $this->confirmed_by,
            'monthly_form_submit.confirmed_at' => $this->confirmed_at,
            'monthly_form_submit.checked_by' => $this->checked_by,
            'monthly_form_submit.checked_at' => $this->checked_at,
            'monthly_form_submit.created_by' => $this->created_by,
            'monthly_form_submit.created_at' => $this->created_at,
            'monthly_form_submit.updated_by' => $this->updated_by,
            'monthly_form_submit.updated_at' => $this->updated_at,
            'monthly_form_submit.pop_ext' => $this->pop_ext,
            'monthly_form_submit.pop_int' => $this->pop_int,
            'monthly_form_submit.is_late' => $this->is_late,
            'monthly_form_submit.is_pass' => $this->is_pass,
            'monthly_form_submit.deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'monthly_form_submit.remark', $this->remark]);

        if (isset($this->isAssessed)) {
            if ($this->isAssessed) {
                $this->isConfirmed = true;
                $query->isChecked();
            } else {
                $this->isLastUncheck = true;
            }
        }
        if (!empty($this->isLastSubmit)) {
            $query->last();
        }

        if (!empty($this->isLastUncheck)) {
            $query->last();
            $query->isConfirmed()->isChecked(false);
        }
        if (isset($this->isConfirmed)) {
            $query->isConfirmed($this->isConfirmed);
        }
        $query->andFilterWhere(['office.office_type_id' => $this->officeType]);
        $query->andFilterWhere(['office.level' => $this->level]);
        
        if (isset($this->parentId)) {
            $query->andFilterWhere(['office.parent_id' => $this->parentId]);
        }

        if (!empty($this->isCheckChildrenByRole)) {
            $currentRole = Yii::$app->session->get('currentRole');
            if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR || $currentRole['role_id'] == Role::REVIEWER ) {
                $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
                $data = ArrayHelper::getColumn(Office::find()->isDeleted(false)->level("<={$level}")->all(), 'id');
            } else {
                $data = ArrayHelper::getColumn(Office::find()->isDeleted(false)->parent(Yii::$app->user->identity->person->office_id)->all(), 'id');
            }
            $query->andWhere(['monthly_form_submit.office_id' => $data]);
        }
        
        return $dataProvider;
    }

}
