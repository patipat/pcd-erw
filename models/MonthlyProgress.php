<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "monthly_progress".
 *
 * @property int $id
 * @property int $year ปี
 * @property int $month เดือน
 * @property int $office_id หน่วยงาน
 * @property int $current_pop จำนวนบุคลกรที่ส่งรายงานแล้ว
 * @property int $total_pop จำนวนบุคลกรทั้งหมด
 * @property string $percent % ที่ส่งรายงาน
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property User $createdBy
 * @property Office $office
 * @property User $updatedBy
 */
class MonthlyProgress extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'monthly_progress';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['year', 'month', 'office_id', 'current_pop', 'total_pop', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['percent'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'year' => Yii::t('app', 'ปี'),
            'month' => Yii::t('app', 'เดือน'),
            'office_id' => Yii::t('app', 'หน่วยงาน'),
            'current_pop' => Yii::t('app', 'จำนวนบุคลกรที่ส่งรายงานแล้ว'),
            'total_pop' => Yii::t('app', 'จำนวนบุคลกรทั้งหมด'),
            'percent' => Yii::t('app', '% ที่ส่งรายงาน'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice() {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return MonthlyProgressQuery the active query used by this AR class.
     */
    public static function find() {
        return new MonthlyProgressQuery(get_called_class());
    }

}
