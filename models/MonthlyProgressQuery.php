<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MonthlyProgress]].
 *
 * @see MonthlyProgress
 */
class MonthlyProgressQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return MonthlyProgress[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MonthlyProgress|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['monthly_progress.deleted' => $deleted]);
    }

    public function office($officeId) {
        return $this->andWhere(['monthly_progress.office_id' => $officeId]);
    }

    public function year($year) {
        return $this->andWhere(['monthly_progress.year' => $year]);
    }

    public function month($month) {
        return $this->andWhere(['monthly_progress.month' => $month]);
    }

}
