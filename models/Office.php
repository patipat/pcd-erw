<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "office".
 *
 * @property int $id รหัส
 * @property string $name สำนักงาน
 * @property string $address ที่อยู่
 * @property string $email อีเมลล์
 * @property string $tel เบอร์โทรศัพท์
 * @property string $line_token Line token
 * @property int $parent_id parent
 * @property int $office_type_id ประเภทหน่วยงาน
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 * @property int $level ระดับ
 *
 * @property MonthlyForm[] $monthlyForms
 * @property MonthlyFormSubmit[] $monthlyFormSubmits
 * @property User $createdBy
 * @property OfficeType $officeType
 * @property province $province
 * @property amphur $amphur
 * @property tambon $tambon
 * @property Office $parent
 * @property Office[] $offices
 * @property User $updatedBy
 * @property OfficePopulationH[] $officePopulationHs
 * @property Person[] $people
 * @property SixMonthForm[] $sixMonthForms
 * @property YearlyAssessment[] $yearlyAssessments
 * @property YearlyAssessmentTotal[] $yearlyAssessmentTotals
 */
class Office extends \yii\db\ActiveRecord {

    const SCENARIO_CREATE_UPDATE = 'create-update';

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'office';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            ['name', 'unique', 'filter' => ['deleted' => 0], 'on' => self::SCENARIO_CREATE_UPDATE],
            [['name'], 'required'],
            [['address'], 'string'],
            [['parent_id', 'office_type_id', 'deleted', 'created_by', 'updated_by', 'level', 'province_id', 'amphur_id', 'tambon_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'email', 'tel', 'line_token'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['office_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfficeType::className(), 'targetAttribute' => ['office_type_id' => 'id']],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']],
            [['amphur_id'], 'exist', 'skipOnError' => true, 'targetClass' => Amphur::className(), 'targetAttribute' => ['amphur_id' => 'id']],
            [['tambon_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tambon::className(), 'targetAttribute' => ['tambon_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัส'),
            'name' => Yii::t('app', 'สำนักงาน'),
            'address' => Yii::t('app', 'ที่อยู่'),
            'email' => Yii::t('app', 'อีเมลล์'),
            'tel' => Yii::t('app', 'เบอร์โทรศัพท์'),
            'line_token' => Yii::t('app', 'Line token'),
            'parent_id' => Yii::t('app', 'สังกัดในหน่วยงาน'),
            'province_id' => Yii::t('app', 'จังหวัด'),
            'amphur_id' => Yii::t('app', 'อำเภอ'),
            'tambon_id' => Yii::t('app', 'ตำบล'),
            'office_type_id' => Yii::t('app', 'ประเภทหน่วยงาน'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'level' => Yii::t('app', 'ระดับ'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonthlyForms() {
        return $this->hasMany(MonthlyForm::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonthlyFormSubmits() {
        return $this->hasMany(MonthlyFormSubmit::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeType() {
        return $this->hasOne(OfficeType::className(), ['id' => 'office_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTambon() {
        return $this->hasOne(Tambon::className(), ['id' => 'tambon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince() {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmphur() {
        return $this->hasOne(Amphur::className(), ['id' => 'amphur_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(Office::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffices() {
        return $this->hasMany(Office::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficePopulationHs() {
        return $this->hasMany(OfficePopulationH::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficePopulationH() {
        return $this->hasOne(OfficePopulationH::className(), ['office_id' => 'id'])
                        ->isDeleted(false)->orderBy('office_population_h.id DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople() {
        return $this->hasMany(Person::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSixMonthForms() {
        return $this->hasMany(SixMonthForm::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYearlyAssessments() {
        return $this->hasMany(YearlyAssessment::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYearlyAssessmentTotals() {
        return $this->hasMany(YearlyAssessmentTotal::className(), ['office_id' => 'id']);
    }

    public function calculateYearlyAssessment($year) {
        $ya = $this->getYearlyAssessments()->isDeleted(false)->year($year)->one();
        if (!isset($ya)) {
            $ya = new YearlyAssessment();
            $ya->office_id = $this->id;
            $ya->year = $year;
        }
        $yearMonths = Yii::$app->util->getYearMonths($year);
        $n = count($yearMonths);
        $cond = [];
        if ($yearMonths[0]['year'] == $yearMonths[$n - 1]['year']) {
            $cond = ['and',
                ['mf.year' => $yearMonths[0]['year']],
                ['>=', 'mf.month', $yearMonths[0]['month']],
                ['<=', 'mf.month', $yearMonths[$n - 1]['month']],
            ];
        } else {
            $cond = ['or',
                [
                    'and',
                    ['mf.year' => $yearMonths[0]['year']],
                    ['>=', 'mf.month', $yearMonths[0]['month']],
                    ['<=', 'mf.month', 12],
                ],
                [
                    'and',
                    ['mf.year' => $yearMonths[$n - 1]['year']],
                    ['>=', 'mf.month', 1],
                    ['<=', 'mf.month', $yearMonths[$n - 1]['month']],
                ],
            ];
        }
        $query = (new \yii\db\Query())
                ->select('CEIL(AVG(pop_int)) AS pop_int, CEIL(AVG(pop_ext)) AS pop_ext, SUM(work_day) AS work_day, SUM(garbage) AS garbage, SUM(bag) AS bag, SUM(cup) AS cup, SUM(foam) AS foam, SUM(mask) AS mask')
                ->from('monthly_form mf')
                ->innerJoin('office o', 'mf.office_id = o.id')
                ->andWhere(['mf.deleted' => 0, 'mf.office_id' => $this->id, 'o.deleted' => 0])
                ->andWhere($cond);
        $rec = $query->one();
//        $lastMonthlyForm = $this->getMonthlyForms()->isDeleted(false)->budgetYear($year)->orderBy([
//                    'monthly_form.year' => SORT_DESC,
//                    'monthly_form.month' => SORT_DESC,
//                ])->one();
//        $pop = isset($lastMonthlyForm) ? $lastMonthlyForm->pop_int : 0;
        $pop = $rec['pop_int'] + $rec['pop_ext'];
        $ya->pop_int = $rec['pop_int'];
        $ya->pop_ext = $rec['pop_ext']; //isset($lastMonthlyForm) ? $lastMonthlyForm->pop_ext : 0;
        $ya->office_population = $ya->pop_int + $ya->pop_ext;
        $ya->work_day = $rec['work_day'];
        $ya->garbage = $rec['garbage'];
        $ya->bag = $rec['bag'];
        $ya->cup = $rec['cup'];
        $ya->foam = $rec['foam'];
        $ya->mask = $rec['mask'];
        $ya->garbage_prev = $pop * 0.34 * $rec['work_day'];
        $ya->garbage_cur = $pop * ($rec['garbage'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->garbage_percent = (($ya->garbage_prev - $ya->garbage_cur) / $ya->garbage_prev) * 100;
        $ya->bag_prev = $pop * 0.94 * $rec['work_day'];
        $ya->bag_cur = $pop * ($rec['bag'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->bag_percent = (($ya->bag_prev - $ya->bag_cur) / $ya->bag_prev) * 100;
        $ya->cup_prev = $pop * 0.4 * $rec['work_day'];
        $ya->cup_cur = $pop * ($rec['cup'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->cup_percent = (($ya->cup_prev - $ya->cup_cur) / $ya->cup_prev) * 100;
        $ya->foam_prev = $pop * 0.47 * $rec['work_day'];
        $ya->foam_cur = $pop * ($rec['foam'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->foam_percent = (($ya->foam_prev - $ya->foam_cur) / $ya->foam_prev) * 100;
        $ya->garbage_score = AssessmentScore::getScore('garbage_percent', $ya->garbage_percent, $year);
        $ya->bag_score = AssessmentScore::getScore('bag_percent', $ya->bag_percent, $year);
        $ya->cup_score = AssessmentScore::getScore('cup_percent', $ya->cup_percent, $year);
        $ya->foam_score = AssessmentScore::getScore('foam_percent', $ya->foam_percent, $year);
        $ya->total_score = $ya->garbage_score + $ya->bag_score + $ya->cup_score + $ya->foam_score;
        if (!$ya->save()) {
            \yii\helpers\VarDumper::dump($ya->errors);
        }
    }

    public function calculateMonthlyAssessment($year, $startMonth, $endMonth)
    {
        $ya = new YearlyAssessment();
        $ya->office_id = $this->id;
        $ya->year = $year;
        $start = \explode('-', $startMonth);
        $end = \explode('-', $endMonth);
        $cond = [];
        if ($start[0] == $year) {
            $cond = [
                'and',
                ['mf.year' => $year],
                ['>=', 'mf.month', $start[1]],
                ['<=', 'mf.month', $end[1]],
            ];
        } else {
            $cond = [
                'or',
                [
                    'and',
                    ['mf.year' => $start[0]],
                    ['>=', 'mf.month', $start[1]],
                    ['<=', 'mf.month', 12],
                ],
                [
                    'and',
                    ['mf.year' => $end[0]],
                    ['>=', 'mf.month', 1],
                    ['<=', 'mf.month', $end[1]],
                ],
            ];
        }
        $query = (new \yii\db\Query())
            ->select('CEIL(AVG(pop_int)) AS pop_int, CEIL(AVG(pop_ext)) AS pop_ext, SUM(work_day) AS work_day, SUM(garbage) AS garbage, SUM(bag) AS bag, SUM(cup) AS cup, SUM(foam) AS foam, SUM(mask) AS mask')
            ->from('monthly_form mf')
            ->innerJoin('office o', 'mf.office_id = o.id')
            ->andWhere(['mf.deleted' => 0, 'mf.office_id' => $this->id, 'o.deleted' => 0])
            ->andWhere($cond);
        $rec = $query->one();
        //        $lastMonthlyForm = $this->getMonthlyForms()->isDeleted(false)->budgetYear($year)->orderBy([
        //                    'monthly_form.year' => SORT_DESC,
        //                    'monthly_form.month' => SORT_DESC,
        //                ])->one();
        //        $pop = isset($lastMonthlyForm) ? $lastMonthlyForm->pop_int : 0;
        $pop = $rec['pop_int'] + $rec['pop_ext'];
        $ya->pop_int = $rec['pop_int'];
        $ya->pop_ext = $rec['pop_ext']; //isset($lastMonthlyForm) ? $lastMonthlyForm->pop_ext : 0;
        $ya->office_population = $ya->pop_int + $ya->pop_ext;
        $ya->work_day = $rec['work_day'];
        $ya->garbage = $rec['garbage'];
        $ya->bag = $rec['bag'];
        $ya->cup = $rec['cup'];
        $ya->foam = $rec['foam'];
        $ya->mask = $rec['mask'];
        $ya->garbage_prev = $pop * 0.34 * $rec['work_day'];
        $ya->garbage_cur = ($pop * $rec['work_day'] == 0) ? 0 : $pop * ($rec['garbage'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->garbage_percent = $ya->garbage_prev == 0 ? 0 : (($ya->garbage_prev - $ya->garbage_cur) / $ya->garbage_prev) * 100;
        $ya->bag_prev = $pop * 0.94 * $rec['work_day'];
        $ya->bag_cur = ($pop * $rec['work_day'] == 0) ? 0 : $pop * ($rec['bag'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->bag_percent = $ya->bag_prev == 0 ? 0 : (($ya->bag_prev - $ya->bag_cur) / $ya->bag_prev) * 100;
        $ya->cup_prev = $pop * 0.4 * $rec['work_day'];
        $ya->cup_cur = ($pop * $rec['work_day'] == 0) ? 0 : $pop * ($rec['cup'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->cup_percent = $ya->cup_prev == 0 ? 0 : (($ya->cup_prev - $ya->cup_cur) / $ya->cup_prev) * 100;
        $ya->foam_prev = $pop * 0.47 * $rec['work_day'];
        $ya->foam_cur = ($pop * $rec['work_day'] == 0) ? 0 : $pop * ($rec['foam'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->foam_percent = $ya->foam_prev == 0 ? 0 : (($ya->foam_prev - $ya->foam_cur) / $ya->foam_prev) * 100;
        $ya->garbage_score = AssessmentScore::getScore('garbage_percent', $ya->garbage_percent, $year);
        $ya->bag_score = AssessmentScore::getScore('bag_percent', $ya->bag_percent, $year);
        $ya->cup_score = AssessmentScore::getScore('cup_percent', $ya->cup_percent, $year);
        $ya->foam_score = AssessmentScore::getScore('foam_percent', $ya->foam_percent, $year);
        $ya->total_score = $ya->garbage_score + $ya->bag_score + $ya->cup_score + $ya->foam_score;
        return $ya;
    }

    public function calculateYearlyAssessmentChildren($year) {
        $ya = $this->getYearlyAssessments()->isDeleted(false)->year($year)->one();
        if (!isset($ya)) {
            $ya = new YearlyAssessment();
            $ya->office_id = $this->id;
            $ya->year = $year;
        }
        $yearMonths = Yii::$app->util->getYearMonths($year);
        $childrenOfficeIds = ArrayHelper::getColumn($this->getOffices()->isDeleted(false)->all(), 'id');
        if (count($childrenOfficeIds) == 0) {
            $childrenOfficeIds[] = $this->id;
        }
//        $cond = '(';
//        foreach ($yearMonths as $ym) {
//            if ($cond != '(') {
//                $cond .= ' OR ';
//            }
//            $cond .= "(mf.year = {$ym['year']} AND mf.month = {$ym['month']})";
//        }
//        $cond = ')';
        $query = (new \yii\db\Query())
                ->select('SUM(work_day) AS work_day, SUM(garbage) AS garbage, SUM(bag) AS bag, SUM(cup) AS cup, SUM(foam) AS foam')
                ->from('monthly_form mf')
                ->innerJoin('office o', 'mf.office_id = o.id')
                ->andWhere(['mf.deleted' => 0, 'mf.office_id' => $childrenOfficeIds, 'o.deleted' => 0])
                ->andWhere(['and',
            ['mf.year' => $yearMonths[0]['year']],
            ['>=', 'mf.month', $yearMonths[0]['month']],
            ['mf.year' => $yearMonths[count($yearMonths) - 1]['year']],
            ['<=', 'mf.month', $yearMonths[count($yearMonths) - 1]['month']],
        ]);
        $rec = $query->one();
        $lastMonthlyForm = $this->getMonthlyForms()->isDeleted(false)->budgetYear($year)->orderBy([
                    'monthly_form.year' => SORT_DESC,
                    'monthly_form.month' => SORT_DESC,
                ])->one();
        $pop = isset($lastMonthlyForm) ? $lastMonthlyForm->pop_int + $lastMonthlyForm->pop_ext : 0;
        $ya->garbage_prev = $pop * 0.34 * $rec['work_day'];
        $ya->garbage_cur = $pop * ($rec['garbage'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->garbage_percent = (($ya->garbage_prev - $ya->garbage_cur) / $ya->garbage_prev) * 100;
        $ya->bag_prev = $pop * 0.94 * $rec['work_day'];
        $ya->bag_cur = $pop * ($rec['bag'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->bag_percent = (($ya->bag_prev - $ya->bag_cur) / $ya->bag_prev) * 100;
        $ya->cup_prev = $pop * 0.4 * $rec['work_day'];
        $ya->cup_cur = $pop * ($rec['cup'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->cup_percent = (($ya->cup_prev - $ya->cup_cur) / $ya->cup_prev) * 100;
        $ya->foam_prev = $pop * 0.47 * $rec['work_day'];
        $ya->foam_cur = $pop * ($rec['cup'] / ($pop * $rec['work_day'])) * $rec['work_day'];
        $ya->foam_percent = (($ya->foam_prev - $ya->foam_cur) / $ya->foam_prev) * 100;
        $ya->save();
        if (isset($this->parent)) {
            $this->parent->calculateYearlyAssessment($year);
        }
    }

    public function getCalculatedLevel() {
        $sd = $this;
        $level = 1;
        while (isset($sd->parent)) {
            $level++;
            $sd = $sd->parent;
        }
        return $level;
    }

    public function getAllChildren() {
        $sql = <<<sql
            SELECT id, name, parent_id 
            FROM (
                SELECT * 
                FROM office 
                WHERE deleted = 0
                ORDER BY parent_id, id
            ) s, (
                SELECT @pv2 := '{$this->id}'
            ) initialisation 
            WHERE FIND_IN_SET(parent_id, @pv2) > 0 and @pv2 := concat(@pv2,',', id)
sql;
        return Yii::$app->db->createCommand($sql)->queryAll();
    }

    public function calculateParentSixMonthForm($year) {
        if (isset($this->parent)) {

            $smf = SixMonthForm::find()->isDeleted(false)->year($year)
                            ->office($this->parent_id)->last()->one();
            if (!isset($smf) || (isset($smf->checked_at) && $smf->is_pass == 0)) {
                $smf = new SixMonthForm();
                $smf->year = $year;
                $smf->office_id = $this->parent_id;
                $smf->save();
            }
            $ais = AssessIssue::find()->isDeleted(false)->year($year)->all();
            foreach ($ais as $ai) {
                $fi = FormIssue::find()->isDeleted(false)->sixMonth($smf->id)->assessIssue($ai->id)->one();
                if (!isset($fi)) {
                    $fi = new FormIssue();
                    $fi->six_month_form_id = $smf->id;
                    $fi->assess_issue_id = $ai->id;
                    $fi->save();
                }
                $fifs = FormIssueFile::find()->joinWith(['formIssue'
                                    , 'formIssue.sixMonthForm', 'formIssue.sixMonthForm.office'])
                                ->isDeleted(false)->assessIssue($ai->id)->officeParent($this->parent_id)->all();
                // echo Json::encode(ArrayHelper::getColumn($fifs, 'formIssue.sixMonthForm.office_id')) . "\n";
                // echo Json::encode(ArrayHelper::getColumn($fifs, 'id')) . "\n";
                foreach ($fifs as $fif) {
                    $newFif = FormIssueFile::find()->isDeleted(false)->formIssue($fi->id)->fileName($fif->file_name)->one();
                    if (!isset($newFif)) {
                        $newFif = new FormIssueFile();
                        $newFif->form_issue_id = $fi->id;
                        $newFif->type = $fif->type;
                        $newFif->name = $fif->name;
                        $newFif->file_name = $fif->file_name;
                        $newFif->save();
                    }
                }
            }
        }
    }

    public function calculateParentMonthlyForm($year, $month) {
//        $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
        if (isset($this->parent)) {

            $sql = <<<sql
                    SELECT MIN(source_type_id) AS source_type_id, SUM(pop_int) AS pop_int, SUM(pop_ext) AS pop_ext
                        , SUM(office_population) AS office_population
                        , CEIL(AVG(work_day)) AS work_day, SUM(garbage) AS garbage
                        , SUM(bag) AS bag, SUM(cup) AS cup, SUM(foam) AS foam, SUM(mask) AS mask
                    FROM monthly_form_submit
                    WHERE id IN (
                        SELECT MAX(mfs.id)
                        FROM monthly_form_submit mfs
                            INNER JOIN office o ON mfs.office_id = o.id
                        WHERE mfs.deleted = 0 AND o.deleted = 0 AND o.parent_id = {$this->parent_id}
                            AND mfs.confirmed_at IS NOT NULL AND mfs.year = {$year} AND mfs.month = {$month}
                        GROUP BY mfs.office_id
                    )
sql;
            $rec = Yii::$app->db->createCommand($sql)->queryOne();
            $mfs = MonthlyFormSubmit::find()->isDeleted(false)->year($year)->month($month)
                            ->office($this->parent_id)->last()->one();
            if (!isset($mfs) || isset($mfs->confirmed_by)) {
                $mfs = new MonthlyFormSubmit();
                $mfs->year = $year;
                $mfs->month = $month;
                $mfs->office_id = $this->parent_id;
            }
            $this->updatePopulation($year, $month);
            $mfs->attributes = $rec;
            $mfs->submitted_at = date('Y-m-d H:i:s');
            $mfs->save();
        }
    }

    public function calculateMonthlyProgress($year, $month) {

        if (isset($this->parent)) {
            $totalPopInt = $this->parent->getTotalChildrenPop('pop_int', $year, $month);
            $confirmedPopInt = $this->parent->getConfirmedChildrenPop('pop_int', $year, $month);
            $mp = MonthlyProgress::find()->isDeleted(false)->office($this->parent_id)->year($year)->month($month)->one();
            if (!isset($mp)) {
                $mp = new MonthlyProgress();
                $mp->office_id = $this->parent_id;
                $mp->year = $year;
                $mp->month = $month;
            }
            $mp->current_pop = $confirmedPopInt;
            $mp->total_pop = $totalPopInt;
            $mp->percent = ($confirmedPopInt / $totalPopInt) * 100;
            $mp->save();
        }
    }

    public function getHasChildren() {
        return $this->childrenCount > 0;
    }

    public function getChildrenCount($officeType = null, $level = null) {
//        $currentRole = Yii::$app->session->get('currentRole');
//        $q = Office::find()->isDeleted(false);
//        if (!empty($officeType) || !empty($level)) {
//            if (!empty($officeType)) {
//                $q->officeType($officeType);
//            }
//            if (!empty($level)) {
//                $q->level($level);
//            }
//        } else {
////            if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR) {
////                $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
////                $q->level('<=' . $level);
////            } else {
////                $q->parent($this->id);
////            }
//            $q->parent($this->id);
//        }
//        return $q->count();
        return $this->getOffices()->isDeleted(false)->count();
    }

    public function getChildren() {
//        $currentRole = Yii::$app->session->get('currentRole');
//        if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR) {
//            $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
//            return Office::find()->isDeleted(false)->level('<='.$level)->all();
//        } else {
//            return $this->getOffices()->isDeleted(false)->all();
//        }
        return $this->getOffices()->isDeleted(false)->all();
    }

//    public function getCurrentChildrenSavedPopNumber() {
//        $childrenIds = ArrayHelper::getColumn($this->getChildren(), 'id');
//        if (count($childrenIds) == 0) {
//            return 0;
//        }
//        $cond = implode(',', $childrenIds);
//        $sql = <<<sql
//            SELECT COUNT(*)
//            FROM office_population_h
//            WHERE id IN (
//                SELECT MAX(op.id)
//                FROM office_population_h op
//                WHERE op.deleted = 0 AND o.office_id IN ({$cond})
//                GROUP BY op.office_id
//            )
//sql;
//        return Yii::$app->db->createCommand($sql)->queryScalar();
//    }

    public function getConfirmedPop($field, $year, $month, $officeType = null, $level = null) {
        if (!isset($year) || !isset($month)) {
            return 0;
        }
        $cond = "";
        if (isset($officeType) || isset($level)) {
            if (isset($officeType)) {
                $cond .= " AND o.office_type_id = {$officeType}";
            }
            if (isset($level)) {
                $cond .= " AND o.level = {$level}";
            }
        } else {
            $cond .= " AND mfs.office_id = {$this->id}";
        }
        $sql = <<<sql
            SELECT mfs.{$field}
            FROM monthly_form_submit mfs
                INNER JOIN office o ON mfs.office_id = o.id
            WHERE mfs.deleted = 0 AND o.deleted = 0 {$cond}
                AND mfs.confirmed_at IS NOT NULL AND mfs.year = {$year}
                AND mfs.month = {$month}
            ORDER BY mfs.id DESC
            LIMIT 1
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalChildrenPop($field, $year, $month, $officeType = null, $level = null) {
//        $sum = 0;
//        $children = $this->getChildren();
//        foreach ($children as $child) {
//            $confirmed = $child->getConfirmedPop($field, $year, $month);
//            if ($confirmed == 0) {
//                $confirmed = isset($child->officePopulationH) ? $child->officePopulationH->{$field} : 0;
//            }
//            $sum += $confirmed;
//        }
//        return $sum;
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $yearMonthCond = '';
        if (isset($year)) {
            $yearMonthCond .= " AND mfs.year = {$year}";
        }
        if (isset($month)) {
            $yearMonthCond .= " AND mfs.month = {$month}";
        }
        $sql = <<<sql
            SELECT SUM(CASE WHEN tb2.{$field} IS NULL THEN (
                    SELECT {$field}
                    FROM office_population_h
                    WHERE deleted = 0 AND office_id = tb1.id
                    ORDER BY id DESC
                    LIMIT 1
                )
                ELSE tb2.{$field} END)
            FROM 
            (
                SELECT o.id
                FROM office o
                WHERE o.deleted = 0 {$cond}
            ) tb1 LEFT JOIN (
                SELECT office_id, {$field}
                FROM monthly_form_submit
                WHERE id IN (
                    SELECT MAX(mfs.id)
                    FROM monthly_form_submit mfs
                        INNER JOIN office o ON mfs.office_id = o.id
                    WHERE mfs.deleted = 0 {$cond}
                        AND mfs.confirmed_at IS NOT NULL {$yearMonthCond}
                    GROUP BY mfs.office_id
                )
            ) tb2 ON tb1.id = tb2.office_id
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getConfirmedChildrenPop($field, $year, $month, $officeType = null, $level = null) {
//        $sum = 0;
//        $children = $this->getChildren();
//        foreach ($children as $child) {
//            $sum += $child->getConfirmedPop($field, $year, $month, $officeType, $level);
//        }
//        return $sum;
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT SUM({$field})
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 {$cond}
                    AND o.deleted = 0 AND mfs.confirmed_at IS NOT NULL AND mfs.year = {$year} AND mfs.month = {$month}
                GROUP BY mfs.office_id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getYearlyAssessment($year) {
        return $this->getYearlyAssessments()->isDeleted(false)->year($year)->one();
    }

    public function getMonthlyAssessment($year,$month) {
        return $this->getMonthlyForms()->isDeleted(false)->year($year)->month($month)->one();
    }    
    
    public function getCurrentSubmittedMonthlyPercent($year) {
        $c = $this->getCurrentSubmittedMonthlyNumber($year);
        $t = $this->getTotalSubmittedMonthlyNumber($year);
        if (!empty($t)) {
            $percent = ($c / $t) * 100;
        } else {
            $percent = 0;
        }
        return $percent;
    }

    public function getCurrentConfirmedMonthlyPercent($year) {
        $c = $this->getCurrentConfirmedMonthlyNumber($year);
        $t = $this->getTotalConfirmedMonthlyNumber($year);
        if (!empty($t)) {
            $percent = ($c / $t) * 100;
        } else {
            $percent = 0;
        }
        return $percent;
    }

    public function getCurrentCheckedMonthlyPercent($year) {
        $c = $this->getCurrentCheckedMonthlyNumber($year);
        $t = $this->getTotalCheckedMonthlyNumber($year);
        if (!empty($t)) {
            $percent = ($c / $t) * 100;
        } else {
            $percent = 0;
        }
        return $percent;
    }

    public function getCurrentFailedMonthlyPercent($year) {
        $c = $this->getCurrentFailedMonthlyNumber($year);
        $t = $this->getTotalFailedMonthlyNumber($year);
        if (!empty($t)) {
            $percent = ($c / $t) * 100;
        } else {
            $percent = 0;
        }
        return $percent;
    }

    public function getCurrentSubmittedMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
                    AND mfs.submitted_at IS NOT NULL AND mfs.office_id = {$this->id}
                    {$cond}
                GROUP BY mfs.office_id, mfs.year, mfs.month
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentConfirmedMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
                    AND mfs.confirmed_at IS NOT NULL AND mfs.office_id = {$this->id}
                    {$cond}
                GROUP BY mfs.office_id, mfs.year, mfs.month
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentPassedMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
                    AND mfs.checked_at IS NOT NULL AND mfs.is_pass = 1 AND mfs.office_id = {$this->id}
                    {$cond}
                GROUP BY mfs.office_id, mfs.year, mfs.month
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentConfirmPendingMonthlyChildren($year, $month, $officeType = null, $level = null) {
//        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
//        $sql = <<<sql
//            SELECT COUNT(*) 
//            FROM monthly_form_submit
//            WHERE id IN (
//                SELECT MAX(mfs.id)
//                FROM monthly_form_submit mfs
//                    INNER JOIN office o ON mfs.office_id = o.id
//                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
//                    AND mfs.confirmed_at IS NULL AND mfs.submitted_at IS NOT NULL AND o.parent_id = {$this->id}
//                    {$cond}
//                GROUP BY mfs.office_id, mfs.year, mfs.month
//            )
//sql;
//        return Yii::$app->db->createCommand($sql)->queryScalar();
        $searchModel = new MonthlyFormSubmitSearch();
        $searchModel->year = $year;
        $searchModel->month = $month;
        if (isset($officeType)) {
            $searchModel->officeType = $officeType;
            $searchModel->level = $level;
        } else {
            $searchModel->parentId = Yii::$app->user->identity->person->office_id;
        }
        $searchModel->isConfirmed = false;
        $searchModel->isLastSubmit = true;
        $dataProvider = $searchModel->search([]);
        $dataProvider->pagination = false;
        return $dataProvider;
    }

    public function getCurrentConfirmPendingMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT COUNT(*) 
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
                    AND mfs.confirmed_at IS NULL AND mfs.submitted_at IS NOT NULL AND mfs.office_id = {$this->id}
                    {$cond}
                GROUP BY mfs.office_id, mfs.year, mfs.month
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentCheckPendingMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT COUNT(*) 
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
                    AND mfs.office_id = {$this->id}
                    {$cond}
                GROUP BY mfs.office_id, mfs.year, mfs.month
            )
            AND checked_at IS NULL AND confirmed_at IS NOT NULL
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getReConfirmedMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT CASE WHEN SUM(c) IS NULL THEN 0 ELSE SUM(c) END FROM (
                SELECT CASE WHEN COUNT(*) = 0 THEN COUNT(*) ELSE COUNT(*) - 1 END AS c
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.confirmed_at IS NOT NULL 
                    AND mfs.office_id = {$this->id} {$cond}
                GROUP BY mfs.office_id, mfs.year, mfs.month
            ) tb
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getLateConfirmedMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit mfs
                INNER JOIN office o ON mfs.office_id = o.id
            WHERE mfs.deleted = 0 AND mfs.confirmed_at IS NOT NULL 
                AND mfs.office_id = {$this->id} {$cond}
                AND mfs.is_late = 1
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentCheckedMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
                    AND mfs.checked_at IS NOT NULL AND mfs.office_id = {$this->id}
                    {$cond}
                GROUP BY mfs.office_id, mfs.year, mfs.month
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentFailedMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
                    AND mfs.checked_at IS NOT NULL AND mfs.is_pass = 0 AND mfs.office_id = {$this->id}
                    {$cond}
                GROUP BY mfs.office_id, mfs.year, mfs.month
            ) AND EXISTS (
                SELECT id
                FROM monthly_form_submit mfs2
                WHERE mfs2.deleted = 0 AND mfs2.office_id = monthly_form_submit.office_id
                    AND mfs2.checked_at IS NOT NULL AND mfs2.is_pass = 0 AND mfs2.office_id = {$this->id}
                    AND mfs2.year = monthly_form_submit.year AND mfs2.month = monthly_form_submit.month
                    AND mfs2.id > monthly_form_submit.id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalFailedMonthlyNumber($year) {
        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit mfs
                INNER JOIN office o ON mfs.office_id = o.id
            WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.checked_at IS NOT NULL AND mfs.is_pass = 0 
                AND mfs.office_id = {$this->id}
                {$cond}
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentConfirmedMonthlyChildren($year, $month, $officeType = null, $level = null) {
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id AND o.deleted = 0
                    AND mfs.confirmed_at IS NOT NULL {$cond}
                    AND mfs.year = {$year} AND mfs.month = {$month}
                GROUP BY mfs.office_id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentCheckedMonthlyChildren($year, $month, $officeType = null, $level = null) {
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
                    AND mfs.checked_at IS NOT NULL {$cond}
                    AND mfs.year = {$year} AND mfs.month = {$month}
                GROUP BY mfs.office_id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalSubmittedMonthlyNumber($year) {
        return Setting::getValue(Setting::MONTHLY_MONTHS);
    }

    public function getTotalConfirmedMonthlyNumber($year) {
//        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
//        $sql = <<<sql
//            SELECT COUNT(*)
//            FROM monthly_form_submit
//            WHERE id IN (
//                SELECT MAX(mfs.id)
//                FROM monthly_form_submit mfs
//                    INNER JOIN office o ON mfs.office_id = o.id
//                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
//                    AND mfs.submitted_at IS NOT NULL AND mfs.office_id = {$this->id}
//                    {$cond}
//                GROUP BY mfs.office_id
//            )
//sql;
//        return Yii::$app->db->createCommand($sql)->queryScalar();
        return $this->getTotalSubmittedMonthlyNumber($year);
    }

    public function getTotalCheckedMonthlyNumber($year) {
//        $cond = \Yii::$app->util->getMonthlyFormBudgetYearCond($year, 'mfs');
//        $sql = <<<sql
//            SELECT COUNT(*)
//            FROM monthly_form_submit
//            WHERE id IN (
//                SELECT MAX(mfs.id)
//                FROM monthly_form_submit mfs
//                    INNER JOIN office o ON mfs.office_id = o.id
//                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id
//                    AND mfs.checked_at IS NULL AND mfs.confirmed_at IS NOT NULL AND mfs.office_id = {$this->id}
//                    {$cond}
//                GROUP BY mfs.office_id
//            )
//sql;
//        return Yii::$app->db->createCommand($sql)->queryScalar();
        return $this->getTotalSubmittedMonthlyNumber($year);
    }

    public function getTotalConfirmedMonthlyChildren($year, $month, $officeType = null, $level = null) {
//        $officeIds = ArrayHelper::getColumn($this->getChildren(), 'id');
//        $cond = implode(',', $officeIds);
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT COUNT(*)
            FROM monthly_form_submit
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM monthly_form_submit mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = monthly_form_submit.office_id and o.deleted = 0
                    {$cond}
                    AND mfs.year = {$year} AND mfs.month = {$month}
                GROUP BY mfs.office_id
            )
            AND confirmed_at IS NOT NULL AND checked_at IS NULL 
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalConfirmedSixMonthChildren($year, $officeType = null, $level = null) {
//        $officeIds = ArrayHelper::getColumn($this->getChildren(), 'id');
//        $cond = implode(',', $officeIds);
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    {$cond}
                    AND mfs.confirmed_at IS NOT NULL
                    AND mfs.year = {$year}
                GROUP BY mfs.office_id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalConfirmedSixMonthChildrenPop($year, $officeType = null, $level = null) {
        //        $officeIds = ArrayHelper::getColumn($this->getChildren(), 'id');
        //        $cond = implode(',', $officeIds);
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT SUM(pop_int)
            FROM office_population_h
            WHERE id IN (
                SELECT MAX(id)
                FROM office_population_h
                WHERE office_id IN (
                    SELECT office_id
                    FROM six_month_form
                    WHERE id IN (
                        SELECT MAX(mfs.id)
                        FROM six_month_form mfs
                            INNER JOIN office o ON mfs.office_id = o.id
                        WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.office_id = six_month_form.office_id
                            {$cond}
                            AND mfs.confirmed_at IS NOT NULL
                            AND mfs.year = {$year}
                        GROUP BY mfs.office_id
                    )
                )
                GROUP BY office_id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalPassedSixMonthChildren($year, $officeType = null, $level = null) {
//        $officeIds = ArrayHelper::getColumn($this->getChildren(), 'id');
//        $cond = implode(',', $officeIds);
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    {$cond}
                    AND mfs.checked_at IS NOT NULL AND mfs.is_pass = 1
                    AND mfs.year = {$year}
                GROUP BY mfs.office_id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalPassedSixMonthChildrenPop($year, $officeType = null, $level = null) {
        //        $officeIds = ArrayHelper::getColumn($this->getChildren(), 'id');
        //        $cond = implode(',', $officeIds);
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT SUM(pop_int)
            FROM office_population_h
            WHERE id IN (
                SELECT MAX(id)
                FROM office_population_h
                WHERE office_id IN (
                    SELECT office_id
                    FROM six_month_form
                    WHERE id IN (
                        SELECT MAX(mfs.id)
                        FROM six_month_form mfs
                            INNER JOIN office o ON mfs.office_id = o.id
                        WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.office_id = six_month_form.office_id
                            {$cond}
                            AND mfs.checked_at IS NOT NULL AND mfs.is_pass = 1
                            AND mfs.year = {$year}
                        GROUP BY mfs.office_id
                    )
                )
                GROUP BY office_id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalNoPassedSixMonthChildren($year, $officeType = null, $level = null) {
//        $officeIds = ArrayHelper::getColumn($this->getChildren(), 'id');
//        $cond = implode(',', $officeIds);
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    {$cond}
                    AND mfs.checked_at IS NOT NULL AND mfs.is_pass = 0
                    AND mfs.year = {$year}
                GROUP BY mfs.office_id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalCheckPendingSixMonthChildren($year, $officeType = null, $level = null) {
//        $officeIds = ArrayHelper::getColumn($this->getChildren(), 'id');
//        $cond = implode(',', $officeIds);
        $cond = Yii::$app->util->getDashboardCond($this, $officeType, $level);
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    {$cond}
                    AND mfs.year = {$year}
                GROUP BY mfs.office_id
            )
            AND confirmed_at IS NOT NULL AND checked_at IS NULL
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalConfirmedSixMonth($year) {
//        $officeIds = ArrayHelper::getColumn($this->getChildren(), 'id');
//        $cond = implode(',', $officeIds);
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND o.deleted = 0
                    AND mfs.confirmed_at IS NOT NULL AND mfs.checked_at IS NULL 
                    AND mfs.year = {$year}
                GROUP BY mfs.office_id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentSubmittedSixMonthNumber($year) {
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    AND mfs.year = {$year} AND mfs.office_id = {$this->id}
                    AND mfs.submitted_at IS NOT NULL
                GROUP BY mfs.office_id, mfs.year
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentConfirmedSixMonthNumber($year) {
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    AND mfs.year = {$year} AND mfs.office_id = {$this->id}
                    AND mfs.confirmed_at IS NOT NULL
                GROUP BY mfs.office_id, mfs.year
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentPassedSixMonthNumber($year) {
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    AND mfs.checked_at IS NOT NULL AND mfs.is_pass = 1 AND mfs.office_id = {$this->id}
                    AND mfs.year = {$year}
                GROUP BY mfs.office_id, mfs.year
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentConfirmPendingSixMonthNumber($year) {
        $sql = <<<sql
            SELECT COUNT(*) 
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    AND mfs.confirmed_at IS NULL AND mfs.submitted_at IS NOT NULL AND mfs.office_id = {$this->id}
                    AND mfs.year = {$year}
                GROUP BY mfs.office_id, mfs.year
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentCheckPendingSixMonthNumber($year) {
        $sql = <<<sql
            SELECT COUNT(*) 
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    AND mfs.office_id = {$this->id}
                    AND mfs.year = {$year}
                GROUP BY mfs.office_id, mfs.year
            )
            AND checked_at IS NULL AND confirmed_at IS NOT NULL
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getReConfirmedSixMonthNumber($year) {
        $sql = <<<sql
            SELECT CASE WHEN SUM(c) IS NULL THEN 0 ELSE SUM(c) END FROM (
                SELECT CASE WHEN COUNT(*) = 0 THEN COUNT(*) ELSE COUNT(*) - 1 END AS c
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.confirmed_at IS NOT NULL 
                    AND mfs.office_id = {$this->id} AND mfs.year = {$year}
                GROUP BY mfs.office_id, mfs.year
            ) tb
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getLateConfirmedSixMonthNumber($year) {

        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form mfs
                INNER JOIN office o ON mfs.office_id = o.id
            WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.confirmed_at IS NOT NULL 
                AND mfs.office_id = {$this->id} AND mfs.year = {$year}
                AND mfs.is_late = 1
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentCheckedSixMonthNumber($year) {
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    AND mfs.year = {$year} AND mfs.office_id = {$this->id}
                    AND mfs.checked_at IS NOT NULL
                GROUP BY mfs.office_id, mfs.year
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalSubmittedSixMonthNumber($year) {
        return AssessIssue::find()->isDeleted(false)->count();
    }

    public function getTotalConfirmedSixMonthNumber($year) {
//        $sql = <<<sql
//            SELECT COUNT(*)
//            FROM six_month_form
//            WHERE id IN (
//                SELECT MAX(mfs.id)
//                FROM six_month_form mfs
//                    INNER JOIN office o ON mfs.office_id = o.id
//                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
//                    AND mfs.year = {$year} AND mfs.office_id = {$this->id}
//                    AND mfs.confirmed_at IS NULL AND mfs.submitted_at IS NOT NULL
//                GROUP BY mfs.office_id
//            )
//sql;
//        return Yii::$app->db->createCommand($sql)->queryScalar();
        return $this->getTotalSubmittedSixMonthNumber($year);
    }

    public function getTotalCheckedSixMonthNumber($year) {
//        $sql = <<<sql
//            SELECT COUNT(*)
//            FROM six_month_form
//            WHERE id IN (
//                SELECT MAX(mfs.id)
//                FROM six_month_form mfs
//                    INNER JOIN office o ON mfs.office_id = o.id
//                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
//                    AND mfs.year = {$year} AND mfs.office_id = {$this->id}
//                    AND mfs.checked_at IS NULL AND mfs.confirmed_at IS NOT NULL
//                GROUP BY mfs.office_id
//            )
//sql;
//        return Yii::$app->db->createCommand($sql)->queryScalar();
        return $this->getTotalSubmittedSixMonthNumber($year);
    }

    public function getCurrentFailedSixMonthNumber($year) {
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form
            WHERE id IN (
                SELECT MAX(mfs.id)
                FROM six_month_form mfs
                    INNER JOIN office o ON mfs.office_id = o.id
                WHERE mfs.deleted = 0 AND mfs.office_id = six_month_form.office_id
                    AND mfs.year = {$year} AND mfs.office_id = {$this->id}
                    AND mfs.checked_at IS NOT NULL AND mfs.is_pass = 0
                GROUP BY mfs.office_id, mfs.year
            ) AND EXISTS (
                SELECT id
                FROM six_month_form mfs2
                WHERE mfs2.deleted = 0 AND mfs2.office_id = six_month_form.office_id
                    AND mfs2.checked_at IS NOT NULL AND mfs2.is_pass = 0 AND mfs2.office_id = {$this->id}
                    AND mfs2.year = six_month_form.year
                    AND mfs2.id > six_month_form.id
            )
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getTotalFailedSixMonthNumber($year) {
        $sql = <<<sql
            SELECT COUNT(*)
            FROM six_month_form mfs
                INNER JOIN office o ON mfs.office_id = o.id
            WHERE mfs.deleted = 0 AND o.deleted = 0 AND mfs.office_id = {$this->id}
                AND mfs.year = {$year} 
                AND mfs.checked_at IS NOT NULL AND mfs.is_pass = 0
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function getCurrentSubmittedSixMonthPercent($year) {
        $c = $this->getCurrentSubmittedSixMonthNumber($year);
        $t = $this->getTotalSubmittedSixMonthNumber($year);
        if (!empty($t)) {
            $percent = ($c / $t) * 100;
        } else {
            $percent = 0;
        }
        return $percent;
    }

    public function getCurrentConfirmedSixMonthPercent($year) {
        $c = $this->getCurrentConfirmedSixMonthNumber($year);
        $t = $this->getTotalConfirmedSixMonthNumber($year);
        if (!empty($t)) {
            $percent = ($c / $t) * 100;
        } else {
            $percent = 0;
        }
        return $percent;
    }

    public function getCurrentCheckedSixMonthPercent($year) {
        $c = $this->getCurrentCheckedSixMonthNumber($year);
        $t = $this->getTotalCheckedSixMonthNumber($year);
        if (!empty($t)) {
            $percent = ($c / $t) * 100;
        } else {
            $percent = 0;
        }
        return $percent;
    }

    public function getCurrentFailedSixMonthPercent($year) {
        $c = $this->getCurrentFailedSixMonthNumber($year);
        $t = $this->getTotalFailedSixMonthNumber($year);
        if (!empty($t)) {
            $percent = ($c / $t) * 100;
        } else {
            $percent = 0;
        }
        return $percent;
    }

    public function getUnsetOfficeNumber() {
        $cond = '';
        $currentRole = Yii::$app->session->get('currentRole');
        $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
        if ($currentRole['role_id'] != Role::SUPER_ADMINISTRATOR) {
            if ($this->hasChildren) {
                $cond = " AND o.parent_id = {$this->id}";
            } else {
                $cond = " AND o.parent_id = {$this->parent_id}";
            }
        } else {
            $cond = " AND o.level <= {$level}";
        }
        $sql = <<<sql
                SELECT COUNT(*)
                FROM office o
                WHERE o.deleted = 0
                    AND NOT EXISTS(
                        SELECT id
                        FROM office_population_h oph
                        WHERE oph.deleted = 0 AND oph.office_id = o.id
                        LIMIT 1
                    )
                    {$cond}
sql;
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }

    public function updatePopulation($year = null, $month = null) {
        if ($this->hasChildren) {
            $op = $this->officePopulationH;
            if (!isset($op)) {
                $op = new OfficePopulationH();
                $op->office_id = $this->id;
            }
            $op->pop_int = $this->getTotalChildrenPop('pop_int', $year, $month);
            $op->pop_ext = $this->getTotalChildrenPop('pop_ext', $year, $month);
            $op->total = $op->pop_int + $op->pop_ext;
            $op->save();
        }
        if (isset($this->parent)) {
            $this->parent->updatePopulation($year, $month);
        }
    }

    public function getTypeName() {
        return "{$this->name} ({$this->officeType->name})";
    }

    /**
     * {@inheritdoc}
     * @return OfficeQuery the active query used by this AR class.
     */
    public static function find() {
        return new OfficeQuery(get_called_class());
    }

}
