<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "office_population_d".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $office_population_h_id หน่วยงาน
 * @property string $office_name หน่วยงาน
 * @property int $pop_int บุคลากรภายใน
 * @property int $pop_ext บุคลากรภายนอก
 * @property int $total รวม
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property OfficePopulationH $officePopulationH
 * @property User $createdBy
 * @property User $updatedBy
 */
class OfficePopulationD extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'office_population_d';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['office_population_h_id', 'pop_int', 'pop_ext', 'total', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['office_name'], 'string', 'max' => 255],
            [['office_population_h_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfficePopulationH::className(), 'targetAttribute' => ['office_population_h_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'office_population_h_id' => Yii::t('app', 'หน่วยงาน'),
            'office_name' => Yii::t('app', 'หน่วยงาน'),
            'pop_int' => Yii::t('app', 'บุคลากรภายใน'),
            'pop_ext' => Yii::t('app', 'บุคลากรภายนอก'),
            'total' => Yii::t('app', 'รวม'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficePopulationH()
    {
        return $this->hasOne(OfficePopulationH::className(), ['id' => 'office_population_h_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return OfficePopulationDQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OfficePopulationDQuery(get_called_class());
    }
}
