<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OfficePopulationD]].
 *
 * @see OfficePopulationD
 */
class OfficePopulationDQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return OfficePopulationD[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return OfficePopulationD|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
