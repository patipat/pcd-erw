<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "office_population_h".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $office_id หน่วยงาน
 * @property int $pop_int บุคลากรภายใน
 * @property int $pop_ext บุคลากรภายนอก
 * @property int $total รวม
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property OfficePopulationD[] $officePopulationDs
 * @property Office $office
 * @property User $createdBy
 * @property User $updatedBy
 */
class OfficePopulationH extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'office_population_h';
    }

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'createdByPerson.fullName',
            'updatedByPerson.fullName',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['pop_int', 'pop_ext'], 'required'],
            [['pop_int', 'pop_ext'], 'filter', 'filter' => function($value) {
                    return str_replace(',', '', $value);
                }],
            [['office_id', 'pop_int', 'pop_ext', 'total', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'office_id' => Yii::t('app', 'หน่วยงาน'),
            'pop_int' => Yii::t('app', 'บุคลากรภายใน'),
            'pop_ext' => Yii::t('app', 'บุคลากรภายนอก'),
            'total' => Yii::t('app', 'รวม'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'updatedByPerson.fullName' => Yii::t('app', 'ผู้แก้ไขข้อมูล'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficePopulationDs() {
        return $this->hasMany(OfficePopulationD::className(), ['office_population_h_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice() {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return OfficePopulationHQuery the active query used by this AR class.
     */
    public static function find() {
        return new OfficePopulationHQuery(get_called_class());
    }

}
