<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OfficePopulationH]].
 *
 * @see OfficePopulationH
 */
class OfficePopulationHQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return OfficePopulationH[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return OfficePopulationH|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['office_population_h.deleted' => $deleted]);
    }

    public function officeId($officeId) {
        return $this->andWhere(['office_population_h.office_id' => $officeId]);
    }

}
