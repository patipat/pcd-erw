<?php


namespace app\models;
use Yii;

/**
 * This is the ActiveQuery class for [[Office]].
 *
 * @see Office
 */
class OfficeQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Office[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Office|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['office.deleted' => $deleted]);
    }

    public function parent($parentId) {
        return $this->andWhere(['office.parent_id' => $parentId]);
    }

    public function level($level) {
        return $this->andFilterCompare('office.level', $level);
    }

    public function officeType($ot) {
        return $this->andWhere(['office.office_type_id' => $ot]);
    }

    public function noConfirmedMonthlyForm($year, $month) {
        $subQuery = (new \yii\db\Query())
                ->select('mfs.id')
                ->from('monthly_form_submit mfs')
                ->andWhere(['mfs.deleted' => 0])
                ->andWhere(['not', ['mfs.confirmed_at' => null]])
                ->andWhere(['mfs.year' => $year, 'mfs.month' => $month])
                ->andWhere('mfs.office_id = office.id');
        return $this->andWhere(['not exists', $subQuery]);
    }

    public function noConfirmedSixMonthForm($year) {
        $subQuery = (new \yii\db\Query())
                ->select('mfs.id')
                ->from('six_month_form mfs')
                ->andWhere(['mfs.deleted' => 0])
                ->andWhere(['not', ['mfs.confirmed_at' => null]])
                ->andWhere(['mfs.year' => $year])
                ->andWhere('mfs.office_id = office.id');
        return $this->andWhere(['not exists', $subQuery]);
    }

    public function amphur($amphurId) {
        return $this->andWhere(['office.amphur_id' => $amphurId]);
    }

    public function province($provinceId) {
        return $this->andWhere(['office.province_id' => $provinceId]);
    }

    public function tambon($tambonId) {
        return $this->andWhere(['office.tambon_id' => $tambonId]);
    }

}
