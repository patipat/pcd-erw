<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Office;

/**
 * OfficeSearch represents the model behind the search form about `app\models\Office`.
 */
class OfficeSearch extends Office {

    public $unsetPopulation,$year,$month, $startMonth, $endMonth;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'parent_id', 'office_type_id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'address', 'email', 'tel', 'line_token', 'deleted', 'created_at', 'updated_at', 'unsetPopulation','year','month', 'startMonth', 'endMonth'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Office::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'office.id' => $this->id,
            'office.parent_id' => $this->parent_id,
            'office.office_type_id' => $this->office_type_id,
            'office.level' => $this->level,
            'office.created_by' => $this->created_by,
            'office.created_at' => $this->created_at,
            'office.updated_by' => $this->updated_by,
            'office.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'office.name', $this->name])
                ->andFilterWhere(['like', 'office.address', $this->address])
                ->andFilterWhere(['like', 'office.email', $this->email])
                ->andFilterWhere(['like', 'office.tel', $this->tel])
                ->andFilterWhere(['like', 'office.line_token', $this->line_token])
                ->andFilterWhere(['like', 'office.deleted', $this->deleted]);

        if ($this->unsetPopulation) {
            $cond = [];
            $currentRole = Yii::$app->session->get('currentRole');
            if ($currentRole['role_id'] != Role::SUPER_ADMINISTRATOR) {
                if (Yii::$app->user->identity->person->office->hasChildren) {
                    $cond = ['office.parent_id' => Yii::$app->user->identity->person->office_id];
                } else {
                    $cond = ['office.parent_id' => Yii::$app->user->identity->person->office->parent_id];
                }
            } else {
                $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
                $cond = ['<=', 'office.level', $level];
            }
            $subQuery = (new \yii\db\Query())
                    ->select('oph.id')
                    ->from('office_population_h oph')
                    ->innerJoin('office o', 'oph.office_id = o.id')
                    ->andWhere(['oph.deleted' => 0])
                    ->andWhere('oph.office_id = office.id');
            if (!empty($cond)) {
                $query->andWhere($cond);
            }
            $query->andWhere(['not exists', $subQuery]);
        }

//        if (empty($this->parent_id)) {
//            $query->andWhere(['parent_id' => null]);
//        } else {
//            $query->andWhere(['parent_id' => $this->parent_id]);
//        }
        return $dataProvider;
    }

}
