<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OfficeType]].
 *
 * @see OfficeType
 */
class OfficeTypeQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return OfficeType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return OfficeType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
    
    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['office_type.deleted' => $deleted]);
    }

}
