<?php

namespace app\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "person".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property string $idcard_no เลขบัตรประชาขน
 * @property int $title_id รหัสคำนำหน้าชื่อ
 * @property string $first_name ชื่อ
 * @property string $last_name นามสกุล
 * @property int $user_id รหัสผู้ใช้
 * @property string $email อีเมลล์
 * @property string $tel เบอร์โทรศัพท์
 * @property int $deleted 0=ไม่ลบ,1=ลบ
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 * @property int $office_id 
 * @property string $line_notify_token
 *
 * @property User $createdBy
 * @property Title $title
 * @property Office $office
 * @property User $updatedBy
 * @property User $user
 * @property PersonRole[] $personRoles 

 */
class Person extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'person';
    }

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'createdByPerson.fullName',
            'updatedByPerson.fullName',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['first_name', 'office_id'], 'required'],
            [['title_id', 'user_id', 'deleted', 'created_by', 'updated_by', 'office_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['email'], 'email'],
            [['idcard_no', 'first_name', 'last_name', 'email', 'tel', 'line_notify_token'], 'trim'],
            [['idcard_no', 'first_name', 'last_name', 'email', 'tel', 'line_notify_token'], 'default'],
            [['idcard_no', 'first_name', 'last_name', 'email', 'tel', 'line_notify_token'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['title_id'], 'exist', 'skipOnError' => true, 'targetClass' => Title::className(), 'targetAttribute' => ['title_id' => 'id']],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'idcard_no' => Yii::t('app', 'เลขบัตรประชาขน'),
            'title_id' => Yii::t('app', 'คำนำหน้าชื่อ'),
            'first_name' => Yii::t('app', 'ชื่อ'),
            'last_name' => Yii::t('app', 'นามสกุล'),
            'office_id' => Yii::t('app', 'หน่วยงาน'),
            'user_id' => Yii::t('app', 'ผู้ใช้'),
            'email' => Yii::t('app', 'อีเมลล์'),
            'tel' => Yii::t('app', 'เบอร์โทรศัพท์'),
            'deleted' => Yii::t('app', '0=ไม่ลบ,1=ลบ'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'fullName' => Yii::t('app', 'ชื่อผู้ใช้งาน'),
            'updatedByPerson.fullName' => Yii::t('app', 'ผู้แก้ไขข้อมูล'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTitle() {
        return $this->hasOne(Title::className(), ['id' => 'title_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice() {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    public function getPersonRoles() {
        return $this->hasMany(PersonRole::className(), ['person_id' => 'id']);
    }

    /**
      /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by'])
                        ->from(['uu' => User::tableName()]);
    }

    public function getCreatedByPerson() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('createdBy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByPerson() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('updatedBy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getFullName() {
        $name = isset($this->title) ? $this->title->name : "";
        $name .= "{$this->first_name} {$this->last_name}";

        return $name;
    }

    public function getcurrentRoleName() {
        $currentRole = Yii::$app->session->get('currentRole');
        $attr = 'name';
        return $currentRole['role'][$attr];
    }

    public function getAvailableRoles() {
        return $this->getPersonRoles()->isDeleted(FALSE)->all();
    }

    public function getHasMoreRoles() {
        return $this->getPersonRoles()->isDeleted(FALSE)->count() > 1;
    }

    public function getYearlyAssessment($year, $officeType = null, $level = null) {
        $yaQuery = YearlyAssessment::find()->joinWith(['office'])->isDeleted(false)->year($year);
        if (!empty($officeType) || !empty($level)) {
            if (!empty($officeType)) {
                $yaQuery->officeType($officeType);
            }
            if (!empty($level)) {
                $yaQuery->officeLevel($level);
            }
        } else {
            $yaQuery->office($this->office_id);
        }
        $yaQuery->select([
            'yearly_assessment.year'
            , 'SUM(yearly_assessment.garbage_prev) AS garbage_prev'
            , 'SUM(yearly_assessment.garbage_cur) AS garbage_cur'
            , 'SUM(yearly_assessment.bag_prev) AS bag_prev'
            , 'SUM(yearly_assessment.bag_cur) AS bag_cur'
            , 'SUM(yearly_assessment.cup_prev) AS cup_prev'
            , 'SUM(yearly_assessment.cup_cur) AS cup_cur'
            , 'SUM(yearly_assessment.foam_prev) AS foam_prev'
            , 'SUM(yearly_assessment.foam_cur) AS foam_cur'
            , '(SUM(yearly_assessment.garbage_prev) - SUM(yearly_assessment.garbage_cur)) * 100 / SUM(yearly_assessment.garbage_prev) AS garbage_percent'
            , '(SUM(yearly_assessment.bag_prev) - SUM(yearly_assessment.bag_cur)) * 100 / SUM(yearly_assessment.bag_prev) AS bag_percent'
            , '(SUM(yearly_assessment.cup_prev) - SUM(yearly_assessment.cup_cur)) * 100 / SUM(yearly_assessment.cup_prev) AS cup_percent'
            , '(SUM(yearly_assessment.foam_prev) - SUM(yearly_assessment.foam_cur)) * 100 / SUM(yearly_assessment.foam_prev) AS foam_percent'
            , 'SUM(pop_int) AS pop_int'
            , 'SUM(pop_ext) AS pop_ext'
            , 'SUM(office_population) AS office_population'
            , 'AVG(work_day) AS work_day'
            , 'SUM(garbage) AS garbage'
            , 'SUM(bag) AS bag'
            , 'SUM(cup) AS cup'
            , 'SUM(foam) AS foam'
        ])->groupBy(['yearly_assessment.year']);
        return $yaQuery->one();
    }

    public function getCompareYearlyAssessment($year, $officeType = null, $level = null) {
        $yaQuery = YearlyAssessment::find()->joinWith(['office'])->isDeleted(false)->year($year);
        if (!empty($officeType) || !empty($level)) {
            if (!empty($officeType)) {
                $yaQuery->officeType($officeType);
            }
            if (!empty($level)) {
                $yaQuery->officeLevel($level);
            }
        } else {
            $yaQuery->office($this->office_id);
        }
        $yaQuery->select([
            'yearly_assessment.year'
            , 'SUM(yearly_assessment.garbage_prev) AS garbage_prev'
            , 'SUM(yearly_assessment.garbage_cur) AS garbage_cur'
            , 'SUM(yearly_assessment.bag_prev) AS bag_prev'
            , 'SUM(yearly_assessment.bag_cur) AS bag_cur'
            , 'SUM(yearly_assessment.cup_prev) AS cup_prev'
            , 'SUM(yearly_assessment.cup_cur) AS cup_cur'
            , 'SUM(yearly_assessment.foam_prev) AS foam_prev'
            , 'SUM(yearly_assessment.foam_cur) AS foam_cur'
            , '(SUM(yearly_assessment.garbage_prev) - SUM(yearly_assessment.garbage_cur)) * 100 / SUM(yearly_assessment.garbage_prev) AS garbage_percent'
            , '(SUM(yearly_assessment.bag_prev) - SUM(yearly_assessment.bag_cur)) * 100 / SUM(yearly_assessment.bag_prev) AS bag_percent'
            , '(SUM(yearly_assessment.cup_prev) - SUM(yearly_assessment.cup_cur)) * 100 / SUM(yearly_assessment.cup_prev) AS cup_percent'
            , '(SUM(yearly_assessment.foam_prev) - SUM(yearly_assessment.foam_cur)) * 100 / SUM(yearly_assessment.foam_prev) AS foam_percent'
            , 'SUM(pop_int) AS pop_int'
            , 'SUM(pop_ext) AS pop_ext'
            , 'SUM(office_population) AS office_population'
            , 'AVG(work_day) AS work_day'
            , 'SUM(garbage) AS garbage'
            , 'SUM(bag) AS bag'
            , 'SUM(cup) AS cup'
            , 'SUM(foam) AS foam'
            , 'SUM(mask) AS mask'
//            , '(SUM(mask)  * 100 / SUM(mask) AS mask_percent'
        ])->groupBy(['yearly_assessment.year']);
        return $yaQuery->all();
    }

    public function getResponsibleOfficesQuery($officeType = null, $level = null) {
        $q = Office::find()->isDeleted(false);
        if (!empty($officeType) || !empty($level)) {
            if (!empty($officeType)) {
                $q->officeType($officeType);
            }
            if (!empty($level)) {
                $q->level($level);
            }
        } else {
            $q->parent($this->office_id);
        }
        return $q;
    }

    public function getResponsibleOffices($officeType = null, $level = null) {
        $q = $this->getResponsibleOfficesQuery($officeType, $level);
        return $q->all();
    }

    public function getResponsibleOfficeCount($officeType = null, $level = null) {
        $q = $this->getResponsibleOfficesQuery($officeType, $level);
        return $q->count();
    }

    public function getAssessedOfficeCount($year, $officeType = null, $level = null) {
        $offices = $this->getResponsibleOffices($officeType, $level);
        $ids = ArrayHelper::getColumn($offices, 'id');
        return YearlyAssessment::find()->isDeleted(false)->office($ids)->year($year)->isAssessed()->count();
    }

    public function getTotalResponsibleOffices() {
        $currentRole = Yii::$app->session->get('currentRole');
        if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR || $currentRole['role_id'] == Role::REVIEWER) {
            $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
            return Office::find()->isDeleted(false)->level("<={$level}")->orderBy('CONVERT(office.name USING TIS620) ASC')->all();
        } else {
            return Office::find()->isDeleted(false)->parent(Yii::$app->user->identity->person->office_id)->orderBy('CONVERT(office.name USING TIS620) ASC')->all();
        }
    }

    /**
     * {@inheritdoc}
     * @return PersonQuery the active query used by this AR class.
     */
    public static function find() {
        return new PersonQuery(get_called_class());
    }

}
