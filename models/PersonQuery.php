<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Person]].
 *
 * @see Person
 */
class PersonQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return Person[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Person|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['person.deleted' => $deleted]);
    }
    
    public function role($role) {
        return $this->andWhere(['person_role.deleted' => FALSE, 'person_role.role_id' => $role]);
    }
    
    public function office($officeId) {
        return $this->andWhere(['person.office_id' => $officeId]);
    }
}
