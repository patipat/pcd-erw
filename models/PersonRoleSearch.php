<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PersonRole;

/**
 * PersonRoleSearch represents the model behind the search form about `app\models\PersonRole`.
 */
class PersonRoleSearch extends PersonRole {

    /**
     * @inheritdoc
     */
    public $expertise, $name, $personDeleted, $officeId, $personDepartment;

    public function rules() {
        return [
            [['id', 'person_id', 'role_id', 'deleted', 'created_by', 'updated_by', 'personDeleted', 'officeId', 'personDepartment'], 'integer'],
            [['created_at', 'updated_at', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = PersonRole::find();
        $query->joinWith(['person']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'person_role.id' => $this->id,
            'person_role.person_id' => $this->person_id,
            'person_role.role_id' => $this->role_id,
            'person_role.deleted' => $this->deleted,
            'person_role.created_by' => $this->created_by,
            'person_role.created_at' => $this->created_at,
            'person_role.updated_by' => $this->updated_by,
            'person_role.updated_at' => $this->updated_at,

            'person.deleted' => $this->personDeleted,
            'person.office_id' => $this->officeId,
            'person.department_id' => $this->personDepartment,
        ]);

        $query->andFilterWhere(['or', ['like', 'CONCAT(person.first_name, person.last_name)', $this->name]]);


        if (!empty($this->notInSubmissionId)) {
            $submission = Submission::findOne($this->notInSubmissionId);
            $subQuery = (new \yii\db\Query())->select('sc.id')->from('submission_committee sc')->andWhere(['sc.deleted' => 0, 'sc.submission_id' => $this->notInSubmissionId])->andWhere('sc.person_id=person_role.person_id');
            $query->andWhere(['not exists', $subQuery]);


//            $subcomQuery = (new \yii\db\Query())->select('pr.id')->from('project_researcher pr')->andWhere(['pr.deleted' => 0, 'pr.project_id' => $submission->project_id])->andWhere('pr.person_id=person_role.person_id');
//            $query->andWhere(['not exists', $subcomQuery]);
        }

        return $dataProvider;
    }

}
