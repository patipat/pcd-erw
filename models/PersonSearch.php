<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Person;

$currentRole = \Yii::$app->session->get('currentRole');

/**
 * PersonSearch represents the model behind the search form about `app\models\Person`.
 */
class PersonSearch extends Person {

    /**
     * @inheritdoc
     */
    public $officeTypeId, $parentId;
    private $_notInPersonRoleId;

    public function getNotInPersonRoleId() {
        return $this->_notInPersonRoleId;
    }

    public function setNotInPersonRoleId($id) {
        $this->_notInPersonRoleId = $id;
    }

    public function rules() {
        return [
            [['id', 'office_id', 'title_id', 'user_id', 'created_by', 'updated_by', 'notInPersonRoleId', 'officeTypeId', 'parentId'], 'integer'],
            [['idcard_no', 'first_name', 'last_name', 'email', 'tel', 'deleted', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Person::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith(['office']);

        $dataProvider->sort->attributes['person.fullName'] = [
            'asc' => ['CONVERT(person.fullName USING TIS620)' => SORT_ASC],
            'desc' => ['CONVERT(person.fullName USING TIS620)' => SORT_DESC],
        ];
        $query->andFilterWhere([
            'person.id' => $this->id,
            'person.office_id' => $this->office_id,
            'person.title_id' => $this->title_id,
            'person.user_id' => $this->user_id,
            'person.created_by' => $this->created_by,
            'person.created_at' => $this->created_at,
            'person.updated_by' => $this->updated_by,
            'person.updated_at' => $this->updated_at,
        ]);
        $query->andFilterWhere(['or', ['=', 'office.id', $this->parentId], ['=', 'office.parent_id', $this->parentId]]);
        $query->andFilterWhere(['office.office_type_id' => $this->officeTypeId]);
//        $query->andFilterWhere(['office.parent_id' => $this->parentId]);
        $query->andFilterWhere(['like', 'person.idcard_no', $this->idcard_no])
//            ->andFilterWhere(['like', 'first_name', $this->first_name])
//            ->andFilterWhere(['like', 'last_name', $this->last_name])
                ->andFilterWhere(['like', 'person.email', $this->email])
                ->andFilterWhere(['like', 'person.tel', $this->tel])
                ->andFilterWhere(['like', 'person.deleted', $this->deleted]);
        $query->andFilterWhere(['or', ['like', 'CONCAT(person.first_name, person.last_name)', $this->first_name]]);
        if (!empty($this->notInPersonRoleId)) {
            $subQuery = (new \yii\db\Query())->select('id')->from('person_role')->where(['person_role.role_id' => $this->notInPersonRoleId])->andWhere('person_role.person_id=person.id')->andWhere('person_role.deleted=0');
            $query->andWhere(['not exists', $subQuery]);
        }
        return $dataProvider;
    }

}
