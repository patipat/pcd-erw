<?php

namespace app\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;
/**
 * This is the model class for table "project_files".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $file_category_id กลุ่มเอกสาร
 * @property string $name ชื่อเอกสาร
 * @property string $description รายละเอียด
 * @property int $status สถานะ
 * @property string $file ไฟล์แนบ
 * @property int $public เปิดเผยแพร่
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property FileCategory $fileCategory
 * @property User $createdBy
 * @property User $updatedBy
 */
class ProjectFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_files';
    }
    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'createdByPerson.fullName',
            'updatedByPerson.fullName',
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_category_id', 'status', 'public', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'file'], 'string', 'max' => 255],
            [['file_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => FileCategory::className(), 'targetAttribute' => ['file_category_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'file_category_id' => Yii::t('app', 'กลุ่มเอกสาร'),
            'name' => Yii::t('app', 'ชื่อเอกสาร'),
            'description' => Yii::t('app', 'รายละเอียด'),
            'status' => Yii::t('app', 'สถานะ'),
            'file' => Yii::t('app', 'ไฟล์แนบ'),
            'public' => Yii::t('app', 'เปิดเผยแพร่'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
                                    'updatedByPerson.fullName' => Yii::t('app', 'ผู้แก้ไขข้อมูล'),

        ];
    }
    public function behaviors() {
        return [
                [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
                [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileCategory()
    {
        return $this->hasOne(FileCategory::className(), ['id' => 'file_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by'])
                        ->from(['uu' => User::tableName()]);
    }

    public function getCreatedByPerson() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('createdBy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByPerson() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('updatedBy');
    }
    public function getFileUrl() {
        return isset($this->file) ? \yii\helpers\Url::to('@web/web/uploads/files/'.$this->fileCategory->id .'/'. $this->file) : null;
    }
    /**
     * {@inheritdoc}
     * @return ProjectFilesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectFilesQuery(get_called_class());
    }
}
