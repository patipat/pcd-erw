<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ProjectFiles]].
 *
 * @see ProjectFiles
 */
class ProjectFilesQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return ProjectFiles[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ProjectFiles|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['project_files.deleted' => $deleted]);
    }

}
