<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Province;

/**
 * ProvinceSearch represents the model behind the search form about `app\models\Province`.
 */
class ProvinceSearch extends Province {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['name', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Province::find();
//        $query->joinWith(['createdByUserProfile createdByUserProfile', 'updatedByUserProfile updatedByUserProfile']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider->sort->attributes['name'] = [
            'asc' => ['CONVERT(province.name USING TIS620)' => SORT_ASC],
            'desc' => ['CONVERT(province.name USING TIS620)' => SORT_DESC],
        ];


        $query->andFilterWhere([
            'province.id' => $this->id,
            'province.deleted' => $this->deleted,
            'province.created_by' => $this->created_by,
            'province.created_at' => $this->created_at,
            'province.updated_by' => $this->updated_by,
            'province.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'province.name', $this->name]);


        return $dataProvider;
    }

}
