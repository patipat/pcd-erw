<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class RegistrationForm extends Model {

    public $first_name;
    public $last_name;
    public $tel;
    public $email;
    public $officeId;
    public $username;
    public $password;
    public $password_repeat;
    public $user_id;
    public $verifyCode;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            [['username'], 'required'],
            [['user_id'], 'integer'],
            ['username', 'match', 'pattern' => '/^[-a-zA-Z0-9_\.@]+$/'],
            ['username', 'filter', 'filter' => 'trim'],
            [
                'username',
                'unique',
                'targetClass' => User::className(),
                'filter' => function($query) {
                    return $query->andWhere(['not', ['status' => User::STATUS_DELETED]])
                                    ->andFilterWhere(['not', ['id' => $this->user_id]]);
                    //['and', ['status'=> User::STATUS_ACTIVE], ['not', ['id' => $this->user_id]]], //['status'=> User::STATUS_ACTIVE, ['not', ['id' => $this->user_id]]]
                },
//                'message' => Yii::t('user', 'This username has already been taken')
            ],
//            [
//                'companyId',
//                'unique',
//                'targetClass' => UserProfile::className(),
//                'filter' => ['and', ['deleted'=> 0], ['not', ['company_id' => $this->companyId]]], //['status'=> User::STATUS_ACTIVE, ['not', ['id' => $this->user_id]]],
////                'message' => Yii::t('user', 'This username has already been taken')
//            ],
            [['password', 'password_repeat'], 'required', 'on' => ['register']],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false, 'message' => 'ยืนยันรหัสผ่านไม่ตรงกับรหัสผ่าน'],
//            [['verifyCode'], 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'username' => 'ชื่อผู้ใช้',
            'password' => 'รหัสผ่าน',
            'password_repeat' => 'ยืนยันรหัสผ่าน',
            'first_name' => 'ชื่อ',
            'last_name' => 'นามสกุล',
            'tel' => 'เบอร์โทร',
            'email' => 'อีเมล์',
            'verifyCode' => 'รหัสตรวจสอบ',
        ];
    }

}
