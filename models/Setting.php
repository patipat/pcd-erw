<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "setting".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property string $key key
 * @property string $name name
 * @property string $value value
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Setting extends \yii\db\ActiveRecord {

    const MONTHLY_NO_SUBMIT_ALERT_DAY = 'MONTHLY_NO_SUBMIT_ALERT_DAY';
    const MONTHLY_COMPLETE_ALERT_DAY = 'MONTHLY_COMPLETE_ALERT_DAY';
    const SIX_MONTH_NO_SUBMIT_ALERT_DAY_MONTH = 'SIX_MONTH_NO_SUBMIT_ALERT_DAY_MONTH';
    const SIX_MONTH_COMPLETE_ALERT_DAY_MONTH = 'SIX_MONTH_COMPLETE_ALERT_DAY_MONTH';
    const FIRST_MONTH = 'FIRST_MONTH';
    const LAST_SUBMIT_DAY_MONTH = 'LAST_SUBMIT_DAY_MONTH';
    const LAST_SUBMIT_DAY_6MONTH = 'LAST_SUBMIT_DAY_6MONTH';
    const CURRENT_YEAR = 'CURRENT_YEAR';
    const SUPER_ADMIN_CHECK_LEVEL = 'SUPER_ADMIN_CHECK_LEVEL';
    const MONTHLY_FIRST_MONTH = 'MONTHLY_FIRST_MONTH';
    const MONTHLY_MONTHS = 'MONTHLY_MONTHS';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['key', 'name', 'value'], 'required'],
            [['deleted', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['key', 'name', 'value'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'key' => Yii::t('app', 'key'),
            'name' => Yii::t('app', 'name'),
            'value' => Yii::t('app', 'value'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    
    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }
    
    /**
     * {@inheritdoc}
     * @return SettingQuery the active query used by this AR class.
     */
    public static function find() {
        return new SettingQuery(get_called_class());
    }

    public static function settingNames() {
        return [
            self::CURRENT_YEAR => 'ปีงบประมาณ',
            self::FIRST_MONTH => 'เดือนแรกของปี',
            self::LAST_SUBMIT_DAY_MONTH => 'วันที่/เดือน สุดท้ายที่สามารถส่งรายงานได้',
            self::LAST_SUBMIT_DAY_6MONTH => 'วันที่/เดือน สุดท้ายที่สามารถส่งรายงาน 6 เดือนได้',
            self::MONTHLY_NO_SUBMIT_ALERT_DAY => 'วันที่ ที่ระบบจะส่งแจ้งเตือนสำหรับรายงานรายเดือนกรณียังไม่ส่งรายงาน',
            self::MONTHLY_COMPLETE_ALERT_DAY => 'วันที่ ที่ระบบจะส่งแจ้งเตือนสำหรับรายงานรายเดือนกรณีส่งรายงาน้ไม่ครบ',
            self::SIX_MONTH_NO_SUBMIT_ALERT_DAY_MONTH => 'วันที่/เดือน ที่ระบบจะส่งแจ้งเตือนสำหรับรายงาน 6 เดือนกรณียังไม่ส่งรายงาน',
            self::SIX_MONTH_COMPLETE_ALERT_DAY_MONTH => 'วันที่/เดือน ที่ระบบจะส่งแจ้งเตือนสำหรับรายงาน 6 เดือนกรณีส่งรายงาน้ไม่ครบ',
            self::SUPER_ADMIN_CHECK_LEVEL => 'ระดับล่างสุดของหน่วยงานที่ Super Admin จะเป็นผู้ตรวจสอบ',
            self::MONTHLY_FIRST_MONTH => 'เดือนแรกที่เก็บข้อมูลรายเดือน',
            self::MONTHLY_MONTHS => 'จำนวนเดือนที่เก็บข้อมูลรายเดือน',
        ];
    }
    
    public static function getValue($key) {
        $s = Setting::find()->andWhere(['key' => $key])->one();
        if (isset($s)) {
            return $s->value;
        }
        return null;
    }

}
