<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "six_month_form".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $office_id หน่วยงาน
 * @property int $year ปี
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 * @property int $submitted_by
 * @property string $submitted_at
 * @property int $confirmed_by
 * @property string $confirmed_at
 * @property int $checked_by
 * @property string $checked_at
 * @property int $is_pass
 * @property string $comment
 * @property int $is_late ยืนยันช้าเกินกำหนด 
 * @property string $score คะแนน 
 * @property string $plus_score บวกคะแนน 
 * @property string $minus_score ลบคะแนน 
 * @property string $total_score คะแนนรวม 
 * @property int $assessed_by ผู้ประเมิน 
 * @property string $assessed_at วันที่ประเมิน 
 *
 * @property FormIssue[] $formIssues
 * @property FormIssueSubmit[] $formIssueSubmits
 * @property User $checkedBy
 * @property User $confirmedBy
 * @property Office $office
 * @property User $submittedBy
 * @property User $createdBy
 * @property User $updatedBy
 * @property User $assessedBy 
 */
class SixMonthForm extends \yii\db\ActiveRecord {
    const SCENARIO_CHECK = 'check';

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'six_month_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['is_pass'], 'required', 'on' => self::SCENARIO_CHECK],
            [['office_id', 'year', 'deleted', 'created_by', 'updated_by', 'submitted_by', 'confirmed_by', 'checked_by', 'is_pass', 'is_late', 'assessed_by'], 'integer'],
            [['created_at', 'updated_at', 'submitted_at', 'confirmed_at', 'checked_at', 'assessed_at'], 'safe'],
            [['comment'], 'string'],
            [['score', 'plus_score', 'minus_score', 'total_score'], 'number'],
            [['assessed_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['assessed_by' => 'id']],
            [['checked_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['checked_by' => 'id']],
            [['confirmed_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['confirmed_by' => 'id']],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['submitted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['submitted_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'office_id' => Yii::t('app', 'หน่วยงาน'),
            'year' => Yii::t('app', 'Year'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'submitted_by' => Yii::t('app', 'ส่งล่าสุดโดย'),
            'submitted_at' => Yii::t('app', 'ส่งล่าสุดวันที่'),
            'confirmed_by' => Yii::t('app', 'ยืนยันล่าสุดโดย'),
            'confirmedBy.person.fullName' => Yii::t('app', 'ยืนยันล่าสุดโดย'),
            'confirmed_at' => Yii::t('app', 'ยืนยันล่าสุดวันที่'),
            'checked_by' => Yii::t('app', 'อนุมัติล่าสุดโดย'),
            'checked_at' => Yii::t('app', 'อนุมัติล่าสุดวันที่'),
            'is_pass' => Yii::t('app', 'ผลการตรวจสอบ'),
            'comment' => Yii::t('app', 'Comment'),
            'is_late' => Yii::t('app', 'ยืนยันช้าเกินกำหนด'),
            'score' => Yii::t('app', 'คะแนน'),
            'plus_score' => Yii::t('app', 'บวกคะแนน'),
            'minus_score' => Yii::t('app', 'ลบคะแนน'),
            'total_score' => Yii::t('app', 'คะแนนรวม'),
            'assessed_by' => Yii::t('app', 'ผู้ประเมิน'),
            'assessed_at' => Yii::t('app', 'วันที่ประเมิน'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssues() {
        return $this->hasMany(FormIssue::className(), ['six_month_form_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnsubmittedFormIssues() {
        return $this->hasMany(FormIssue::className(), ['six_month_form_id' => 'id'])->isDeleted(false)->isSubmitted(false);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUncheckedFormIssues() {
        return $this->hasMany(FormIssue::className(), ['six_month_form_id' => 'id'])->isDeleted(false)->isChecked(false);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassedFormIssues() {
        return $this->hasMany(FormIssue::className(), ['six_month_form_id' => 'id'])->isDeleted(false)->isPass(false);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormIssueSubmits() {
        return $this->hasMany(FormIssueSubmit::className(), ['six_month_form_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckedBy() {
        return $this->hasOne(User::className(), ['id' => 'checked_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedBy() {
        return $this->hasOne(User::className(), ['id' => 'confirmed_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice() {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubmittedBy() {
        return $this->hasOne(User::className(), ['id' => 'submitted_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getAssessedBy() {
        return $this->hasOne(User::className(), ['id' => 'assessed_by']);
    }

    public function getStatusColor() {
        $color = '';
        if (isset($this->checked_at)) {
            if ($this->is_pass) {
                $color = 'success';
            } else {
                $color = 'danger';
            }
        } else if (isset($this->confirmed_at)) {
            $color = 'warning';
        } else if (isset($this->submitted_at)) {
            $color = 'info';
        }
        return $color;
    }

    public function getStatusLabel() {
        $label = '';
        if (isset($this->checked_at)) {
            if ($this->is_pass) {
                $label = Yii::t('app', 'ตรวจสอบแล้ว');
            } else {
                $label = Yii::t('app', 'ข้อมูลตีกลับ');
            }
        } else if (isset($this->confirmed_at)) {
            $label = Yii::t('app', 'รอการตรวจสอบ');
        } else if (isset($this->submitted_at)) {
            $label = Yii::t('app', 'รอการยืนยัน');
        }
        return $label;
    }

    public function getStatusBadge() {
        $color = $this->getStatusColor();
        $label = $this->getStatusLabel();
        $res = "<span class='label label-{$color}'>{$label}</span>";
        return $res;
    }

    public function getSubmittedByName() {
        if (isset($this->submitted_at)) {
            if (isset($this->submitted_by)) {
                return $this->submittedBy->person->fullName;
            } else {
                return Yii::t('app', 'ระบบอัตโนมัติ');
            }
        } else {
            return '';
        }
    }
    
    public function updateTotalScore() {
        $this->score = array_sum(ArrayHelper::getColumn($this->passedFormIssues, 'score'));
    }

    /**
     * {@inheritdoc}
     * @return SixMonthFormQuery the active query used by this AR class.
     */
    public static function find() {
        return new SixMonthFormQuery(get_called_class());
    }

}
