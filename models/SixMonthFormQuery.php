<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SixMonthForm]].
 *
 * @see SixMonthForm
 */
class SixMonthFormQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return SixMonthForm[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SixMonthForm|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['six_month_form.deleted' => $deleted]);
    }
    
    public function isPass($pass = TRUE) {
        return $this->andWhere(['six_month_form.is_pass' => $pass]);
    }

    public function office($officeId) {
        return $this->andWhere(['six_month_form.office_id' => $officeId]);
    }

    public function year($year) {
        return $this->andWhere(['six_month_form.year' => $year]);
    }

    public function isSubmitted($submitted = TRUE) {
        if ($submitted) {
            return $this->andWhere(['not', ['six_month_form.submitted_at' => null]]);
        } else {
            return $this->andWhere(['six_month_form.submitted_at' => null]);
        }
    }

    public function isConfirmed($confirmed = TRUE) {
        if ($confirmed) {
            return $this->andWhere(['not', ['six_month_form.confirmed_at' => null]]);
        } else {
            return $this->andWhere(['six_month_form.confirmed_at' => null]);
        }
    }

    public function isChecked($checked = TRUE) {
        if ($checked) {
            return $this->andWhere(['not', ['six_month_form.checked_at' => null]]);
        } else {
            return $this->andWhere(['six_month_form.checked_at' => null]);
        }
    }

    public function last() {
        $subQuery = (new \yii\db\Query())->select('MAX(id) AS id')
                ->from('six_month_form smf')
                ->andWhere('smf.year = six_month_form.year')
                ->andWhere('smf.office_id = six_month_form.office_id')
                ->andWhere('smf.deleted=0');
        return $this->andWhere(['six_month_form.id' => $subQuery]);
    }

    public function lastUncheck() {
        $subQuery = (new \yii\db\Query())->select('MAX(id) AS id')
                ->from('six_month_form smf')
                ->andWhere('smf.year = six_month_form.year')
                ->andWhere('smf.office_id = six_month_form.office_id')
                ->andWhere('smf.deleted=0')
                ->andWhere(['smf.checked_at' => null])
                ->andWhere(['not', ['smf.confirmed_at' => null]]);
        return $this->andWhere(['six_month_form.id' => $subQuery]);
    }

}
