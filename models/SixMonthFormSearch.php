<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SixMonthForm;
use yii\helpers\ArrayHelper;

/**
 * SixMonthFormSearch represents the model behind the search form about `app\models\SixMonthForm`.
 */
class SixMonthFormSearch extends SixMonthForm {

    /**
     * @inheritdoc
     */
    public $isLastSubmit;
    public $isLastUncheck;
    public $isCheckChildrenByRole;
    public $officeType;
    public $level;
    public $isConfirmed;

    public function rules() {
        return [
            [['id', 'office_id', 'year', 'created_by', 'updated_by', 'submitted_by', 'confirmed_by', 'checked_by', 'is_pass', 'officeType', 'level'], 'integer'],
            [['deleted', 'created_at', 'updated_at', 'submitted_at', 'confirmed_at', 'checked_at', 'comment', 'isLastSubmit', 'isLastUncheck', 'isCheckChildrenByRole', 'isConfirmed'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = SixMonthForm::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith(['office']);
        $dataProvider->sort->attributes['office.name'] = [
            'asc' => ['CONVERT(office.name USING TIS620)' => SORT_ASC],
            'desc' => ['CONVERT(office.name USING TIS620)' => SORT_DESC],
        ];

        $query->andFilterWhere([
            'six_month_form.id' => $this->id,
            'six_month_form.office_id' => $this->office_id,
            'six_month_form.year' => $this->year,
            'six_month_form.created_by' => $this->created_by,
            'six_month_form.created_at' => $this->created_at,
            'six_month_form.updated_by' => $this->updated_by,
            'six_month_form.updated_at' => $this->updated_at,
            'six_month_form.submitted_by' => $this->submitted_by,
            'six_month_form.submitted_at' => $this->submitted_at,
            'six_month_form.confirmed_by' => $this->confirmed_by,
            'six_month_form.confirmed_at' => $this->confirmed_at,
            'six_month_form.checked_by' => $this->checked_by,
            'six_month_form.checked_at' => $this->checked_at,
            'six_month_form.is_pass' => $this->is_pass,
        ]);
        if (!empty($this->isLastSubmit)) {
            $query->last();
        }

        if (!empty($this->isLastUncheck)) {
            $query->lastUncheck();
        }
        if (isset($this->isConfirmed)) {
            $query->isConfirmed($this->isConfirmed);
        }

        $query->andFilterWhere(['office.office_type_id' => $this->officeType]);
        $query->andFilterWhere(['office.level' => $this->level]);
        $query->andFilterWhere(['like', 'six_month_form.deleted', $this->deleted])
                ->andFilterWhere(['like', 'six_month_form.comment', $this->comment]);
        if (!empty($this->isCheckChildrenByRole)) {
            $currentRole = Yii::$app->session->get('currentRole');
            if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR || $currentRole['role_id'] == Role::REVIEWER) {
                $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
                $data = ArrayHelper::getColumn(Office::find()->isDeleted(false)->level("<={$level}")->all(), 'id');
            } else {
                $data = ArrayHelper::getColumn(Office::find()->isDeleted(false)->parent(Yii::$app->user->identity->person->office_id)->all(), 'id');
            }
            $query->andWhere(['six_month_form.office_id' => $data]);
        }
        return $dataProvider;
    }

}
