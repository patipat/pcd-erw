<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SourceType]].
 *
 * @see SourceType
 */
class SourceTypeQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return SourceType[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return SourceType|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['source_type.deleted' => $deleted]);
    }
}
