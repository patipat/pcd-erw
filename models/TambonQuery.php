<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Tambon]].
 *
 * @see Tambon
 */
class TambonQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Tambon[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Tambon|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['tambon.deleted' => $deleted]);
    } 
    public function amphur($amphurId) {
        return $this->andWhere(['tambon.amphur_id' => $amphurId]);
    } 
}
