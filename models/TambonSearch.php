<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tambon;

/**
 * TambonSearch represents the model behind the search form about `app\models\Tambon`.
 */
class TambonSearch extends Tambon
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'province_id', 'amphur_id', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['name', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tambon::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider->sort->attributes['name'] = [
            'asc' => ['CONVERT(tambon.name USING TIS620)' => SORT_ASC],
            'desc' => ['CONVERT(tambon.name USING TIS620)' => SORT_DESC],
        ];

        $query->andFilterWhere([
            'tambon.id' => $this->id,
            'tambon.province_id' => $this->province_id,
            'tambon.amphur_id' => $this->amphur_id,
            'tambon.deleted' => $this->deleted,
            'tambon.created_by' => $this->created_by,
            'tambon.created_at' => $this->created_at,
            'tambon.updated_by' => $this->updated_by,
            'tambon.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'tambon.name', $this->name]);
 
        return $dataProvider;
    }
}
