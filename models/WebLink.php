<?php

namespace app\models;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "web_link".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $type ประเภทเว็บลิงค์
 * @property string $name ชื่อเว็บลิงค์
 * @property string $url url
 * @property string $banner banner
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class WebLink extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'web_link';
    }

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), [
            'createdByPerson.fullName',
            'updatedByPerson.fullName',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['type', 'deleted', 'created_by', 'updated_by'], 'integer'],
                [['name', 'url'], 'required'],
                [['created_at', 'updated_at'], 'safe'],
                [['name', 'url', 'banner'], 'string', 'max' => 255],
                [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
                [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'รหัสอัตโนมัติ',
            'type' => 'ประเภทเว็บลิงค์',
            'name' => 'รายละเอียดไฟล์ดาวน์โหลด',
            'url' => 'ไฟล์',
            'banner' => 'banner',
            'deleted' => '0=ใช้งาน,1=ไม่ใช้งาน',
            'created_by' => 'สร้างโดย',
            'created_at' => 'สร้างเมื่อ',
            'updated_by' => 'ปรับปรุงโดย',
            'updated_at' => 'ปรับปรุงเมื่อ',
            'updatedByPerson.fullName' => Yii::t('app', 'ผู้แก้ไขข้อมูล'),
        ];
    }

    public function behaviors() {
        return [
                [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
                [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by'])
                        ->from(['uu' => User::tableName()]);
    }

    public function getCreatedByPerson() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('createdBy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedByPerson() {
        return $this->hasOne(Person::className(), ['user_id' => 'id'])
                        ->via('updatedBy');
    }

    public function getFileUrl() {
        return isset($this->url) ? \yii\helpers\Url::to('@web/uploads/download/' . $this->url) : null;
    }

    /**
     * {@inheritdoc}
     * @return WebLinkQuery the active query used by this AR class.
     */
    public static function find() {
        return new WebLinkQuery(get_called_class());
    }

}
