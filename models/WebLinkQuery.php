<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[WebLink]].
 *
 * @see WebLink
 */
class WebLinkQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return WebLink[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return WebLink|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['web_link.deleted' => $deleted]);
    }

}
