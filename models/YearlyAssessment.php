<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "yearly_assessment".
 *
 * @property int $id รหัสอัตโนมัติ
 * @property int $office_id หน่วยงาน
 * @property int $year ปี
 * @property string $garbage_prev garbage_prev
 * @property string $garbage_cur garbage_cur
 * @property string $bag_prev bag_prev
 * @property string $bag_cur bag_cur
 * @property string $cup_prev cup_prev
 * @property string $cup_cur cup_cur
 * @property string $foam_prev foam_prev
 * @property string $foam_cur foam_cur
 * @property string $garbage_percent garbage_percent
 * @property string $bag_percent bag_percent
 * @property string $cup_percent cup_percent
 * @property string $foam_percent foam_percent
 * @property string $garbage_score garbage_score
 * @property string $bag_score bag_score
 * @property string $cup_score cup_score
 * @property string $foam_score foam_score
 * @property string $plus_score plus_score
 * @property string $minus_score minus_score
 * @property string $total_score total_score
 * @property int $confirmed_by ยืนยันข้อมูลโดย
 * @property string $confirmed_at ยืนยันข้อมูลเมื่อ
 * @property int $status สถานะ
 * @property string $population_total จำนวนบุคคลากร
 * @property int $deleted 0=ใช้งาน,1=ไม่ใช้งาน
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property int $updated_by ปรับปรุงโดย
 * @property string $updated_at ปรับปรุงเมื่อ
 * @property int $pop_int จำนวนบุคคลากรภายใน(คน) 
 * @property int $pop_ext จำนวนบุคคลากรภายนอก(คน) 
 * @property int $office_population จำนวนบุคคลากรในหน่วยงาน(คน) 
 * @property int $work_day จำนวนวันทำงาน(วัน) 
 * @property string $garbage ปริมาณขยะมูลฝอย (กก.) 
 * @property string $bag จำนวนถุงพลาสติก (ใบ) 
 * @property string $cup จำนวนแก้วพลาสติก (ใบ) 
 * @property string $mask  
 * @property string $mask_score
 * @property string $mask_percent 
 * @property string $mask_cur 
 * @property string $mask_prev 
 *
 * @property Office $office
 * @property User $confirmedBy
 * @property User $createdBy
 * @property User $updatedBy
 */
class YearlyAssessment extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'yearly_assessment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['office_id', 'year', 'confirmed_by', 'status', 'deleted', 'created_by', 'updated_by', 'pop_int', 'pop_ext', 'office_population', 'work_day'], 'integer'],
            [['garbage_prev', 'garbage_cur', 'bag_prev', 'bag_cur', 'cup_prev', 'cup_cur', 'foam_prev', 'foam_cur', 'garbage_percent', 'bag_percent', 'cup_percent', 'foam_percent', 'garbage_score', 'bag_score', 'cup_score', 'foam_score', 'plus_score', 'minus_score', 'total_score', 'population_total', 'garbage', 'bag', 'cup', 'foam', 'mask', 'mask_score', 'mask_percent', 'mask_cur', 'mask_prev'], 'number'],
            [['confirmed_at', 'created_at', 'updated_at'], 'safe'],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
            [['confirmed_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['confirmed_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'รหัสอัตโนมัติ'),
            'office_id' => Yii::t('app', 'หน่วยงาน'),
            'year' => Yii::t('app', 'ปี'),
            'garbage_prev' => Yii::t('app', 'garbage_prev'),
            'garbage_cur' => Yii::t('app', 'garbage_cur'),
            'bag_prev' => Yii::t('app', 'bag_prev'),
            'bag_cur' => Yii::t('app', 'bag_cur'),
            'cup_prev' => Yii::t('app', 'cup_prev'),
            'cup_cur' => Yii::t('app', 'cup_cur'),
            'foam_prev' => Yii::t('app', 'foam_prev'),
            'mask_prev' => Yii::t('app', 'mask_prev'),
            'foam_cur' => Yii::t('app', 'foam_cur'),
            'mask_cur' => Yii::t('app', 'mask_cur'),
            'garbage_percent' => Yii::t('app', 'garbage_percent'),
            'bag_percent' => Yii::t('app', 'bag_percent'),
            'cup_percent' => Yii::t('app', 'cup_percent'),
            'foam_percent' => Yii::t('app', 'foam_percent'),
            'mask_percent' => Yii::t('app', 'mask_percent'),
            'garbage_score' => Yii::t('app', 'คะแนนขยะ'),
            'bag_score' => Yii::t('app', 'คะแนนถุง'),
            'cup_score' => Yii::t('app', 'คะแนนแก้ว'),
            'foam_score' => Yii::t('app', 'คะแนนโฟม'),
            'mask_score' => Yii::t('app', 'คะแนนหน้ากากอนามัย'),
            'plus_score' => Yii::t('app', 'บวกคะแนน'),
            'minus_score' => Yii::t('app', 'ลบคะแนน'),
            'total_score' => Yii::t('app', 'รวมคะแนน'),
            'confirmed_by' => Yii::t('app', 'ยืนยันข้อมูลโดย'),
            'confirmed_at' => Yii::t('app', 'ยืนยันข้อมูลเมื่อ'),
            'status' => Yii::t('app', 'สถานะ'),
            'population_total' => Yii::t('app', 'จำนวนบุคคลากร'),
            'deleted' => Yii::t('app', '0=ใช้งาน,1=ไม่ใช้งาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_by' => Yii::t('app', 'ปรับปรุงโดย'),
            'updated_at' => Yii::t('app', 'ปรับปรุงเมื่อ'),
            'pop_int' => Yii::t('app', 'จำนวนบุคคลากรภายใน(คน)'),
            'pop_ext' => Yii::t('app', 'จำนวนบุคคลากรภายนอก(คน)'),
            'office_population' => Yii::t('app', 'จำนวนบุคคลากรในหน่วยงาน(คน)'),
            'work_day' => Yii::t('app', 'จำนวนวันทำงาน(วัน)'),
            'garbage' => Yii::t('app', 'ปริมาณขยะมูลฝอย (กก.)'),
            'bag' => Yii::t('app', 'จำนวนถุงพลาสติก (ใบ)'),
            'cup' => Yii::t('app', 'จำนวนแก้วพลาสติก (ใบ)'),
            'foam' => Yii::t('app', 'จำนวนโฟมบรรจุอาหาร (ใบ)'),
            'mask' => Yii::t('app', 'จำนวนหน้ากากอนามัย (ชิ้น)'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function() {
                    return date('Y-m-d H:i:s');
                },
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice() {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfirmedBy() {
        return $this->hasOne(User::className(), ['id' => 'confirmed_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * {@inheritdoc}
     * @return YearlyAssessmentQuery the active query used by this AR class.
     */
    public static function find() {
        return new YearlyAssessmentQuery(get_called_class());
    }

    public static function getAssessmentStatusLabels() {
        return [
            0 => 'ยังไม่ประเมิน',
            1 => 'ประเมินแล้ว',
        ];
    }

}
