<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[YearlyAssessment]].
 *
 * @see YearlyAssessment
 */
class YearlyAssessmentQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return YearlyAssessment[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return YearlyAssessment|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function isDeleted($deleted = TRUE) {
        return $this->andWhere(['yearly_assessment.deleted' => $deleted]);
    }

    public function office($officeId) {
        return $this->andWhere(['yearly_assessment.office_id' => $officeId]);
    }

    public function year($year) {
        return $this->andWhere(['yearly_assessment.year' => $year]);
    }

    public function officeType($officeType) {
        return $this->andWhere(['office.office_type_id' => $officeType]);
    }

    public function officeLevel($level) {
        return $this->andWhere(['office.level' => $level]);
    }
    
    public function isAssessed($isAssessed=true) {
        if ($isAssessed) {
            return $this->andWhere(['not', ['yearly_assessment.confirmed_at' => null]]);
        }
        return $this->andWhere(['yearly_assessment.confirmed_at' => null]);
    }

}
