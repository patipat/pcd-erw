<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\YearlyAssessment;

/**
 * YearlyAssessmentSearch represents the model behind the search form about `app\models\YearlyAssessment`.
 */
class YearlyAssessmentSearch extends YearlyAssessment {

    public $officeType;
    public $level;
    public $isAssessed;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'office_id', 'year', 'confirmed_by', 'status', 'created_by', 'updated_by', 'pop_int', 'pop_ext', 'office_population', 'work_day', 'officeType', 'level', 'isAssessed'], 'integer'],
            [['garbage_prev', 'garbage_cur', 'bag_prev', 'bag_cur', 'cup_prev', 'cup_cur', 'foam_prev', 'foam_cur', 'garbage_percent', 'bag_percent', 'cup_percent', 'foam_percent', 'garbage_score', 'bag_score', 'cup_score', 'foam_score', 'plus_score', 'minus_score', 'total_score', 'population_total', 'garbage', 'bag', 'cup', 'foam'], 'number'],
            [['confirmed_at', 'deleted', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = YearlyAssessment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['office']);
        $query->andFilterWhere([
            'yearly_assessment.id' => $this->id,
            'yearly_assessment.office_id' => $this->office_id,
            'yearly_assessment.year' => $this->year,
            'yearly_assessment.garbage_prev' => $this->garbage_prev,
            'yearly_assessment.garbage_cur' => $this->garbage_cur,
            'yearly_assessment.bag_prev' => $this->bag_prev,
            'yearly_assessment.bag_cur' => $this->bag_cur,
            'yearly_assessment.cup_prev' => $this->cup_prev,
            'yearly_assessment.cup_cur' => $this->cup_cur,
            'yearly_assessment.foam_prev' => $this->foam_prev,
            'yearly_assessment.foam_cur' => $this->foam_cur,
            'yearly_assessment.garbage_percent' => $this->garbage_percent,
            'yearly_assessment.bag_percent' => $this->bag_percent,
            'yearly_assessment.cup_percent' => $this->cup_percent,
            'yearly_assessment.foam_percent' => $this->foam_percent,
            'yearly_assessment.garbage_score' => $this->garbage_score,
            'yearly_assessment.bag_score' => $this->bag_score,
            'yearly_assessment.cup_score' => $this->cup_score,
            'yearly_assessment.foam_score' => $this->foam_score,
            'yearly_assessment.plus_score' => $this->plus_score,
            'yearly_assessment.minus_score' => $this->minus_score,
            'yearly_assessment.total_score' => $this->total_score,
            'yearly_assessment.confirmed_by' => $this->confirmed_by,
            'yearly_assessment.confirmed_at' => $this->confirmed_at,
            'yearly_assessment.status' => $this->status,
            'yearly_assessment.population_total' => $this->population_total,
            'yearly_assessment.created_by' => $this->created_by,
            'yearly_assessment.created_at' => $this->created_at,
            'yearly_assessment.updated_by' => $this->updated_by,
            'yearly_assessment.updated_at' => $this->updated_at,
            'yearly_assessment.pop_int' => $this->pop_int,
            'yearly_assessment.pop_ext' => $this->pop_ext,
            'yearly_assessment.office_population' => $this->office_population,
            'yearly_assessment.work_day' => $this->work_day,
            'yearly_assessment.garbage' => $this->garbage,
            'yearly_assessment.bag' => $this->bag,
            'yearly_assessment.cup' => $this->cup,
            'yearly_assessment.foam' => $this->foam,
            'yearly_assessment.deleted' => $this->deleted,
        ]);
        
        if (isset($this->isAssessed)) {
            if ($this->isAssessed) {
                $query->isAssessed();
            } else {
                $query->isAssessed(false);
            }
        }

        $query->andFilterWhere(['office.office_type_id' => $this->officeType]);
        $query->andFilterWhere(['office.level' => $this->level]);

        return $dataProvider;
    }

}
