<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[YearlyAssessmentTotal]].
 *
 * @see YearlyAssessmentTotal
 */
class YearlyAssessmentTotalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return YearlyAssessmentTotal[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return YearlyAssessmentTotal|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
