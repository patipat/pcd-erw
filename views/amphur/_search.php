<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use app\models\Province;
use kartik\select2\Select2;
use yii\helpers\Url;

?>
<div class="amphur-search margin-bottom-10">
    <?php
    $form = ActiveForm::begin([
                'method' => 'get',
                'action' => Url::to(['amphur/index']),
                'options' => [
                    'data-pjax' => 1,
//                            'target' => '#crud-datatable-ticket-h-pjax'
//                    'class' => 'form-inline'
                ],
                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>
    <?= $form->field($searchModel, 'name')->textInput([]); ?>
    <?php
    // Usage with ActiveForm and model
    $data = ArrayHelper::map(Province::find()->isDeleted(FALSE)->orderBy('CONVERT(province.name USING TIS620) ASC')->all(), 'id', 'name');
//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($searchModel, 'province_id')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => 'เลือกจังหวัด'],
        'pluginOptions' => [
            'allowClear' => true,
            'width'=>'250px'
        ],
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>