<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Amphur */

?>
<div class="amphur-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
