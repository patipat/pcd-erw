<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Amphur */
?>
<div class="amphur-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'province_id',
            'name',
            'deleted',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
