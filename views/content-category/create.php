<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContentCategory */

?>
<div class="content-category-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
