<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ContentGroup */

?>
<div class="content-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
