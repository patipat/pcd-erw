<?php

use yii\helpers\Html;
use app\assets\AppAsset;

$asset = AppAsset::register($this);
$baseUrl = $asset->baseUrl;
if (!empty($model->image)) {
    $image = $model->image;
} else {
    $image = 'director.png';
}
?>
<div style=" margin-left: 2px;" class="row">
    <div class="post-quote">
        <img src="<?= $baseUrl; ?>/web/uploads/content/images/<?= $image; ?>" class="pull-left img-thumbnail padding-20" alt="" style="width: 120px; height: 80px;">
        <div class="post-heading">

            <?=
            Html::a($model->title, ['content/detail', 'id' => $model->id], [
                'data-confirm' => false, 'data-method' => false,
                'data-toggle' => 'tooltip', 'target' => '_blank', 'style' => 'text-decoration: none'])
            ?>
            <li><i class="icon-calendar"></i> <?= Yii::$app->formatter->asDate($model->created_at); ?></li>
        </div>
        <div class="span-all"><?= $model->intro_text; ?></div>
    </div>
</div>


<style>
    .span-all {
        display: block;
        float: left;
        min-height: 1px;
        width: 80%
    }

</style>
