<?php

use yii\helpers\Url;
use yii\helpers\Html;

return [

        [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'title',
        'width' => '65%',
        'format' => 'raw',
        'value' => function ($dataProvider) {
            return Html::a('<i class="icon-star" aria-hidden="true"></i><span>' . $dataProvider->title . '</span>', ['content/show', 'id' => $dataProvider->id], ['role' => 'modal-remote','data-pjax'=>'0','style'=>'text-decoration: none']);
        }
    ],
        [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'updated_at',
        'format' => ['date', 'php:d/m/Y'],
        'filter' => FALSE,
        'width' => '10%'
    ],
];
