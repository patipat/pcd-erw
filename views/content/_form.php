<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use bajadev\ckeditor\CKEditor;
use kartik\widgets\FileInput;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\redactor\widgets\Redactor;
use yii\redactor\widgets\RedactorAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-form margin-top-10">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>
    
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php if($groupId != \app\models\Content::DOWNLOAD){ ?>
    <?php
    echo $form->field($model, 'main_text')->widget(CKEditor::className(), [
        'editorOptions' => [
            'preset' => 'full', // basic, standard, full
            'inline' => false,
//            'filebrowserBrowseUrl' => 'browse-images',
            'filebrowserUploadUrl' => Url::to(['/site/upload']),
            'extraPlugins' => 'imageuploader',
        ],
    ]);
    ?>
    <?php } ?>
    <?php if($groupId == \app\models\Content::DOWNLOAD){ ?>
    <?=
    $form->field($model, 'file_name')->widget(FileInput::classname(), [
        'options' => [
            'accept' => '.doc,.docx,.pdf,.xlsx,.xls','jpg','png','gif','mp4','.ppt','.pptx','.zip','.rar',
//            'multiple' => 'true',
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['doc', 'docx', 'pdf','jpg','png','gif','mp4','xlsx','xls','ppt','pptx','zip','rar'],
            'showUpload' => false,
            'browseLabel' => '',
            'removeLabel' => '',
            'initialPreviewAsData' => true,
            'initialCaption' => $model->file_name,
            'initialPreviewConfig' => [
                ['caption' => $model->file_name]
            ],
//            'overwriteInitial'=>false
        ],
    ]);
    ?>
    <?php }  ?>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
