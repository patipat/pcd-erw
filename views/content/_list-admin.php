<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
?>
<div class="services-2 d-flex">
    <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-experiment-results"></span></div>
    <div class="text">
        <h3>
            <?php if($model->content_group_id == \app\models\Content::DOWNLOAD){?>
            <?=
            Html::a($model->title, [$model->fileUrl], [
                'target' => '_blank','style' => 'text-decoration: none'])
            ?>
            <?php }else{ ?>
            <?=
            Html::a($model->title, ['content/detail', 'id' => $model->id], [
                'data-confirm' => false, 'data-method' => false,
                'data-toggle' => 'tooltip', 'style' => 'text-decoration: none'])
            ?>
            <?php } ?>
        </h3>
        <div class="meta">
            <div><span class="icon-calendar"></span> <?= Yii::$app->formatter->asDate($model->created_at); ?><span class="icon-person"></span> <?= $model->createdBy->username; ?></div>
        </div>
    </div>
</div>

