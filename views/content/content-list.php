<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
if (isset($categoryId)) {
    $this->title = $searchModel->contentCategory->name;
} else {
    $this->title = $searchModel->contentGroup->name;
}
$this->params['breadcrumbs'][] = $this->title;

//CrudAsset::register($this);
?>
<div class="col-md-12 p-4 p-md-3">
    <!--    <form id="w0" action="/pcd-erw/content/content-list" method="get" data-pjax="1">
            <div class="row">
                <div class="col-md-8">
                    <input type="hidden" name="group" value=<?= $group->id; ?>> 
                    <div class="form-group">
                        <input type="text" id="contentsearch-title" class="form-control" name="ContentSearch[title]" placeholder="ชื่อเรื่อง">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="submit" value="ค้นหา" class="btn btn-primary py-2 px-5">
                    </div>
                </div>
            </div>
        </form>-->
    <?php
    $form = ActiveForm::begin([
                'method' => 'get',
                'layout' => 'inline',
                'options' => [
                    'data-pjax' => 1,
                ],
//                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>
    <?= $form->field($searchModel, 'title')->textInput([]); ?>
    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <div class="panel">
        <div class="panel-body">
            <?php
            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => '{items}<div class = "block-27">{pager}</div>',
                'itemView' => function ($model, $key, $index, $widget)use($searchModel) {
                    return $this->render('_list', ['model' => $model, 'searchModel' => $searchModel]);
                },
                'itemOptions' => [
                    'class' => 'col-md-6',
                ],
                'pager' => [
                    'id' => 'block-27',
                    'prevPageLabel' => '<i class="ion-ios-arrow-back p-md-3"></i>',
                    'nextPageLabel' => '<i class="ion-ios-arrow-forward p-md-3"></i>',
                ]
                , 'id' => 'block-27',
                'options' => [
                    'class' => 'row mt-1 pt-1'
                ],
            ]);
            /* @var $this yii\web\View */
            /* @var $model app\models\Pet */
            ?>    
        </div>
    </div>
</div>
<style>
    #block-27 ul {
        padding: 0;
        margin: 0; 
        text-align: center;
    }
    #block-27 ul li {
        margin-left: 10px;
        margin-bottom: 4px;
        font-weight: 400;
    }
    #block-27 ul li a, #block-27 ul li span {
        border: 1px solid #28a745;
        text-align: center;
        display: inline-block;
        width: 40px;
        height: 40px;
        line-height: 40px;
        border-radius: 50%; }
    #block-27 ul li.active a, #block-27 ul li.active span {
        background: #28a745;
        color: #fff;
        border: 1px solid transparent; }
</style>