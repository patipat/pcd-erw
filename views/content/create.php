<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
?>
<div class="content-create">
    <?=
    $this->render('_form', [
        'model' => $model,
        'groupId' => $groupId,
        'categoryId' => $categoryId
    ])
    ?>
</div>
