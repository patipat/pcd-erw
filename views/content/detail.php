<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Content */


$this->title = \app\models\Content::getContentLabel()[$model->content_group_id];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12 p-4 p-md-3"> 
        <div class="panel-body">
            <span class=" highlight"><strong > <?= $model->title; ?> </strong></span>
            <p><?= $model->intro_text; ?></p>
            <p><?= $model->main_text; ?></p>
        </div>
</div>
