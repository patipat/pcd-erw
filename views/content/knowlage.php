<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;

$asset = AppAsset::register($this);
$baseUrl = $asset->baseUrl;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ความรู้ด้านการอนุรักษ์พลังงานภาคขนส่ง ');
$this->params['breadcrumbs'][] = $this->title;
//CrudAsset::register($this);
?>
<h1><strong>ความรู้</strong>ด้านการอนุรักษ์พลังงานภาคขนส่ง <div class="cta floatright"><?=
        Html::a(yii::t('app', 'อ่านบทความทั้งหมด'), ['content/index-all', 'groupId' => \app\models\Content::KNOWLAGE], [
            'data-confirm' => false, 'data-method' => false,
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-large btn-theme btn-rounded'])
        ?></div></h1>

<?php foreach ($contentsNews as $content): ?>
    <?php
    if (!empty($content->image)) {
        $image = $content->image;
    } else {
        $image = 'director.png';
    }
    ?>
    <div style=" margin-left: 2px;" class="row">
        <div class="post-quote">
            <img src="<?= $baseUrl; ?>/web/uploads/content/images/<?= $image; ?>" class="pull-left img-thumbnail padding-20" alt="" style="width: 100px; height: 80px;">
            <div class="post-heading">
                <?=
                    Html::a('<span class=" highlight"><strong >'.$content->title . '</strong></span>', ['content/detail','id'=>$content->id], [
                        'data-confirm' => false, 'data-method' => false,
                        'data-toggle' => 'tooltip', 'target' => '_blank', 'style' => 'text-decoration: none'])
                    ?> <i class="icon-calendar"></i> <?= Yii::$app->formatter->asDate($content->created_at); ?>
            </div>
               <?= $content->intro_text; ?>
        </div>
    </div>

<?php endforeach; ?>

<?php foreach ($contentsDetail as $contentDetail): ?>
    <div id="contentShow-<?= $contentDetail->id ?>" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="myContentModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 id="myContentModalLabel"><?= $contentDetail->title ?></h4>
        </div>
        <div class="modal-body">
            <p>
                <?= isset($contentDetail->intro_text) ? $contentDetail->intro_text : ''; ?>
            </p>
            <p>
                <?= isset($contentDetail->main_text) ? $contentDetail->main_text : ''; ?>
            </p>            
        </div>
    </div>
<?php endforeach; ?>