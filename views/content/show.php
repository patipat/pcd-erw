<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
$title = $model->contentGroup->name ;
$this->title = $title ;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-view">
 
    <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title"><?= $model->title ?></h3>
        </div>
        <div class="panel-body">
          <?= $model->main_text; ?>
        </div>
      </div>
</div>
