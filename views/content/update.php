<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-update">
    <div class="panel">
        <div class="panel-body">
            <?=
            $this->render('_form', [
                'model' => $model,
                'groupId' => $groupId,
            ])
            ?>
        </div>
    </div>
</div>
