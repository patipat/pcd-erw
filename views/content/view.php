<?php

use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12 p-4 p-md-3">
    <div class="panel">
        <div class="panel-heading margin-top-20">
        </div>
        <div class="panel-body">
            <?= $model->main_text; ?>
        </div>
    </div>

</div>
