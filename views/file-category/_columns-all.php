<?php

use yii\helpers\Html;
?>
<div class="col-md-2  text-center margin-20">
    <div class="icondemo vertical-align-middle">
        <i class="icon wb-folder" aria-hidden="true" style="font-size: 100px;"></i>
        <div class="icon-title"><?= $model->name ?></div>
        <?= Html::a('<i class="icon icon wb-file"></i> ', ['project-files/index', 'fileCategoryId' => $model->id], ['data-pjax' => 0, 'data-toggle' => 'tooltip','target' => '_blank','title' => 'โชว์ไฟล์']) ?>
        <?= Html::a('<i class="icon icon wb-add-file"></i> ', ['project-files/create', 'fileCategoryId' => $model->id], ['role' => 'modal-remote', 'data-toggle' => 'tooltip','title' => 'เพิ่มไฟล์']) ?>
        <?= Html::a('<i class="icon icon wb-edit"></i> ', ['file-category/update', 'id' => $model->id], ['role' => 'modal-remote', 'data-toggle' => 'tooltip','title' => 'แก้ไขโฟลเดอร์']) ?>
        <?= Html::a('<i class="icon icon wb-trash"></i> ', ['file-category/delete', 'id' => $model->id], ['role' => 'modal-remote', 'title' => 'ลบโฟลเดอร์',
            'data-confirm' => false, 'data-method' => false, // for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => Yii::t('app', 'ยืนยันการลบ'),
            'data-confirm-message' => Yii::t('app', 'ต้องการลบรายการนี้ใช่หรือไม่ ?'),
            'data-confirm-ok' => Yii::t('app', 'ใช่'),
            'data-confirm-cancel' => Yii::t('app', 'ไม่')]) ?>
    </div>
</div>

