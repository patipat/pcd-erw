<?php

use yii\helpers\Html;
?>
<div class="col-md-2  text-center margin-20">
    <div class="icondemo vertical-align-middle">
        <i class="icon wb-folder" aria-hidden="true" style="font-size: 100px;"></i>
        <div class="icon-title"><?= $model->name ?></div>
        <?= Html::a('<i class="icon icon wb-file"></i> ', ['project-files/show', 'fileCategoryId' => $model->id], ['role' => 'modal-remote', 'data-toggle' => 'tooltip','target' => '_blank','title' => 'โชว์ไฟล์']) ?>

    </div>
</div>

