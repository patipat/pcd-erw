<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FileCategory */

?>
<div class="file-category-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
