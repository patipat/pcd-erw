<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'โฟลเดอร์เอกสาร');
$this->params['breadcrumbs'][] = $this->title;


//CrudAsset::register($this);
?>
<div class="file-category-index">
    <div id="ajaxCrudDatatable">
        <div class="btn-toolbar kv-grid-toolbar toolbar-container pull-right" >
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'เพิ่มข้อมูลโฟลเดอร์เอกสาร'), ['file-category/create'], ['role' => 'modal-remote', 'class' => 'btn btn-success']) ?>
            <?= Html::a('<i class="glyphicon glyphicon-repeat"></i> ' . Yii::t('app', 'โหลดใหม่'), Url::current(), ['data-pjax' => 1, 'class' => 'btn btn-default']) ?>
        </div>
        <?php \yii\widgets\Pjax::begin(['id' => 'file-category-pjax', 'timeout' => FALSE, 'enablePushState' => FALSE]); ?>
        <?php
        echo ListView::widget([
            'id' => 'crud-datatable-file-category',
            'dataProvider' => $dataProvider,
            'layout' => '{summary}<hr>{items}{pager}',
            'itemView' => '_columns-all',
            'pager' => array(
                'firstPageLabel' => '<i class="icon-double-angle-left"></i>',
                'lastPageLabel' => '<i class="icon-double-angle-right"></i>',
                'prevPageLabel' => '<i class="icon-angle-left"></i>',
                'nextPageLabel' => '<i class="icon-angle-right"></i>',
            ),
            'id' => 'pagination',
        ]);
        /* @var $this yii\web\View */
        /* @var $model app\models\Pet */
        ?>  
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

