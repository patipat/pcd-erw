<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FileCategory */
?>
<div class="file-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
