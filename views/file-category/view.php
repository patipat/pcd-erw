<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FileCategory */
?>
<div class="file-category-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',

        ],
    ]) ?>

</div>
