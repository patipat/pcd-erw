<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FormIssueFile */

?>
<div class="form-issue-file-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
