<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FormIssueFile */
?>
<div class="form-issue-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
