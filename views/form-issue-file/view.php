<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FormIssueFile */
?>
<div class="form-issue-file-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'form_issue_id',
            'form_issue_submit_id',
            'parent_id',
            'type',
            'name',
            'file_name',
            'deleted',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
