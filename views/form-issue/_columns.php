<?php

use yii\helpers\Url;
use app\models\Role;
$currentRole = Yii::$app->session->get('currentRole');

return [
    //[
    //    'class' => 'kartik\grid\CheckboxColumn',
    //    'width' => '20px',
    //],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'assessIssue.name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'statusBadge',
        'format' => 'raw',
        'hAlign' => 'center',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'score',
        'format' => ['decimal', 0],
        'hAlign' => 'center',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'comment',
        'value' => function ($model) {
            return !empty($model->comment) ? $model->comment : "";
        }
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'submitted_at',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'confirmed_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'confirmed_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'checked_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'checked_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'is_pass',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'score',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'year',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'deleted',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//            return Url::to([$action, 'id' => $key]);
//        },
        'controller' => 'form-issue',
        'template' => '{picture} {file} {check}',
        'viewOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'รายละเอียด'), 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'แก้ไข'), 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'ลบ'),
            'data-confirm' => false, 'data-method' => false, // for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => Yii::t('app', 'ยืนยันการลบ'),
            'data-confirm-message' => Yii::t('app', 'ต้องการลบรายการนี้ใช่หรือไม่ ?'),
            'data-confirm-ok' => Yii::t('app', 'ใช่'),
            'data-confirm-cancel' => Yii::t('app', 'ไม่'),
        ],
        'buttons' => [
            'file' => function($url, $model) {
                return \yii\helpers\Html::a('<i class="icon fa-file-text-o"></i>',
                                Url::to(['form-issue/view', 'id' => $model->id, 'type' => app\models\FormIssueFile::TYPE_FILE
                                ]),
                                ['role' => 'modal-remote']);
            },
            'picture' => function($url, $model) {
                return \yii\helpers\Html::a('<i class="icon fa-file-photo-o"></i>',
                                ['form-issue/view', 'type' => app\models\FormIssueFile::TYPE_IMAGE
                                    , 'id' => $model->id],
                                ['role' => 'modal-remote']);
            },
            'check' => function($url, $model) {
                return \yii\helpers\Html::a('<i class="icon md-assignment-check"></i>',
                                ['form-issue/check', 'id' => $model->id],
                                ['role' => 'modal-remote']);
            },
        ],
        'visibleButtons' => [
//            'check' => function($model)use($currentRole) {
//                return !isset($model->checked_at);
//            }
        ]
    ],
];
