<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\FormIssue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-issue-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>


    <?php
    if ($type == app\models\FormIssueFile::TYPE_IMAGE) {
        echo $form->field($issueFile, 'uploadFiles[]')->widget(FileInput::classname(), [
            'options' => [
                'multiple' => true,
                'accept' => '*',
            ],
            'pluginOptions' => [
                'showUpload' => false,
                'browseLabel' => '',
                'removeLabel' => '',
                'initialPreview' => [
                    isset($issueFile->file_name) ? $issueFile->fileUrl : null
                ],
                'initialPreviewAsData' => true,
                'initialCaption' => $issueFile->file_name,
                'initialPreviewConfig' => [
                    ['caption' => $issueFile->file_name]
                ],
            ],
        ]);
    } else {
        echo $form->field($issueFile, 'name')->label('ชื่อเอกสาร')->textInput(['maxlength' => true]);
        echo $form->field($issueFile, 'uploadFiles')->widget(FileInput::classname(), [
            'options' => [
                'accept' => '*',
            ],
            'pluginOptions' => [
                'required' => true,
                'showUpload' => false,
                'browseLabel' => '',
                'removeLabel' => '',
                'initialPreview' => [
                    isset($issueFile->file_name) ? $issueFile->fileUrl : null
                ],
                'initialPreviewAsData' => true,
                'initialCaption' => $issueFile->file_name,
                'initialPreviewConfig' => [
                    ['caption' => $issueFile->file_name]
                ],
            ],
        ]);
    }
    ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
