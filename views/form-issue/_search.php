<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FormIssue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-issue-search">

    <?php $form = ActiveForm::begin([
        'action' => Url::to(['form-issue/index']),
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
        ],
        'type' => ActiveForm::TYPE_INLINE,
    ]); ?>
    
    <?= $form->field($model, 'six_month_form_id') ?>

    <?= $form->field($model, 'assess_issue_id') ?>

    <?= $form->field($model, 'form_issue_submit_id') ?>

    <?= $form->field($model, 'submitted_by') ?>

    <?= $form->field($model, 'submitted_at') ?>

    <?= $form->field($model, 'confirmed_by') ?>

    <?= $form->field($model, 'confirmed_at') ?>

    <?= $form->field($model, 'checked_by') ?>

    <?= $form->field($model, 'checked_at') ?>

    <?= $form->field($model, 'is_pass') ?>

    <?= $form->field($model, 'score') ?>

    <?= $form->field($model, 'deleted') ?>

    <?= $form->field($model, 'created_by') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'updated_by') ?>

    <?= $form->field($model, 'updated_at') ?>

  
	<div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>
    
</div>
