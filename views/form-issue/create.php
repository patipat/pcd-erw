<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FormIssue */
?>
<div class="form-issue-create">
<?=
$this->render('_form', [
    'model' => $model,
    'issueFile' => $issueFile,
    'type'=>$type
])
?>
</div>
