<?php

use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\FormIssueFile;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\FormIssue */
$currentRole = Yii::$app->session->get('currentRole');
$files = ($type == FormIssueFile::TYPE_FILE ? $model->files : $model->images);
$fileUrls = ArrayHelper::getColumn($files, 'fileUrl');
//yii\helpers\VarDumper::dump($fileUrls);
//exit;
$previewConfigs = ArrayHelper::getColumn($files, 'previewConfig');
//yii\helpers\VarDumper::dump($previewConfigs);
//exit;   
echo FileInput::widget([
    'name' => 'files',
    'options' => [
        'accept' => '*',
    ],
    'pluginOptions' => [
        'showUpload' => false,
        'showBrowse' => false,
        'showRemove' => false,
        'showCaption' => false,
        'showClose' => false,
        'dropZoneEnabled' => false,
        'fileActionSettings' => [
            'showDrag' => false,
            'showRemove' => ((!isset($model->checked_by) || $model->is_pass == 0) 
                // && Yii::$app->util->isValidSixMonthSubmitDate($model->sixMonthForm->year)
                ) ,
        ],
        'deleteUrl' => Url::to(['form-issue-file/delete']),
        'overwriteInitial' => false,
        'initialPreview' => $fileUrls,
//        'initialPreviewFileType' => 'any',
        'initialPreviewAsData' => true,
        'initialPreviewDownloadUrl' => Url::to(['form-issue-file/download']),
//        'initialCaption' => $issueFile->file_name,
        'initialPreviewConfig' => $previewConfigs,
    ],
]);
?>
<?php
//$css = <<<css
//    .file-caption-main {
//        display: none;
//    }
//css;
//$this->registerCss($css);