<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use kartik\dialog\Dialog;
use yii\web\JsExpression;
use yii\bootstrap\Modal;

AppAsset::register($this);
//\app\assets\TableAsset::register($this);
//\app\assets\RemarkIconsAsset::register($this);
//\app\assets\LaddaAsset::register($this);
//johnitvn\ajaxcrud\CrudAsset::register($this);
//app\assets\ToastrAsset::register($this);
//\app\assets\NumeralAsset::register($this);
//\app\assets\MomentAsset::register($this);
//app\assets\ThaiDateAsset::register($this);

echo Dialog::widget([
    'libName' => 'dlgError',
    'options' => [
        'type' => Dialog::TYPE_DANGER,
        'title' => Yii::t('app', 'ข้อผิดพลาด'),
        'buttons' => [
                [
                    'id' => 'btn-ok',
                    'label' => Yii::t('app', 'ปิด'),
                    'icon' => '',
                    'cssClass' => 'btn btn-danger btn-raised',
                    'action' => new JsExpression("function(dialog) {
                        dialog.close();
                    }")
                ],
                [
                    'id' => 'btn-cancel',
                    'cssClass' => 'hidden',
                ]
            ],
    ], // default options
]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script>Breakpoints();</script>
    </head>
    <body class="<?= isset($this->params['bodyClass']) ? $this->params['bodyClass'] : "" ?>" data-spy="scroll" data-target="#navbar-measure">
        <?php $this->beginBody() ?>

        <?php
        if (!Yii::$app->user->isGuest) {
            echo $this->renderFile('@app/views/widgets/_navbar.php');
            echo $this->renderFile('@app/views/widgets/_sidebar.php');
        }
        ?>

        <!-- Page -->
        <div class="page content">
            <?php if (!Yii::$app->user->isGuest): ?>
                <div class="page-header page-toolbar padding-vertical-10">
                    <h1 class="page-title <?= isset($this->params['title-color']) ? $this->params['title-color'] : "" ?>"><?= $this->title ?> <?= isset($this->params['button']) ? $this->params['button'] : "" ?></h1>
                    <div class="page-header-actions">
                        <?=
                        \yii\widgets\Breadcrumbs::widget([
                            'homeLink' => [
                                'label' => 'หน้าหลัก',
                                'url' => ['site/index']
                            ],
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//                            'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>\n",
//                            'activeItemTemplate' => "<li class='breadcrumb-item active'>{link}</li>\n"
                        ])
                        ?>
                    </div>
                </div>

            <?php endif; ?>
            <div class="page-content container-fluid has-page-toolbar">
                <?= $content ?>
            </div>
        </div>
        <footer class="site-footer">
            <div class="site-footer-legal">© <?= date('Y') ?> <a href="#">กรมควบคุมมลพิษ</a></div>
        </footer>
        <?php
        Modal::begin([
            "id" => "ajaxCrudModal",
            "options" => [
                "tabindex" => FALSE,
                'class' => 'modal-primary',
                'data-backdrop' => 'static',
            ],
            "footer" => "", // always need it for jquery plugin
        ])
        ?>
        <?php Modal::end(); ?>
        <?php $this->endBody() ?>
        <script>
            (function (document, window, $) {
                'use strict';
                var Site = window.Site;
                $(document).ready(function () {
                    Site.run();
                });
            })(document, window, jQuery);
            $('.btn-logout').on('click', function (e) {
                e.preventDefault();
                $.ajax({
                    url: '<?= Url::to(['site/logout']) ?>',
                    method: 'POST',
                    dataType: 'JSON',
                    success: function (data, textStatus, jqXHR) {
                        window.location = data.location;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        dlgError.dialog(textStatus + ': ' + jqXHR.status + ' ' + errorThrown + '</br>' + jqXHR.responseText, function (result) {});
                    }
                });
            });
            
            function acknowledge(alertId) {
//                alert(alertId);
//                var alertCount = $('.alert-count').text();
//                $('.alert-count').text(--alertCount);
//                $(event.target).hide();
//                var $item = $(event.target).closest('.list-group-item');
//                $item.addClass('bg-grey-300');
//                $item.hide('slow');

                $.ajax({
                    url: '<?= Url::to(['alert/acknowledge']) ?>',
                    data: {id: alertId},
                    success: function (data, textStatus, jqXHR) {
                        updateAlertMenu();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        dlgError.alert(textStatus + ": " + jqXHR.status + " " + errorThrown + "</br>" + jqXHR.responseText);
                    }
                });
            }
            function acknowledgeAll() {
                $.ajax({
                    url: '<?= Url::to(['alert/acknowledge-all']) ?>',
                    success: function (data, textStatus, jqXHR) {
                        updateAlertMenu();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        dlgError.alert(textStatus + ": " + jqXHR.status + " " + errorThrown + "</br>" + jqXHR.responseText);
                    }
                });
            }

            function updateAlertMenu() {
                $.ajax({
                    url: '<?= Url::to(['alert/get-alert-list']) ?>',
                    success: function (data, textStatus, jqXHR) {
                        $('.alert-count').text(data.count);
                        var li = $('#alert-dropdown').parent();
                        $('#alert-dropdown').remove();
                        li.append(data.menu);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        BootstrapDialog.closeAll();
                        setTimeout(() => {
                            dlgError.alert(textStatus + ": " + jqXHR.status + " " + errorThrown + "</br>" + jqXHR.responseText);
                        }, 500);
                    }
                });
            }

            function checkUnshownAlert() {
                var url = '<?= Url::to(['submission/project-submission']) ?>';
                $.ajax({
                    url: '<?= Url::to(['alert/get-unshown-alerts', 't' => time()]) ?>',
                    success: function (data, textStatus, jqXHR) {
                        var n = data.length;
//                        console.log(data)
                        for (var i = 0; i < n; i++) {
                            var color = 'error';
                            var onclick = '$(this).closest(\'.toast\').fadeOut(1000);acknowledge(' + data[i].id + ');';
//                            color = 'error';
//                            var op = url.indexOf('?') == -1 ? '?' : '&';
//                            onclick += "window.open('" + url + op + "submissionId=" + data[i].submission_id + "', '_blank');";
//                            if (i == 0) {
//                                color = 'success';
//                            }
                            toastr.info(data[i].message + '<br><button class="btn btn-xs btn-primary" onclick="' + onclick + '"><i class="icon md-check"></i> <?= Yii::t('app', 'รับทราบ') ?></button>', '', {
                                iconClass: 'toast-just-text toast-' + color,
                                messageClass: data[i].id
//                                onclick: function () {
//                                    alertShown(this.messageClass);
//                                }
                            });
                        }
                        updateAlertMenu();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        BootstrapDialog.closeAll();
                        setTimeout(() => {
                            dlgError.alert(textStatus + ": " + jqXHR.status + " " + errorThrown + "</br>" + jqXHR.responseText);
                        }, 500);
                    }
                });
            }

            setInterval(function () {
//                updateAlertMenu();
                checkUnshownAlert();
            }, <?= Yii::$app->params['alertCheckInterval'] ?>);
            checkUnshownAlert();
        </script>
    </body>
</html>
<?php $this->endPage() ?>
