<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
//use yii\helpers\Url;
use app\assets\AppAssetSite;

//use kartik\dialog\Dialog;

AppAssetSite::register($this);
$controllerl = Yii::$app->controller;
$homecheker = $controllerl->id . '/' . $controllerl->action->id;
?>
<?php $this->beginPage() ?>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!--<script>Breakpoints();</script>-->
    </head>
    <?php $this->beginBody() ?>
    <body>
        <nav class="navbar py-4 navbar-expand-lg ftco_navbar navbar-light bg-light flex-row">
            <div class="container">
                <div class="row no-gutters d-flex align-items-start align-items-center px-3 px-md-0">
                    <div class="col-lg-2 pr-4 align-items-center"> 
                        <?=
                        Html::a('มาตรการลด<span>และคัดแยกขยะมูลฝอยในหน่วยงานภาครัฐ</span>', ['/site/main'], [
                            'style' => 'text-decoration: none', 'class' => 'navbar-brand'])
                        ?>
                    </div>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
            <div class="container d-flex align-items-center">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="oi oi-menu"></span> Menu
                </button>
                <p class="button-custom order-lg-last mb-0">
                    <?php if (\Yii::$app->user->isGuest) { ?>
                        <?=
                        Html::a('เข้าสู่ระบบจัดการข้อมูล', ['/site/login'], [
                            'style' => 'text-decoration: none', 'class' => 'btn btn-secondary py-2 px-3', 'target' => '_blank'])
                        ?>
                    <?php } else { ?>
                        <?=
                        Html::a('เข้าสู่ระบบจัดการข้อมูล', ['/site/index'], [
                            'style' => 'text-decoration: none', 'class' => 'btn btn-secondary py-2 px-3', 'target' => '_blank'])
                        ?>
                    <?php } ?>
                </p>
                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <?=
                            Html::a('หน้าหลัก', ['/site/main'], [
                                'style' => 'text-decoration: none', 'class' => 'nav-link pl-0'])
                            ?>
                        </li>

                        <li class="nav-item">
                            <?=
                            Html::a('ข้อมูลเกี่ยวกับโครงการ', ['/content/view', 'id' => 1], [
                                'style' => 'text-decoration: none', 'class' => 'nav-link'])
                            ?>
                        </li>
                        <li class="nav-item dropdown">
                            <?=
                            Html::a('รายงานความก้าวหน้า', ['/site/progress'], [
                                'style' => 'text-decoration: none', 'class' => 'nav-link'])
                            ?>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><span class="ion-ios-arrow-round-forward"></span> 
                                    <?=
                                    Html::a('รายงานรอบเดือนส่วนราชการ', ['office/yearly-assessment-report-web', 'year' => 2562, 'officeType' => 1, 'level' => 2], [
                                        'style' => 'text-decoration: none'])
                                    ?></li>
                                <li class="dropdown-item"><span class="ion-ios-arrow-round-forward"></span>
                                    <?=
                                    Html::a('รายงานรอบเดือนส่วนจังหวัด', ['office/yearly-assessment-report-web', 'year' => 2562, 'officeType' => 2, 'level' => 2], [
                                        'style' => 'text-decoration: none'])
                                    ?>
                                </li>
                                <li class="dropdown-item"><span class="ion-ios-arrow-round-forward"></span> 
                                    <?=
                                    Html::a('รายงานรอบเดือนส่วน ทสจ. สสภ.', ['office/yearly-assessment-report-web', 'year' => 2562, 'officeType' => 3, 'level' => 2], [
                                        'style' => 'text-decoration: none'])
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <?=
                            Html::a('ข้อมูลมาตรการ', ['/content/content-list', 'group' => \app\models\Content::MEASURE], [
                                'style' => 'text-decoration: none', 'class' => 'nav-link'])
                            ?>
                        </li>
                        <li class="nav-item">
                            <?=
                            Html::a('บทความทางวิชาการ', ['/content/content-list', 'group' => \app\models\Content::ACADAMIC], [
                                'style' => 'text-decoration: none', 'class' => 'nav-link'])
                            ?>
                        </li>
                        <li class="nav-item dropdown">
                            <?=
                            Html::a('ดาวน์โหลด', ['/content/content-list', 'group' => \app\models\Content::DOWNLOAD], [
                                'style' => 'text-decoration: none', 'class' => 'nav-link'])
                            ?>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><span class="ion-ios-arrow-round-forward"></span> 
                                    <?=
                                    Html::a('คู่มือ', ['/content/content-list', 'group' => \app\models\Content::DOWNLOAD, 'categoryId' => 1], [
                                        'style' => 'text-decoration: none'])
                                    ?></li>
                                <li class="dropdown-item"><span class="ion-ios-arrow-round-forward"></span>
                                    <?=
                                    Html::a('โปสเตอร์', ['/content/content-list', 'group' => \app\models\Content::DOWNLOAD, 'categoryId' => 2], [
                                        'style' => 'text-decoration: none'])
                                    ?>
                                </li>
                                <li class="dropdown-item"><span class="ion-ios-arrow-round-forward"></span> 
                                    <?=
                                    Html::a('ไฟล์วีดีโอ', ['/content/content-list', 'group' => \app\models\Content::DOWNLOAD, 'categoryId' => 3], [
                                        'style' => 'text-decoration: none'])
                                    ?>
                                </li>
                                <li class="dropdown-item"><span class="ion-ios-arrow-round-forward"></span> 
                                    <?=
                                    Html::a('PPT', ['/content/content-list', 'group' => \app\models\Content::DOWNLOAD, 'categoryId' => 4], [
                                        'style' => 'text-decoration: none'])
                                    ?>
                                </li>
                                <li class="dropdown-item"><span class="ion-ios-arrow-round-forward"></span> 
                                    <?=
                                    Html::a('ภาพกิจกรรม (zip file)', ['/content/content-list', 'group' => \app\models\Content::DOWNLOAD, 'categoryId' => 5], [
                                        'style' => 'text-decoration: none'])
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <?=
                            Html::a('ติดต่อสอบถาม', ['/content/view', 'id' => 2], [
                                'style' => 'text-decoration: none', 'class' => 'nav-link'])
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END nav -->
        <?php if ($homecheker == 'site/main') { ?>
            <section class="home-slider owl-carousel">
                <div class="slider-item" style="background-image:url(<?= yii\helpers\Url::to('@web/theme/images/bg_1.jpg'); ?>);">
                </div>

                <div class="slider-item" style="background-image:url(<?= yii\helpers\Url::to('@web/theme/images/bg_2.jpg'); ?>);">
                </div>
            </section>
        <?php } else { ?>

            <section class="hero-wrap hero-wrap-2" data-stellar-background-ratio="0.5">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row no-gutters slider-text align-items-center justify-content-center">
                        <div class="col-md-9 ftco-animate text-center">
                            <h1 class="mb-2 bread"><?= $this->title ?> </h1>
                        </div>
                    </div>
                </div>
            </section>
        <?php } ?>
        <?= $content ?>


        <footer class="ftco-footer ftco-bg-dark ftco-section">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> กรมควบคุมมลพิษ Email : webmaster@pcd.go.th Tel : 0 2298-2000</p>
                </div>
            </div>
        </footer>



        <!-- loader -->
        <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
            <?php $this->endBody() ?>

    </body>
</html>

<?php $this->endPage() ?>
