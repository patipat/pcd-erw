<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\FormIssue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monthly-file-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>


    <?php

    echo $form->field($model, 'name')->label('ชื่อเอกสาร')->textInput(['maxlength' => true]);
    echo $form->field($model, 'uploadFiles')->widget(FileInput::classname(), [
        'options' => [
            'accept' => '*',
        ],
        'pluginOptions' => [
            'required' => true,
            'showUpload' => false,
            'browseLabel' => '',
            'removeLabel' => '',
            'initialPreview' => [
                isset($model->file_name) ? $model->fileUrl : null
            ],
            'initialPreviewAsData' => true,
            'initialCaption' => $model->file_name,
            'initialPreviewConfig' => [
                ['caption' => $model->file_name]
            ],
        ],
    ]);
    ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>