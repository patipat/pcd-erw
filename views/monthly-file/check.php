<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\MonthlyFormSubmit */
?>
<div class="monthly-form-submit-check">

    <div class="monthly-form-submit-form">

        <?php $form = ActiveForm::begin(); ?>
        <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

        <?= $form->field($model, 'is_pass')->radioList(Yii::$app->util->getPassFailLabels()) ?>
        
        <?= $form->field($model, 'comment')->textarea(); ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>


</div>
