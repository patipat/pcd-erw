<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FormIssue */
?>
<div class="monthly-file-create">
<?=
$this->render('_form', [
    'model' => $model,
])
?>
</div>
