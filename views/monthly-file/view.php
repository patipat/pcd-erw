<?php

use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\FormIssueFile;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\FormIssue */
$currentRole = Yii::$app->session->get('currentRole');
$fileUrls = ArrayHelper::getColumn($files, 'fileUrl');
//yii\helpers\VarDumper::dump($fileUrls);
//exit;
$previewConfigs = ArrayHelper::getColumn($files, 'previewConfig');
//yii\helpers\VarDumper::dump($previewConfigs);
//exit;   
if (isset($addButton)) {
    echo $addButton;
}
echo FileInput::widget([
    'name' => 'files',
    'options' => [
        'accept' => '*',
    ],
    'pluginOptions' => [
        'showUpload' => false,
        'showBrowse' => false,
        'showRemove' => false,
        'showCaption' => false,
        'showClose' => false,
        'dropZoneEnabled' => false,
        'fileActionSettings' => [
            'showDrag' => false,
            'showRemove' => true,
        ],
        'deleteUrl' => Url::to(['monthly-file/delete']),
        'overwriteInitial' => false,
        'initialPreview' => $fileUrls,
//        'initialPreviewFileType' => 'any',
        'initialPreviewAsData' => true,
        'initialPreviewDownloadUrl' => Url::to(['monthly-file/download']),
//        'initialCaption' => $issueFile->file_name,
        'initialPreviewConfig' => $previewConfigs,
    ],
]);
?>
<?php
//$css = <<<css
//    .file-caption-main {
//        display: none;
//    }
//css;
//$this->registerCss($css);