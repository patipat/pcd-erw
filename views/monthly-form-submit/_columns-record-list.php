<?php

use yii\helpers\Url;

$items = [
    //[
    //    'class' => 'kartik\grid\CheckboxColumn',
    //    'width' => '20px',
    //],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
        'headerOptions' => [
            'style' => 'display: none;',
        ]
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pop_int',
        'format' => ['decimal', 0],
        'header' => 'ภายใน',
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pop_ext',
        'format' => ['decimal', 0],
        'header' => 'ภายนอก',
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'office_population',
        'format' => ['decimal', 0],
        'header' => 'ทั้งหมด',
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'work_day',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'garbage',
        'format' => ['decimal', 2],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'bag',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'cup',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'foam',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ]
];
if ($searchModel->year == '2565') {
    $items[] = [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'mask',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ];
}


$items = array_merge($items, [
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'statusBadge',
        'format' => 'raw',
        'headerOptions' => [
            'style' => 'display: none;',
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'confirmed_at',
        'format' => ['date', 'php:d/m/Y'],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'checked_at',
        'format' => ['date', 'php:d/m/Y'],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
    ]
        ]);
return $items;

