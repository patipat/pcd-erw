<?php

use kartik\grid\GridView;
use yii\helpers\Url;

return [
    //[
    //    'class' => 'kartik\grid\CheckboxColumn',
    //    'width' => '20px',
    //],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return $model->office->hasChildren ? GridView::ROW_COLLAPSED : "";
        },
        'detailUrl' => Url::to(['monthly-form-submit/sub-list']),
        // 'detail' => function ($model, $key, $index, $column) {
        //     $searchModel = new app\models\FormIssueSearch();
        //     $searchModel->deleted = 0;
        //     $searchModel->six_month_form_id = $model->id;
        //     $dataProvider = $searchModel->search([]);
        //     return Yii::$app->controller->renderPartial('@app/views/form-issue/index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
        // },
        'headerOptions' => [
            'class' => 'kartik-sheet-style',
            'style' => 'display: none;',
        ],
        'expandOneOnly' => true,
        'hiddenFromExport' => true,
        'detailRowCssClass' => 'detail-row',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
        'headerOptions' => [
            'style' => 'display: none;',
        ]
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'office.name',
        'headerOptions' => [
            'style' => 'display: none;',
        ]
    ],
    // [
    //     'class' => '\kartik\grid\DataColumn',
    //     'attribute' => 'year',
    //     'headerOptions' => [
    //         'style' => 'display: none;',
    //     ]
    // ],
    // [
    //     'class' => '\kartik\grid\DataColumn',
    //     'attribute' => 'monthName',
    //     'headerOptions' => [
    //         'style' => 'display: none;',
    //     ]
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pop_int',
        'format' => ['decimal', 0],
        'header' => 'ภายใน',
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'pop_ext',
        'format' => ['decimal', 0],
        'header' => 'ภายนอก',
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'office_population',
        'format' => ['decimal', 0],
        'header' => 'ทั้งหมด',
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'work_day',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'garbage',
        'format' => ['decimal', 2],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'bag',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'cup',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'foam',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'mask',
        'format' => ['decimal', 0],
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'statusBadge',
        'format' => 'raw',
        'headerOptions' => [
            'style' => 'display: none;',
        ],
    ],
        // [
        //     'class' => 'kartik\grid\ActionColumn',
        //     'dropdown' => false,
        //     'vAlign' => 'middle',
        //     'urlCreator' => function($action, $model, $key, $index) {
        //         return Url::to([$action, 'id' => $key]);
        //     },
        //     'template' => '{record} {check}',
        //     'viewOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'รายละเอียด'), 'data-toggle' => 'tooltip'],
        //     'updateOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'แก้ไข'), 'data-toggle' => 'tooltip'],
        //     'deleteOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'ลบ'),
        //         'data-confirm' => false, 'data-method' => false, // for overide yii data api
        //         'data-request-method' => 'post',
        //         'data-toggle' => 'tooltip',
        //         'data-confirm-title' => Yii::t('app', 'ยืนยันการลบ'),
        //         'data-confirm-message' => Yii::t('app', 'ต้องการลบรายการนี้ใช่หรือไม่ ?'),
        //         'data-confirm-ok' => Yii::t('app', 'ใช่'),
        //         'data-confirm-cancel' => Yii::t('app', 'ไม่'),
        //     ],
        //     'buttons' => [
        //         'record' => function($url, $model) {
        //             return \yii\helpers\Html::a('<i class="icon md-format-list-bulleted"></i>', 
        //                     ['monthly-form-submit/record-list', 'MonthlyFormSubmitSearch[office_id]' => $model->office_id
        //                         , 'MonthlyFormSubmitSearch[year]' => $model->year, 'MonthlyFormSubmitSearch[month]' => $model->month], 
        //                     ['role' => 'modal-remote']);
        //         },
        //         'check' => function($url, $model) {
        //             return \yii\helpers\Html::a('<i class="icon md-assignment-check"></i>', 
        //                     ['monthly-form-submit/check', 'id' => $model->id], [
        //                         'role' => 'modal-remote', 'title' => Yii::t('app', 'ตรวจสอบ'),
        //             ]);
        //         },
        //     ],
        //     'visibleButtons' => [
        //         'check' => function($model) {
        //             return Yii::$app->util->checkPermission('monthly-form-submit.check') && isset($model->confirmed_at) && !isset($model->checked_at);
        //         },
        //         'record' => function($model) {
        //             return Yii::$app->util->checkPermission('monthly-form-submit.record-list') && $model->hasRecord;
        //         },
        //     ]
        // ],
];
