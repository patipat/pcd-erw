<?php
$lastYearForm = $model->lastYearForm;
?>
<table class="table table-bordered table-striped table-condensed">
    <tbody>
        <tr>
            <td class="font-weight-900">ปี</td>
            <td class="bg-green-600 white text-center"><?= $model->year ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= $lastYearForm->year ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">เดือน</td>
            <td class="bg-green-600 white text-center"><?= $model->monthName ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= $lastYearForm->monthName ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">การเก็บข้อมูล</td>
            <td class="bg-green-600 white text-center"><?= $model->sourceType->name ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= $lastYearForm->sourceType->name ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">จำนวนวันทำงาน(วัน)</td>
            <td class="bg-green-600 white text-center"><?= Yii::$app->formatter->asDecimal($model->work_day, 0) ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= Yii::$app->formatter->asDecimal($lastYearForm->work_day, 0) ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">จำนวนบุคลากรภายใน</td>
            <td class="bg-green-600 white text-center"><?= Yii::$app->formatter->asDecimal($model->pop_int, 0) ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= Yii::$app->formatter->asDecimal($lastYearForm->pop_int, 0) ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">จำนวนบุคลากรภายนอก</td>
            <td class="bg-green-600 white text-center"><?= Yii::$app->formatter->asDecimal($model->pop_ext, 0) ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= Yii::$app->formatter->asDecimal($lastYearForm->pop_ext, 0) ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">รวมบุคลากร</td>
            <td class="bg-green-600 white text-center"><?= Yii::$app->formatter->asDecimal($model->office_population, 0) ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= Yii::$app->formatter->asDecimal($lastYearForm->office_population, 0) ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">ปริมาณขยะมูลฝอย (กก.)</td>
            <td class="bg-green-600 white text-center"><?= Yii::$app->formatter->asDecimal($model->garbage, 2) ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= Yii::$app->formatter->asDecimal($lastYearForm->garbage, 2) ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">จำนวนถุงพลาสติก (ใบ)</td>
            <td class="bg-green-600 white text-center"><?= Yii::$app->formatter->asDecimal($model->bag, 0) ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= Yii::$app->formatter->asDecimal($lastYearForm->bag, 0) ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">จำนวนแก้วพลาสติก (ใบ)</td>
            <td class="bg-green-600 white text-center"><?= Yii::$app->formatter->asDecimal($model->cup, 0) ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= Yii::$app->formatter->asDecimal($lastYearForm->cup, 0) ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">จำนวนโฟมบรรจุอาหาร (ใบ)</td>
            <td class="bg-green-600 white text-center"><?= Yii::$app->formatter->asDecimal($model->foam, 0) ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= Yii::$app->formatter->asDecimal($lastYearForm->foam, 0) ?></td>
            <?php endif; ?>
        </tr>
        <?php if($model->year == '2565'){?>
        <tr>
            <td class="font-weight-900">จำนวนหน้ากากอนามัย (ชิ้น)</td>
            <td class="bg-green-600 white text-center"><?= Yii::$app->formatter->asDecimal($model->mask, 0) ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= Yii::$app->formatter->asDecimal($lastYearForm->mask, 0) ?></td>
            <?php endif; ?>
        </tr>
        <?php } ?>
        <tr>
            <td class="font-weight-900">หมายเหตุ</td>
            <td class="bg-green-600 white text-center"><?= $model->remark ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= $lastYearForm->remark ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">ส่งล่าสุดโดย</td>
            <td class="bg-green-600 white text-center"><?= isset($model->submittedBy) ? $model->submittedBy->person->fullName : '' ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= isset($lastYearForm->submittedBy) ? $lastYearForm->submittedBy->person->fullName : '' ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">ส่งล่าสุดวันที่</td>
            <td class="bg-green-600 white text-center"><?= isset($model->submitted_at) ? Yii::$app->thaiFormatter->asDate($model->submitted_at, 'php:d/m/Y') : '' ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= isset($lastYearForm->submitted_at) ? Yii::$app->thaiFormatter->asDate($lastYearForm->submitted_at, 'php:d/m/Y') : '' ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">ยืนยันล่าสุดโดย</td>
            <td class="bg-green-600 white text-center"><?= isset($model->confirmedBy) ? $model->confirmedBy->person->fullName : '' ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= isset($lastYearForm->confirmedBy) ? $lastYearForm->confirmedBy->person->fullName : '' ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">ยืนยันล่าสุดวันที่</td>
            <td class="bg-green-600 white text-center"><?= isset($model->confirmed_at) ? Yii::$app->thaiFormatter->asDate($model->confirmed_at, 'php:d/m/Y') : '' ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= isset($lastYearForm->confirmed_at) ? Yii::$app->thaiFormatter->asDate($lastYearForm->confirmed_at, 'php:d/m/Y') : '' ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">ตรวจสอบล่าสุดโดย</td>
            <td class="bg-green-600 white text-center"><?= isset($model->checkedBy) ? $model->checkedBy->person->fullName : '' ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= isset($lastYearForm->checkedBy) ? $lastYearForm->checkedBy->person->fullName : '' ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">ตรวจสอบล่าสุดวันที่</td>
            <td class="bg-green-600 white text-center"><?= isset($model->checked_at) ? Yii::$app->thaiFormatter->asDate($model->checked_at, 'php:d/m/Y') : '' ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= isset($lastYearForm->checked_at) ? Yii::$app->thaiFormatter->asDate($lastYearForm->checked_at, 'php:d/m/Y') : '' ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">สถานะ</td>
            <td class="bg-green-600 white text-center"><?= $model->statusBadge ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= $lastYearForm->statusBadge ?></td>
            <?php endif; ?>
        </tr>
        <tr>
            <td class="font-weight-900">หมายเหตุการตรวจสอบ</td>
            <td class="bg-green-600 white text-center"><?= $model->comment ?></td>
            <?php if (isset($lastYearForm)) : ?>
                <td class="text-center"><?= $lastYearForm->comment ?></td>
            <?php endif; ?>
        </tr>
    </tbody>
</table>
<?php
$css = <<<cs
    th {
        font-weight: 900;
    }
cs;
$this->registerCss($css);
