<?php

use kartik\detail\DetailView;


if($model->year == 2565){
    $a = [
        [
            'columns' => [
                [
                    'attribute' => 'year',
                    //                        'label' => 'Book #',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'monthName',
                    //                        'format' => 'raw',
                    //                        'value' => '<kbd>' . $model->book_code . '</kbd>',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'displayOnly' => true
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'source_type_id',
                    'value' => $model->sourceType->name,
                ],
                [
                    'attribute' => 'work_day',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:30%'],
                ],
            ]
        ],
        [
            'columns' => [
                [
                    'attribute' => 'pop_int',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:10%'],
                ],
                [
                    'attribute' => 'pop_ext',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:10%'],
                ],
                [
                    'attribute' => 'office_population',
                    'label' => 'รวม',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:10%'],
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'garbage',
                    'format' => ['decimal', 2],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:30%'],
                ],
                [
                    'attribute' => 'bag',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:30%'],
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'cup',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:15%'],
                ],
                [
                    'attribute' => 'foam',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:15%'],
                ],
                [
                    'attribute' => 'mask',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:15%'],
                ],
            ],
        ],
        'remark:ntext',
        [
            'columns' => [
                [
                    'attribute' => 'submitted_by',
                    'value' => isset($model->submittedBy) ? $model->submittedBy->person->fullName : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'submitted_at',
                    'value' => isset($model->submitted_at) ? Yii::$app->thaiFormatter->asDate($model->submitted_at, 'php:d/m/Y') : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'confirmed_by',
                    'value' => isset($model->confirmedBy) ? $model->confirmedBy->person->fullName : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'confirmed_at',
                    'value' => isset($model->confirmed_at) ? Yii::$app->thaiFormatter->asDate($model->confirmed_at, 'php:d/m/Y') : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'checked_by',
                    'value' => isset($model->checkedBy) ? $model->checkedBy->person->fullName : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'checked_at',
                    'value' => isset($model->checked_at) ? Yii::$app->thaiFormatter->asDate($model->checked_at, 'php:d/m/Y') : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
            ],
        ],
        'statusBadge:raw',
        'comment:text',
    ];
}else{
     $a = [
        [
            'columns' => [
                [
                    'attribute' => 'year',
                    //                        'label' => 'Book #',
                    'displayOnly' => true,
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'monthName',
                    //                        'format' => 'raw',
                    //                        'value' => '<kbd>' . $model->book_code . '</kbd>',
                    'valueColOptions' => ['style' => 'width:30%'],
                    'displayOnly' => true
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'source_type_id',
                    'value' => $model->sourceType->name,
                ],
                [
                    'attribute' => 'work_day',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:30%'],
                ],
            ]
        ],
        [
            'columns' => [
                [
                    'attribute' => 'pop_int',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:10%'],
                ],
                [
                    'attribute' => 'pop_ext',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:10%'],
                ],
                [
                    'attribute' => 'office_population',
                    'label' => 'รวม',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:10%'],
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'garbage',
                    'format' => ['decimal', 2],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:30%'],
                ],
                [
                    'attribute' => 'bag',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:30%'],
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'cup',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:15%'],
                ],
                [
                    'attribute' => 'foam',
                    'format' => ['decimal', 0],
                    'valueColOptions' => ['class' => 'text-right', 'style' => 'width:15%'],
                ],
            ],
        ],
        'remark:ntext',
        [
            'columns' => [
                [
                    'attribute' => 'submitted_by',
                    'value' => isset($model->submittedBy) ? $model->submittedBy->person->fullName : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'submitted_at',
                    'value' => isset($model->submitted_at) ? Yii::$app->thaiFormatter->asDate($model->submitted_at, 'php:d/m/Y') : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'confirmed_by',
                    'value' => isset($model->confirmedBy) ? $model->confirmedBy->person->fullName : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'confirmed_at',
                    'value' => isset($model->confirmed_at) ? Yii::$app->thaiFormatter->asDate($model->confirmed_at, 'php:d/m/Y') : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
            ],
        ],
        [
            'columns' => [
                [
                    'attribute' => 'checked_by',
                    'value' => isset($model->checkedBy) ? $model->checkedBy->person->fullName : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
                [
                    'attribute' => 'checked_at',
                    'value' => isset($model->checked_at) ? Yii::$app->thaiFormatter->asDate($model->checked_at, 'php:d/m/Y') : '',
                    'valueColOptions' => ['style' => 'width:30%']
                ],
            ],
        ],
        'statusBadge:raw',
        'comment:text',
    ];   
}
echo DetailView::widget([
    'model' => $model,
    'condensed' => true,
    'hover' => true,
    'mode' => DetailView::MODE_VIEW,
    'enableEditMode' => false,
    'attributes' => $a
]);
