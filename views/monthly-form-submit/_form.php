<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\SourceType;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\MonthlyFormSubmit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monthly-form-submit-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

    <?php if (isset($model->confirmed_by)): ?>
    <div class="alert alert-danger">
        <h4>
        <?= Yii::t('app', 'รายงานเดือน {month} ปี {year} ถูกยืนยันข้อมูลแล้ว เมื่อวันที่ {confirm_at}', [
            'year' => $model->year,
            'month' => $model->monthName,
            'confirm_at' => Yii::$app->thaiFormatter->asDate($model->confirmed_at, 'php:d/m/Y'),
        ]) ?>
        </h4>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'year')->textInput(['disabled' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'month')->textInput(['disabled' => true, 'value' => $model->monthName]) ?>
        </div>
    </div>
    <?= $form->field($model, 'source_type_id')->radioList(ArrayHelper::map(SourceType::find()->isDeleted(false)->orderBy('CONVERT(source_type.name USING TIS620)')->all(), 'id', 'name')) ?>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'pop_int')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ])
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'pop_ext')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ])
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'work_day')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ])
            ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <?=
            $form->field($model, 'garbage')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => '2',
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ])
            ?>
        </div>
        <div class="col-md-6">
            <?=
            $form->field($model, 'bag')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'cup')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ])
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'foam')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ])
            ?>
        </div>
                <div class="col-md-4">
            <?=
            $form->field($model, 'mask')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ])
            ?>
        </div>
    </div>
    
    <?= $form->field($model, 'remark')->textarea(['rows' => 3]) ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
