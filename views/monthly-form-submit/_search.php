<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MonthlyFormSubmit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monthly-form-submit-search">

    <?php $form = ActiveForm::begin([
        'action' => Url::to(['monthly-form-submit/index']),
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
        ],
        'type' => ActiveForm::TYPE_INLINE,
    ]); ?>
    
    <?= $form->field($model, 'office_id') ?>

    <?= $form->field($model, 'source_type_id') ?>

    <?= $form->field($model, 'year') ?>

    <?= $form->field($model, 'month') ?>

    <?= $form->field($model, 'office_population') ?>

    <?= $form->field($model, 'work_day') ?>

    <?= $form->field($model, 'garbage') ?>

    <?= $form->field($model, 'bag') ?>

    <?= $form->field($model, 'cup') ?>

    <?= $form->field($model, 'foam') ?>

    <?= $form->field($model, 'ref_id') ?>

    <?= $form->field($model, 'remark') ?>

    <?= $form->field($model, 'submitted_by') ?>

    <?= $form->field($model, 'submitted_at') ?>

    <?= $form->field($model, 'confirmed_by') ?>

    <?= $form->field($model, 'confirmed_at') ?>

    <?= $form->field($model, 'checked_by') ?>

    <?= $form->field($model, 'checked_at') ?>

    <?= $form->field($model, 'is_pass') ?>

    <?= $form->field($model, 'deleted') ?>

    <?= $form->field($model, 'created_by') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'updated_by') ?>

    <?= $form->field($model, 'updated_at') ?>

    <?= $form->field($model, 'pop_ext') ?>

    <?= $form->field($model, 'pop_int') ?>

  
	<div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>
    
</div>
