<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MonthlyFormSubmit */
?>
<div class="monthly-form-submit-check">

    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

    <?=
    $this->renderFile('@app/views/monthly-form-submit/view.php', [
        'model' => $model,
        'redirect' => Url::to(['monthly-form-submit/check', 'id' => $model->id, 'file' => 1], true),
        'file' => isset($file) ? $file : null,
        'from' => 'check',
    ]);
    ?>
    <div class="monthly-form-submit-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'is_pass')->radioList(Yii::$app->util->getPassFailLabels()) ?>

        <?= $form->field($model, 'comment')->textarea(); ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>


</div>