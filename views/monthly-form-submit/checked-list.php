<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MonthlyFormSubmitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ตรวจสอบรายงานรายเดือน');
$this->params['breadcrumbs'][] = $this->title;
if($searchModel->year == '2565'){
        $columns = [
                        ['content' => '&nbsp;', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => '#', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'หน่วยงาน', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'ปี', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'เดือน', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'จำนวนบุคลากร', 'options' => ['colspan' => 3, 'class' => 'text-center']],
                        ['content' => 'จำนวนวันทำงาน', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'ขยะ(กก.)', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'ถุง(ใบ)', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'แก้ว(ใบ)', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'โฟม(ใบ)', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'หน้ากากอนามัย(ชิ้น)', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'สถานะ', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                    ];
        }else{
                    $columns = [
                        ['content' => '&nbsp;', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => '#', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'หน่วยงาน', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'ปี', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'เดือน', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'จำนวนบุคลากร', 'options' => ['colspan' => 3, 'class' => 'text-center']],
                        ['content' => 'จำนวนวันทำงาน', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'ขยะ(กก.)', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'ถุง(ใบ)', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'แก้ว(ใบ)', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'โฟม(ใบ)', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                        ['content' => 'สถานะ', 'options' => ['rowspan' => 2, 'class' => 'text-center']],
                    ];
        }
$dataProvider->sort->defaultOrder = [
    'office.name' => SORT_ASC,
    'year' => SORT_ASC,
    'month' => SORT_ASC,
]
?>
<div class="monthly-form-submit-check-list">
    <div class="ajaxCrudDatatable">
        <?=
        GridView::widget([
            'id' => 'crud-datatable-monthly-form-submit',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'floatHeader' => true,
            'floatHeaderOptions' => ['top' => 66],
            'pjax' => true,
            'pjaxSettings' => [
                'beforeGrid' => $this->render('_search-checked-list', ['model' => $searchModel]),
            ],
            'beforeHeader' => [
                [
                    'columns' => $columns
                ]
            ],
            'columns' => require(__DIR__ . '/_columns-checked-list.php'),
            'toolbar' => [
                [
                    'options' => [
                        'class' => '',
                    ],
                    'content' =>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i> ' . Yii::t('app', 'โหลดใหม่'), Url::current(), ['data-pjax' => 1, 'class' => 'btn btn-default btn-raised'])
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => FALSE,
                'before' => '<div class="pull-left">{summary}</div>',
//                'after' => FALSE,
//                'footer' => FALSE,
            ],
            'pager' => array(
                'firstPageLabel' => '<i class="icon md-skip-previous"></i>',
                'lastPageLabel' => '<i class="icon md-skip-next"></i>',
                'prevPageLabel' => '<i class="icon md-fast-rewind"></i>',
                'nextPageLabel' => '<i class="icon md-fast-forward"></i>',
            ),
        ])
        ?>
    </div>
</div>
<?php
$elOffice = Html::getInputId($searchModel, 'office_id');
$elMonth = Html::getInputId($searchModel, 'month');
$js = <<<js
    $(document).on('pjax:complete', '#crud-datatable-monthly-form-submit-pjax', function() {
        var elOffice = $("#{$elOffice}"), // your input id for the HTML select input
            settings = elOffice.attr('data-krajee-select2');
        settings = window[settings];
        // reinitialize plugin
        elOffice.select2(settings);
        $('.loading-{$elOffice}').hide();
        
        var elMonth = $("#{$elMonth}"), // your input id for the HTML select input
            settings = elMonth.attr('data-krajee-select2');
        settings = window[settings];
        // reinitialize plugin
        elMonth.select2(settings);
        $('.loading-{$elMonth}').hide();
    });
js;
$this->registerJs($js);
