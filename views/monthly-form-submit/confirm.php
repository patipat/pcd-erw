<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\MonthlyFormSubmit */
?>
<div class="monthly-form-submit-check">
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

    <?= $this->renderFile('@app/views/monthly-form-submit/view.php', [
        'model' => $model,
        'redirect' => Url::to(['monthly-form-submit/confirm', 'id' => $model->id, 'file' => 1], true),
        'file' => isset($file) ? $file : null,
        'from' => 'confirm',
    ]); ?>

    <div class="monthly-form-submit-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="hidden">
            <?= $form->field($model, 'source_type_id')->hiddenInput(); ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>


</div>