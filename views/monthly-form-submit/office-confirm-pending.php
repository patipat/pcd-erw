<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Office */
?>
<div class="office-view">
    <?php
    $i = 1;
    $models = $dataProvider->models;
    foreach ($models as $un):
        echo  '<p class="padding-left-50">'. $i++ .'. ' . $un->office->name.'</p>';
    endforeach
    
    ?>
</div>
