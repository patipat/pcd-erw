<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MonthlyFormSubmitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ประวัติรายงานรายเดือน');
$this->params['breadcrumbs'][] = $this->title;

?>
<?=
 $this->renderFile('@app/views/monthly-form-submit/_record-list.php', [
     'dataProvider' => $dataProvider,
 ]);
?>