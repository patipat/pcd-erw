<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MonthlyFormSubmit */
?>
<div class="monthly-form-submit-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
