<?php

use app\models\MonthlyFile;
use app\models\Office;
use kartik\detail\DetailView;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $model app\models\MonthlyFormSubmit */
$url = Url::to(['monthly-file/create', 'officeId' => $model->office_id, 'year' => $model->year, 'month' => $model->month, 'monthlySubmitId' => $model->id, 'from' => $from]);
$addButton =<<<b
    <div>
        <a href="{$url}" class="btn btn-success" role="modal-remote">เพิ่มไฟล์</a>
    </div>
b;
?>
<div class="monthly-form-submit-view">
<?php
echo Tabs::widget([
    'items' => [
        [
            'label' => 'ข้อมูลปัจจุบัน',
            'content' => $this->renderFile('@app/views/monthly-form-submit/_current.php', [
                'model' => $model,
            ]),
            'active' => !isset($file) ? true : false
        ],
        [
            'label' => 'ข้อมูลเปรียบเทียบ',
            'content' => $this->renderFile('@app/views/monthly-form-submit/_compare.php', [
                'model' => $model,
            ]),
        ],
        [
            'label' => 'ไฟล์',
            'content' => $this->renderFile('@app/views/monthly-file/view.php', [
                'files' => $model->monthlyFiles,
                'addButton' => $addButton,
            ]),
            'active' => isset($file) ? true : false
        ],
    ],
]);
?>
    
</div>
