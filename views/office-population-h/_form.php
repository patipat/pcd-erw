<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\OfficePopulationH */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="office-population-h-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

    <?=
    $form->field($model, 'pop_int')->widget(MaskedInput::class, [
        'options' => [
            'class' => 'form-control',
        ],
        'clientOptions' => [
            'alias' => 'numeric',
            'prefix' => '',
            'groupSeparator' => ',',
            'allowMinus' => false,
            'autoGroup' => true,
            'autoUnmask' => true,
            'unmaskAsNumber' => true,
        ]
    ])
    ?>

    <?=
    $form->field($model, 'pop_ext')->widget(MaskedInput::class, [
        'options' => [
            'class' => 'form-control',
        ],
        'clientOptions' => [
            'alias' => 'numeric',
            'prefix' => '',
            'groupSeparator' => ',',
            'allowMinus' => false,
            'autoGroup' => true,
            'autoUnmask' => true,
            'unmaskAsNumber' => true,
        ]
    ])
    ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
