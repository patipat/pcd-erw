<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OfficePopulationH */
?>
<div class="office-population-h-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'office_id',
            'pop_int',
            'pop_ext',
            'total',
            'deleted',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
