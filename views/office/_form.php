<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;

$currentRole = Yii::$app->session->get('currentRole');

/* @var $this yii\web\View */
/* @var $model app\models\Office */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="office-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

    <?php if ($currentRole['role_id'] == app\models\Role::SUPER_ADMINISTRATOR){ ?>
    <?php
    // Usage with ActiveForm and model
    $data = ArrayHelper::map(\app\models\OfficeType::find()->orderBy('CONVERT(office_type.name USING TIS620) ASC')->all(), 'id', 'name');
//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($model, 'office_type_id')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
        <?php
    $data = [];    
    if (!empty($model->office_type_id)) {
        $data = ArrayHelper::map(\app\models\Office::find()->isDeleted(FALSE)->officeType($model->office_type_id)->orderBy('CONVERT(office.name USING TIS620)')->all(), 'id', 'name');
 // \yii\helpers\VarDumper::dump($data, 10, TRUE);    
        
    }
    echo $form->field($model, 'parent_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $data,
        'options' => [
            'class'=>Yii::$app->util->getFormControlClass($model->parent_id),
            'placeholder' => ''
        ],
        'select2Options' => [
            'pluginOptions' => ['allowClear' => true]
        ],
        'pluginOptions' => [
            'depends' => [Html::getInputId($model, 'office_type_id')],
            'url' => Url::to(['/office/list']),
            'placeholder' => '',
        ]
    ]);
    ?>
    <?php } ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>
    <?php
    // Usage with ActiveForm and model
    $data = ArrayHelper::map(app\models\Province::find()->isDeleted(FALSE)->orderBy('CONVERT(province.name USING TIS620) ASC')->all(), 'id', 'name');
//   \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($model, 'province_id')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?php
    $data = [];
    if (!empty($model->province_id)) {
        $data = ArrayHelper::map(app\models\Amphur::find()->isDeleted(FALSE)->province($model->province_id)->orderBy('CONVERT(amphur.name USING TIS620)')->all(), 'id', 'name');
        // \yii\helpers\VarDumper::dump($data, 10, TRUE);
    }
    echo $form->field($model, 'amphur_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $data,
        'options' => [
            'class' => Yii::$app->util->getFormControlClass($model->amphur_id)
        ],
        'select2Options' => [
            'pluginOptions' => ['allowClear' => true]
        ],
        'pluginOptions' => [
            'depends' => [Html::getInputId($model, 'province_id')],
            'url' => Url::to(['/amphur/list']),
            'placeholder' => '',
        ]
    ]);
    ?>
    <?php
    $data = [];
    if (!empty($model->amphur_id)) {
        $data = ArrayHelper::map(app\models\Tambon::find()->isDeleted(FALSE)->amphur($model->amphur_id)->orderBy('CONVERT(tambon.name USING TIS620)')->all(), 'id', 'name');
        // \yii\helpers\VarDumper::dump($data, 10, TRUE);
    }
    echo $form->field($model, 'tambon_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $data,
        'options' => [
            'class' => Yii::$app->util->getFormControlClass($model->tambon_id)
        ],
        'select2Options' => [
            'pluginOptions' => ['allowClear' => true]
        ],
        'pluginOptions' => [
            'depends' => [Html::getInputId($model, 'amphur_id')],
            'url' => Url::to(['/tambon/lists']),
            'placeholder' => '',
        ]
    ]);
    ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tel')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'line_token')->textInput(['maxlength' => true]) ?>


        <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

<?php ActiveForm::end(); ?>

</div>
