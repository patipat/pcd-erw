<?php

use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Office */
/* @var $form yii\widgets\ActiveForm */

$currentRole = \Yii::$app->session->get('currentRole');
?>

<div class="office-search  margin-bottom-10">

    <?php
    $form = ActiveForm::begin([
        'action' => Url::to(['office/monthly-assessment-report', 'year' => $searchModel->year, 'officeType' => $searchModel->office_type_id, 'level' => $searchModel->level]),
        'method' => 'get',
        'options' => [
            'data-pjax' => 1,
        ],
        'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>

    <?php
    echo $form->field($searchModel, 'year', [
        'options' => [
            'style' => 'width: 150px',
            'class' => 'form-group',
        ]
    ])->widget(Select2::classname(), [
        'data' => Yii::$app->util->getYears(),
        // 'data' => [],
        'options' => ['placeholder' => 'ปี'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?php
    $data = [];
    if (!empty($searchModel->year)) {
        $months = Yii::$app->util->getYearMonths($searchModel->year);
        $data = ArrayHelper::map($months, 'code', 'label');
    }
    echo $form->field($searchModel, 'startMonth', [
        'options' => [
            'style' => 'width: 150px',
            'class' => 'form-group',
        ]
    ])->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $data,
        'options' => [
            'placeholder' => 'จากเดือน'
        ],
        'select2Options' => [
            'pluginOptions' => ['allowClear' => true]
        ],
        'pluginOptions' => [
            'depends' => [Html::getInputId($searchModel, 'year')],
            'url' => Url::to(['/setting/get-months']),
            'placeholder' => '',
        ]
    ]);
    ?>
    <?php
    echo $form->field($searchModel, 'endMonth', [
        'options' => [
            'style' => 'width: 150px',
            'class' => 'form-group',
        ]
    ])->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $data,
        'options' => [
            'placeholder' => 'ถึงเดือน'
        ],
        'select2Options' => [
            'pluginOptions' => ['allowClear' => true]
        ],
        'pluginOptions' => [
            'depends' => [Html::getInputId($searchModel, 'year')],
            'url' => Url::to(['/setting/get-months']),
            'placeholder' => '',
        ]
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>