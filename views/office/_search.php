<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\Office */
/* @var $form yii\widgets\ActiveForm */
$currentRole = \Yii::$app->session->get('currentRole');
?>

<div class="office-search  margin-bottom-10">

    <?php
    $form = ActiveForm::begin([
                'action' => Url::to(['office/index']),
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1,
                ],
                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>

    <?= $form->field($model, 'name') ?>
    <?php
    // Usage with ActiveForm and model
    $data = ArrayHelper::map(\app\models\OfficeType::find()->orderBy('CONVERT(office_type.name USING TIS620) ASC')->all(), 'id', 'name');
//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($model, 'office_type_id')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '250px'
        ],
    ]);
    ?>
    <?php
    $data = [];
    if (!empty($model->office_type_id)) {
        $data = ArrayHelper::map(\app\models\Office::find()->isDeleted(FALSE)->officeType($model->office_type_id)->orderBy('CONVERT(office.name USING TIS620)')->all(), 'id', 'typeName');
        // \yii\helpers\VarDumper::dump($data, 10, TRUE);    
    }
    echo $form->field($model, 'parent_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $data,
        'options' => [
            'class' => Yii::$app->util->getFormControlClass($model->parent_id),
        ],
        'select2Options' => [
            'pluginOptions' => ['allowClear' => true,'width'=>'300px'
],
        ],
        'pluginOptions' => [
            'depends' => [Html::getInputId($model, 'office_type_id')],
            'url' => Url::to(['/office/list']),
            'placeholder' => 'เลือกข้อมูล',
            'width' => '300px'
        ]
    ]);
    ?>   
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
