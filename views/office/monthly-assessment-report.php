<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\YearlyAssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

app\assets\FreezeTableSiteAsset::register($this);
app\assets\TableExportAsset::register($this);

$this->title = Yii::t('app', 'คะแนนประเมินรอบ 12 เดือน');
$this->params['breadcrumbs'][] = $this->title;
$dataProvider->pagination = false;
$models = $dataProvider->models;
$modelParents = ArrayHelper::index($models, null, 'parent.name');
$yas = [];
?>
<div class="pull-right">

    <?=
    Html::button('Excel', [
        'class' => 'btn btn-success',
        'onclick' => new JsExpression("_exportInstance.export2file(_exportData.data, _exportData.mimeType, 'รายงานประเมิน_12_เดือน', _exportData.fileExtension, _exportData.merges, _exportData.RTL, 'ข้อมูล');")
    ])
    ?>
</div>
<div class="panel">
    <?=
    $this->renderFile('@app/views/office/_monthly-assessment-report-search.php', [
        'searchModel' => $searchModel,
    ]);
    ?>
    <div class="panel-content" style="">

        <div class="freeze-table">
            <table id="yearly-assessment-grid" class="table table-bordered table-condensed table-striped" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th class="text-nowrap text-center bg-white" rowspan="2">ตัวชี้วัด <br><?= !empty($searchModel->month) ? Yii::$app->formatter->asDate((new \DateTime("{$searchModel->year}-{$searchModel->month}-01"))->format('Y-m-d H:i:s'), 'php:F') . ' /' : "" ?>  <?= $searchModel->year ?></th>
                        <?php foreach ($modelParents as $parentName => $models): ?>
                            <th class="text-center" colspan="<?= count($models) ?>"><?= $parentName ?></th>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <?php foreach ($modelParents as $parentName => $models): ?>
                            <?php
                            foreach ($models as $model):
                                if (!empty($searchModel->year) && !empty($searchModel->startMonth) && !empty($searchModel->endMonth)) {
                                    $yas[] = $model->calculateMonthlyAssessment($searchModel->year, $searchModel->startMonth, $searchModel->endMonth);
                                }
                                // if (!empty($searchModel->month)) {
                                //     $yas[] = $model->getMonthlyAssessment($searchModel->year, $searchModel->month);
                                // } else {
                                //     $yas[] = $model->getYearlyAssessment($searchModel->year);
                                // }
                                ?>
                                <th class="text-center bg-white"><?= $model->name ?></th>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', 'จำนวนบุคลากรในหน่วยงาน (คน)');
                            } else {
                                echo Yii::t('app', 'จำนวนบุคลากรในหน่วยงาน (คน)');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <?php if ($searchModel->year >= '2563') { ?>
                                <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->pop_int + $ya->pop_ext, 0) : "&nbsp;" ?></td>
                            <?php } else { ?>
                                <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->pop_int, 0) : "&nbsp;" ?></td>
                            <?php } ?>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', 'จำนวนวันทำงาน (วัน)');
                            } else {
                                echo Yii::t('app', 'จำนวนวันทำงานจนถึงวันรายงาน (วัน)');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->work_day, 0) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', 'น้ำหนักขยะมูลฝอยที่ส่งกำจัด (กิโลกรัม)');
                            } else {
                                echo Yii::t('app', 'ปริมาณขยะมูลฝอยที่เกิดขึ้นจนถึงวันรายงาน (กิโลกรัม)');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->garbage) ? Yii::$app->formatter->asDecimal($ya->garbage, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', 'จำนวนถุงพลาสติกหูหิ้วที่ทิ้งในถังขยะ (ใบ)');
                            } else {
                                echo Yii::t('app', 'จำนวนถุงพลาสติกหูหิ้วจากการสำรวจจนถึงวันรายงาน (ใบ)');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->bag) ? Yii::$app->formatter->asDecimal($ya->bag, 0) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', 'จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่ทิ้งในถังขยะ (ใบ)');
                            } else {
                                echo Yii::t('app', 'จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งจากการสำรวจจนถึงวันรายงาน (ใบ)');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->cup) ? Yii::$app->formatter->asDecimal($ya->cup, 0) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', 'จำนวนโฟมบรรจุอาหารที่ทิ้งในถังขยะ (ใบ)');
                            } else {
                                echo Yii::t('app', 'จำนวนโฟมบรรจุอาหารจากการสำรวจจนถึงวันรายงาน (ใบ)');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->foam) ? Yii::$app->formatter->asDecimal($ya->foam, 0) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', 'จำนวนหน้ากากอนามัยที่ทิ้งในถังขยะ (ชิ้น)');
                            } else {
                                echo Yii::t('app', 'จำนวนหน้ากากอนามัยจากการสำรวจจนถึงวันรายงาน (ชิ้น)');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->mask) ? Yii::$app->formatter->asDecimal($ya->mask, 0) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* น้ำหนักขยะมูลฝอยที่เกิดขึ้น ปี 2561 (กิโลกรัม)');
                            } else {
                                echo Yii::t('app', '* ปริมาณขยะมูลฝอยที่เกิดขึ้น ปี 2561 (กิโลกรัม)');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->garbage_prev) ? Yii::$app->formatter->asDecimal($ya->garbage_prev, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* น้ำหนักขยะมูลฝอยที่ชั่งได้ ปี {year} (กิโลกรัม)', ['year' => $searchModel->year]);
                            } else {
                                echo Yii::t('app', '* ปริมาณขยะมูลฝอยที่เกิดขึ้น ปี {year} (กิโลกรัม)', ['year' => $searchModel->year]);
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->garbage_cur) ? Yii::$app->formatter->asDecimal($ya->garbage_cur, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* จำนวนถุงพลาสติกหูหิ้วที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $searchModel->year]);
                            } else {
                                echo Yii::t('app', '* จำนวนถุงพลาสติกหูหิ้วที่เกิดขึ้น ปี 2561 (ใบ)');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->bag_prev) ? Yii::$app->formatter->asDecimal($ya->bag_prev, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* จำนวนถุงพลาสติกหูหิ้วที่ทิ้งในถังขยะ {year} (ใบ)', ['year' => $searchModel->year]);
                            } else {
                                echo Yii::t('app', '* จำนวนถุงพลาสติกหูหิ้วที่เกิดขึ้น ปี {year} (ใบ)', ['year' => $searchModel->year]);
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->bag_cur) ? Yii::$app->formatter->asDecimal($ya->bag_cur, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $searchModel->year]);
                            } else {
                                echo Yii::t('app', '* จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $searchModel->year]);
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->cup_prev) ? Yii::$app->formatter->asDecimal($ya->cup_prev, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่ทิ้งในถังขยะ ปี {year} (ใบ)', ['year' => $searchModel->year]);
                            } else {
                                echo Yii::t('app', '* จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่เกิดขึ้น ปี {year} (ใบ)', ['year' => $searchModel->year]);
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->cup_cur) ? Yii::$app->formatter->asDecimal($ya->cup_cur, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* จำนวนโฟมบรรจุอาหารที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $searchModel->year]);
                            } else {
                                echo Yii::t('app', '* จำนวนโฟมบรรจุอาหารที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $searchModel->year]);
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->foam_prev) ? Yii::$app->formatter->asDecimal($ya->foam_prev, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* จำนวนโฟมบรรจุอาหารที่ทิ้งในถังขยะ ปี {year} (ใบ)', ['year' => $searchModel->year]);
                            } else {
                                echo Yii::t('app', '* จำนวนโฟมบรรจุอาหารที่เกิดขึ้น ปี {year} (ใบ)', ['year' => $searchModel->year]);
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->foam_cur) ? Yii::$app->formatter->asDecimal($ya->foam_cur, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>

                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* ร้อยละปริมาณขยะมูลฝอยที่ส่งกำจัดที่ลดลง');
                            } else {
                                echo Yii::t('app', '* ร้อยละปริมาณขยะมูลฝอยที่ลดลง');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->garbage_percent) ? Yii::$app->formatter->asDecimal($ya->garbage_percent, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* ร้อยละจำนวนถุงพลาสติกหูหิ้วที่ลดลง');
                            } else {
                                echo Yii::t('app', '* ร้อยละจำนวนถุงพลาสติกหูหิ้วที่ลดลง');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->bag_percent) ? Yii::$app->formatter->asDecimal($ya->bag_percent, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* ร้อยละจำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่ลดลง');
                            } else {
                                echo Yii::t('app', '* ร้อยละจำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่ลดลง');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->cup_percent) ? Yii::$app->formatter->asDecimal($ya->cup_percent, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?php
                            if ($searchModel->year >= '2563') {
                                echo Yii::t('app', '* ร้อยละจำนวนโฟมบรรจุอาหารที่ลดลง');
                            } else {
                                echo Yii::t('app', '* ร้อยละจำนวนโฟมบรรจุอาหารที่ลดลง');
                            }
                            ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->foam_percent) ? Yii::$app->formatter->asDecimal($ya->foam_percent, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?= Yii::t('app', '* คะแนนขยะ') ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->garbage_score) ? Yii::$app->formatter->asDecimal($ya->garbage_score, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?= Yii::t('app', '* คะแนนถุง') ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->bag_score) ? Yii::$app->formatter->asDecimal($ya->bag_score, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?= Yii::t('app', '* คะแนนแก้ว') ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->cup_score) ? Yii::$app->formatter->asDecimal($ya->cup_score, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?= Yii::t('app', '* คะแนนโฟม') ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->foam_score) ? Yii::$app->formatter->asDecimal($ya->foam_score, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?= Yii::t('app', '* คะแนนเพิ่ม') ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->plus_score) ? Yii::$app->formatter->asDecimal($ya->plus_score, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?= Yii::t('app', '* คะแนนลด') ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->minus_score) ? Yii::$app->formatter->asDecimal($ya->minus_score, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td class="text-nowrap"><?= Yii::t('app', '* คะแนนรวม') ?></td>
                        <?php foreach ($yas as $ya): ?>
                            <td class="text-right"><?= isset($ya->total_score) ? Yii::$app->formatter->asDecimal($ya->total_score, 2) : "&nbsp;" ?></td>
                        <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    let _exportInstance = null;
    let _exportData = null;
</script>
<?php
$js = <<<js
    $('.freeze-table').freezeTable({});
    var ExportButtons = document.getElementById('yearly-assessment-grid');
//    console.log(ExportButtons);
    _exportInstance = new TableExport(ExportButtons, {
        formats: ['xlsx'],
        exportButtons: false
    });
//        console.log(_exportInstance.getExportData());
    //                                        // "id" of selector    // format
    _exportData = _exportInstance.getExportData()['yearly-assessment-grid']['xlsx'];
//    console.log(_exportData);
    for (var i = 0; i < _exportData.data.length; i++) {
        if (_exportData.data[i][0]) {
            _exportData.data[i][0].t = 's';
        }
        for (var j = 0; j < _exportData.data[i].length; j++) {
            if (_exportData.data[i][j]) {
                _exportData.data[i][j].v = _exportData.data[i][j].v.replaceAll(',', '');
                if (i > 1 && j > 0) {
                    _exportData.data[i][j].t = 'n';
                }
                
            }
        }
    }
//        console.log(_exportData);
js;
$this->registerJs($js);
