<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Office */
?>
<div class="office-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
