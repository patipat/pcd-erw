<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Office */
?>
<div class="office-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'address:ntext',
            'email:email',
            'tel',
            'line_token',
            'parent_id',
            'office_type_id',
            'deleted',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
