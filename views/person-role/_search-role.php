<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$currentRole = \Yii::$app->session->get('currentRole');

/* @var $this yii\web\View */
/* @var $model app\models\PersonRole */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-role-search margin-bottom-10">

    <?php
    $form = ActiveForm::begin([
//        'action' => Url::to(['person-role/index']),
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1,
                ],
                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>

    <?= $form->field($model, 'name') ?>

    <?php
    // Usage with ActiveForm and model
    if ($currentRole['role_id'] == \app\models\Role::SUPER_ADMINISTRATOR) {
        $data = ArrayHelper::map(\app\models\Office::find()->isDeleted(false)->orderBy('CONVERT(office.name USING TIS620) ASC')->all(), 'id', 'typeName');
    } else {
        $data = ArrayHelper::map(\app\models\Office::find()->isDeleted(false)->andWhere(['or', ['=', 'office.id', Yii::$app->user->identity->person->office_id], ['=', 'office.parent_id', Yii::$app->user->identity->person->office_id]])->orderBy('CONVERT(office.name USING TIS620) ASC')->all(), 'id', 'typeName');
    }
//    $data = ArrayHelper::map(\app\models\Office::find()->orderBy('CONVERT(office.name USING TIS620) ASC')->all(), 'id', 'name');
//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($model, 'officeId')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => 'เลือกหน่วยงาน'],
        'pluginOptions' => [
            'allowClear' => false,
            'width' => '200px'
        ],
    ]);
    ?>

    <div class="form-group">
<?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
