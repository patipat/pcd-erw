<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PersonRole */
?>
<div class="person-role-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'role_id',
            'person_id',
            'deleted',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
