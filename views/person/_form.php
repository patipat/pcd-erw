<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$currentRole = \Yii::$app->session->get('currentRole');

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>
    <div class="row">
        <div class="col-md-3">
            <?php
            // Usage with ActiveForm and model
            $data = ArrayHelper::map(\app\models\Title::find()->orderBy('CONVERT(title.name USING TIS620) ASC')->all(), 'id', 'name');
//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
            echo $form->field($model, 'title_id')->widget(Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => ''],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-md-4"><?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-5"><?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?></div>
    </div>
    <?php
    if ($currentRole['role_id'] == \app\models\Role::SUPER_ADMINISTRATOR) {
        $data = ArrayHelper::map(\app\models\Office::find()->isDeleted(false)->orderBy('CONVERT(office.name USING TIS620) ASC')->all(), 'id', 'typeName');
    } else {
        $data = ArrayHelper::map(\app\models\Office::find()->isDeleted(false)->andWhere(['or', ['=', 'office.id', Yii::$app->user->identity->person->office_id], ['=', 'office.parent_id', Yii::$app->user->identity->person->office_id]])->orderBy('CONVERT(office.name USING TIS620) ASC')->all(), 'id', 'typeName');
    }
    echo $form->field($model, 'office_id')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?> 
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6"><?= $form->field($model, 'tel')->textInput(['maxlength' => true]) ?></div>
    </div>
    <?php
    if ((!$model->id) || ($model->office->level != 2)) {
        $dis = false;
    } else {
        $dis = true;
    }
    ?>
    <div class="row" >
        <div class="col-lg-4">
            <?= $form->field($regForm, 'username')->textInput(['maxlength' => true, 'readonly' => $dis]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($regForm, 'password')->passwordInput(['maxlength' => true, 'readonly' => $dis]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($regForm, 'password_repeat')->passwordInput(['maxlength' => true, 'readonly' => $dis]) ?>  
        </div>
    </div>

    <?php if ($currentRole['role_id'] == app\models\Role::SUPER_ADMINISTRATOR): ?>
        <?= $form->field($model, 'line_notify_token')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>



    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
