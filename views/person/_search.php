<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="person-search margin-bottom-10">

    <?php
    $form = ActiveForm::begin([
                'action' => Url::to(['person/index']),
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1,
                ],
                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>
    
    <?= $form->field($model, 'first_name') ?>
    <?php
    // Usage with ActiveForm and model
    $data = ArrayHelper::map(\app\models\OfficeType::find()->orderBy('CONVERT(office_type.name USING TIS620) ASC')->all(), 'id', 'name');
//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($model, 'officeTypeId')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '250px'
        ],
    ]);
    ?>
    <?php
    $data = [];
    if (!empty($model->officeTypeId)) {
        $data = ArrayHelper::map(\app\models\Office::find()->isDeleted(FALSE)->officeType($model->officeTypeId)->orderBy('CONVERT(office.name USING TIS620)')->all(), 'id', 'typeName');
        // \yii\helpers\VarDumper::dump($data, 10, TRUE);    
    }
    echo $form->field($model, 'office_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $data,
        'options' => [
            'class' => Yii::$app->util->getFormControlClass($model->office_id),
        ],
        'select2Options' => [
            'pluginOptions' => ['allowClear' => true,'width'=>'300px'
],
        ],
        'pluginOptions' => [
            'depends' => [Html::getInputId($model, 'officeTypeId')],
            'url' => Url::to(['/office/list']),
            'placeholder' => 'เลือกข้อมูล',
            'width' => '300px'
        ]
    ]);
    ?> 



    <div class="form-group">
<?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
