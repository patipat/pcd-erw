<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
ini_set('max_execution_time', 300);

/* @var $this yii\web\View */
/* @var $model app\models\Person */
?>
<div class="person-view">
    <?php $form = ActiveForm::begin() ?>
    <?= Html::submitButton('<i class="glyphicon glyphicon-arrow-up"></i> นำเข้าข้อมูล', ['class' => 'btn btn-warning btn-lg']) ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'width' => '20px',
            ],
            [
                'value' => function($model) {
                    return $model[0][0];
                }
            ],
            [
                'value' => function($model) {
                    return $model[0][1];
                }
            ],
            [
                'value' => function($model) {
                    return $model[0][2];
                }
            ],
            [
                'value' => function($model) {
                    return $model[0][3];
                }
            ],
            [
                'value' => function($model) {
                    return $model[0][4];
                }
            ],
            [
                'value' => function($model) {
                    return $model[0][5];
                }
            ],
        ]
    ])
    ?>
    <?php ActiveForm::end() ?>
</div>
