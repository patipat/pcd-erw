<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$currentRole = \Yii::$app->session->get('currentRole');
$this->title = Yii::t('app', 'ผู้ใช้งาน');
if ($currentRole['role_id'] == \app\models\Role::SUPER_ADMINISTRATOR) {
$this->params['breadcrumbs'][] = ['label' => 'จัดการค่าตั้งต้น', 'url' => ['site/master-list']];
}else{
$this->params['breadcrumbs'][] = ['label' => 'จัดการผู้ใช้งาน', 'url' => ['site/person']];
}
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="person-index">
    <div class="ajaxCrudDatatable">
                <?= $this->render('_search-admin', ['model' => $searchModel]) ?>

        <?=GridView::widget([
            'id'=>'crud-datatable-person',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'floatHeader' => true,
            'floatHeaderOptions' => ['top' => 66],
            'pjax'=>true,
            'pjaxSettings' => [
//                'beforeGrid' => $this->render('_search', ['model'=>$searchModel]),
            ],
            'columns' => require(__DIR__.'/_columns-admin.php'),
            'toolbar' => [
                [
                    'options' => [
                        'class' => '',
                    ],
                    'content' =>
                    Html::a('<i class="glyphicon glyphicon-plus"></i> ' .Yii::t('app', 'เพิ่ม') . Yii::t('app', 'เจ้าหน้าที่'), ['create'], ['role' => 'modal-remote', 'class' => 'btn btn-success btn-raised']) .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i> ' .Yii::t('app', 'โหลดใหม่'), Url::current(), ['data-pjax' => 1, 'class' => 'btn btn-default btn-raised'])
                ],
            ],    
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary',
                'heading' => FALSE,
                'before' => '<div class="pull-left">{summary}</div>',
//                'after' => FALSE,
//                'footer' => FALSE,
            ],
            'pager' => array(
                'firstPageLabel' => '<i class="icon md-skip-previous"></i>',
                'lastPageLabel' => '<i class="icon md-skip-next"></i>',
                'prevPageLabel' => '<i class="icon md-fast-rewind"></i>',
                'nextPageLabel' => '<i class="icon md-fast-forward"></i>',
            ),
        ])?>
    </div>
</div>
