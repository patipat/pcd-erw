<?php

use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'fileCategory.name',
    ],
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($dataProvider) {
            return Html::a($dataProvider->name, ['web/uploads/files/' . $dataProvider->file_category_id . '/' . $dataProvider->file], [
                        'target' => '_blank',
                        'data-pjax' => 0,
            ]);
        }
    ],

];
