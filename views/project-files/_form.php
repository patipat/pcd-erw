<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectFiles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-files-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

    <?php
    $data = ArrayHelper::map(\app\models\FileCategory::find()->isDeleted(FALSE)->orderBy('CONVERT(file_category.name USING TIS620) ASC')->all(), 'id', 'name');
    echo $form->field($model, 'file_category_id')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'file[]')->widget(FileInput::classname(), [
        'options' => [
            'accept' => '*',
        ],
        'pluginOptions' => [
            'showUpload' => false,
            'browseLabel' => '',
            'removeLabel' => '',
            'initialPreview' => [
                $model->fileUrl
            ],
            'initialPreviewAsData' => true,
            'initialCaption' => $model->file,
            'initialPreviewConfig' => [
                ['caption' => $model->file]
            ],
        ],
    ]);
    ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
