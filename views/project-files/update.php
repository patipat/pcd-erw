<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectFiles */
?>
<div class="project-files-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
