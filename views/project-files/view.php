<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectFiles */
?>
<div class="project-files-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fileCategory.name',
            'name',
            'description:ntext',
            'file',

        ],
    ]) ?>

</div>
