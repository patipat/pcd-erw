<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>
<div class="vehicle-type-search margin-bottom-10">
    <?php
    $form = ActiveForm::begin([
                'method' => 'get',
                'action' => Url::to(['province/index']),
                'options' => [
                    'data-pjax' => 1,
//                            'target' => '#crud-datatable-ticket-h-pjax'
//                    'class' => 'form-inline'
                ],
                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>
    <?= $form->field($searchModel, 'name')->textInput([]); ?>



    <div class="form-group">
<?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
</div>