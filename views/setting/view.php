<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Setting */
?>
<div class="setting-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'key',
            'name',
            'value',
            'deleted',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
