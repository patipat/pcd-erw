<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Setting;

app\assets\PieProgressAsset::register($this);
$total = Yii::$app->user->identity->person->getResponsibleOfficeCount($model->officeType, $model->level);
$assessed = Yii::$app->user->identity->person->getAssessedOfficeCount($year, $model->officeType, $model->level);
$percent = empty($total) ? 0 : ($assessed / $total) * 100;
?>
<a href="<?= Url::to(['yearly-assessment/index', 'YearlyAssessmentSearch[year]' => $year
        , 'YearlyAssessmentSearch[officeType]' => $model->officeType
        , 'YearlyAssessmentSearch[level]' => $model->level]) ?>" style="text-decoration: none;">
<div class="widget widget-shadow widget-completed-options">
    <div class="widget-content padding-5">
        <div class="text-center">ประเมินรอบ 12 เดือน</div>
        <div class="row">
            <div class="col-xs-8">
                <div class="counter text-left blue-grey-700">
                    <div class="counter-label margin-top-0 text-center">
                        ประเมินแล้ว / ทั้งหมด
                    </div>
                    <div class="counter-number font-size-24 margin-top-0 text-center">
                        <?= Yii::$app->formatter->asDecimal($assessed) ?> / <?= Yii::$app->formatter->asDecimal($total) ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div style="max-width: 60px; margin: 0 auto;">
                <div class="pie-progress pie-progress-sm" data-plugin="pieProgress" data-valuemax="100"
                     data-barcolor="#57c7d4" data-size="100" data-barsize="10"
                     data-goal="<?= $percent ?>" aria-valuenow="<?= $percent ?>" role="progressbar">
                    <span class="pie-progress-number blue-grey-700 font-size-16">
                        <?= Yii::$app->formatter->asDecimal($percent, 2) ?>%
                    </span>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</a>