<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$ya = Yii::$app->user->identity->person->getYearlyAssessment($year, $dashboardSearch->officeType, $dashboardSearch->level);
?>

<div class="row">
    <div class="col-md-3 col-sm-6">
        <a href="<?= Url::to(['site/yearly-assessment', 'DashboardForm[officeType]' => $dashboardSearch->officeType, 'DashboardForm[level]' => $dashboardSearch->level,'year'=>$year]) ?>" style="text-decoration: none;" role="modal-remote">
        <div class="widget widget-shadow">
            <div class="widget-content padding-10 bg-white">
                <div class="counter counter-lg">
                    <div class="counter-label text-uppercase">ร้อยละลดลงขยะมูลฝอย</div>
                    <div class="counter-number-group" style="display: flex; justify-content: center; align-items: center;">
                        <?= Html::img('@web/images/garbage.png', ['class' => 'margin-right-10', 'style' => 'height: 40px']) ?>
                        <?php if (isset($ya)): ?>
                            <span class="counter-icon margin-right-10 <?= $ya->garbage_percent >= 0 ? "green-600" : "red-600" ?>">
                                <i class="md-<?= $ya->garbage_percent >= 0 ? 'chevron-down' : 'chevron-up' ?>"></i>
                            </span>
                        <?php else: ?>
                            <span class="counter-icon margin-right-10">
                                <i class="md-minus"></i>
                            </span>
                        <?php endif; ?>
                        <span class="counter-number font-size-30 <?= isset($ya) && $ya->garbage_percent >= 0 ? "green-600" : "red-600" ?>"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->garbage_percent, 2) : "0" ?>%</span>
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6">
        <a href="<?= Url::to(['site/yearly-assessment', 'DashboardForm[officeType]' => $dashboardSearch->officeType, 'DashboardForm[level]' => $dashboardSearch->level,'year'=>$year]) ?>" style="text-decoration: none;" role="modal-remote">
        <div class="widget widget-shadow">
            <div class="widget-content padding-10 bg-white">
                <div class="counter counter-lg">
                    <div class="counter-label text-uppercase">ร้อยละลดลงถุงพลาสติก</div>
                    <div class="counter-number-group" style="display: flex; justify-content: center; align-items: center;">
                        <?= Html::img('@web/images/bag.jpg', ['class' => 'margin-right-10', 'style' => 'height: 40px']) ?>
                        <?php if (isset($ya)): ?>
                            <span class="counter-icon margin-right-10 <?= $ya->bag_percent >= 0 ? "green-600" : "red-600" ?>">
                                <i class="md-<?= $ya->bag_percent >= 0 ? 'chevron-down' : 'chevron-up' ?>"></i>
                            </span>
                        <?php else: ?>
                            <span class="counter-icon margin-right-10">
                                <i class="md-minus"></i>
                            </span>
                        <?php endif; ?>
                        <span class="counter-number font-size-30 <?= isset($ya) && $ya->bag_percent >= 0 ? "green-600" : "red-600" ?>"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->bag_percent, 2) : "0" ?>%</span>
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6">
        <a href="<?= Url::to(['site/yearly-assessment', 'DashboardForm[officeType]' => $dashboardSearch->officeType, 'DashboardForm[level]' => $dashboardSearch->level,'year'=>$year]) ?>" style="text-decoration: none;" role="modal-remote">
        <div class="widget widget-shadow">
            <div class="widget-content padding-10 bg-white">
                <div class="counter counter-lg">
                    <div class="counter-label text-uppercase">ร้อยละลดลงแก้วพลาสติก</div>
                    <div class="counter-number-group" style="display: flex; justify-content: center; align-items: center;">
                        <?= Html::img('@web/images/cup.png', ['class' => 'margin-right-10', 'style' => 'height: 40px']) ?>
                        <?php if (isset($ya)): ?>
                            <span class="counter-icon margin-right-10 <?= $ya->cup_percent >= 0 ? "green-600" : "red-600" ?>">
                                <i class="md-<?= $ya->cup_percent >= 0 ? 'chevron-down' : 'chevron-up' ?>"></i>
                            </span>
                        <?php else: ?>
                            <span class="counter-icon margin-right-10">
                                <i class="md-minus"></i>
                            </span>
                        <?php endif; ?>
                        <span class="counter-number font-size-30 <?= isset($ya) && $ya->cup_percent >= 0 ? "green-600" : "red-600" ?>"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->cup_percent, 2) : "0" ?>%</span>
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-md-3 col-sm-6">
        <a href="<?= Url::to(['site/yearly-assessment', 'DashboardForm[officeType]' => $dashboardSearch->officeType, 'DashboardForm[level]' => $dashboardSearch->level,'year'=>$year]) ?>" style="text-decoration: none;" role="modal-remote">
        <div class="widget widget-shadow">
            <div class="widget-content padding-10 bg-white">
                <div class="counter counter-lg">
                    <div class="counter-label text-uppercase">ร้อยละลดลงโฟม</div>
                    <div class="counter-number-group" style="display: flex; justify-content: center; align-items: center;">
                        <?= Html::img('@web/images/foam.png', ['class' => 'margin-right-10', 'style' => 'height: 40px']) ?>
                        <?php if (isset($ya)): ?>
                            <span class="counter-icon margin-right-10 <?= $ya->foam_percent >= 0 ? "green-600" : "red-600" ?>">
                                <i class="md-<?= $ya->foam_percent >= 0 ? 'chevron-down' : 'chevron-up' ?>"></i>
                            </span>
                        <?php else: ?>
                            <span class="counter-icon margin-right-10">
                                <i class="md-minus"></i>
                            </span>
                        <?php endif; ?>
                        <span class="counter-number font-size-30 <?= isset($ya) && $ya->foam_percent >= 0 ? "green-600" : "red-600" ?>"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->foam_percent, 2) : "0" ?>%</span>
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>

</div>
