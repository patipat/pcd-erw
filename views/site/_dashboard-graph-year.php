
<?php

use miloschuman\highcharts\Highcharts;
use yii\helpers\ArrayHelper;
use app\models\MonthlyFormSubmit;
use yii\helpers\Url;
use yii\web\JsExpression;

$garbage = [];
$bag = [];
$cup = [];
$foam = [];
$mask = [];
$avg = [];
$filterYears = [];

for ($i = 4; $i >= 0; $i--) {
    $filterYears[] = $dashboardSearch->year - $i;
}
$chartDatas = Yii::$app->user->identity->person->getCompareYearlyAssessment($filterYears, $dashboardSearch->officeType, $dashboardSearch->level);

//$years = ArrayHelper::getColumn($chartDatas, 'year');
$chartDatasByYear = ArrayHelper::index($chartDatas, null, 'year');
//\yii\helpers\VarDumper::dump($chartDatas, 10, true);
//exit;
foreach ($filterYears as $filterYear) {
//    $mfs = MonthlyFormSubmit::find()->isDeleted(FALSE)
//            ->office(Yii::$app->user->identity->person->office_id)->year($year)->month($yearMonth['month'])->isConfirmed()->last()->one();

    $garbagePercent = isset($chartDatasByYear[$filterYear]) ? floatval($chartDatasByYear[$filterYear][0]['garbage_percent']) : null;
    $garbage[] = $garbagePercent;

    $bagPercent = isset($chartDatasByYear[$filterYear]) ? floatval($chartDatasByYear[$filterYear][0]['bag_percent']) : null;
    $bag[] = $bagPercent;

    $cupPercent = isset($chartDatasByYear[$filterYear]) ? floatval($chartDatasByYear[$filterYear][0]['cup_percent']) : null;
    $cup[] = $cupPercent;

    $foamPercent = isset($chartDatasByYear[$filterYear]) ? floatval($chartDatasByYear[$filterYear][0]['foam_percent']) : null;
    $foam[] = $foamPercent;


    $avg[] = ($garbagePercent + $bagPercent + $cupPercent + $foamPercent ) / 5;
}
//yii\helpers\VarDumper::dump($avg);
$garImg = Url::to('@web/images/garbage.png');
$bagImg = Url::to('@web/images/bag.jpg');
$cupImg = Url::to('@web/images/cup.png');
$foamImg = Url::to('@web/images/foam.png');
$maskImg = Url::to('@web/images/mask.png');

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'credits' => [
            'enabled' => FALSE
        ],
        'lang' => [
            'decimalPoint' => '.',
            'thousandsSep' => ','
        ],
        'chart' => [
            'height' => 550,
            'type' => 'column',
            'style' => [
                'fontSize' => '13px',
                'fontFamily' => 'Prompt',
            ],
        ],
        'title' => [
            'text' => "อัตราส่วนร้อยละที่ลดลงของปริมาณขยะรวมทั้งหมดย้อนหลัง 5 ปี"
        ],
        'exporting' => [
            'allowHTML' => true
        ],
        'xAxis' => [
            'categories' => $filterYears,
            'title' => [
                'text' => 'ปี'
            ]
        ],
        'yAxis' => [
            'title' => [
                'text' => 'ร้อยละ'
            ]
        ],
//        'plotOptions' => [
//            'series' => [
//                'dataLabels' => [
//                    'useHTML' => true,
//                    'enabled' => true,
//                    //rotation: -90,
//                    'color' => '#FFFFFF',
//                    'align' => 'right',
//                    'x' => -10,
//                    'y' => 60,
//                    'formatter' => new \yii\web\JsExpression('function(){
//                            console.log(this);
//                            //return "<span>"+this.y+"</span></br><img src=\"http://www.badania-prenatalne.info.pl/images/test-papp-a.png\" />";
//                        }'),
//                    'style' => [
//                        'fontSize' => '13px',
//                        'fontFamily' => 'Verdana, sans-serif'
//                    ]
//                ]
//            ]
//        ],
        'legend' => [
            'useHTML' => true,
            'labelFormatter' => new \yii\web\JsExpression('function () {
                    if(this.name == "ขยะ"){
                        //return "<img width=\"35\" src=\"https://cdn0.iconfinder.com/data/icons/waste-recycling/64/496_waste-garbage-trash-organic-512.png\" />" + this.name;
                        return "<img width=\"35\" src=\"' . $garImg . '\" />" + this.name;
                    }if(this.name == "ถุงพลาสติก"){
                        //return "<img width=\"35\" src=\"https://icon-library.net/images/bag-icon-png/bag-icon-png-7.jpg\" />" + this.name;
                        return "<img width=\"40\" src=\"' . $bagImg . '\" />" + this.name;
                    }if(this.name == "แก้วพลาสติก"){
                        //return "<img width=\"25\" src=\"https://www.sccpre.cat/mypng/full/209-2094313_coffee-cup-comments-instagram-story-icons-drink.png\" /> " + this.name;
                        return "<img width=\"25\" src=\"' . $cupImg . '\" />" + this.name;
                    }if(this.name == "กล่องโฟม"){
                        //return "<img width=\"35\" src=\"https://www.regionalrecycling.ca/wp-content/themes/regionalrecycling/img/icons-recycle-plastic-foam-containers-trays.png\" />" + this.name;
                        return "<img width=\"35\" src=\"' . $foamImg . '\" />" + this.name;
                    }if(this.name == "กล่องโฟม"){
                        return "<img width=\"35\" src=\"' . $maskImg . '\" />" + this.name;
                    }
                }'),
        ],
        'series' => [
            [
                'name' => 'ขยะ',
//                'yAxis' => 0,
                'data' => $garbage,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 1,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://cdn0.iconfinder.com/data/icons/waste-recycling/64/496_waste-garbage-trash-organic-512.png\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $garImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
            [
                'name' => 'ถุงพลาสติก',
//                'yAxis' => 1,
                'data' => $bag,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://icon-library.net/images/bag-icon-png/bag-icon-png-7.jpg\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $bagImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
            [
                'name' => 'แก้วพลาสติก',
//                'yAxis' => 1,
                'data' => $cup,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://www.sccpre.cat/mypng/full/209-2094313_coffee-cup-comments-instagram-story-icons-drink.png\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $cupImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
            [
                'name' => 'กล่องโฟม',
//                'yAxis' => 1,
                'data' => $foam,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://www.regionalrecycling.ca/wp-content/themes/regionalrecycling/img/icons-recycle-plastic-foam-containers-trays.png\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $foamImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],

            [
                'type' => 'spline',
                'name' => 'ค่าเฉลี่ยร้อยละปริมาณขยะรวมทั้งหมด',
                'data' => $avg,
                'marker' => [
                    'lineWidth' => 2,
                    'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                    'fillColor' => 'white',
                ],
            ],
        ]
    ],
]);
?>