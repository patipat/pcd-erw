
<?php

use miloschuman\highcharts\Highcharts;
use yii\helpers\ArrayHelper;
use app\models\MonthlyFormSubmit;
use yii\helpers\Url;

//yii\helpers\VarDumper::dump($data);
//$monthlys = \app\models\MonthlyFormSubmit::find()->isDeleted(FALSE)->office(Yii::$app->user->identity->person->office_id)->all();
$yearMonths = Yii::$app->util->getYearMonths($year);
$monthNames = ArrayHelper::getColumn($yearMonths, 'monthName');
//yii\helpers\VarDumper::dump($monthNames);
$garbage = [];
$bag = [];
$cup = [];
$foam = [];
$mask = [];
$chartData = MonthlyFormSubmit::getChartDataByYear($year, Yii::$app->user->identity->person->office, $dashboardSearch->officeType, $dashboardSearch->level);
//\yii\helpers\VarDumper::dump($chartData);
$chartDataByYearMonth = ArrayHelper::index($chartData, null, 'yearmonth');
//\yii\helpers\VarDumper::dump($chartDataByYearMonth);
foreach ($yearMonths as $yearMonth) {
//    $mfs = MonthlyFormSubmit::find()->isDeleted(FALSE)
//            ->office(Yii::$app->user->identity->person->office_id)->year($year)->month($yearMonth['month'])->isConfirmed()->last()->one();

    if (isset($chartDataByYearMonth[$yearMonth['year'] . $yearMonth['month']])) {
        $mfs = $chartDataByYearMonth[$yearMonth['year'] . $yearMonth['month']];
        $garbage[] = floatval($mfs[0]['garbage']);
        $bag[] = floatval($mfs[0]['bag']);
        $cup[] = floatval($mfs[0]['cup']);
        $foam[] = floatval($mfs[0]['foam']);
        $mask[] = floatval($mfs[0]['mask']);
    } else {
        $garbage[] = null;
        $bag[] = null;
        $cup[] = null;
        $foam[] = null;
        $mask[] = null;
    }
}
$garImg = Url::to('@web/images/garbage.png');
$bagImg = Url::to('@web/images/bag.jpg');
$cupImg = Url::to('@web/images/cup.png');
$foamImg = Url::to('@web/images/foam.png');
$maskImg = Url::to('@web/images/mask.png');

if($year == '2565'){
    $sr = [
            [
                'name' => 'ขยะ',
                'yAxis' => 0,
                'data' => $garbage,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 1,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://cdn0.iconfinder.com/data/icons/waste-recycling/64/496_waste-garbage-trash-organic-512.png\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $garImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
            [
                'name' => 'ถุงพลาสติก',
                'yAxis' => 1,
                'data' => $bag,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://icon-library.net/images/bag-icon-png/bag-icon-png-7.jpg\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $bagImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
            [
                'name' => 'แก้วพลาสติก',
                'yAxis' => 1,
                'data' => $cup,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://www.sccpre.cat/mypng/full/209-2094313_coffee-cup-comments-instagram-story-icons-drink.png\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $cupImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
            [
                'name' => 'กล่องโฟม',
                'yAxis' => 1,
                'data' => $foam,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://www.regionalrecycling.ca/wp-content/themes/regionalrecycling/img/icons-recycle-plastic-foam-containers-trays.png\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $foamImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
                        [
                'name' => 'หน้ากากอนามัย',
                'yAxis' => 1,
                'data' => $mask,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $maskImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
        ];
}else{
    $sr = [
            [
                'name' => 'ขยะ',
                'yAxis' => 0,
                'data' => $garbage,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 1,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://cdn0.iconfinder.com/data/icons/waste-recycling/64/496_waste-garbage-trash-organic-512.png\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $garImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
            [
                'name' => 'ถุงพลาสติก',
                'yAxis' => 1,
                'data' => $bag,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://icon-library.net/images/bag-icon-png/bag-icon-png-7.jpg\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $bagImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
            [
                'name' => 'แก้วพลาสติก',
                'yAxis' => 1,
                'data' => $cup,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://www.sccpre.cat/mypng/full/209-2094313_coffee-cup-comments-instagram-story-icons-drink.png\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $cupImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
            [
                'name' => 'กล่องโฟม',
                'yAxis' => 1,
                'data' => $foam,
                'dataLabels' => [
                    'useHTML' => true,
                    'enabled' => true,
                    //rotation: -90,
                    'color' => '#FFFFFF',
                    'align' => 'right',
                    'x' => 5,
                    'y' => 0,
                    'formatter' => new \yii\web\JsExpression('function(){
                            //console.log(this);
                            //return "<img width=\""+this.point.shapeArgs.width+"\" src=\"https://www.regionalrecycling.ca/wp-content/themes/regionalrecycling/img/icons-recycle-plastic-foam-containers-trays.png\" />";
                            return "<img width=\""+this.point.shapeArgs.width+"\" src=\"' . $foamImg . '\" />";
                        }'),
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Prompt'
                    ]
                ]
            ],
        ];
}
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'credits' => [
            'enabled' => FALSE
        ],
        'lang' => [
            'decimalPoint' => '.',
            'thousandsSep' => ','
        ],
        'chart' => [
            'height' => 500,
            'type' => 'column',
            'style' => [
                'fontSize' => '13px',
                'fontFamily' => 'Prompt',
            ],
        ],
        'title' => [
            'text' => "ปริมาณขยะประจำปี " . $year
        ],
        'exporting' => [
            'allowHTML' => true
        ],
        'xAxis' => [
            'categories' => $monthNames,
            'title' => [
                'text' => 'เดือน'
            ]
        ],
        'yAxis' => [
            [
                'labels' => [
//                    'format' => 'กิโลกรัม',
                    'style' => [
                        'color' => 'Highcharts.getOptions().colors[2]'
                    ]
                ],
                'title' => [
                    'text' => 'กิโลกรัม'
                ],
                'opposite' => true,
            ],
            [
                'labels' => [
//                    'format' => 'ใบ',
                    'style' => [
                        'color' => 'Highcharts.getOptions().colors[2]'
                    ]
                ],
                'title' => [
                    'text' => 'ใบ'
                ]
            ]
        ],
//        'plotOptions' => [
//            'series' => [
//                'dataLabels' => [
//                    'useHTML' => true,
//                    'enabled' => true,
//                    //rotation: -90,
//                    'color' => '#FFFFFF',
//                    'align' => 'right',
//                    'x' => -10,
//                    'y' => 60,
//                    'formatter' => new \yii\web\JsExpression('function(){
//                            console.log(this);
//                            //return "<span>"+this.y+"</span></br><img src=\"http://www.badania-prenatalne.info.pl/images/test-papp-a.png\" />";
//                        }'),
//                    'style' => [
//                        'fontSize' => '13px',
//                        'fontFamily' => 'Verdana, sans-serif'
//                    ]
//                ]
//            ]
//        ],
        'legend' => [
            'useHTML' => true,
            'labelFormatter' => new \yii\web\JsExpression('function () {
                    if(this.name == "ขยะ"){
                        //return "<img width=\"35\" src=\"https://cdn0.iconfinder.com/data/icons/waste-recycling/64/496_waste-garbage-trash-organic-512.png\" />" + this.name;
                        return "<img width=\"35\" src=\"' . $garImg . '\" />" + this.name;
                    }if(this.name == "ถุงพลาสติก"){
                        //return "<img width=\"35\" src=\"https://icon-library.net/images/bag-icon-png/bag-icon-png-7.jpg\" />" + this.name;
                        return "<img width=\"40\" src=\"' . $bagImg . '\" />" + this.name;
                    }if(this.name == "แก้วพลาสติก"){
                        //return "<img width=\"25\" src=\"https://www.sccpre.cat/mypng/full/209-2094313_coffee-cup-comments-instagram-story-icons-drink.png\" /> " + this.name;
                        return "<img width=\"25\" src=\"' . $cupImg . '\" />" + this.name;
                    }if(this.name == "กล่องโฟม"){
                        //return "<img width=\"35\" src=\"https://www.regionalrecycling.ca/wp-content/themes/regionalrecycling/img/icons-recycle-plastic-foam-containers-trays.png\" />" + this.name;
                        return "<img width=\"35\" src=\"' . $foamImg . '\" />" + this.name;
                    }if(this.name == "หน้ากากอนามัย"){
                        //return "<img width=\"35\" src=\"https://www.welcare.co.th/wp-content/uploads/2018/12/mask.png\" />" + this.name;
                        return "<img width=\"35\" src=\"' . $maskImg . '\" />" + this.name;
                    }
                }'),
        ],
        'series' => $sr
    ],
]);
?>