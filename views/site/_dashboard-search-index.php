<form id="w0" action="/pcd-erw/site/progress" method="get" data-pjax="1">
            <div class="row">
                <div class="col-md-6">
                    <label class="sr-only has-star" for="dashboardform-officetype">ประเภทหน่วยงาน</label>
                    <select class="form-control" name="DashboardForm[officeType]">
                        <option value="3">ทสจ. สสภ.</option>
                        <option value="1" selected="">ส่วนงานราชการ</option>
                        <option value="2">ส่วนจังหวัด</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label class="sr-only has-star" for="dashboardform-level">ระดับ</label>
                    <select class="form-control" name="DashboardForm[level]">
                        <option value="1" selected="">ระดับ 1</option>
                        <option value="2">ระดับ 2</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">ค้นหา</button>    </div>
                </div>
            </div>
        </form>