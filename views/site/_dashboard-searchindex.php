<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Office */
/* @var $form yii\widgets\ActiveForm */
$currentRole = \Yii::$app->session->get('currentRole');
?>

<div class="office-search  margin-bottom-10">

    <?php
    $form = ActiveForm::begin([
                'action' => Url::to(['site/index']),
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1,
                ],
                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>

    <?php
    // Usage with ActiveForm and model
    $data = ArrayHelper::map(\app\models\OfficeType::find()->orderBy('CONVERT(office_type.name USING TIS620) ASC')->all(), 'id', 'name');
//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($model, 'officeType')->dropDownList($data);
    ?>
    <?php
    echo $form->field($model, 'level')->dropDownList(Yii::$app->util->getLevelLabels());
    ?>    
    <?= $form->field($model, 'year')->textInput(['type' => 'number', 'style' => 'width: 100px']); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
