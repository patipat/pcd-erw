<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Setting;

app\assets\MatchHeightAsset::register($this);
?>
<div class="row height-full" data-plugin="matchHeight">
    <div class="col-lg-2">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="panel-title padding-5 text-center">
                    บุคลากร 
                    <?= isset(Yii::$app->user->identity->person->office->officePopulationH->total) ? Yii::$app->user->identity->person->office->officePopulationH->total : "0"; ?> 
                    คน
                </div>
            </div>
            <ul class="nav-quick nav-quick-bordered nav-quick-sm row">
                <li class="col-md-6 col-xs-4">
                    <div class="padding-5">
                        <div class="font-size-14 red-700 text-truncate">ภายใน</div>
                        <div class="font-size-24 red-700"><?=
                            isset(Yii::$app->user->identity->person->office->officePopulationH->pop_int) ?
                                    Yii::$app->formatter->asDecimal(Yii::$app->user->identity->person->office->officePopulationH->pop_int) : "0";
                            ?></div>
                        <!--                        <div class="font-size-14 red-700 hidden-sm">&nbsp;</div>
                                                <div class="font-size-20 red-700 hidden-sm" style="padding-bottom: 11px">&nbsp;</div>-->
                    </div>
                </li>
                <li class="col-md-6 col-xs-4">
                    <div class="padding-5">
                        <div class="font-size-14 red-700 text-truncate">ภายนอก</div>
                        <div class="font-size-24 red-700"><?=
                            isset(Yii::$app->user->identity->person->office->officePopulationH->pop_ext) ?
                                    Yii::$app->formatter->asDecimal(Yii::$app->user->identity->person->office->officePopulationH->pop_ext) : "0";
                            ?></div>
                        <!--                        <div class="font-size-14 red-700 hidden-sm">&nbsp;</div>
                                                <div class="font-size-20 red-700 hidden-sm" style="padding-bottom: 11px">&nbsp;</div>-->
                    </div>
                </li>
            </ul>
        </div>

    </div>

    <div class="col-lg-5">
        <a href="<?= Url::to(['site/monthly-form-status', 'year' => $year]) ?>" style="text-decoration: none;">
            <?php
            $sm = Yii::$app->user->identity->person->office->getCurrentSubmittedMonthlyNumber($year);
//                $smPercent = Yii::$app->user->identity->person->office->getCurrentSubmittedMonthlyPercent($year);
            $cf = Yii::$app->user->identity->person->office->getCurrentConfirmedMonthlyNumber($year);
            $ps = Yii::$app->user->identity->person->office->getCurrentPassedMonthlyNumber($year);
//                $cfPercent = Yii::$app->user->identity->person->office->getCurrentConfirmedMonthlyPercent($year);
            $cfp = Yii::$app->user->identity->person->office->getCurrentConfirmPendingMonthlyNumber($year);
            $ckp = Yii::$app->user->identity->person->office->getCurrentCheckPendingMonthlyNumber($year);

            $rc = Yii::$app->user->identity->person->office->getReConfirmedMonthlyNumber($year);
            $lc = Yii::$app->user->identity->person->office->getLateConfirmedMonthlyNumber($year);
            $fl = Yii::$app->user->identity->person->office->getTotalFailedMonthlyNumber($year);
//                $fPercent = Yii::$app->user->identity->person->office->getCurrentFailedMonthlyPercent($year);
            ?>
            <div class="panel panel-info">
                <div class="panel-heading bg-blue-600">
                    <div class="panel-title padding-5 text-center">
                        รายงานรายเดือน (ทั้งหมด <?= Yii::$app->formatter->asDecimal(Setting::getValue(Setting::MONTHLY_MONTHS)); ?> เดือน)
                    </div>
                </div>

                <ul class="nav-quick nav-quick-bordered nav-quick-sm row">
                    <li class="col-md-1 col-sm-1 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 green-700 text-truncate">ส่ง</div>
                            <div class="font-size-24 green-700">
                                <?= Yii::$app->formatter->asDecimal($sm); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-1 col-sm-1 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 green-700 text-truncate">ยืนยัน</div>
                            <div class="font-size-24 green-700">
                                <?= Yii::$app->formatter->asDecimal($cf); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-1 col-sm-1 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 green-700 text-truncate">ผ่าน</div>
                            <div class="font-size-24 green-700">
                                <?= Yii::$app->formatter->asDecimal($ps); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 yellow-700 text-truncate">รอยืนยัน</div>
                            <div class="font-size-24 yellow-700">
                                <?= Yii::$app->formatter->asDecimal($cfp); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 yellow-700 text-truncate">รอตรวจสอบ</div>
                            <div class="font-size-24 yellow-700">
                                <?= Yii::$app->formatter->asDecimal($ckp); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-1 col-sm-1 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 red-700 text-truncate">แก้ไข</div>
                            <div class="font-size-24 red-700">
                                <?= Yii::$app->formatter->asDecimal($rc); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 red-700 text-truncate">เกินกำหนด</div>
                            <div class="font-size-24 red-700">
                                <?= Yii::$app->formatter->asDecimal($lc); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 red-700 text-truncate">ตีกลับ</div>
                            <div class="font-size-24 red-700">
                                <?= Yii::$app->formatter->asDecimal($fl); ?>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </a>

    </div>
    <div class="col-lg-5">
        <a href="<?= Url::to(['form-issue-status', 'year' => $year]) ?>" style="text-decoration: none;">
            <?php
            $sixSmt = Yii::$app->user->identity->person->office->getTotalSubmittedSixMonthNumber($year);
            $sm = Yii::$app->user->identity->person->office->getCurrentSubmittedSixMonthNumber($year);
            $cf = Yii::$app->user->identity->person->office->getCurrentConfirmedSixMonthNumber($year);
            $ps = Yii::$app->user->identity->person->office->getCurrentPassedSixMonthNumber($year);
            $cfp = Yii::$app->user->identity->person->office->getCurrentConfirmPendingSixMonthNumber($year);
            $ckp = Yii::$app->user->identity->person->office->getCurrentCheckPendingSixMonthNumber($year);

            $rc = Yii::$app->user->identity->person->office->getReConfirmedSixMonthNumber($year);
            $lc = Yii::$app->user->identity->person->office->getLateConfirmedSixMonthNumber($year);
            $fl = Yii::$app->user->identity->person->office->getTotalFailedSixMonthNumber($year);
            ?>
            <div class="panel panel-info">
                <div class="panel-heading bg-green-600">
                    <div class="panel-title padding-5 text-center">
                        รายงาน 6 เดือน (ทั้งหมด <?= Yii::$app->formatter->asDecimal($sixSmt); ?> รายการ)
                    </div>
                </div>

                <ul class="nav-quick nav-quick-bordered nav-quick-sm row">
                    <li class="col-md-1 col-sm-1 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 green-700 text-truncate">ส่ง</div>
                            <div class="font-size-24 green-700">
                                <?= Yii::$app->formatter->asDecimal($sm); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-1 col-sm-1 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 green-700 text-truncate">ยืนยัน</div>
                            <div class="font-size-24 green-700">
                                <?= Yii::$app->formatter->asDecimal($cf); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-1 col-sm-1 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 green-700 text-truncate">ผ่าน</div>
                            <div class="font-size-24 green-700">
                                <?= Yii::$app->formatter->asDecimal($ps); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 yellow-700 text-truncate">รอยืนยัน</div>
                            <div class="font-size-24 yellow-700">
                                <?= Yii::$app->formatter->asDecimal($cfp); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 yellow-700 text-truncate">รอตรวจสอบ</div>
                            <div class="font-size-24 yellow-700">
                                <?= Yii::$app->formatter->asDecimal($ckp); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-1 col-sm-1 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 red-700 text-truncate">แก้ไข</div>
                            <div class="font-size-24 red-700">
                                <?= Yii::$app->formatter->asDecimal($rc); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 red-700 text-truncate">เกินกำหนด</div>
                            <div class="font-size-24 red-700">
                                <?= Yii::$app->formatter->asDecimal($lc); ?>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-2 col-sm-2 col-xs-6">
                        <div class="padding-5">
                            <div class="font-size-14 red-700 text-truncate">ตีกลับ</div>
                            <div class="font-size-24 red-700">
                                <?= Yii::$app->formatter->asDecimal($fl); ?>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </a>
    </div>
</div>
