<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Role;

$lastSubmitDate = Yii::$app->util->getLast6SubmitDate($year);
$now = new DateTime();
$yesterday = (clone $now)->sub(new DateInterval('P1D'));
$currentRole = Yii::$app->session->get('currentRole');
?>

<?php
Pjax::begin([
    'id' => 'form-issue-status-pjax'
])
?>
<div class="panel">

    <div class="panel-heading">
        <h3 class="panel-title">รายงานรอบ 6 เดือน ปีงบประมาณ <?= $year ?>
            <span class="font-size-20"><?= $smf->statusBadge; ?></span>
            <div class="panel-desc red-600">* วันสุดท้ายที่ส่งรายงานได้คือวันที่ <?= Yii::$app->thaiFormatter->asDate($lastSubmitDate->format('Y-m-d'), 'php:d/m/Y'); ?></div>
        </h3>
        <div class="panel-actions">
            <?php if (isset($smf->checked_at) && !$smf->is_pass): ?>
                <a href="<?=
                Url::to(['six-month-form/resubmit', 'id' => $smf->id])
                ?>" 
                   class="btn btn-warning btn-raised white" role="modal-remote" 
                   data-request-method="post" data-pjax="0"
                   data-confirm-cancel="ไม่" data-confirm-ok="ใช่" data-confirm-message="ต้องการแก้ไขข้อมูลรายงาน 6 เดือน ประจำปี <?= $year; ?> ใช่หรือไม่ ?"
                   data-confirm-title="แก้ไขข้อมูล"><?= Yii::t('app', 'แก้ไขข้อมูล') ?>
                </a>
            <?php endif; ?>

            <?php if (!isset($smf->checked_at) && isset($smf->confirmed_at) 
                // && Yii::$app->util->isValidSixMonthSubmitDate($year) 
                && (($currentRole['role_id'] == Role::ADMINISTRATOR && $smf->office->parent_id == Yii::$app->user->identity->person->office_id) || $currentRole['role_id'] == Role::SUPER_ADMINISTRATOR)): ?>
                <a href="<?=
                Url::to(['six-month-form/check', 'id' => Yii::$app->user->identity->person->office_id,
                    'year' => $year, 'reload' => '#form-issue-status-pjax'])
                ?>" 
                   class="btn btn-success btn-raised white" role="modal-remote" 
                   data-request-method="post" data-pjax="0"
                   data-confirm-cancel="ไม่" data-confirm-ok="ตรวจสอบข้อมูลรายงาน" data-confirm-message="ต้องการตรวจสอบข้อมูลรายงาน 6 เดือน ประจำปี <?= $year; ?> ใช่หรือไม่ ?"
                   data-confirm-title="ตรวจสอบข้อมูลรายงาน"><?= Yii::t('app', 'ตรวจสอบข้อมูลรายงาน 6 เดือน') ?>
                </a>
            <?php endif; ?>

            <?php if (!isset($smf->confirmed_at) && isset($smf->submitted_at) 
                && isset(Yii::$app->user->identity->person->office->officePopulationH) 
                // && Yii::$app->util->isValidSixMonthSubmitDate($year)
                && $currentRole['role_id'] == Role::ADMINISTRATOR): ?>
                <a href="<?=
                Url::to(['six-month-form/confirm', 'id' => $smf->id, 'reload' => '#form-issue-status-pjax'])
                ?>" 
                   class="btn btn-success btn-raised white" role="modal-remote" 
                   data-request-method="post" data-pjax="0"
                   data-confirm-cancel="ไม่" 
                   data-confirm-ok="ยืนยันข้อมูลรายงาน" data-confirm-message="ต้องการยืนยันข้อมูลรายงาน 6 เดือน ประจำปี <?= $year; ?> ใช่หรือไม่ ?"
                   data-confirm-title="ยืนยันข้อมูลรายงาน"><?= Yii::t('app', 'ยืนยันข้อมูลรายงาน 6 เดือน') ?>
                </a>
            <?php endif; ?>

            <?php if (!isset($smf->submitted_at) 
                // && Yii::$app->util->isValidSixMonthSubmitDate($year) 
                && $currentRole['role_id'] == Role::STAFF): ?>
                <a href="<?= Url::to(['six-month-form/report', 'id' => $smf->id, 'reload' => '#form-issue-status-pjax'])
                ?>" 
                   class="btn btn-success btn-raised white" role="modal-remote"
                   data-request-method="post" data-pjax="0"
                   data-confirm-cancel="ไม่" data-confirm-ok="ส่งรายงาน" 
                   data-confirm-message="ต้องการส่งรายงาน 6 เดือน ประจำปี <?= $year; ?> ใช่หรือไม่ ?" 
                   data-confirm-title="ยืนยันการส่งรายงาน"><?= Yii::t('app', 'ส่งรายงาน 6 เดือน') ?>
                </a>
            <?php endif; ?>
        </div>

    </div>
    <?php if (!empty($smf->comment)): ?>
        <div class="alert alert-danger">
            <?= $smf->comment; ?>
        </div>
    <?php endif; ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>#</td>
                    <td>รายการ</td>
                    <td>สถานะ</td>
                    <td>Comment</td>
                    <td>จัดการ</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($formIssues as $index => $fi): ?>
                    <tr>
                        <td><?= $index + 1 ?></td>
                        <td><?= $fi->assessIssue->name; ?></td>
                        <td>
                            <?php
                            if (isset($fi->submitted_at)) {
                                echo $fi->statusBadge;
                            }
                            ?>
                        </td>
                        <td><?= $fi->comment; ?></td>
                        <td width="25%">
                            <?php if ($fi->hasFile): ?>
                                <a href="<?= Url::to(['form-issue/view', 'id' => $fi->id, 'type' => app\models\FormIssueFile::TYPE_FILE, 'reload' => '#form-issue-status-pjax']) ?>" role="modal-remote" title="<?= Yii::t('app', 'ดูไฟล์') ?>"><i class="icon fa-file-text-o"></i> ดูไฟล์</a><br>
                            <?php else: ?>
                                <?php // if (Yii::$app->util->isValidSixMonthSubmitDate($year)): ?>
                                <a href="<?= Url::to(['form-issue/create', 'id' => $fi->id, 'type' => \app\models\FormIssueFile::TYPE_FILE, 'reload' => '#form-issue-status-pjax']) ?>" role="modal-remote" title="<?= Yii::t('app', 'บันทึกไฟล์') ?>"><i class="icon fa-file-text-o"></i> บันทึกไฟล์</a><br>
                                <?php // endif; ?>
                            <?php endif; ?>
                            <?php if ($fi->hasImage): ?>
                                <a href="<?= Url::to(['form-issue/view', 'id' => $fi->id, 'type' => app\models\FormIssueFile::TYPE_IMAGE, 'reload' => '#form-issue-status-pjax']) ?>" role="modal-remote" title="<?= Yii::t('app', 'ดูรูปภาพ') ?>"><i class="icon fa-file-photo-o"></i> ดูรูปภาพ</a>
                            <?php else: ?>
                                <?php // if (Yii::$app->util->isValidSixMonthSubmitDate($year)): ?>
                                <a href="<?= Url::to(['form-issue/create', 'id' => $fi->id, 'type' => \app\models\FormIssueFile::TYPE_IMAGE, 'reload' => '#form-issue-status-pjax']) ?>" role="modal-remote" title="<?= Yii::t('app', 'บันทึกรูปภาพ') ?>"><i class="icon fa-file-photo-o"></i> บันทึกรูปภาพ</a>
                                <?php // endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php Pjax::end(); ?>