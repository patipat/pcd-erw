<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MonthlyFormSubmit;
use app\models\Setting;
use yii\widgets\Pjax;
use app\models\Role;

app\assets\PieProgressAsset::register($this);
$currentRole = Yii::$app->session->get('currentRole');

Pjax::begin([
    'id' => 'monthly-form-progress-pjax',
    'options' => [
        'class' => 'height-full',
    ]
])
?>
<div class="panel height-full" id="monthly-form-progress">
    <div class="panel-heading">
        <h3 class="panel-title">ความก้าวหน้ารายงานรายเดือนหน่วยงานภายใต้สังกัด ปีงบประมาณ <?= $year ?>
        </h3>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-condensed table-bordered">
            <thead>
                <tr>
                    <th class="text-center" rowspan="2">#</th>
                    <th class="text-center" rowspan="2">ปี</th>
                    <th class="text-center" rowspan="2">เดือน</th>
                    <th class="text-center" colspan="5">จำนวนหน่วยงาน</th>
                    <th class="text-center" colspan="3">จำนวนบุคลากร</th>
                </tr>
                <tr>
                    <th class="text-center">รอยืนยัน</th>
                    <th class="text-center">ยืนยัน</th>
                    <th class="text-center">รอตรวจสอบ</th>
                    <th class="text-center">ตรวจสอบแล้ว</th>
                    <th class="text-center">หน่วยงานที่ยังไม่ส่ง</th>
                    <th class="text-center">ทั้งหมด(หน่วยงาน)</th>
                    <th class="text-center">ยืนยัน</th>
                    <th class="text-center">ทั้งหมด</th>
                    <th class="text-center">%</th>
                </tr>
            </thead>
            <tbody>
                <?php
//                \yii\helpers\VarDumper::dump($dashboardSearch->attributes);
//                exit;
                $totalChildren = Yii::$app->user->identity->person->getResponsibleOfficeCount($dashboardSearch->officeType, $dashboardSearch->level);
                foreach ($months as $key => $m):
                    $confirmedPendingChildren = Yii::$app->user->identity->person->office->getCurrentConfirmPendingMonthlyChildren($m['year'], $m['month'], $dashboardSearch->officeType, $dashboardSearch->level);
                    $confirmedCount = Yii::$app->user->identity->person->office->getCurrentConfirmedMonthlyChildren($m['year'], $m['month'], $dashboardSearch->officeType, $dashboardSearch->level);
                    $totalConfirmedCount = Yii::$app->user->identity->person->office->getTotalConfirmedMonthlyChildren($m['year'], $m['month'], $dashboardSearch->officeType, $dashboardSearch->level);
                    $confirmedPop = Yii::$app->user->identity->person->office->getConfirmedChildrenPop('pop_int', $m['year'], $m['month'], $dashboardSearch->officeType, $dashboardSearch->level);
                    $totalPop = Yii::$app->user->identity->person->office->getTotalChildrenPop('pop_int', $m['year'], $m['month'], $dashboardSearch->officeType, $dashboardSearch->level);
                    $checkedCount = Yii::$app->user->identity->person->office->getCurrentCheckedMonthlyChildren($m['year'], $m['month'], $dashboardSearch->officeType, $dashboardSearch->level);
                    ?>
                    <tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $m['year'] ?></td>
                        <td><?= $m['monthName'] ?></td>
                        <td class="text-right">
                            <?= empty($confirmedPendingChildren->totalCount) ? "" : Html::a(Yii::$app->formatter->asDecimal($confirmedPendingChildren->totalCount, 0)
                                            , Url::to(['monthly-form-submit/office-confirm-pending'
                                                , 'year' => $m['year']
                                                , 'month' => $m['month']
                                                , 'officeType' => $dashboardSearch->officeType
                                                , 'level' => $dashboardSearch->level
                                            ]), [
                                        'data-pjax' => 0, 'role' => 'modal-remote'
                            ]); ?>
                        </td>
                        <td class="text-right">
                            <?= empty($confirmedCount) ? "" : Yii::$app->formatter->asDecimal($confirmedCount, 0); ?>
                        </td>
                        <td class="text-right">
                            <?php
                            if ($currentRole['role_id'] == app\models\Role::REVIEWER) {
                                echo Yii::$app->formatter->asDecimal($totalConfirmedCount, 0);
                            } else {
                                ?>
                                <?=
                                empty($totalConfirmedCount) ? "" : Html::a(Yii::$app->formatter->asDecimal($totalConfirmedCount, 0)
                                                , Url::to(['monthly-form-submit/checked-list', 'MonthlyFormSubmitSearch[year]' => $m['year']
                                                    , 'MonthlyFormSubmitSearch[month]' => $m['month']
                                                    , 'MonthlyFormSubmitSearch[isAssessed]' => 0
                                                    , 'MonthlyFormSubmitSearch[officeType]' => $dashboardSearch->officeType
                                                    , 'MonthlyFormSubmitSearch[level]' => $dashboardSearch->level]), [
                                            'data-pjax' => 0
                                ]);
                                ?>
                            <?php } ?>
                        </td>
                        <td class="text-right"><?=
                            empty($checkedCount) ? "" : Html::a(Yii::$app->formatter->asDecimal($checkedCount, 0)
                                            , Url::to(['monthly-form-submit/checked-list', 'MonthlyFormSubmitSearch[year]' => $m['year']
                                                , 'MonthlyFormSubmitSearch[month]' => $m['month']
                                                , 'MonthlyFormSubmitSearch[isAssessed]' => 1
                                                , 'MonthlyFormSubmitSearch[officeType]' => $dashboardSearch->officeType
                                                , 'MonthlyFormSubmitSearch[level]' => $dashboardSearch->level]), [
                                        'data-pjax' => 0
                            ]);
                            ?></td>
                        <td class="text-right">
                            <?=
                            empty($totalChildren) ? "" : Html::a(Yii::$app->formatter->asDecimal($totalChildren - $confirmedCount, 0)
                                            , Url::to(['office/office-unsend'
                                                , 'year' => $m['year']
                                                , 'month' => $m['month']
                                                , 'officeType' => $dashboardSearch->officeType
                                                , 'nameMonth' => $m['monthName'],
                                            ]), [
                                        'data-pjax' => 0, 'role' => 'modal-remote'
                            ]);
                            ?>
                        </td>
                        <td class="text-right">
                            <?= empty($totalChildren) ? "" : Yii::$app->formatter->asDecimal($totalChildren, 0); ?>
                        </td>
                        <td class="text-right">
                            <?= empty($confirmedPop) ? "" : Yii::$app->formatter->asDecimal($confirmedPop, 0); ?>
                        </td>
                        <td class="text-right">
                            <?= empty($totalPop) ? "" : Yii::$app->formatter->asDecimal($totalPop, 0); ?>
                        </td>
                        <td class="text-center">
                            <?php $percent = empty($totalPop) ? 0 : ($confirmedPop * 100 / $totalPop); ?>
                            <div class="text-red" style="width: 40px; margin: 0 auto;">
                                <div class="pie-progress pie-progress-xs" data-plugin="pieProgress" data-barcolor="<?= Yii::$app->util->getProgressColor($percent) ?>"
                                     data-size="60" data-barsize="2" data-goal="<?= Yii::$app->formatter->asDecimal($percent, 2) ?>" aria-valuenow="<?= Yii::$app->formatter->asDecimal($percent, 2) ?>"
                                     role="progressbar">
                                    <div style="font-size: 1em;" class="pie-progress-number <?= Yii::$app->util->getProgressColor($percent) ?>-600" data-toggle="tooltip" title="New Clients"><?= floor($percent) ?></div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php Pjax::end(); ?>
<?php
$css = <<<css
    td {
        vertical-align: middle !important;
    }
css;
$this->registerCss($css);
