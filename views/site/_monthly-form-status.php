<?php

use app\models\MonthlyFile;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MonthlyFormSubmit;
use app\models\Setting;
use yii\widgets\Pjax;
use app\models\Role;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

$lastSubmitDate = Yii::$app->util->getLastSubmitDate($year);
$now = new DateTime();
$yesterday = (clone $now)->sub(new DateInterval('P1D'));
$currentRole = Yii::$app->session->get('currentRole');

GridView::widget([
    'id' => 'crud-datatable-monthly-form-submit',
    'dataProvider' => new ArrayDataProvider([
        'allModels' => []
            ]),
]);

Pjax::begin([
    'id' => 'monthly-form-status-pjax'
])
?>
<div class="panel" id="monthly-form-status">
    <div class="panel-heading">
        <h3 class="panel-title">รายงานรายเดือน ปีงบประมาณ <?= $year ?>
            <div class="panel-desc red-600">* วันสุดท้ายที่ส่งรายงานได้คือวันที่ <?= Yii::$app->thaiFormatter->asDate($lastSubmitDate->format('Y-m-d'), 'php:d/m/Y'); ?></div>
        </h3>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-condensed table-bordered">
            <thead>
                <tr>
                    <th rowspan="2" class="text-center">#</th>
                    <th rowspan="2" class="text-center">ปี</th>
                    <th rowspan="2" class="text-center">เดือน</th>
                    <th rowspan="2" class="text-center">สถานะ</th>
                    <th colspan="3" class="text-center">จำนวนบุคลากร</th>
                    <th rowspan="2" class="text-center">วันทำงาน</th>
                    <th rowspan="2" class="text-center">ขยะ(กก.)</th>
                    <th rowspan="2" class="text-center">ถุง(ใบ)</th>
                    <th rowspan="2" class="text-center">แก้ว(ใบ)</th>
                    <th rowspan="2" class="text-center">โฟม(ใบ)</th>
                    <?php if ($year == '2565') { ?>
                        <th rowspan="2" class="text-center">หน้ากากอนามัย (ชิ้น)</th>
                    <?php } ?>
<!--                    <th class="text-left">ผู้บันทึกข้อมูล</th>
                    <th class="text-left">วันที่ยืนยันข้อมูล</th>
                    <th class="text-left">ผู้ยืนยันข้อมูล</th>
                    <th class="text-left">วันที่ตรวจสอบข้อมูล</th>
                    <th class="text-left">ผู้ตรวจสอบข้อมูล</th>
                    <th class="text-left">หมายเหตุตรวจสอบ</th>-->
                    <th rowspan="2" class="text-center">หมายเหตุตรวจสอบ</th>
                    <th rowspan="2" class="text-center">ดำเนินการ</th>
                </tr>
                <tr>
                    <th class="text-center">ภายใน</th>
                    <th class="text-center">ภายนอก</th>
                    <th class="text-center">ทั้งหมด</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($months as $key => $m):
                    $mfs = MonthlyFormSubmit::find()->isDeleted(false)->year($m['year'])->month($m['month'])->office(Yii::$app->user->identity->person->office_id)->last()->one();
                    // $fileExists = MonthlyFile::find()->isDeleted(false)->year($m['year'])->month($m['month'])->office(Yii::$app->user->identity->person->office_id)->exists();
                    ?>
                    <tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $m['year'] ?></td>
                        <td><?= $m['monthName'] ?></td>
                        <td>
                            <?= Yii::$app->util->getMonthlyFormStatusBadge($mfs); ?>
                        </td>
                        <td><?= isset($mfs) ? Yii::$app->formatter->asDecimal($mfs->pop_int, 0) : "" ?></td>
                        <td><?= isset($mfs) ? Yii::$app->formatter->asDecimal($mfs->pop_ext, 0) : "" ?></td>
                        <td><?= isset($mfs) ? Yii::$app->formatter->asDecimal($mfs->office_population, 0) : "" ?></td>
                        <td><?= isset($mfs) ? Yii::$app->formatter->asDecimal($mfs->work_day, 0) : "" ?></td>
                        <td><?= isset($mfs) ? Yii::$app->formatter->asDecimal($mfs->garbage, 2) : "" ?></td>
                        <td><?= isset($mfs) ? Yii::$app->formatter->asDecimal($mfs->bag, 0) : "" ?></td>
                        <td><?= isset($mfs) ? Yii::$app->formatter->asDecimal($mfs->cup, 0) : "" ?></td>
                        <td><?= isset($mfs) ? Yii::$app->formatter->asDecimal($mfs->foam, 0) : "" ?></td>
                        <?php if ($year == '2565') { ?>
                            <td><?= isset($mfs) ? Yii::$app->formatter->asDecimal($mfs->mask, 0) : "" ?></td>
                        <?php } ?>
                        <td><?= isset($mfs->comment) ? $mfs->comment : "" ?></td>
                        <td class="text-nowrap">
                            <?php
                            if (Yii::$app->util->checkPermission('monthly-form-submit.confirm') && isset($mfs) && !isset($mfs->confirmed_by) && isset(Yii::$app->user->identity->person->office->officePopulationH) && Yii::$app->util->isValidSubmitDate($year) && $currentRole['role_id'] == Role::ADMINISTRATOR):
                                ?>
                                <?=
                                Html::a('<i class="icon md-check"></i>', Url::to(['monthly-form-submit/confirm', 'id' => $mfs->id, 'reload' => '#monthly-form-status-pjax']), ['role' => 'modal-remote', 'title' => Yii::t('app', 'ยืนยันข้อมูล'),
//                                    'data-confirm' => false, 'data-method' => false, // for overide yii data api
//                                    'data-request-method' => 'post',
//                                    'data-confirm-title' => Yii::t('app', 'ยืนยันข้อูล'),
//                                    'data-confirm-message' => Yii::t('app', 'ต้องการยืนยันข้อมูลรายการนี้ใช่หรือไม่ ?'),
//                                    'data-confirm-ok' => Yii::t('app', 'ใช่'),
//                                    'data-confirm-cancel' => Yii::t('app', 'ไม่'),
                                ])
                                ?>
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->util->checkPermission('monthly-form-submit.create') && !Yii::$app->user->identity->person->office->hasChildren && isset(Yii::$app->user->identity->person->office->officePopulationH) && Yii::$app->util->isValidSubmitDate($year)):
                                ?>
                                <?=
                                Html::a('<i class="icon md-edit"></i>', Url::to(['monthly-form-submit/create', 'year' => $m['year'], 'month' => $m['month'], 'reload' => '#monthly-form-status-pjax']), [
                                    'role' => 'modal-remote', 'title' => Yii::t('app', 'บันทึกข้อมูล'),
                                ])
                                ?>    
                            <?php endif; ?>
                            <?php if (Yii::$app->util->checkPermission('monthly-form-submit.view') && isset($mfs)): ?>
                                <?=
                                Html::a('<i class="icon md-view-list"></i>', Url::to(['monthly-form-submit/view', 'id' => $mfs->id]), [
                                    'role' => 'modal-remote', 'title' => Yii::t('app', 'แสดงข้อมูล'),
                                ])
                                ?> 
                            <?php endif; ?>
                            <?php if (!isset($mfs)): ?>
                                <a href="<?= Url::to(['monthly-file/view', 'officeId' => Yii::$app->user->identity->person->office_id, 'year' => $m['year'], 'month' => $m['month'], 'reload' => '']) ?>" role="modal-remote" title="<?= Yii::t('app', 'จัดการไฟล์') ?>"><i class="icon fa-file-text-o"></i> จัดการไฟล์</a><br>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php Pjax::end(); ?>