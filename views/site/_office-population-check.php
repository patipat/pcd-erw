<?php
use yii\widgets\Pjax;
use yii\helpers\Url;

?>

<?php if (!isset(Yii::$app->user->identity->person->office->officePopulationH) && !Yii::$app->user->identity->person->office->hasChildren): ?>
    <?php
    Pjax::begin([
        'id' => 'office-population-h-alert-pjax'
    ]);
    ?>
    <div class="alert alert-danger font-size-18" role="alert">
        <?= Yii::t('app', 'หน่วยงานของท่านยังไม่มีการบันทึกข้อมูลจำนวนบุคลากร') ?>
        <a href="<?= Url::to(['office-population-h/create', 'officeId' => Yii::$app->user->identity->person->office_id, 'reload' => '#office-population-h-alert-pjax']) ?>" class="btn btn-default btn-raised" role="modal-remote"><?= Yii::t('app', 'คลิก') ?></a>
        <?= Yii::t('app', 'เพื่อบันทึกข้อมูลจำนวนบุคลากร') ?>
    </div>
    <?php Pjax::end(); ?>
<?php endif; ?>
