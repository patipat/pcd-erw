<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MonthlyFormSubmit;
use app\models\Setting;
use yii\widgets\Pjax;
use app\models\Role;

app\assets\PieProgressAsset::register($this);
$currentRole = Yii::$app->session->get('currentRole');

Pjax::begin([
    'id' => 'six-month-form-progress-pjax',
    'options' => [
        'class' => 'height-full',
    ],
]);

$total = Yii::$app->user->identity->person->getResponsibleOfficeCount($dashboardSearch->officeType, $dashboardSearch->level);
$totalPop = Yii::$app->user->identity->person->office->getTotalChildrenPop('pop_int', $year, $year < date('Y') + 543 ? 8 : date('n'), $dashboardSearch->officeType, $dashboardSearch->level);
$confirm = Yii::$app->user->identity->person->office->getTotalConfirmedSixMonthChildren($year, $dashboardSearch->officeType, $dashboardSearch->level);
$confirmPop = Yii::$app->user->identity->person->office->getTotalConfirmedSixMonthChildrenPop($year, $dashboardSearch->officeType, $dashboardSearch->level);
//$confirm = Yii::$app->user->identity->person->office->getTotalConfirmedSixMonthChildren($year);
$percent = empty($total) ? 0 : $confirm * 100 / $total;
$percentPop = empty($totalPop) ? 0 : $confirmPop * 100 / $totalPop;
$check = Yii::$app->user->identity->person->office->getTotalPassedSixMonthChildren($year, $dashboardSearch->officeType, $dashboardSearch->level);
$checkPop = Yii::$app->user->identity->person->office->getTotalPassedSixMonthChildrenPop($year, $dashboardSearch->officeType, $dashboardSearch->level);
$ds = $total - $confirm;
$checkNo = Yii::$app->user->identity->person->office->getTotalNoPassedSixMonthChildren($year, $dashboardSearch->officeType, $dashboardSearch->level);
$percentCheck = empty($total) ? 0 : $check * 100 / $total;
$percentCheckPop = empty($totalPop) ? 0 : $checkPop * 100 / $totalPop;
$checkPending = Yii::$app->user->identity->person->office->getTotalCheckPendingSixMonthChildren($year, $dashboardSearch->officeType, $dashboardSearch->level)
?>
<div class="panel height-full" id="six-month-form-progress">
    <div class="panel-heading">
        <h3 class="panel-title">ความก้าวหน้ารายงาน 6 เดือนหน่วยงานภายใต้สังกัด ปีงบประมาณ <?= $year ?>
        </h3>
    </div>
    <div class="widget widget-completed-options">
        <div class="widget-content padding-30 padding-top-0">
            <div class="counter text-left blue-grey-700">
                <div class="counter-label margin-top-10 text-center font-size-20">จำนวนหน่วยงานทั้งหมด</div>
                <div class="counter-label text-center font-size-20"><?= Yii::$app->formatter->asDecimal($total) ?></div>
                                <div class="counter-label margin-top-10 text-center font-size-20">จำนวนบุคลกรทั้งหมด</div>
                <div class="counter-label text-center font-size-20"><?= Yii::$app->formatter->asDecimal($totalPop) ?></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="text-center font-size-16">ยืนยัน(หน่วยงาน)</div>
                        <div class="counter-number font-size-20 text-center">
                            <?=
                            empty($confirm) ? "" : Html::a(Yii::$app->formatter->asDecimal($confirm)
                                            , Url::to(['six-month-form/check-list-confirm', 'SixMonthFormSearch[year]' => $year
                                                , 'SixMonthFormSearch[officeType]' => $dashboardSearch->officeType
                                                , 'SixMonthFormSearch[level]' => $dashboardSearch->level]), [
                                        'data-pjax' => 0,
                                        'style' => 'text-decoration: none'
                            ]);
                            ?>
                        </div>
                        <div class="text-center font-size-16">จำนวนบุคลากร</div>
                        <div class="counter-number font-size-20 text-center"> <?= Yii::$app->formatter->asDecimal($confirmPop) ?>

                        </div>
                        <div style="max-width: 100px; margin: 0 auto;">
                            <div class="pie-progress pie-progress-sm" data-plugin="pieProgress" data-valuemax="100"
                                 data-barcolor="#62a8ea" data-size="100" data-barsize="10"
                                 data-goal="<?= $percentPop ?>" aria-valuenow="<?= $percentPop ?>" role="progressbar">
                                <span class="pie-progress-number blue-grey-700 font-size-20">
                                    <?= Yii::$app->formatter->asDecimal($percentPop, 2) ?>%
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="text-center font-size-16">ผ่าน(หน่วยงาน)</div>
                        <div class="counter-number font-size-20 text-center">
                            <?=
                            empty($check) ? "" : Html::a(Yii::$app->formatter->asDecimal($check)
                                            , Url::to(['six-month-form/check-list-checked', 'SixMonthFormSearch[year]' => $year
                                                , 'SixMonthFormSearch[officeType]' => $dashboardSearch->officeType
                                                , 'SixMonthFormSearch[level]' => $dashboardSearch->level, 'isPass' => 1]), [
                                        'data-pjax' => 0,
                                        'style' => 'text-decoration: none'
                            ]);
                            ?>

                        </div>
                                                <div class="text-center font-size-16">จำนวนบุคลากร</div>
                        <div class="counter-number font-size-20 text-center"><?= Yii::$app->formatter->asDecimal($checkPop) ?>

                        </div>
                        <div style="max-width: 100px; margin: 0 auto;">
                            <div class="pie-progress pie-progress-sm" data-plugin="pieProgress" data-valuemax="100"
                                 data-barcolor="#62a8ea" data-size="100" data-barsize="10"
                                 data-goal="<?= $percentCheckPop ?>" aria-valuenow="<?= $percentCheckPop ?>" role="progressbar">
                                <span class="pie-progress-number blue-grey-700 font-size-20">
                                    <?= Yii::$app->formatter->asDecimal($percentCheckPop, 2) ?>%
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="counter-label margin-top-10 text-center font-size-20">รอตรวจสอบ</div>
            <div class="counter-number font-size-40 margin-top-10 text-center">
                <?=
                empty($total) ? "" : Html::a(Yii::$app->formatter->asDecimal($checkPending)
                                , Url::to(['six-month-form/check-list', 'SixMonthFormSearch[year]' => $year
                                    , 'SixMonthFormSearch[officeType]' => $dashboardSearch->officeType
                                    , 'SixMonthFormSearch[level]' => $dashboardSearch->level, 'list' => true]), [
                            'data-pjax' => 0,
                            'style' => 'text-decoration: none'
                ]);
                ?>
            </div>
            <div class="col-md-6">
                <div class="counter-label margin-top-10 text-center font-size-20">ไม่ผ่าน</div>
                <div class="counter-number font-size-40 margin-top-10 text-center">
                    <?=
                    empty($checkNo) ? "0" : Html::a(Yii::$app->formatter->asDecimal($checkNo)
                                    , Url::to(['six-month-form/check-list-checked', 'SixMonthFormSearch[year]' => $year
                                        , 'SixMonthFormSearch[officeType]' => $dashboardSearch->officeType
                                        , 'SixMonthFormSearch[level]' => $dashboardSearch->level, 'isPass' => 0]), [
                                'data-pjax' => 0,
                                'style' => 'text-decoration: none'
                    ]);
                    ?>

                </div>
            </div>
            <div class="col-md-6">
                <div class="counter-label margin-top-10 text-center font-size-20">ยังไม่ยืนยัน</div>
                <div class="counter-number font-size-40 margin-top-10 text-center">
                    <?=
                    empty($ds) ? "0" : Html::a(Yii::$app->formatter->asDecimal($ds)
                                    , Url::to(['office/office-unsend-six'
                                        , 'year' => $year
                                        , 'officeType' => $dashboardSearch->officeType
                                    ]), [
                                'data-pjax' => 0, 'role' => 'modal-remote', 'style' => 'text-decoration: none'
                    ]);
                    ?>
                </div>
            </div>

            <!--            <div class="row">
                            <div class="col-xs-6">
                                
                            </div>
                            <div class="col-xs-6">
                                
                            </div>
                        </div>-->
        </div>
    </div>
</div>
<?php Pjax::end(); ?>