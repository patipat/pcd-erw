<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MonthlyFormSubmit;
use app\models\Setting;
use yii\widgets\Pjax;
use app\models\Role;

Pjax::begin([
    'id' => 'monthly-form-status-pjax'
])
?>


<table class="table table-striped table-condensed table-bordered">
    <tbody>
        <tr>
            <td colspan="2" style="background-color:#92cddc; text-align: center"><font style="text-align: center"><?= Yii::t('app', 'แบบฟอร์มรายงานผลการดำเนินการตามมาตรการลด และคัดแยกขยะมูลฝอยในหน่วยงานภาครัฐ') ?></font></td>
        </tr>
        <?php if (isset($office)): ?>
            <tr>
                <td colspan="2" ><?= Yii::t('app', 'หน่วยงาน') ?>: <?= $office->name ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', 'จำนวนบุคลากรในหน่วยงาน (คน)');
                } else {
                    echo Yii::t('app', 'จำนวนบุคลากรในหน่วยงาน (คน)');
                }
                ?></td>
            <?php if ($year >= '2563') { ?>
                <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->pop_int + $ya->pop_ext, 0) : "&nbsp;" ?></td>
            <?php } else { ?>
                <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->pop_int, 0) : "&nbsp;" ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', 'จำนวนวันทำงาน (วัน)');
                } else {
                    echo Yii::t('app', 'จำนวนวันทำงานจนถึงวันรายงาน (วัน)');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->work_day, 0) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', 'น้ำหนักขยะมูลฝอยที่ส่งกำจัด (กิโลกรัม)');
                } else {
                    echo Yii::t('app', 'ปริมาณขยะมูลฝอยที่เกิดขึ้นจนถึงวันรายงาน (กิโลกรัม)');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->garbage, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', 'จำนวนถุงพลาสติกหูหิ้วที่ทิ้งในถังขยะ (ใบ)');
                } else {
                    echo Yii::t('app', 'จำนวนถุงพลาสติกหูหิ้วจากการสำรวจจนถึงวันรายงาน (ใบ)');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->bag, 0) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', 'จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่ทิ้งในถังขยะ (ใบ)');
                } else {
                    echo Yii::t('app', 'จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งจากการสำรวจจนถึงวันรายงาน (ใบ)');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->cup, 0) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', 'จำนวนโฟมบรรจุอาหารที่ทิ้งในถังขยะ (ใบ)');
                } else {
                    echo Yii::t('app', 'จำนวนโฟมบรรจุอาหารจากการสำรวจจนถึงวันรายงาน (ใบ)');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->foam, 0) : "&nbsp;" ?></td>
        </tr>
        <?php if ($year == '2565') { ?>
                <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', 'จำนวนหน้ากากอนามัยที่ทิ้งในถังขยะ (ชิ้น)');
                } else {
                    echo Yii::t('app', 'จำนวนหน้ากากอนามัยจากการสำรวจจนถึงวันรายงาน (ชิ้น)');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->mask, 0) : "&nbsp;" ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* น้ำหนักขยะมูลฝอยที่เกิดขึ้น ปี 2561 (กิโลกรัม)');
                } else {
                    echo Yii::t('app', '* ปริมาณขยะมูลฝอยที่เกิดขึ้น ปี 2561 (กิโลกรัม)');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->garbage_prev, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* น้ำหนักขยะมูลฝอยที่ชั่งได้ ปี {year} (กิโลกรัม)', ['year' => $year]);
                } else {
                    echo Yii::t('app', '* ปริมาณขยะมูลฝอยที่เกิดขึ้น ปี {year} (กิโลกรัม)', ['year' => $year]);
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->garbage_cur, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* จำนวนถุงพลาสติกหูหิ้วที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $year]);
                } else {
                    echo Yii::t('app', '* จำนวนถุงพลาสติกหูหิ้วที่เกิดขึ้น ปี 2561 (ใบ)');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->bag_prev, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* จำนวนถุงพลาสติกหูหิ้วที่ทิ้งในถังขยะ {year} (ใบ)', ['year' => $year]);
                } else {
                    echo Yii::t('app', '* จำนวนถุงพลาสติกหูหิ้วที่เกิดขึ้น ปี {year} (ใบ)', ['year' => $year]);
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->bag_cur, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $year]);
                } else {
                    echo Yii::t('app', '* จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $year]);
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->cup_prev, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่ทิ้งในถังขยะ ปี {year} (ใบ)', ['year' => $year]);
                } else {
                    echo Yii::t('app', '* จำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่เกิดขึ้น ปี {year} (ใบ)', ['year' => $year]);
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->cup_cur, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* จำนวนโฟมบรรจุอาหารที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $year]);
                } else {
                    echo Yii::t('app', '* จำนวนโฟมบรรจุอาหารที่เกิดขึ้น ปี 2561 (ใบ)', ['year' => $year]);
                }
                ?></td>            
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->foam_prev, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* จำนวนโฟมบรรจุอาหารที่ทิ้งในถังขยะ ปี {year} (ใบ)', ['year' => $year]);
                } else {
                    echo Yii::t('app', '* จำนวนโฟมบรรจุอาหารที่เกิดขึ้น ปี {year} (ใบ)', ['year' => $year]);
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->foam_cur, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* ร้อยละปริมาณขยะมูลฝอยที่ส่งกำจัดที่ลดลง');
                } else {
                    echo Yii::t('app', '* ร้อยละปริมาณขยะมูลฝอยที่ลดลง');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->garbage_percent, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* ร้อยละจำนวนถุงพลาสติกหูหิ้วที่ลดลง');
                } else {
                    echo Yii::t('app', '* ร้อยละจำนวนถุงพลาสติกหูหิ้วที่ลดลง');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->bag_percent, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* ร้อยละจำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่ลดลง');
                } else {
                    echo Yii::t('app', '* ร้อยละจำนวนแก้วพลาสติกแบบใช้ครั้งเดียวทิ้งที่ลดลง');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->cup_percent, 2) : "&nbsp;" ?></td>
        </tr>
        <tr>
            <td><?php
                if ($year >= '2563') {
                    echo Yii::t('app', '* ร้อยละจำนวนโฟมบรรจุอาหารที่ลดลง');
                } else {
                    echo Yii::t('app', '* ร้อยละจำนวนโฟมบรรจุอาหารที่ลดลง');
                }
                ?></td>
            <td class="text-right"><?= isset($ya) ? Yii::$app->formatter->asDecimal($ya->foam_percent, 2) : "&nbsp;" ?></td>
        </tr>

    </tbody>
</table>
<?php Pjax::end(); ?>