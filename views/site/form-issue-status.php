<?php

use yii\helpers\Html;

$this->title = 'รายงาน 6 เดือน :: ' . Yii::$app->user->identity->person->office->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<?=

$this->renderFile('@app/views/site/_form-issue-status.php', [
    'assessIssues' => $assessIssues,
    'formIssues' => $formIssues,
    'year' => $year,
    'smf' => $smf
])
?>