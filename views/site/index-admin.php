<?php

use yii\helpers\Html;

$this->title = 'DASHBOARD';
?>
<div class="row">
    <div class="col-lg-6 col-sm-6" style="height: 199px;">
        <!-- Widget Linearea One-->
        <div class="widget widget-shadow" id="widgetLineareaOne">
            <div class="widget-content">
                <div class="padding-20 padding-top-10">
                    <div class="clearfix">
                        <div class="grey-800 pull-left padding-vertical-10">
                            <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom margin-right-5"></i> ความก้าวหน้าข้อมูลรายเดือนประจำปีงบประมาณ 2562</div>
                        <span class="pull-right grey-700 font-size-30">0%</span>

                        <div class="col-sm-6">
                            <div class="margin-bottom-20 grey-500">
                                <i class="icon md-folder red-500 font-size-16"></i> หน่วยงานที่ยังไม่บันทึกข้อมูล<br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> ส่วนรายการ 153 หน่วยงาน<br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> จังหวัด 76 จังหวัด <br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> ทสจ. สสภ. 92 หน่วยงาน
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="margin-bottom-20 grey-500">
                                <i class="icon md-folder blue-500  font-size-16"></i> หน่วยงานทั้งหมด<br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> ส่วนรายการ 153 หน่วยงาน<br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> จังหวัด 76 จังหวัด <br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> ทสจ. สสภ. 92 หน่วยงาน
                            </div>
                        </div>
                    </div>
                    <div class="counter-label">
                        <div class="progress progress-xs margin-bottom-10 bg-grey-400">
                            <div class="progress-bar progress-bar-info bg-red-800" style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="90" role="progressbar">
                            </div>
                        </div>
                        <div class="counter counter-sm text-left">
                            <div class="counter-number-group">
                                <span class="counter-number font-size-14">80%</span>
                                <span class="counter-number-related font-size-14">ปริมาณที่ต้องการ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Widget Linearea One -->
    </div>
    <div class="col-lg-6 col-sm-6" style="height: 250px;">
        <!-- Widget Linearea One-->
        <div class="widget widget-shadow" id="widgetLineareaOne">
            <div class="widget-content">
                <div class="padding-20 padding-top-10">
                    <div class="clearfix">
                        <div class="grey-800 pull-left padding-vertical-10">
                            <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom margin-right-5"></i> ความก้าวหน้าข้อมูล 6 เดือนประจำปีงบประมาณ 2562</div>
                        <span class="pull-right grey-700 font-size-30">69%</span>
                        <div class="col-sm-6">
                            <div class="margin-bottom-20 grey-500">
                                <i class="icon md-folder red-500 font-size-16"></i> หน่วยงานที่ยังไม่บันทึกข้อมูล<br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> ส่วนรายการ 153 หน่วยงาน<br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> จังหวัด 76 จังหวัด <br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> ทสจ. สสภ. 92 หน่วยงาน
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="margin-bottom-20 grey-500">
                                <i class="icon md-folder blue-500  font-size-16"></i> หน่วยงานทั้งหมด<br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> ส่วนรายการ 153 หน่วยงาน<br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> จังหวัด 76 จังหวัด <br>
                                <i class="icon md-long-arrow-up green-500 font-size-16"></i> ทสจ. สสภ. 92 หน่วยงาน
                            </div>
                        </div>
                    </div>
                    <div class="counter-label">
                        <div class="progress progress-xs margin-bottom-10 bg-grey-400">
                            <div class="progress-bar progress-bar-info bg-red-800" style="width: 69%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar">
                                <span class="sr-only">80%</span>
                            </div>
                        </div>
                        <div class="counter counter-sm text-left">
                            <div class="counter-number-group">
                                <span class="counter-number font-size-14">80%</span>
                                <span class="counter-number-related font-size-14">ปริมาณที่ต้องการ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Widget Linearea One -->
    </div>    

</div>
<div class="row" data-plugin="matchHeight" data-by-row="true">
    <div class="col-xlg-7 col-md-6">
        <!-- Panel Projects Status -->
        <div class="panel" id="projects-status">
            <div class="panel-heading">
                <h3 class="panel-title">ข้อมูลรายเดือนที่ต้องตรวจสอบ
                </h3>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>รายการ</td>
                            <td>สถานะ</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>มีการสื่อสารจากผู้บริหารองค์การให้บุคคลากรได้รับรู้และเข้าใจอย่างทั่วถึงต่อนโยบายการลด คัดแยกขยะมูลฝอยในหน่วยงานภาครัฐ</td>
                            <td>
                                <span class="label label-primary">ผ่าน</span>
                            </td>

                        </tr>
                        <tr>
                            <td>2</td>
                            <td>มีการแต่งตั้งคณะทำงานปฏิบัติการลด และคัดแยกขยะมูลฝอย โดยมีผู้บริหารองค์การเป็นประธานคณะทำงานและผู้แทนจากบุคคลากรทุกระดับและทุกฝ่าย</td>
                            <td>
                                <span class="label label-primary">ผ่าน</span>
                            </td>

                        </tr>
                        <tr>
                            <td>3</td>
                            <td>มีการสำรวจ ประเมิน และจัดเก็บข้อมูลปริมาณขยะมูลฝอยโฟมบรรจุอาหาร ถุงพลาสติกหูหิ้ว และแก้วพลาสติกแบบใช้ครั้งเดียวของหน่วยงาน</td>
                            <td>
                                <span class="label label-danger">ไม่ผ่าน</span>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End Panel Projects Stats -->
        
    </div>
    <div class="col-xlg-7 col-md-6">
        <!-- Panel Projects Status -->
        <div class="panel" id="projects-status">
            <div class="panel-heading">
                <h3 class="panel-title">ข้อมูลรายเดือนที่ต้องตรวจสอบ
                </h3>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>รายการ</td>
                            <td>สถานะ</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>มีการสื่อสารจากผู้บริหารองค์การให้บุคคลากรได้รับรู้และเข้าใจอย่างทั่วถึงต่อนโยบายการลด คัดแยกขยะมูลฝอยในหน่วยงานภาครัฐ</td>
                            <td>
                                <span class="label label-primary">ผ่าน</span>
                            </td>

                        </tr>
                        <tr>
                            <td>2</td>
                            <td>มีการแต่งตั้งคณะทำงานปฏิบัติการลด และคัดแยกขยะมูลฝอย โดยมีผู้บริหารองค์การเป็นประธานคณะทำงานและผู้แทนจากบุคคลากรทุกระดับและทุกฝ่าย</td>
                            <td>
                                <span class="label label-primary">ผ่าน</span>
                            </td>

                        </tr>
                        <tr>
                            <td>3</td>
                            <td>มีการสำรวจ ประเมิน และจัดเก็บข้อมูลปริมาณขยะมูลฝอยโฟมบรรจุอาหาร ถุงพลาสติกหูหิ้ว และแก้วพลาสติกแบบใช้ครั้งเดียวของหน่วยงาน</td>
                            <td>
                                <span class="label label-danger">ไม่ผ่าน</span>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End Panel Projects Stats -->
        
    </div>
</div>
<div class="row" data-plugin="matchHeight" data-by-row="true">

    <div class="col-xlg-3 col-lg-4 col-md-12">
        <!-- Panel Web Designer -->
        <div class="widget widget-shadow">
            <div class="widget-content text-center bg-white padding-40">
                <div class="avatar avatar-100 margin-bottom-20">
                    <?=
                    Html::img("@web/images/1.jpg");
                    ?>
                </div>
                <p class="font-size-20 blue-grey-700">ปกรณ์  ใจรักชาติ</p>
                <p class="blue-grey-400 margin-bottom-20">เจ้าหน้าที่บันทึกข้อมูล</p>
                <p class="margin-bottom-35">หน่วยงาน กรมควบคุมมลพิษ กระทรวงทรัพยากรธรรมชาติและสิ่งแวดล้อม
                </p>

                <button type="button" class="btn btn-primary padding-horizontal-40">เข้าใช้งานเมื่อเวลา : 09:40 น.</button>
            </div>
        </div>
        <!-- End Panel Web Designer -->
    </div>


</div>