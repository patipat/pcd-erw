<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MonthlyFormSubmit;
use app\models\Setting;
use yii\widgets\Pjax;

$this->title = 'DASHBOARD';
?>

<?= $this->renderFile('@app/views/site/_dashboard-status-report.php') ?>
<?= $this->renderFile('@app/views/site/_office-population-check.php') ?>
<?= $this->renderFile('@app/views/site/_dashboard-graph.php') ?>
<div class="row">
    <div class="col-xlg-7 col-md-6">
        <!-- Panel Projects Status -->
        <?=
        $this->renderFile('@app/views/site/_monthly-form-status.php', [
            'year' => $year,
            'months' => $months,
        ])
        ?>
        <!-- End Panel Projects Stats -->
    </div>

    <div class="col-xlg-7 col-md-6">
        <!-- Panel Projects Status -->
        <?=
        $this->renderFile('@app/views/site/_yearly-assessment.php', [
            'year' => $year,
            'office' => Yii::$app->user->identity->person->office,
        ])
        ?>
        <!-- End Panel Projects Stats -->
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <!-- Panel Projects Status -->
        <?=
        $this->renderFile('@app/views/site/_form-issue-status.php', [
            'assessIssues' => $assessIssues,
            'year' => $year,
            'smf' => $smf
        ])
        ?>
        <!-- End Panel Projects Stats -->
    </div>
</div>