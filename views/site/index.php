<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MonthlyFormSubmit;
use app\models\Setting;
use yii\widgets\Pjax;

app\assets\MatchHeightAsset::register($this);

$this->title = 'DASHBOARD :: ' . Yii::$app->user->identity->person->office->officeType->name . ' : ' . Yii::$app->user->identity->person->office->name . ' (' . Yii::$app->user->identity->person->currentRoleName . ')';
$currentRole = Yii::$app->session->get('currentRole');
?>

<?php
if ($currentRole['role_id'] == app\models\Role::STAFF):
    echo $this->renderFile('@app/views/site/_office-population-check.php');
endif;
?>

<?php
if ($currentRole['role_id'] != app\models\Role::SUPER_ADMINISTRATOR && $currentRole['role_id'] != app\models\Role::REVIEWER):
    echo $this->renderFile('@app/views/site/_dashboard-status-report.php', [
        'year' => $dashboardSearch->year,
        'smf' => $smf,
        'months' => $months,
    ]);
endif;
?>


<div class="row height-full" data-plugin="matchHeight">
    <?php if ($currentRole['role_id'] == app\models\Role::SUPER_ADMINISTRATOR || $currentRole['role_id'] == app\models\Role::REVIEWER): ?>
        <div class="col-md-9">
            <?=
            $this->renderFile('@app/views/site/_dashboard-search.php', [
                'model' => $dashboardSearch,
            ]);
            ?>
        </div>
    <?php endif; ?>
    <?php if ($currentRole['role_id'] == app\models\Role::STAFF || $currentRole['role_id'] == app\models\Role::ADMINISTRATOR): ?>
        <div class="col-md-9">
            <?=
            $this->renderFile('@app/views/site/_dashboard-search-sa.php', [
                'model' => $dashboardSearch,
            ]);
            ?>
        </div>
    <?php endif; ?>
    <?php if ($currentRole['role_id'] == app\models\Role::SUPER_ADMINISTRATOR || $currentRole['role_id'] == app\models\Role::REVIEWER): ?>
        <div class="col-md-3">
            <?=
            $this->renderFile('@app/views/site/_dashboard-assessment-status.php', [
                'model' => $dashboardSearch,
                'year' => $dashboardSearch->year,
            ]);
            ?>
        </div>
    <?php endif; ?>

</div>
<?=
$this->renderFile('@app/views/site/_dashboard-graph.php', [
    'year' => $dashboardSearch->year,
    'months' => $months,
    'dashboardSearch' => $dashboardSearch,
])
?>
<?=
$this->renderFile('@app/views/site/_dashboard-graph-year.php', [
    'dashboardSearch' => $dashboardSearch,
])
?>
<?=
$this->renderFile('@app/views/site/_dashboard-garbage-percent.php', [
    'year' => $dashboardSearch->year,
    'months' => $months,
    'dashboardSearch' => $dashboardSearch,
])
?>
<?php if ((Yii::$app->user->identity->person->office->hasChildren && $currentRole['role_id'] == app\models\Role::ADMINISTRATOR) || $currentRole['role_id'] == app\models\Role::SUPER_ADMINISTRATOR || $currentRole['role_id'] == app\models\Role::REVIEWER): ?>
    <div class="row height-full" data-plugin="matchHeight">
        <div class="col-md-9">
            <?=
            $this->renderFile('@app/views/site/_monthly-form-progress.php', [
                'year' => $dashboardSearch->year,
                'months' => $months,
                'dashboardSearch' => $dashboardSearch,
            ])
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $this->renderFile('@app/views/site/_six-month-form-progress.php', [
                'year' => $dashboardSearch->year,
                'months' => $months,
                'dashboardSearch' => $dashboardSearch,
            ])
            ?>
        </div>
    </div>
<?php endif; ?>

