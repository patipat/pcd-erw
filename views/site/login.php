<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'login :: E-REPORT มาตรการลด คัดแยกขยะมูลฝอยในหน่วยงานภาครัฐ ';
?>
<div class="page animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content">
        <div class="page-brand-info">
            <div class="brand">
                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <h3 class="panel-title">ข่าวแจ้งเกี่ยวกับระบบ</h3>
                    </div>
                    <div class="panel-body">
                        <h4>ประกาศ</h4>
                        <li>ประกาศเริ่มการใช้งานระบบ</li>
                        <h4>ดาวน์โหลด</h4>
                        <li>ดาวน์โหลดคู่มือการใช้งานสำหรับผู้ดูแลระบบ</li>
                        <li>ดาวน์โหลดคู่มือการใช้งานสำหรับเจ้าหน้าที่</li>
                    </div>
                    <div class="panel-footer">
                        @กรมควบคุมมลพิษ 92 ซ.พหลโยธิน 7 ถ.พหลโยธิน พญาไท กทม. 10400 02-298-2000
                    </div>
                </div>
            </div>
        </div>
        <div class="page-login-main">
            <div class="brand visible-xs">
                <img class="brand-img" src="../web/images/bg.jpg" alt="...">
                <h3 class="brand-text font-size-40"><?= yii::t('app', 'E-REPORT') ?></h3>
                    

            </div>
            <h3 class="font-size-24"><?= yii::t('app', 'เข้าสู่ระบบ') ?>
            </h3>
            <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>
            <?php
            $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-12 control-label'],
                        ],
                        'options' => [
                            'class' => 'margin-left-30'
                        ]
            ]);
            ?>
            <div class="form-group form-material floating"><?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?></div>
            <div class="form-group form-material floating"><?= $form->field($model, 'password')->passwordInput() ?></div>
            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>