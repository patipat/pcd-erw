<?php

use yii\helpers\Html;

$this->title = 'E-REPORT มาตรการลด คัดแยกขยะมูลฝอยในหน่วยงานภาครัฐ';
?>



<section class="ftco-section">
    <div class="container">
        <div class="ftco-departments">
            <div class="row">
                <div class="col-md-12 nav-link-wrap">
                    <div class="nav nav-pills d-flex text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link ftco-animate active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">ข่าวประชาสัมพันธ์</a>
                        <a class="nav-link ftco-animate" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">มาตรการ</a>
                        <a class="nav-link ftco-animate" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false">บทความทางวิชาการ</a>
                        <a class="nav-link ftco-animate" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">ดาวน์โหลด</a>

                    </div>
                </div>
                <div class="col-md-12 tab-wrap">

                    <div class="tab-content bg-light p-4 p-md-3 ftco-animate" id="v-pills-tabContent">

                        <div class="tab-pane py-2 fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="day-1-tab">
                            <div class="row departments">
                                <div class="col-lg-12">

                                    <div class="row mt-1 pt-1">
                                        <?php foreach ($newss as $news) { ?>

                                            <div class="col-lg-6">
                                                <div class="services-2 d-flex">
                                                    <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-experiment-results"></span></div>
                                                    <div class="text">
                                                        <h3><?=
                                                            Html::a($news->title, ['content/detail', 'id' => $news->id], [
                                                                'data-confirm' => false, 'data-method' => false,
                                                                'data-toggle' => 'tooltip', 'target' => '_blank', 'style' => 'text-decoration: none'])
                                                            ?></h3>
                                                        <div class="meta">
                                                            <div><a href="#"><span class="icon-calendar"></span> <?= Yii::$app->thaiFormatter->asDate($news->created_at, 'php:d F Y'); ?></a> <a href="#"><span class="icon-person"></span> <?= $news->createdBy->username; ?></a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-day-3-tab">
                            <div class="row departments">
                                <div class="col-md-12">
                                    <div class="row mt-1 pt-1">
                                        <?php foreach ($measures as $measure) { ?>

                                            <div class="col-lg-6">
                                                <div class="services-2 d-flex">
                                                    <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-experiment-results"></span></div>
                                                    <div class="text">
                                                        <h3><?=
                                                            Html::a($measure->title, ['content/detail', 'id' => $measure->id], [
                                                                'data-confirm' => false, 'data-method' => false,
                                                                'data-toggle' => 'tooltip', 'target' => '_blank', 'style' => 'text-decoration: none'])
                                                            ?></h3>
                                                        <div class="meta">
                                                            <div><a href="#"><span class="icon-calendar"></span> <?= Yii::$app->thaiFormatter->asDate($measure->created_at, 'php:d F Y'); ?></a> <a href="#"><span class="icon-person"></span> <?= $measure->createdBy->username; ?></a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-day-4-tab">
                            <div class="row departments">
                                <div class="col-md-12">
                                    <div class="row mt-1 pt-1">
                                        <?php foreach ($acadamics as $acadamic) { ?>

                                            <div class="col-lg-6">
                                                <div class="services-2 d-flex">
                                                    <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-experiment-results"></span></div>
                                                    <div class="text">
                                                        <h3><?=
                                                            Html::a($acadamic->title, ['content/detail', 'id' => $acadamic->id], [
                                                                'data-confirm' => false, 'data-method' => false,
                                                                'data-toggle' => 'tooltip', 'target' => '_blank', 'style' => 'text-decoration: none'])
                                                            ?></h3>
                                                        <div class="meta">
                                                            <div><a href="#"><span class="icon-calendar"></span> <?= Yii::$app->thaiFormatter->asDate($acadamic->created_at, 'php:d F Y'); ?></a> <a href="#"><span class="icon-person"></span> <?= $acadamic->createdBy->username; ?></a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-day-2-tab">
                            <div class="row departments">
                                <div class="col-md-12">
                                    <div class="row mt-1 pt-1">
                                        <?php foreach ($downloads as $download) { ?>

                                            <div class="col-lg-6">
                                                <div class="services-2 d-flex">
                                                    <div class="icon mt-2 mr-3 d-flex justify-content-center align-items-center"><span class="flaticon-experiment-results"></span></div>
                                                    <div class="text">
                                                        <h3><?=
                                                            Html::a($download->title, ['content/detail', 'id' => $download->id], [
                                                                'data-confirm' => false, 'data-method' => false,
                                                                'data-toggle' => 'tooltip', 'target' => '_blank', 'style' => 'text-decoration: none'])
                                                            ?></h3>
                                                        <div class="meta">
                                                            <div><a href="#"><span class="icon-calendar"></span> <?= Yii::$app->thaiFormatter->asDate($download->created_at, 'php:d F Y'); ?></a> <a href="#"><span class="icon-person"></span> <?= $download->createdBy->username; ?></a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>       
