<?php

use yii\helpers\Html;

$this->title = 'รายงานรายเดือน :: ' . Yii::$app->user->identity->person->office->name;
$this->params['breadcrumbs'][] = $this->title;

?>
<?=
$this->renderFile('@app/views/site/_monthly-form-status.php', [
    'year' => $year,
    'months' => $months,
])
?>