<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Setting;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

$this->title = 'รายงานความก้าวหน้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div class="col-md-12 p-4 p-md-3">
        <!--<form id="w1" action="/pcd-erw/site/progress" method="get" data-pjax="1">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="sr-only has-star" for="dashboardform-officetype">ประเภทหน่วยงาน</label>
                            <select class="form-control" name="DashboardForm[officeType]">
                                <option value="3">ทสจ. สสภ.</option>
                                <option value="1" selected="">ส่วนงานราชการ</option>
                                <option value="2">ส่วนจังหวัด</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label class="sr-only has-star" for="dashboardform-level">ระดับ</label>
                            <select class="form-control" name="DashboardForm[level]">
                                <option value="1" selected="">ระดับ 1</option>
                                <option value="2">ระดับ 2</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">ค้นหา</button>    </div>
                        </div>
                    </div>
                </form>-->
        <?php
        $form = ActiveForm::begin([
//                'action' => Url::to(['site/index']),
                    'method' => 'get',
                    'layout' => 'inline',
                    'options' => [
                        'data-pjax' => 1,
                    ],
//                'type' => ActiveForm::TYPE_INLINE,
        ]);
        ?>

        <?php
        // Usage with ActiveForm and model
        $data = ArrayHelper::map(\app\models\OfficeType::find()->orderBy('CONVERT(office_type.name USING TIS620) ASC')->all(), 'id', 'name');
//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
        echo $form->field($dashboardSearch, 'officeType')->dropDownList($data);
        ?>
        <?php
        echo $form->field($dashboardSearch, 'level')->dropDownList(Yii::$app->util->getLevelLabels());
        ?>    
        <?= $form->field($dashboardSearch, 'year')->textInput(['type' => 'number', 'style' => 'width: 100px']); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
        </div>

        
    </div>

    <div class="col-md-12 p-4 p-md-3">
        <?=
        $this->renderFile('@app/views/site/_dashboard-graph-index.php', [
            'year' => $year,
            'months' => $months,
            'dashboardSearch' => $dashboardSearch,
        ])
        ?>


    </div>
</div>
<?php ActiveForm::end(); ?>

