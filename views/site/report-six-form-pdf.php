<?php
/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\widgets\MaskedInput;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\ServiceRecordH;

$typeName = \app\models\OfficeType::findOne(['id' => $officeType]);
$this->title = "รายงานผล 6 เดือน ( {$typeName->name} )";
$this->params['breadcrumbs'][] = $this->title;

$offices = app\models\Office::find()->isDeleted(false)->officeType($officeType)->level($level)->all();
$assessIssues = \app\models\AssessIssue::find()->isDeleted(false)->all();
?>
<div class="site-about">
    <h3 class="panel-title text-center margin-bottom-10">ผลคะแนนตัวชี้วัดมาตรการลด และคัดแยกขยะมูลฝอยในหน่วยงานภาครัฐ รอบ 6 เดือน ณ วันที่ 7 เมษายน <?= $year ?></h3><br>
    <div class="row">
        <div class="col-md-12">
            <a id="create-plan-link" class="hidden" role="modal-remote" style="text-decoration: none"></a>
            <table style="border-collapse: collapse; border: 1px solid black; width: 100%">
                <thead>
                    <tr class="bg-blue-100 yellow-50" style="background-color: #c6e2c7">
                        <td colspan="2" rowspan="1" style="border: 1px solid black;" class="text-center padding-10"> ชื่อหน่วยงาน </td>
                        <?php foreach ($assessIssues as $assessIssue): ?>
                            <td style="border: 1px solid black; width: 5%" class="text-center"><?= $assessIssue->id; ?></td>
                        <?php endforeach; ?>
                        <td style="border: 1px solid black; width: 5%" class="text-center">รวม</td>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    foreach ($offices as $office):
                        $total = \app\models\FormIssue::find()->joinWith(['sixMonthForm'])->isDeleted(false)->office($office->id)->year($year)->sum('form_issue.score');
                        ?> 
                        <tr>
                            <td style="border: 1px solid black; width: 3%" class="text-center"> <?= $i; ?> </td>
                            <td style="border: 1px solid black;" class="padding-5"> <?= $office->name; ?> </td>
                            <?php
                            foreach ($assessIssues as $assessIssue):
                                $fi = \app\models\FormIssue::find()->joinWith(['sixMonthForm'])->isDeleted(false)->office($office->id)->year($year)->assessIssue($assessIssue->id)->one();
                                ?>
                                <td style="border: 1px solid black;" class="text-center"><?= isset($fi) ? number_format($fi->score, 0) : "" ?></td>
                                <?php
                            endforeach;
                            ?>
                            <td style="border: 1px solid black;" class="text-center"><?= isset($total) ? number_format($total, 0) : "" ?> </td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

