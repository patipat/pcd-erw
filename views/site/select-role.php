<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'เข้าสู่ระบบ';
$this->params['breadcrumbs'][] = $this->title;
//\yii\helpers\VarDumper::dump(Yii::$app->session->get('currentRole'));
?>
<div class="panel col-md-6 col-md-offset-3 margin-top-50">
    <div class="panel-body text-left  margin-50">
        <div class="brand">
            <div class="text-center"><?= Html::img('@web/images/logo.png', ['width' => 90]); ?></div>
            <div class="brand-text font-size-18 text-center text-primary">E-REPORT การลดและคัดแยกขยะมูลฝอย ในหน่วยงานภาครัฐ </div>
            <!--<div class="font-size-20 font-weight-900">CORE</div>-->
        </div>
        <h4 class="text-center"><?= Yii::t('app', 'กรุณาเลือกหน้าที่') ?></h4>
        <?php foreach ($roles as $role): ?>
        <?= Html::a($role->role->name, ['site/select-role', 'personRoleId' => $role->id], ['class' => 'btn btn-block btn-primary btn-lg']); ?>
        <?php endforeach; ?>
    </div>
</div>