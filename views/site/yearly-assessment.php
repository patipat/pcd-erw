<?php

$this->title = "ผลการดำเนินการ ปีงบประมาณ {$year}";
$this->params['breadcrumbs'][] = $this->title;
?>
<?=

$this->renderFile('@app/views/site/_yearly-assessment.php', [
    'year' => $year,
    'ya' => $ya,
    'office' => $office,
])
?>