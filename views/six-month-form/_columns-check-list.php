<?php

use yii\helpers\Url;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Role;

return [
    //[
    //    'class' => 'kartik\grid\CheckboxColumn',
    //    'width' => '20px',
    //],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function ($model, $key, $index, $column) {
            $searchModel = new app\models\FormIssueSearch();
            $searchModel->deleted = 0;
            $searchModel->six_month_form_id = $model->id;
            $dataProvider = $searchModel->search([]);

            return Yii::$app->controller->renderPartial('@app/views/form-issue/index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true,
        'hiddenFromExport' => false,
        'detailRowCssClass' => 'detail-row',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'office_id',
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'data' => ArrayHelper::map(Yii::$app->user->identity->person->getTotalResponsibleOffices(), 'id', 'typeName'),
            'options' => ['placeholder' => 'ทั้งหมด'],
            'pluginOptions' => [
                'allowClear' => TRUE,
            ]
        ],
        'value' => function($model) {
            return $model->office->name;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'year',
        'label' => 'ปีงบประมาณ'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'score',
        'label' => 'คะแนน',
        'vAlign' => 'middle',
        'hAlign' => 'center',
        'value' => function($model) {
            $score = \app\models\FormIssue::find()->joinWith(['sixMonthForm'])->sixMonth($model->id)->isDeleted(false)->office($model->office_id)->year($model->year)->sum('form_issue.score');
            return isset($score) ? number_format($score, 0) : "";
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'comment',
//        'label' => 'ปีงบประมาณ'
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'office.officeType.name',
////        'label' => 'ปีงบประมาณ'
//    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'office.level',
////        'label' => 'ปีงบประมาณ'
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'deleted',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'created_by',
//    ],
//    // [
//        // 'class'=>'\kartik\grid\DataColumn',
//        // 'attribute'=>'created_at',
//    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'updated_by',
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'submitted_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'submitted_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'confirmed_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'confirmed_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'checked_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'checked_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'is_pass',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'comment',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{history} {check}',
        'viewOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'รายละเอียด'), 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'แก้ไข'), 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'ลบ'),
            'data-confirm' => false, 'data-method' => false, // for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => Yii::t('app', 'ยืนยันการลบ'),
            'data-confirm-message' => Yii::t('app', 'ต้องการลบรายการนี้ใช่หรือไม่ ?'),
            'data-confirm-ok' => Yii::t('app', 'ใช่'),
            'data-confirm-cancel' => Yii::t('app', 'ไม่'),
        ],
        'buttons' => [
            'record' => function($url, $model) {
                return \yii\helpers\Html::a('<i class="icon md-format-list-bulleted"></i>',
                                ['six-month-form/record', 'officeId' => $model->office_id
                                    , 'year' => $model->year, 'month' => $model->month],
                                ['role' => 'modal-remote']);
            },
            'check' => function($url, $model) {
                return \yii\helpers\Html::a('<i class="icon md-assignment-check"></i>',
                                ['six-month-form/check', 'id' => $model->id], [
                            'role' => 'modal-remote', 'title' => Yii::t('app', 'ตรวจสอบ'),
//                            'data-confirm' => false, 'data-method' => false, // for overide yii data api
//                            'data-request-method' => 'post',
//                            'data-confirm-title' => Yii::t('app', 'ตรวจสอบข้อมูลรายงาน 6 เดือน'),
//                            'data-confirm-message' => Yii::t('app', "ตรวจสอบข้อมูลรายงาน 6 เดือน {$model->office->name} ปี {$model->year} แล้วใช่หรือไม่ ?"),
//                            'data-confirm-ok' => Yii::t('app', 'ใช่'),
//                            'data-confirm-cancel' => Yii::t('app', 'ไม่'),
                ]);
            },
        ],
        'visibleButtons' => [
            'check' => function($model) {
                return isset($model->confirmed_at) && !isset($model->checked_at);
            },
            'record' => function($model) {
                return $model->hasRecord;
            },
        ]
    ],
];
