<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Office;
use app\models\Role;
use app\models\Setting;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\MonthlyFormSubmit */
/* @var $form yii\widgets\ActiveForm */
$currentRole = Yii::$app->session->get('currentRole');
?>

<div class="six-month-form-search margin-bottom-10">

    <?php
    $form = ActiveForm::begin([
                'action' => Url::to(['monthly-form-submit/check-list']),
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1,
                ],
                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>

    <?php
    // Usage with ActiveForm and model
//    if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR) {
//        $level = Setting::getValue(Setting::SUPER_ADMIN_CHECK_LEVEL);
//        $data = ArrayHelper::map(Office::find()->isDeleted(false)->level("<={$level}")->orderBy('CONVERT(office.name USING TIS620) ASC')->all(), 'id', 'name');
//    } else {
//        $data = ArrayHelper::map(Office::find()->isDeleted(false)->parent(Yii::$app->user->identity->person->office_id)->orderBy('CONVERT(office.name USING TIS620) ASC')->all(), 'id', 'name');
//    }
    $data = ArrayHelper::map(Yii::$app->user->identity->person->getTotalResponsibleOffices(), 'id', 'name');

//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
//    echo $form->field($model, 'office_id', [
//        'options' => [
//            'style' => 'width: 300px',
//            'class' => 'form-group',
//        ]
//    ])->widget(Select2::classname(), [
//        'data' => $data,
//        'options' => ['placeholder' => 'หน่วยงาน'],
//        'pluginOptions' => [
//            'allowClear' => true
//        ],
//    ]);
    ?>

    <?php $form->field($model, 'year')->textInput(['type' => 'number']) ?>

    <?php
//    if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR) {
//        $data = ArrayHelper::map(\app\models\OfficeType::find()->orderBy('CONVERT(office_type.name USING TIS620) ASC')->all(), 'id', 'name');
////    \yii\helpers\VarDumper::dump($data, 10, TRUE);
//        echo $form->field($model, 'officeType')->dropDownList($data, ['prompt' => 'ประเภทหน่วยงาน']);
//
//        echo $form->field($model, 'level')->dropDownList(Yii::$app->util->getLevelLabels(), ['prompt' => 'ระดับหน่วยงาน']);
//    }
    ?>

    <div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
