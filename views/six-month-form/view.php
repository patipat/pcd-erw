<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SixMonthForm */
?>
<div class="six-month-form-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'office_id',
            'year',
            'deleted',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
            'submitted_by',
            'submitted_at',
            'confirmed_by',
            'confirmed_at',
            'checked_by',
            'checked_at',
            'is_pass',
            'comment:ntext',
        ],
    ]) ?>

</div>
