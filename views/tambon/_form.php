<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Province;
use app\models\Amphur;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Tambon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tambon-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>
    

    <?php
    
    // Usage with ActiveForm and model
    $data = ArrayHelper::map(Province::find()->isDeleted(FALSE)->orderBy('CONVERT(province.name USING TIS620) ASC')->all(), 'id', 'name');
//   \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($model, 'province_id')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>

    <?php
    $data = [];    
    if (!empty($model->province_id)) {
        $data = ArrayHelper::map(Amphur::find()->isDeleted(FALSE)->province($model->province_id)->orderBy('CONVERT(amphur.name USING TIS620)')->all(), 'id', 'name');
 // \yii\helpers\VarDumper::dump($data, 10, TRUE);    
        
    }
    echo $form->field($model, 'amphur_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $data,
        'options' => [
            'class'=>Yii::$app->util->getFormControlClass($model->amphur_id)
        ],
        'select2Options' => [
            'pluginOptions' => ['allowClear' => true]
        ],
        'pluginOptions' => [
            'depends' => [Html::getInputId($model, 'province_id')],
            'url' => Url::to(['/amphur/list']),
            'placeholder' => '',
        ]
    ]);
    ?>
    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_eng')->textInput(['maxlength' => true]) ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'เพิ่ม') : Yii::t('app', 'บันทึก'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    setTimeout(function() {
        jQuery('.field-<?= Html::getInputId($model, 'province_id') ?> .select2-selection--single').focus();
    }, 500);

</script>
</div>
