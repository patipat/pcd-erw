<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Province;
use app\models\Amphur;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
?>
<div class="tambon-search margin-bottom-10">
    <?php
    $form = ActiveForm::begin([
                'method' => 'get',
                'action' => Url::to(['tambon/index']),

                'options' => [
                    'data-pjax' => 1,
//                            'target' => '#crud-datatable-ticket-h-pjax'
//                    'class' => 'form-inline'
                ],
                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>


    <?= $form->field($searchModel, 'name')->textInput([]); ?>
    <?php
    // Usage with ActiveForm and model
    $data = ArrayHelper::map(Province::find()->isDeleted(FALSE)->orderBy('CONVERT(province.name USING TIS620) ASC')->all(), 'id', 'name');
//   \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($searchModel, 'province_id')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => 'เลือกจังหวัด'],
        'pluginOptions' => [
            'allowClear' => true,
            'width' => '250px'
        ],
    ]);
    ?>

    <?php
    $data = [];
    if (!empty($model->province_id)) {
        $data = ArrayHelper::map(Amphur::find()->isDeleted(FALSE)->province($searchModel->province_id)->orderBy('CONVERT(amphur.name USING TIS620)')->all(), 'id', 'name');
        // \yii\helpers\VarDumper::dump($data, 10, TRUE);    
    }
    echo $form->field($searchModel, 'amphur_id')->widget(DepDrop::classname(), [
        'type' => DepDrop::TYPE_SELECT2,
        'data' => $data,
        'options' => [
            'class' => Yii::$app->util->getFormControlClass($searchModel->amphur_id)
        ],
        'select2Options' => [
            'pluginOptions' => ['allowClear' => true,'width' => '250px'
]
        ],
        'pluginOptions' => [
            'depends' => [Html::getInputId($searchModel, 'province_id')],
            'url' => Url::to(['/amphur/list']),
            'placeholder' => 'เลือกอำเภอ',
        ]
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>