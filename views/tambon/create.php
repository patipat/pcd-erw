<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tambon */

?>
<div class="tambon-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
