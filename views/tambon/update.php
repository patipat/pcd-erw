<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tambon */
?>
<div class="tambon-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
