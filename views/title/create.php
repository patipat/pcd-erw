<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Title */

?>
<div class="title-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
