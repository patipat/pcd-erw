<?php

use yii\helpers\Html;
use app\assets\AppAsset;

$asset = AppAsset::register($this);
$baseUrl = $asset->baseUrl;
?>
<div class="span-list-weblink" style="margin-bottom: 20px;">
    <div class="panel" >
        <div class="panel-body">
            <div class="testimonial" style=" height: 75px;">
                <p class="text">
                    <?= $model->name ?>
                    <br>
                    <a href="<?= $baseUrl; ?>/web/uploads/download/<?= $model->url; ?>" target="_blank" style="margin-left: -10px;">ดาวน์โหลดไฟล์</a>
                </p>
            </div>
        </div>

    </div>
</div>
<style>
    .span-list-weblink {
        width: 280px;
    }
</style>
