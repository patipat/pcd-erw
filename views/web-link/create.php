<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WebLink */

?>
<div class="web-link-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
