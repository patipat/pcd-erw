<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ไฟล์ดาวน์โหลด ');
$this->params['breadcrumbs'][] = $this->title;

//CrudAsset::register($this);
?>
<div class="content-index">
    <div id="ajaxCrudDatatable">
        <?php
        echo ListView::widget([
            'id' => 'crud-datatable-content-pjax',
            'dataProvider' => $dataProvider,
            'layout' => '{summary}<hr><div class="row" style="margin-left:-10px;">{items}</div>{pager}',
            'itemView' => '_columns-all',
            'pager' => array(
                'firstPageLabel' => '<i class="icon-double-angle-left"></i>',
                'lastPageLabel' => '<i class="icon-double-angle-right"></i>',
                'prevPageLabel' => '<i class="icon-angle-left"></i>',
                'nextPageLabel' => '<i class="icon-angle-right"></i>',
            ), 'id' => 'pagination',
        ]);
        /* @var $this yii\web\View */
        /* @var $model app\models\Pet */
        ?>    </div>
</div>

<style>
#pagination a, #pagination ul,#pagination li  {
    display: block;
    float: left;
    margin: 0 7px 0 0;
    padding: 7px 10px 6px 10px;
    font-size: 14px;
    line-height: 12px;
    color: #888;
    font-weight: 600;
}
</style>
