<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WebLink */
?>
<div class="web-link-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'url:url',
        ],
    ]) ?>

</div>
