<?php

use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;

$currentRole = Yii::$app->session->get('currentRole');
?>

<nav class="site-navbar navbar navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <span class="navbar-brand-text hidden-xs-down"> E-REPORT มาตรการลด คัดแยกขยะมูลฝอยในหน่วยงานภาครัฐ</span>
        </div>

    </div>
    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <?php
            echo Nav::widget([
                'options' => ['class' => 'nav navbar-toolbar'],
                'dropDownCaret' => '',
                'encodeLabels' => FALSE,
                'items' => [
                    [
                        'label' => Html::tag('i', Html::tag('span', 'Toggle menubar', ['class' => 'sr-only']) . Html::tag('span', '', ['class' => 'hamburger-bar']), ['class' => 'icon hamburger hamburger-arrow-left']),
//                    'url' => Url::to(['site/dashboard']),
                        'encode' => FALSE,
                        'options' => ['class' => 'nav-item hidden-float', 'id' => 'toggleMenubar'],
                        'linkOptions' => ['class' => 'nav-link', 'data-toggle' => 'menubar', 'href' => '#', 'role' => 'button'],
//                    'visible' => Yii::$app->user->can('core.site.dashboard'),
                    ],
                    ['label' => Html::tag('i', '', ['class' => 'site-menu-icon md-settings']) . Html::tag('span', yii::t('app', 'ข้อมูลตั้งค่าระบบ'), ['class' => 'site-menu-title']), 'url' => ['site/master-list'], 'visible' => $currentRole['role_id'] == app\models\Role::SUPER_ADMINISTRATOR],
                    ['label' => Html::tag('i', '', ['class' => 'site-menu-icon md-settings']) . Html::tag('span', yii::t('app', 'จัดการข้อมูลผู้ใช้งาน'), ['class' => 'site-menu-title']), 'url' => ['site/person'], 'visible' => $currentRole['role_id'] == app\models\Role::ADMINISTRATOR],
//                        ['label' => Html::tag('i', '', ['class' => 'site-menu-icon md-settings']) . Html::tag('span', yii::t('app', 'รายงาน'), ['class' => 'site-menu-title']), 'url' => ['site/index'], 'visible' => true],
                ],
            ]);
//            $currentRole = Yii::$app->session->get('currentRole');
            $roles = Yii::$app->user->identity->person->getAvailableRoles();
            $items = [
                [
                    'label' => Html::tag('i', '', ['class' => 'icon md-account']) . 'จัดการข้อมูลส่วนตัว',
                    'url' => Url::to(['person/update','id'=>Yii::$app->user->identity->person->id]),
                    'encode' => FALSE,
//                                'linkOptions' => ['class' => 'btn-logout'],
                ],
            ];
            if (count($roles) > 1) {
                $items[] = '<li role="separator" class="divider"></li>';
                foreach ($roles as $role) {
                    $items[] = [
                        'label' => Html::tag('i', '', ['class' => 'icon md-chevron-right']) . $role['role']['name'],
                        'url' => ['site/select-role', 'personRoleId' => $role->id],
                        'encode' => FALSE,
                    ];
                }
                
            }
            $items[] = '<li role="separator" class="divider"></li>';
            $items[] = [
                'label' => Html::tag('i', '', ['class' => 'icon md-power']) . 'ออกจากระบบ',
                'url' => Url::to(['site/logout']),
                'encode' => FALSE,
                'linkOptions' => ['class' => 'btn-logout'],
            ];
            $alerts = Yii::$app->user->identity->getLatestAlerts();
            $alertItems = \app\models\Alert::buildAlertItems($alerts);
            echo Nav::widget([
                'options' => ['class' => 'nav navbar-toolbar navbar-right navbar-toolbar-right text-right margin-right-0'],
                'dropDownCaret' => '',
                'encodeLabels' => FALSE,
                'items' => [
                    [
                        'label' => '<i class="icon md-notifications"></i><span class="badge badge-danger up alert-count">' . count($alerts) . '</span>',
                        'options' => ['style' => 'min-width: 40px;'],
                        'linkOptions' => [
                            'data-animation' => 'scale-up',
                            'role' => 'button',
                        ],
                        'dropDownOptions' => [
                            'class' => 'dropdown-menu-right dropdown-menu-media',
                            'id' => 'alert-dropdown',
                            'role' => 'menu',
                        ],
                        'items' => $alertItems
                    ],
                    [
                        'label' =>
                        Html::tag('div', Yii::$app->user->identity->person->fullName . '<br><small>' . Yii::$app->user->identity->person->currentRoleName . '</small><br><small>' . Yii::$app->user->identity->person->office->name .' ('.Yii::$app->user->identity->person->office->officeType->name.')'. '</small>', ['class' => 'padding-right-5 inline-block line-height-16'])
                        . Html::beginTag('span', ['class' => 'avatar avatar-online vertical-align-top'])
//                            . Html::img('@web/ionicons/png/512/android-contact.png')
//                        . Html::img('@web/images/material/ic_face_white_48dp_2x.png')
                        . Html::tag('span', '', ['class' => 'icon md-face font-size-40'])
                        . Html::tag('i')
                        . Html::endTag('span'),
                        'linkOptions' => [
                            'class' => 'navbar-avatar padding-5 font-weight-900',
                            'data-animation' => 'scale-up',
                            'role' => 'button',
                        ],
                        'dropDownOptions' => [
//                            'class' => 'dropdown-menu-right dropdown-menu-media',
                            'role' => 'menu',
                        ],
                        'items' => $items,
                    ],
                ],
            ]);
            ?>

            <div class="navbar-brand navbar-brand-center padding-20" data-toggle="gridmenu">
            <!--<img class="navbar-brand-logo" src="../assets/images/logo.png" title="Remark">-->
                <span class="navbar-brand-text">E-REPORT มาตรการลด คัดแยกขยะมูลฝอยในหน่วยงานภาครัฐ</span>
            </div>
        </div>
    </div>
</nav>