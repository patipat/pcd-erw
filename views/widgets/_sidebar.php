<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\RegistrationType;
use app\models\Setting;

$currentRole = Yii::$app->session->get('currentRole');
$year = Setting::getValue(Setting::CURRENT_YEAR);
?>
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu" >
                    <li class="site-menu-item active">

                        <a class="animsition-link" href="<?= Url::to(['/site/index']) ?>">
                            <i class="site-menu-icon icon fa-home" aria-hidden="true"></i>
                            <span class="site-menu-title">หน้าหลัก</span>
                        </a>
                        <?php if ($currentRole['role_id'] == \app\models\Role::SUPER_ADMINISTRATOR): ?>
                        <li class="site-menu-item">
                            <?= Html::a('<i class="site-menu-icon md-receipt " aria-hidden="true"></i><span class="site-menu-title">ข่าวประชาสัมพันธ์</span>', ['content/index', 'groupId' => \app\models\Content::NEWS]) ?>
                        </li>
                        <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <i class="site-menu-icon md-view-web" aria-hidden="true"></i>
                                <span class="site-menu-title">ข้อมูลทั่วไปของโครงการ</span>
                                <p class="site-menu-arrow"></p>

                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">ข้อมูลเกี่ยวกับโครงการ</span>', ['content/update', 'id' => 1], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'ข้อมูลเกี่ยวกับโครงการ']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">ติดต่อสอบถาม</span>', ['content/update', 'id' => 2], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'ติดต่อสอบถาม']) ?>
                                </li>

                            </ul>
                        </li>
                        <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <i class="site-menu-icon md-menu" aria-hidden="true"></i>
                                <span class="site-menu-title">บทความ</span>
                                <p class="site-menu-arrow"></p>

                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">บทความทางวิชาการ</span>', ['content/index', 'groupId' => \app\models\Content::ACADAMIC], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'บทความทางวิชาการ']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">บทความมาตรการ</span>', ['content/index', 'groupId' => \app\models\Content::MEASURE], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'บทความมาตรการ']) ?>
                                </li>

                            </ul>
                        </li>
                        <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <i class="site-menu-icon md-menu" aria-hidden="true"></i>
                                <span class="site-menu-title">ดาวน์โหลด</span>
                                <p class="site-menu-arrow"></p>

                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">คู่มือ</span>', ['content/index', 'groupId' => \app\models\Content::DOWNLOAD, 'categoryId' => 1], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'คู่มือ']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">โปสเตอร์</span>', ['content/index', 'groupId' => \app\models\Content::DOWNLOAD, 'categoryId' => 2], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'โปสเตอร์']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">ไฟล์วีดีโอ</span>', ['content/index', 'groupId' => \app\models\Content::DOWNLOAD, 'categoryId' => 3], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'ไฟล์วีดีโอ']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">PPT</span>', ['content/index', 'groupId' => \app\models\Content::DOWNLOAD, 'categoryId' => 4], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'PPT']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">ภาพกิจกรรม (zip file)</span>', ['content/index', 'groupId' => \app\models\Content::DOWNLOAD, 'categoryId' => 5], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'ภาพกิจกรรม (zip file)']) ?>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>  
                        <?php if($currentRole['role_id'] == \app\models\Role::ADMINISTRATOR){ ?>
                                                <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                                <span class="site-menu-title">รายงานรอบ 12 เดือน</span>
                                <p class="site-menu-arrow"></p>

                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">รายงานปริมาณขยะ</span>', ['office/yearly-assessment-report-office', 'year' => $year, 'officeId' => Yii::$app->user->identity->person->office_id], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'รายงานปริมาณขยะ']) ?>
                                </li>
                            </ul>
                        </li>
                        <?php }  ?>
                    <?php if ($currentRole['role_id'] == \app\models\Role::SUPER_ADMINISTRATOR || $currentRole['role_id'] == \app\models\Role::REVIEWER): ?>

                        <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                                <span class="site-menu-title">รายงานรอบ 12 เดือน</span>
                                <p class="site-menu-arrow"></p>

                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">รายงานรายเดือนส่วนราชการ</span>', ['office/yearly-assessment-report', 'year' => $year, 'officeType' => 1, 'level' => 2], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'รายงานรายเดือนส่วนราชการ']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">รายงานรายเดือนส่วนจังหวัด</span>', ['office/yearly-assessment-report', 'year' => $year, 'officeType' => 2, 'level' => 2], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'รายงานรายเดือนส่วนจังหวัด']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">รายงานรายเดือนส่วน ทสจ. สสภ.</span>', ['office/yearly-assessment-report', 'year' => $year, 'officeType' => 3, 'level' => 2], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'รายงานรายเดือนส่วน ทสจ. สสภ.']) ?>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                                <span class="site-menu-title">รายงานรอบ 12 เดือน (เลือกเดือน)</span>
                                <p class="site-menu-arrow"></p>

                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">รายงานรายเดือนส่วนราชการ</span>', ['office/monthly-assessment-report', 'year' => $year, 'officeType' => 1, 'level' => 2], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'รายงานรายเดือนส่วนราชการ']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">รายงานรายเดือนส่วนจังหวัด</span>', ['office/monthly-assessment-report', 'year' => $year, 'officeType' => 2, 'level' => 2], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'รายงานรายเดือนส่วนจังหวัด']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">รายงานรายเดือนส่วน ทสจ. สสภ.</span>', ['office/monthly-assessment-report', 'year' => $year, 'officeType' => 3, 'level' => 2], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'รายงานรายเดือนส่วน ทสจ. สสภ.']) ?>
                                </li>
                            </ul>
                        </li>
                        <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <i class="site-menu-icon md-chart" aria-hidden="true"></i>
                                <span class="site-menu-title">รายงานรอบ 6 เดือน</span>
                                <p class="site-menu-arrow"></p>

                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">รายงานรอบ 6 เดือน</span>', ['six-month-form/check-list', 'year' => $year], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'รายงานรอบเดือน']) ?>
                                    <?= Html::a('<span class="site-menu-title">ส่วนราชการ</span>', ['site/report-six-form', 'officeType' => 1, 'level' => 2, 'year' => $year], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'ส่วนราชการ']) ?>
                                    <?= Html::a('<span class="site-menu-title">ส่วนจังหวัด</span>', ['site/report-six-form', 'officeType' => 2, 'level' => 1, 'year' => $year], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'ส่วนจังหวัด']) ?>
                                    <?= Html::a('<span class="site-menu-title">ส่วน ทสจ. สสภ.</span>', ['site/report-six-form', 'officeType' => 3, 'level' => 1, 'year' => $year], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'ส่วน ทสจ. สสภ.']) ?>

                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>  
                    <?php if ($currentRole['role_id'] == \app\models\Role::STAFF || $currentRole['role_id'] == \app\models\Role::ADMINISTRATOR): ?>
                        <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <i class="site-menu-icon md-view-web" aria-hidden="true"></i>
                                <span class="site-menu-title">ดาวน์โหลด</span>
                                <p class="site-menu-arrow"></p>

                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">คู่มือ</span>', ['/content/content-list-admin', 'group' => \app\models\Content::DOWNLOAD, 'categoryId' => 1], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'คู่มือ']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">โปสเตอร์</span>', ['/content/content-list-admin', 'group' => \app\models\Content::DOWNLOAD, 'categoryId' => 2], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'โปสเตอร์']) ?>
                                </li>
                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">PPT</span>', ['/content/content-list-admin', 'group' => \app\models\Content::DOWNLOAD, 'categoryId' => 4], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'PPT']) ?>
                                </li>
                                                                <li class="site-menu-item">
                                    <?= Html::a('<span class="site-menu-title">ภาพกิจกรรม (zip file)</span>', ['/content/content-list-admin', 'group' => \app\models\Content::DOWNLOAD, 'categoryId' => 5], ['class' => 'animsition-link', 'data-toggle' => 'tooltip', 'title' => 'ภาพกิจกรรม (zip file)']) ?>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>

                </ul>
            </div>
        </div>
    </div>
</div>