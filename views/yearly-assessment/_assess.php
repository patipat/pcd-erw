<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\YearlyAssessment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="yearly-assessment-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'garbage_score')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                    'readonly' => true,
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'digits' => 0,
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'bag_score')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                    'readonly' => true,
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'digits' => 0,
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cup_score')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                    'readonly' => true,
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'digits' => 0,
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'foam_score')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                    'readonly' => true,
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'digits' => 0,
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'plus_score')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                    'onkeyup' => new JsExpression('calculateScore()'),
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'digits' => 0,
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'minus_score')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                    'onkeyup' => new JsExpression('calculateScore()'),
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'digits' => 0,
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => false,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'total_score')->widget(MaskedInput::class, [
                'options' => [
                    'class' => 'form-control',
                    'readonly' => true,
                ],
                'clientOptions' => [
                    'alias' => 'numeric',
                    'digits' => 0,
                    'prefix' => '',
                    'groupSeparator' => ',',
                    'allowMinus' => true,
                    'autoGroup' => true,
                    'autoUnmask' => true,
                    'unmaskAsNumber' => true,
                ]
            ]) ?>
        </div>
    </div>
    <?php
    $rc = $model->office->getReConfirmedMonthlyNumber($model->year);
    $lc = $model->office->getLateConfirmedMonthlyNumber($model->year);
    $fl = $model->office->getTotalFailedMonthlyNumber($model->year);
    ?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-condensed table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="text-center">แก้ไข</th>
                        <th class="text-center">เกินกำหนด</th>
                        <th class="text-center">ตีกลับ</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-right"><?= Yii::$app->formatter->asDecimal($rc, 0); ?></td>
                        <td class="text-right"><?= Yii::$app->formatter->asDecimal($lc, 0); ?></td>
                        <td class="text-right"><?= Yii::$app->formatter->asDecimal($fl, 0); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive" style="height: calc(100vh - 400px);">
            <?=
            $this->renderFile('@app/views/site/_yearly-assessment.php', [
                'ya' => $model,
                'year' => $model->year,
            ]);
            ?>
            </div>
        </div>
    </div>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
