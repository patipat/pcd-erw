<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Role;
$currentRole = Yii::$app->session->get('currentRole');

return [
    //[
    //    'class' => 'kartik\grid\CheckboxColumn',
    //    'width' => '20px',
    //],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'office.name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'year',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'garbage_score',
        'format' => ['decimal', 0],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'bag_score',
        'format' => ['decimal', 0],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'cup_score',
        'format' => ['decimal', 0],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'foam_score',
        'format' => ['decimal', 0],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'plus_score',
        'format' => ['decimal', 0],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'minus_score',
        'format' => ['decimal', 0],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'total_score',
        'format' => ['decimal', 0],
        'hAlign' => 'right',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'confirmed_by',
        'value' => function($model) {
            return isset($model->confirmed_by) ? $model->confirmedBy->person->fullName : "";
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'confirmed_at',
        'value' => function($model) {
            return isset($model->confirmed_at) ? Yii::$app->thaiFormatter->asDate($model->confirmed_at, 'php:d/m/Y') : "";
        }
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'status',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'population_total',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'deleted',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_by',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'pop_int',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'pop_ext',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'office_population',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'work_day',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'garbage',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'bag',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'cup',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'foam',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{assess} {view}',
        'viewOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'รายละเอียด'), 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'แก้ไข'), 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'ลบ'),
            'data-confirm' => false, 'data-method' => false, // for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => Yii::t('app', 'ยืนยันการลบ'),
            'data-confirm-message' => Yii::t('app', 'ต้องการลบรายการนี้ใช่หรือไม่ ?'),
            'data-confirm-ok' => Yii::t('app', 'ใช่'),
            'data-confirm-cancel' => Yii::t('app', 'ไม่'),
        ],
        'buttons' => [
            'view' => function($url, $model) {
                return Html::a('<i class="icon md-format-list-bulleted"></i>',
                                ['yearly-assessment/view', 'id' => $model->id],
                                ['role' => 'modal-remote', 'title' => Yii::t('app', 'รายละเอียด')]);
            },
            'assess' => function($url, $model) {
                return \yii\helpers\Html::a('<i class="icon md-assignment-check"></i>',
                                ['yearly-assessment/assess', 'id' => $model->id], [
                            'role' => 'modal-remote', 'title' => Yii::t('app', 'ตรวจสอบ'),
                ]);
            },
        ],
        'visibleButtons' => [
            'view' => function($model)use($currentRole) {
                return Yii::$app->util->checkPermission('yearly-assessment.view');
            },
            'assess' => function($model)use($currentRole) {
                return Yii::$app->util->checkPermission('yearly-assessment.assess') && !isset($model->confirmed_at)  && $currentRole['role_id'] == Role::SUPER_ADMINISTRATOR;
            },
        ]
    ],
];
