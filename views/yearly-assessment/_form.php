<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\YearlyAssessment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="yearly-assessment-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $this->renderFile('@app/views/widgets/_alert.php'); ?>

    <?= $form->field($model, 'office_id')->textInput() ?>

    <?= $form->field($model, 'year')->textInput() ?>

    <?= $form->field($model, 'garbage_prev')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'garbage_cur')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bag_prev')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bag_cur')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cup_prev')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cup_cur')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foam_prev')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foam_cur')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'garbage_percent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bag_percent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cup_percent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foam_percent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'garbage_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bag_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cup_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foam_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plus_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'minus_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'confirmed_by')->textInput() ?>

    <?= $form->field($model, 'confirmed_at')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'population_total')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deleted')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'pop_int')->textInput() ?>

    <?= $form->field($model, 'pop_ext')->textInput() ?>

    <?= $form->field($model, 'office_population')->textInput() ?>

    <?= $form->field($model, 'work_day')->textInput() ?>

    <?= $form->field($model, 'garbage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bag')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cup')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foam')->textInput(['maxlength' => true]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton(Yii::t('app', 'บันทึก'), ['class' => 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
