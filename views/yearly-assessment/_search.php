<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use app\models\Role;
use app\models\YearlyAssessment;

/* @var $this yii\web\View */
/* @var $model app\models\YearlyAssessment */
/* @var $form yii\widgets\ActiveForm */
$currentRole = Yii::$app->session->get('currentRole');
?>

<div class="yearly-assessment-search margin-bottom-10">

    <?php
    $form = ActiveForm::begin([
                'action' => Url::to(['yearly-assessment/index']),
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1,
                ],
                'type' => ActiveForm::TYPE_INLINE,
    ]);
    ?>
    <?php
    $data = ArrayHelper::map(Yii::$app->user->identity->person->getTotalResponsibleOffices(), 'id', 'typeName');

//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
    echo $form->field($model, 'office_id', [
        'options' => [
            'style' => 'width: 300px',
            'class' => 'form-group',
        ]
    ])->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => 'หน่วยงาน'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?= $form->field($model, 'year')->textInput(['type' => 'number']) ?>

    <?php
    
    if ($currentRole['role_id'] == Role::SUPER_ADMINISTRATOR || $currentRole['role_id'] == Role::REVIEWER ) {
        $data = ArrayHelper::map(\app\models\OfficeType::find()->orderBy('CONVERT(office_type.name USING TIS620) ASC')->all(), 'id', 'name');
//    \yii\helpers\VarDumper::dump($data, 10, TRUE);
        echo $form->field($model, 'officeType')->dropDownList($data, ['prompt' => 'ประเภทหน่วยงาน']);

        echo $form->field($model, 'level')->dropDownList(Yii::$app->util->getLevelLabels(), ['prompt' => 'ระดับหน่วยงาน']);
    }
    ?>
    <?php
    echo $form->field($model, 'isAssessed')->dropDownList(YearlyAssessment::getAssessmentStatusLabels(), ['prompt' => 'สถานะการประเมิน']);
    ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
