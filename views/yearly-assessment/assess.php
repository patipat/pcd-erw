<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\YearlyAssessment */

?>
<div class="yearly-assessment-assess">
    <?= $this->render('_assess', [
        'model' => $model,
    ]) ?>
</div>
