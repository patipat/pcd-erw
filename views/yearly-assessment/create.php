<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\YearlyAssessment */

?>
<div class="yearly-assessment-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
