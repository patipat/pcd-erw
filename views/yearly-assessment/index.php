<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\YearlyAssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

app\assets\NumeralAsset::register($this);

$this->title = Yii::t('app', 'ประเมินรอบ 12 เดือน');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yearly-assessment-index">
    <div class="ajaxCrudDatatable">
        <?=
        GridView::widget([
            'id' => 'crud-datatable-yearly-assessment',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'floatHeader' => true,
            'floatHeaderOptions' => ['top' => 66],
            'pjax' => true,
            'pjaxSettings' => [
                'beforeGrid' => $this->render('_search', ['model' => $searchModel]),
            ],
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                [
                    'options' => [
                        'class' => '',
                    ],
                    'content' =>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i> ' . Yii::t('app', 'โหลดใหม่'), Url::current(), ['data-pjax' => 1, 'class' => 'btn btn-default btn-raised'])
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => FALSE,
                'before' => '<div class="pull-left">{summary}</div>',
//                'after' => FALSE,
//                'footer' => FALSE,
            ],
            'pager' => array(
                'firstPageLabel' => '<i class="icon md-skip-previous"></i>',
                'lastPageLabel' => '<i class="icon md-skip-next"></i>',
                'prevPageLabel' => '<i class="icon md-fast-rewind"></i>',
                'nextPageLabel' => '<i class="icon md-fast-forward"></i>',
            ),
        ])
        ?>
    </div>
</div>
<script type="text/javascript">
            
    function calculateScore() {
        let garbageScore = numeral($('#yearlyassessment-garbage_score').val()).value();
        let bagScore = numeral($('#yearlyassessment-bag_score').val()).value();
        let cupScore = numeral($('#yearlyassessment-cup_score').val()).value();
        let foamScore = numeral($('#yearlyassessment-foam_score').val()).value();
        
        let plusScore = numeral($('#yearlyassessment-plus_score').val()).value();
        let minusScore = numeral($('#yearlyassessment-minus_score').val()).value();
        let totalScore = garbageScore + bagScore + cupScore + foamScore + plusScore - minusScore;
        $('#yearlyassessment-total_score').val(totalScore);
    }
</script>
<?php
$elOffice = Html::getInputId($searchModel, 'office_id');
$js = <<<js
    $(document).on('pjax:complete', '#crud-datatable-yearly-assessment-pjax', function() {
        var elOffice = $("#{$elOffice}"), // your input id for the HTML select input
            settings = elOffice.attr('data-krajee-select2');
        settings = window[settings];
        // reinitialize plugin
        elOffice.select2(settings);
        $('.loading-{$elOffice}').hide();
    });
js;
$this->registerJs($js);
