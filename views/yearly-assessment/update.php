<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\YearlyAssessment */
?>
<div class="yearly-assessment-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
