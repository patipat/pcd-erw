<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\YearlyAssessment */
?>
<div class="yearly-assessment-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'office_id',
            'year',
            'garbage_prev',
            'garbage_cur',
            'bag_prev',
            'bag_cur',
            'cup_prev',
            'cup_cur',
            'foam_prev',
            'foam_cur',
            'garbage_percent',
            'bag_percent',
            'cup_percent',
            'foam_percent',
            'garbage_score',
            'bag_score',
            'cup_score',
            'foam_score',
            'plus_score',
            'minus_score',
            'total_score',
            'confirmed_by',
            'confirmed_at',
            'status',
            'population_total',
            'deleted',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
            'pop_int',
            'pop_ext',
            'office_population',
            'work_day',
            'garbage',
            'bag',
            'cup',
            'foam',
            'mask',
        ],
    ]) ?>

</div>
